# Wyatt Software Modules

## About

This repo contains the source code for a group of .NET projects that form the "building blocks" of a .NET 8.0 web API.

This entire repo is intended to be added as a git sub-module.

```bash
cd <your-solution-folder>
git submodule add https://gitlab.com/wyatt-software/modules/server-modules
```

An API built with these modules can be used as the server-side part of a web application based on the AWS platform:

![Web Application Diagram](./docs/images/app-diagram-server.png)

\* _This is a simplified diagram that doesn't incorporate HTTPS. See the [Configure HTTPS](./docs/remote-server/https/README.md) docs for more info._

| Repo                                                                 | Description                                            |
| -------------------------------------------------------------------- | ------------------------------------------------------ |
| [client-web-modules](https://gitlab.com/wyatt-software/modules/client-web-modules)     | Source code for `@ws-modules` npm packages            |
| [server-modules](https://gitlab.com/wyatt-software/modules/server-modules) (this repo) | Source code for `WyattSoftware.Modules` .NET projects |

## Module dependencies

You can choose to add only the modules you need, but certain modules depend on others.

![Module dependencies](./docs/images/module-dependencies.png)

The `Identity` and `Gallery` modules are special cases that allow specific parts of their functionality to be extended with add-ons implemented in other modules. This allows things like User avatars to only be implemented when the `Gallery` module is installed, or for a Topic in the `Forum` module to be updated when it's thumbnail image is deleted.

\* _The "Add-ons" relationship from the `Forum` module to the `Gallery` module is not shown in this diagram._

## Modules

| Module    | Description                                                                                                    |
| :-------- | :------------------------------------------------------------------------------------------------------------- |
| Core      | Base module that all others depend on for interfaces and core functionality.                                   |
| Identity  | Users, roles, authentications, and authorisation.                                                              |
| Search    | Searchable indexes (other modules can add entries to this).                                                    |
| Content   | Pages (searchable), organised into a tree structure.                                                           |
| Gallery   | User images organised into albums.                                                                             |
| Messaging | Private messages between users.                                                                                |
| Forum     | Traditional community forum, organised into categories, sections, topics (searchable), and posts (searchable). |

## Documentation

- [Visual Studio solution structure](./docs/solution-structure/README.md)
- [Set up your local dev environment](./docs/local-development/README.md)
- [Database](./docs/database/README.md)
- [Config files](./docs/config/README.md)
- [Remote server](./docs/remote-server/README.md)
- [Example solution](./docs/example-solution)

