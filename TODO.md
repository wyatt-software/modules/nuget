/ [Home](./README.md) / TODO list

# TODO list

## SQL

- Replace all `VARCHAR` uuid's with `BINARY(16)`
  - generate using `UUID_TO_BIN(UUID(), 1)`
  - the second argument is the `swap_flag` used to move the time part of the UUID to make inserting into indexes more performant
  - to convert back to a human-readable UUID string, you must use the equivalent `BIN_TO_UUID(id, 1)`
  - see the [MySql 8.0 docs](https://dev.mysql.com/doc/refman/8.0/en/miscellaneous-functions.html#function_uuid-to-bin)

## Docs

- Example solution
- Caching
- Identity

  - Use an Http-Only cookie. See https://medium.com/@sadnub/simple-and-secure-api-authentication-for-spas-e46bcea592ad

- Forum
  - Automatic topic thumbs
