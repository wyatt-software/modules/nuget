﻿// <copyright file="GalleryOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Gallery section.
/// </summary>
public class GalleryOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets Extensions of image file types the site allows uploads of.
    /// </para>
    /// <para>
    /// Eg: <c>".gif,.jpg,.jpeg,.png"</c>.
    /// </para>
    /// </summary>
    public string AllowedImageExtensions { get; set; } = ".gif,.jpg,.jpeg,.png";

    /// <summary>
    /// <para>
    /// Gets or sets the sub-folder name under the storage folder or bucket where gallery images are stored.
    /// </para
    /// <para>
    /// Eg: <c>"gallery"</c> .
    /// </para>
    /// </summary>
    public string GalleryFolderName { get; set; } = "gallery";

    /// <summary>
    /// <para>
    /// Gets or sets the name of the user gallery folder to create and upload images to when uploaded from the
    /// markdown editor.
    /// </para
    /// <para>
    /// Eg: <c>"Uploads"</c> .
    /// </para>
    /// </summary>
    public string UploadAlbumName { get; set; } = "Uploads";

    /// <summary>
    /// <para>
    /// Gets or sets the name of the user gallery folder to create and upload avatars to when uploaded from the edit
    /// profile page.
    /// </para
    /// <para>
    /// Eg: <c>"Profile pictures"</c> .
    /// </para>
    /// </summary>
    public string AvatarAlbumName { get; set; } = "Profile pictures";
}
