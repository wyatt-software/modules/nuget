﻿// <copyright file="IAlbumRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the gallery album repository.
/// </summary>
public interface IAlbumRepository : IRepository<AlbumEntity>
{
    /// <summary>
    /// Gets the total number of albums for a given user.
    /// </summary>
    /// <param name="userId">The user to count albums for.</param>
    /// <returns>The total number of albums for a given user.</returns>
    Task<int> GetCountAsync(int userId = 0);

    /// <summary>
    /// Gets the albums for a given user.
    /// </summary>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="userId">The user to get albums for.</param>
    /// <returns>The albums for a given user.</returns>
    Task<List<AlbumEntity>> FetchAsync(int userId = 0, int offset = 0, int limit = 20);

    /// <summary>
    /// Get any albums with the given image as their cover.
    /// </summary>
    /// <param name="imageId">The potential cover image.</param>
    /// <returns>A collection of albums.</returns>
    Task<List<AlbumEntity>> FetchByCoverAsync(int imageId);

    /// <summary>
    /// Gets the album for a given user by it's alias.
    /// </summary>
    /// <param name="username">The user the album belongs to.</param>
    /// <param name="alias">The alias (codename) of the album.</param>
    /// <returns>The album for a given user by it's alias.</returns>
    Task<AlbumEntity?> ReadAsync(string username, string alias);

    /// <summary>
    /// Gets the album for a given user by it's alias.
    /// </summary>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="alias">The alias (codename) of the album.</param>
    /// <returns>The album for a given user by it's alias.</returns>
    Task<AlbumEntity?> ReadAsync(int userId, string alias);

    /// <summary>
    /// Gets the album for a given user by it's alias.
    /// </summary>
    /// <param name="albumId">The ID of the album.</param>
    /// <returns>The album for a given user by it's alias.</returns>
    new Task<AlbumEntity?> ReadAsync(int albumId);

    /// <summary>
    /// Finds an album with the given display name.
    /// </summary>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="displayName">The display name to find.</param>
    /// <returns>The album if it exists, or null.</returns>
    Task<AlbumEntity?> ReadByDisplayNameAsync(int userId, string displayName);

    /// <summary>
    /// Gets an alias for the given display name, that doesn't conflict with any existing aliases for this user.
    /// </summary>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="displayName">The display name to generate the unique (to this user) alias from.</param>
    /// <returns>
    /// An alias for the given display name, that doesn't conflict with any existing aliases for this user.
    /// </returns>
    Task<string> GenerateUniqueAliasAsync(int userId, string displayName);
}