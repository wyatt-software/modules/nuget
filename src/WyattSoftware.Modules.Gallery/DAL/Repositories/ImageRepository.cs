﻿// <copyright file="ImageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Dapper implementation of the gallery image repository.
/// </summary>
public class ImageRepository : BaseRepository<ImageEntity>, IImageRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ImageRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public ImageRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(int userId = 0, int albumId = 0)
    {
        var query = this.Db.Query(this.Table).AsCount();
        query = FilterImages(query, userId, albumId);
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<ImageEntity>> FetchAsync(int offset = 0, int limit = 20, int userId = 0, int albumId = 0)
    {
        var query = this.Db.Query(this.Table)
            .OrderByDesc("created")
            .Offset(offset)
            .Limit(limit);

        query = FilterImages(query, userId, albumId);
        var results = await query.GetAsync<ImageEntity>();
        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<ImageEntity> ReadAsync(Guid uuid)
    {
        return await this.Db.Query(this.Table)
            .Where("uuid", uuid.ToString())
            .FirstAsync<ImageEntity>();
    }

    /// <summary>
    /// Modifies the query to filter by the given search criteria.
    /// </summary>
    /// <param name="query">The query to modify.</param>
    /// <param name="userId">Get only images belonging to this user.</param>
    /// <param name="albumId">Get only images belonging to this album.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterImages(Query query, int userId = 0, int albumId = 0)
    {
        return query
            .When(userId > 0, q => q.Where("identity_user_id", userId))
            .When(albumId > 0, q => q.Where("gallery_album_id", albumId));
    }
}
