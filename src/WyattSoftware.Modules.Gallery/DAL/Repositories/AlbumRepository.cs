﻿// <copyright file="AlbumRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Repositories;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Dapper implementation of the gallery album repository.
/// </summary>
public class AlbumRepository : BaseRepository<AlbumEntity>, IAlbumRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AlbumRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public AlbumRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(int userId = 0)
    {
        var query = this.Db.Query(this.Table).AsCount();
        query = FilterAlbums(query, userId);
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<AlbumEntity>> FetchAsync(int userId = 0, int offset = 0, int limit = 20)
    {
        var query = this.Db.Query(this.Table)
            .OrderBy("display_name")
            .Offset(offset)
            .Limit(limit);

        query = FilterAlbums(query, userId);
        var results = await query.GetAsync<AlbumEntity>();
        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<List<AlbumEntity>> FetchByCoverAsync(int imageId)
    {
        var results = await this.Db.Query(this.Table)
            .Where("gallery_image_id", imageId)
            .GetAsync<AlbumEntity>();

        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<AlbumEntity?> ReadAsync(int userId, string alias)
    {
        return await this.Db.Query(this.Table)
            .Where("identity_user_id", userId)
            .Where("alias", alias)
            .FirstOrDefaultAsync<AlbumEntity>();
    }

    /// <inheritdoc/>
    public async Task<AlbumEntity?> ReadAsync(string username, string alias)
    {
        return await this.Db.Query(this.Table + " AS GA")
            .Select("GA.*")
            .Join("identity_user AS AU", "AU.id", "GA.identity_user_id")
            .Where("AU.username", username)
            .Where("GA.alias", alias)
            .FirstOrDefaultAsync<AlbumEntity>();
    }

    /// <inheritdoc/>
    public async Task<AlbumEntity?> ReadByDisplayNameAsync(int userId, string displayName)
    {
        return await this.Db.Query(this.Table)
            .Where("identity_user_id", userId)
            .Where("display_name", displayName)
            .FirstOrDefaultAsync<AlbumEntity>();
    }

    /// <inheritdoc/>
    public async Task<string> GenerateUniqueAliasAsync(int userId, string displayName)
    {
        return await this.Connection.ExecuteScalarAsync<string>(
            "SELECT func_gallery_album_get_unique_alias(@userId, @displayName);", new { userId, displayName });
    }

    /// <summary>
    /// Modifies the query to filter by the given search criteria.
    /// </summary>
    /// <param name="query">The query to modify.</param>
    /// <param name="userId">Get only albums belonging to this user.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterAlbums(Query query, int userId = 0)
    {
        return query
            .When(userId > 0, q => q.Where("identity_user_id", userId));
    }
}
