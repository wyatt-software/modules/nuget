﻿// <copyright file="IImageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the gallery image repository.
/// </summary>
public interface IImageRepository : IRepository<ImageEntity>
{
    /// <summary>
    /// Gets the total number of images for a given user's album.
    /// </summary>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="albumId">The album to count images for.</param>
    /// <returns>The total number of images for a given user's album.</returns>
    Task<int> GetCountAsync(int userId = 0, int albumId = 0);

    /// <summary>
    /// Gets images for a given user's album.
    /// </summary>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="albumId">The album to get images for.</param>
    /// <returns>The images for a given user's album.</returns>
    Task<List<ImageEntity>> FetchAsync(
        int offset = 0, int limit = 20, int userId = 0, int albumId = 0);

    /// <summary>
    /// Attempts to get an image by it's UUID.
    /// </summary>
    /// <param name="uuid">The UUID of the image.</param>
    /// <returns>The image or null.</returns>
    Task<ImageEntity> ReadAsync(Guid uuid);
}
