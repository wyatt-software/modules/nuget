﻿// <copyright file="AlbumEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Gallery album entity.
/// </summary>
[Table("gallery_album")]
public class AlbumEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Gets or sets a foreign key to the account user this album belongs to.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the image to use as this albums thumbnail.
    /// </summary>
    [ForeignKey("gallery_image.id")]
    [Column("gallery_image_id")]
    public int? ImageId { get; set; }

    /// <summary>
    /// Gets or sets a unique (for this user) URL-friendly alias for this album.
    /// </summary>
    [Column("alias")]
    [Required]
    [StringLength(200)]
    [RegularExpression(
        @"^[0-9a-z-]+$",
        ErrorMessage = "Only lowercase letters, numbers, and the dash character are allowed.")]
    public string Alias { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name for this album.
    /// </summary>
    [Column("display_name")]
    [Required]
    [StringLength(200)]
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a brief description for this album.
    /// </summary>
    [Column("description")]
    [StringLength(200)]
    public string Description { get; set; } = string.Empty;
}
