﻿// <copyright file="ImageEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.DAL.Entities;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Gallery image entity.
/// </summary>
[Table("gallery_image")]
public class ImageEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Gets or sets a foreign key to the account user this image belongs to.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the album this image belongs to.
    /// </summary>
    [ForeignKey("gallery_album.id")]
    [Column("gallery_album_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int AlbumId { get; set; }

    /// <summary>
    /// Gets or sets the universally unique ID for this image.
    /// </summary>
    [Column("uuid")]
    [Required]
    public Guid Uuid { get; set; } = Guid.NewGuid();

    /// <summary>
    /// Gets or sets the filename for this image, including the extension.
    /// </summary>
    /// <remarks>
    /// This is the filename of the uploaded file. Eg: "Sunny-chewing-her-Kong.jpg".
    /// This is NOT the filename used in the filesystem. We use the Uuid for that instead. Eg:
    /// "9825d0e7-7611-417b-b842-0b452c551748.jpg".
    /// </remarks>
    [Column("filename")]
    [Required]
    [StringLength(200)]
    [RegularExpression(
        @"[0-9A-Za-z.-_]+$",
        ErrorMessage = "Filename is invalid. Only letters, numbers, dots, dashes, and underscores allowed.")]
    public string Filename { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the filename extension for this image (including the dot).
    /// </summary>
    /// <example>".jpg".</example>
    [Column("extension")]
    [Required]
    [StringLength(50)]
    public string Extension { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a caption for the image.
    /// </summary>
    [Column("caption")]
    [StringLength(200)]
    public string Caption { get; set; } = string.Empty;
}
