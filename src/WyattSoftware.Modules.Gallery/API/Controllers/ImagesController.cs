﻿// <copyright file="ImagesController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Gallery.API.DTOs;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Services;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.API.Extensions;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Handles gallery images.
/// </summary>
[Route("gallery/images")]
public class ImagesController : WsControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly IImageService imageService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ImagesController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="imageService">Injected IImageService.</param>
    public ImagesController(
        ICurrentSession currentSession,
        IImageService imageService)
    {
        this.currentSession = currentSession;
        this.imageService = imageService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    public static void InitMapping(IUserAddOn userAddOn)
    {
        Mapper.AddMap<Image?, ImageResponse?>(image => Map(image));

        Mapper.AddMap<PaginatedResponse<Image>, PaginatedResponse<ImageResponse>>(images =>
            new PaginatedResponse<ImageResponse>()
            {
                Total = images.Total,
                Offset = images.Offset,
                Limit = images.Limit,
                Results = images.Results.Select(x => Mapper.Map<ImageResponse>(x)).ToList(),
            });

        userAddOn.DtoMappers.Add("image", image => Map(image));
    }

    /// <summary>
    /// Get a collection of gallery images.
    /// </summary>
    /// <param name="imageIds">Optionally specify an comma-separated list of image ids.</param>
    /// <param name="album">The albumId or alias to get images for.</param>
    /// <param name="username">The username of the user who owns the album (if albumId is unknown).</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|album] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of gallery images.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(
        string imageIds = "",
        string album = "",
        string username = "",
        string expand = "",
        int offset = 0,
        int limit = 20)
    {
        var response = await this.imageService.FetchAsync(imageIds, album, username, expand, offset, limit);
        var mappedResults = Mapper.Map<PaginatedResponse<ImageResponse>>(response);
        return this.ResultsWithPaginationHeaders(mappedResults);
    }

    /// <summary>
    /// Uploads an image to storage and creates a record of it.
    /// </summary>
    /// <param name="albumId">
    /// <para>
    /// The album to upload the image to.
    /// </para>
    /// <para>
    /// If omitted, the default album is used (eg: "Uploads" or "Profile Pictures", depending if <c>avatar</c> is
    /// <c>true</c>).
    /// </para>
    /// </param>
    /// <param name="setAsAvatar">If <c>true</c>, the image is set to the user's avatar.</param>
    /// <returns>The newly created image.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Upload(int albumId = 0, bool setAsAvatar = false)
    {
        var file = this.HttpContext.Request.Form.Files.AsFileDtos().First();

        var image = await this.imageService.UploadAsync(file, albumId, this.currentSession.UserId, setAsAvatar);
        await image.ExpandAsync("all");

        var response = Mapper.Map<ImageResponse>(image);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single gallery image.
    /// </summary>
    /// <param name="imageId">The primary key of the image.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|album] .</para>
    /// </param>
    /// <returns>A single gallery image.</returns>
    [HttpGet]
    [Route("{imageId:int}")]
    public async Task<IActionResult> Read(int imageId, string expand = "")
    {
        var image = await this.imageService.ReadAsync(imageId, expand);

        var response = Mapper.Map<ImageResponse>(image);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a gallery image.
    /// </summary>
    /// <param name="imageId">The primary key of the image.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated gallery image.</returns>
    [HttpPut]
    [Route("{imageId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Update(int imageId, [FromBody]ImageUpdateRequest request)
    {
        var image = await this.imageService.ReadAsync(imageId);
        this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");

        image.Filename = request.Filename;
        image.Caption = request.Caption;
        await image.UpdateAsync();
        await image.ExpandAsync("all");

        var response = Mapper.Map<ImageResponse>(image);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a gallery image.
    /// </summary>
    /// <param name="imageId">The primary key of the image.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{imageId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Delete(int imageId)
    {
        var image = await this.imageService.ReadAsync(imageId);
        this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");

        await image.DeleteAsync();

        return this.Ok(null);
    }

    /// <summary>
    /// Sets the image as the cover of the album it belongs to.
    /// </summary>
    /// <param name="imageId">The primary key of the image.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{imageId:int}/actions/set-as-cover")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> SetAsCover(int imageId)
    {
        var image = await this.imageService.ReadAsync(imageId);
        this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");

        await image.SetAsCoverAsync();

        return this.Ok(null);
    }

    /// <summary>
    /// Sets the image as the avatar of the user it belongs to.
    /// </summary>
    /// <param name="imageId">The primary key of the image.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{imageId:int}/actions/set-as-avatar")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> SetAsAvatar(int imageId)
    {
        var image = await this.imageService.ReadAsync(imageId);
        this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");

        await image.SetAsAvatarAsync();

        return this.Ok(null);
    }

    /// <summary>
    /// Maps an image domain model to an API response DTO.
    /// </summary>
    /// <param name="domainModel">The domain model.</param>
    /// <returns>A response DTO or null.</returns>
    private static ImageResponse? Map(IDomainModel? domainModel)
    {
        var image = (Image?)domainModel;
        if (image == null)
        {
            return null;
        }

        var response = new ImageResponse();
        response.InjectFrom(image);

        response.Album = image.Album != null
            ? Mapper.Map<AlbumResponse>(image.Album)
            : null;

        response.User = image.User != null
            ? Mapper.Map<UserResponse>(image.User)
            : null;

        return response;
    }
}
