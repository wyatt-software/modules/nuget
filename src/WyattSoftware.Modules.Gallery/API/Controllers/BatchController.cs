﻿// <copyright file="BatchController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.Controllers;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WyattSoftware.Modules.Gallery.API.DTOs;
using WyattSoftware.Modules.Gallery.BLL.Services;

/// <summary>
/// Handles batch operations relating to the gallery.
/// </summary>
[Route("gallery/batch")]
public class BatchController : ControllerBase
{
    private readonly IImageService imageService;

    /// <summary>
    /// Initializes a new instance of the <see cref="BatchController"/> class.
    /// </summary>
    /// <param name="imageService">Injected IImageService.</param>
    public BatchController(
        IImageService imageService)
    {
        this.imageService = imageService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
    }

    /// <summary>
    /// Moves a set of images to an album.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("move-images")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> MoveImages([FromBody] ImageActionMoveRequest request)
    {
        await this.imageService.MoveAsync(request.ImageIds, request.AlbumId);
        return this.Ok(null);
    }

    /// <summary>
    /// Deletes a set of images.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("delete-images")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> DeleteImage([FromBody] ImageActionDeleteRequest request)
    {
        await this.imageService.DeleteAsync(request.ImageIds);
        return this.Ok(null);
    }
}
