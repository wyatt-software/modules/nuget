﻿// <copyright file="AlbumsController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.Controllers;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Gallery.API.DTOs;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Services;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Context;

/// <summary>
/// Handles gallery albums.
/// </summary>
[Route("gallery/albums")]
public class AlbumsController : WsControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly IAlbumFactory albumFactory;
    private readonly IAlbumService albumService;

    /// <summary>
    /// Initializes a new instance of the <see cref="AlbumsController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    /// <param name="albumService">Injected IAlbumService.</param>
    public AlbumsController(
        ICurrentSession currentSession,
        IAlbumFactory albumFactory,
        IAlbumService albumService)
    {
        this.currentSession = currentSession;
        this.albumFactory = albumFactory;
        this.albumService = albumService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Album?, AlbumResponse?>(album =>
        {
            if (album == null)
            {
                return null;
            }

            var response = new AlbumResponse();
            response.InjectFrom(album);

            response.User = album.User != null
                ? Mapper.Map<UserResponse>(album.User)
                : null;

            response.Image = album.Image != null
                ? Mapper.Map<ImageResponse>(album.Image)
                : null;

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Album>, PaginatedResponse<AlbumResponse>>(albums =>
            new PaginatedResponse<AlbumResponse>()
            {
                Total = albums.Total,
                Offset = albums.Offset,
                Limit = albums.Limit,
                Results = albums.Results.Select(x => Mapper.Map<AlbumResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get a collection of gallery albums.
    /// </summary>
    /// <param name="username">The username of a user to get albums for.</param>
    /// <param name="alias">The alias of the album if you want to get a specific one.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image|user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of gallery albums.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(
        string username = "", string alias = "", string expand = "", int offset = 0, int limit = 20)
    {
        var albums = await this.albumService.FetchAsync(username, alias, expand, offset, limit);
        var response = Mapper.Map<PaginatedResponse<AlbumResponse>>(albums);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a gallery album.
    /// </summary>
    /// <param name="request">The request body.</param>
    /// <returns>The newly created gallery album.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Create([FromBody]AlbumCreateRequest request)
    {
        // Try to construct a domain model to trigger validation.
        var album = this.albumFactory.New(
            this.currentSession.UserId,
            request.DisplayName,
            request.Description);

        await album.CreateAsync();
        await album.ExpandAsync("all");

        var response = Mapper.Map<AlbumResponse>(album);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single gallery album.
    /// </summary>
    /// <param name="albumId">The id of the gallery album.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image|user] .</para>
    /// </param>
    /// <returns>A single gallery album.</returns>
    [HttpGet]
    [Route("{albumId:int}")]
    public async Task<IActionResult> Read(int albumId, string expand = "")
    {
        var album = await this.albumService.ReadAsync(albumId, expand);

        var response = Mapper.Map<AlbumResponse>(album);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a gallery album.
    /// </summary>
    /// <param name="albumId">The id of the gallery album.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated gallery album.</returns>
    [HttpPut]
    [Route("{albumId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Update(int albumId, [FromBody]AlbumUpdateRequest request)
    {
        var album = await this.albumService.ReadAsync(albumId);
        this.currentSession.AssertResourceOwnerOrRole(album.UserId, "Moderator");

        album.DisplayName = request.DisplayName;
        album.Description = request.Description;
        await album.UpdateAsync();
        await album.ExpandAsync("all");

        var response = Mapper.Map<AlbumResponse>(album);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a gallery album.
    /// </summary>
    /// <param name="albumId">The id of the gallery album.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{albumId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Delete(int albumId)
    {
        var album = await this.albumService.ReadAsync(albumId);
        this.currentSession.AssertResourceOwnerOrRole(album.UserId, "Moderator");

        await album.DeleteAsync();

        return this.Ok(null);
    }
}
