﻿// <copyright file="ImageUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

/// <summary>
/// Image create request.
/// </summary>
/// <remarks>Mapped to the <see cref="Image" /> model.</remarks>
public class ImageUpdateRequest
{
    /// <summary>
    /// Gets or sets the filename for this image, including the extension.
    /// </summary>
    public string Filename { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a caption for the image.
    /// </summary>
    public string Caption { get; set; } = string.Empty;
}
