﻿// <copyright file="AlbumActionSetCoverRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

/// <summary>
/// Request images to move.
/// </summary>
public class AlbumActionSetCoverRequest
{
    /// <summary>
    /// Gets or sets the image to set the cover to.
    /// </summary>
    public int? ImageId { get; set; }
}
