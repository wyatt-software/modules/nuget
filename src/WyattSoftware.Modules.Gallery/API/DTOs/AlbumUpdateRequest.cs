﻿// <copyright file="AlbumUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

/// <summary>
/// Album create request.
/// </summary>
/// <remarks>Mapped to the <see cref="Album" /> model.</remarks>
public class AlbumUpdateRequest
{
    /// <summary>
    /// Gets or sets the display name for this album.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a brief description for this album.
    /// </summary>
    public string Description { get; set; } = string.Empty;
}
