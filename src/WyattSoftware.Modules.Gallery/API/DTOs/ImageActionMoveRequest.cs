﻿// <copyright file="ImageActionMoveRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

/// <summary>
/// Request images to move.
/// </summary>
public class ImageActionMoveRequest
{
    /// <summary>
    /// Gets or sets a list of image IDs representing the images to move.
    /// </summary>
    public int[] ImageIds { get; set; } = Array.Empty<int>();

    /// <summary>
    /// Gets or sets the album to move the images to.
    /// </summary>
    public int AlbumId { get; set; }
}
