﻿// <copyright file="ImageResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

using System;
using WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Image create request.
/// </summary>
/// <remarks>Mapped from the <see cref="Image" /> model.</remarks>
public class ImageResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the Universally Unique ID.
    /// </summary>
    public Guid Uuid { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the album this image belongs to.
    /// </summary>
    public int AlbumId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the community user this image belongs to.
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets the filename for this image, including the extension.
    /// </summary>
    public string Filename { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the filename extension for this image (including the dot).
    /// </summary>
    /// <example>".jpg" .</example>
    public string Extension { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a caption for the image.
    /// </summary>
    public string Caption { get; set; } = string.Empty;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the album this image belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=album".
    /// </remarks>
    public AlbumResponse? Album { get; set; }

    /// <summary>
    /// Gets or sets the user who this album belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=user".
    /// </remarks>
    public UserResponse? User { get; set; }
}
