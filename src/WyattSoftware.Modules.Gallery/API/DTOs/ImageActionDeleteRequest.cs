﻿// <copyright file="ImageActionDeleteRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

/// <summary>
/// Request images to delete.
/// </summary>
public class ImageActionDeleteRequest
{
    /// <summary>
    /// Gets or sets a list of image IDs representing the images to delete.
    /// </summary>
    public int[] ImageIds { get; set; } = Array.Empty<int>();
}
