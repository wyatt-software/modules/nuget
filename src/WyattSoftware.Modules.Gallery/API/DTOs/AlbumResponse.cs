﻿// <copyright file="AlbumResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.API.DTOs;

using WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Album create request.
/// </summary>
/// <remarks>Mapped from the <see cref="Album" /> model.</remarks>
public class AlbumResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the image to use as this albums thumbnail.
    /// </summary>
    public int? ImageId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the community user this album belongs to.
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a unique (for this user) URL-friendly alias for this album.
    /// </summary>
    public string Alias { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name for this album.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a brief description for this album.
    /// </summary>
    public string Description { get; set; } = string.Empty;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the user who this album belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=user".
    /// </remarks>
    public UserResponse? User { get; set; }

    /// <summary>
    /// Gets or sets the cover image for this album.
    /// </summary>
    /// <remarks>
    /// Use "expand=image".
    /// </remarks>
    public ImageResponse? Image { get; set; }
}
