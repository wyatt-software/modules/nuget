﻿// <copyright file="ImageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.DTOs;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Gallery.Options;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IImageService"/> interface.
/// </summary>
public partial class ImageService : BaseDomainService<Image, ImageEntity>, IImageService
{
    private readonly GalleryOptions galleryOptions;
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly IImageRepository imageRepository;
    private readonly IUserFactory userFactory;
    private readonly IAlbumFactory albumFactory;
    private readonly IImageFactory imageFactory;

    // Note: Usually a service shouldn't reference other services, but we use the .EnsureAsync() method when
    // uploading to the default album.
    private readonly IAlbumService albumService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ImageService"/> class.
    /// </summary>
    /// <param name="galleryOptions">Injected IOptions{GalleryOptions}.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param
    /// <param name="albumService">Injected IAlbumService.</param>
    public ImageService(
        IOptions<GalleryOptions> galleryOptions,
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IAlbumFactory albumFactory,
        IImageFactory imageFactory,
        IAlbumService albumService)
        : base(imageRepository, imageFactory)
    {
        this.galleryOptions = galleryOptions.Value;
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.albumFactory = albumFactory;
        this.imageFactory = imageFactory;
        this.albumService = albumService;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Image>> FetchAsync(
        string imageIds = "",
        string album = "",
        string username = "",
        string expand = "",
        int offset = 0,
        int limit = 20)
    {
        int total;
        List<ImageEntity> imageEntities;

        if (!string.IsNullOrEmpty(imageIds))
        {
            var parsedImageIds = imageIds.Split(',').Select(x => int.Parse(x)).ToArray();
            imageEntities = await this.imageRepository.FetchAsync(parsedImageIds);
            total = imageEntities.Count;
            offset = 0;
            limit = 0;
        }
        else
        {
            // User specified?
            var userId = 0;
            if (!string.IsNullOrEmpty(username))
            {
                var userEntity = await this.userRepository.ReadAsync(username);
                if (userEntity == null)
                {
                    throw new KeyNotFoundException($"User \"{username}\" not found.");
                }

                userId = userEntity.Id;
            }

            var albumId = 0;
            if (!string.IsNullOrEmpty(album))
            {
                // Album alias used instead of ID ?
                var albumEntity = int.TryParse(album, out albumId)
                    ? await this.albumRepository.ReadAsync(albumId)
                    : await this.albumRepository.ReadAsync(username, album);

                if (albumEntity != null)
                {
                    albumId = albumEntity.Id;
                }
            }

            total = await this.imageRepository.GetCountAsync(userId, albumId);
            imageEntities = await this.imageRepository.FetchAsync(offset, limit, userId, albumId);
        }

        var results = imageEntities.Select(this.imageFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Image>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    public async Task<Image> UploadAsync(FileDto file, int albumId, int userId, bool setAsAvatar = false)
    {
        if (userId == 0)
        {
            userId = this.currentSession.UserId;
        }

        if (userId == 0)
        {
            throw new UnauthorizedAccessException();
        }

        var userEntity = await this.userRepository.ReadAsync(userId);
        if (userEntity == null)
        {
            throw new KeyNotFoundException($"User {userId} not found.");
        }

        if (albumId == 0)
        {
            var albumName = setAsAvatar ? this.galleryOptions.AvatarAlbumName : this.galleryOptions.UploadAlbumName;
            var album = await this.albumService.EnsureAsync(userId, albumName);
            albumId = album.Id;
        }
        else
        {
            var albumEntity = await this.albumRepository.ReadAsync(albumId);
            if (albumEntity == null)
            {
                throw new KeyNotFoundException($"Album {albumId} not found.");
            }
        }

        var image = this.imageFactory.New(userId, albumId);
        await image.UploadAsync(file);
        await image.CreateAsync();

        if (setAsAvatar)
        {
            userEntity.Properties["imageId"] = image.Id;
            await this.userRepository.UpdateAsync(userEntity);
        }

        return image;
    }

    /// <inheritdoc />
    public async Task<Image> UploadAsync(string url, int albumId, int userId, string caption = "")
    {
        if (userId == 0)
        {
            userId = this.currentSession.UserId;
        }

        var albumEntity = await this.albumRepository.ReadAsync(albumId);
        if (albumEntity == null)
        {
            throw new KeyNotFoundException($"Could not find album {albumId}.");
        }

        var album = this.albumFactory.New(albumEntity);

        var image = this.imageFactory.New(userId, album.Id);
        await image.UploadAsync(url);
        image.Caption = caption;
        await image.CreateAsync();

        return image;
    }

    /// <inheritdoc />
    public async Task<Image> ReadAsync(int imageId, string expand = "")
    {
        var imageEntity = await this.imageRepository.ReadAsync(imageId);
        if (imageEntity == null)
        {
            throw new KeyNotFoundException($"Image {imageId} not found.");
        }

        return await this.imageFactory.New(imageEntity).ExpandAsync(expand);
    }

    /// <inheritdoc />
    public async Task DeleteAsync(int[] imageIds)
    {
        var imageEntities = await this.imageRepository.FetchAsync(imageIds);
        foreach (var imageEntity in imageEntities)
        {
            var image = this.imageFactory.New(imageEntity);
            this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");
            await image.DeleteAsync();
        }
    }

    /// <inheritdoc />
    public async Task MoveAsync(int[] imageIds, int albumId)
    {
        var albumEntity = await this.albumRepository.ReadAsync(albumId);
        if (albumEntity == null)
        {
            throw new KeyNotFoundException($"Album {albumId} not found.");
        }

        var imageEntities = await this.imageRepository.FetchAsync(imageIds);
        var albumIds = imageEntities.Select(x => x.AlbumId).ToArray();
        var sourceAlbumEntities = await this.albumRepository.FetchAsync(albumIds);

        var uncoveredAlbumIds = new List<int>();

        foreach (var imageEntity in imageEntities)
        {
            this.currentSession.AssertResourceOwnerOrRole(imageEntity.UserId, "Moderator");

            var wasAlbumCover = sourceAlbumEntities
                .FirstOrDefault(x => x.Id == imageEntity.AlbumId)
                ?.ImageId == imageEntity.Id;

            if (wasAlbumCover)
            {
                uncoveredAlbumIds.Add(imageEntity.AlbumId);
            }

            imageEntity.AlbumId = albumId;
            await this.imageRepository.UpdateAsync(imageEntity);
        }

        var distinctUncoveredAlbumIds = uncoveredAlbumIds.Distinct().ToArray();
        var uncovredAlbumEntities = await this.albumRepository.FetchAsync(distinctUncoveredAlbumIds);

        foreach (var uncovredAlbumEntity in uncovredAlbumEntities)
        {
            uncovredAlbumEntity.ImageId = null;
            await this.albumRepository.UpdateAsync(uncovredAlbumEntity);
            var uncovredAlbum = this.albumFactory.New(uncovredAlbumEntity);
            await uncovredAlbum.EnsureCoverImageAsync();
        }

        var destinationAlbum = this.albumFactory.New(albumEntity);
        await destinationAlbum.EnsureCoverImageAsync();
    }
}
