﻿// <copyright file="AlbumService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IAlbumService"/> interface.
/// </summary>
public partial class AlbumService : BaseDomainService<Album, AlbumEntity>, IAlbumService
{
    private readonly IUserRepository userRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly IImageRepository imageRepository;
    private readonly IUserFactory userFactory;
    private readonly IAlbumFactory albumFactory;
    private readonly IImageFactory imageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="AlbumService"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    public AlbumService(
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IAlbumFactory albumFactory,
        IImageFactory imageFactory)
        : base(albumRepository, albumFactory)
    {
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.albumFactory = albumFactory;
        this.imageFactory = imageFactory;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Album>> FetchAsync(
        string username, string alias, string expand = "", int offset = 0, int limit = 20)
    {
        // Username specified?
        var userId = 0;
        if (!string.IsNullOrEmpty(username))
        {
            var userEntity = await this.userRepository.ReadAsync(username);
            if (userEntity == null)
            {
                throw new KeyNotFoundException($"User \"{username}\" not found.");
            }

            userId = userEntity.Id;
        }

        // Alias specified?
        if (!string.IsNullOrEmpty(alias))
        {
            var albumEntity = await this.albumRepository.ReadAsync(username, alias);
            if (albumEntity == null)
            {
                throw new KeyNotFoundException($"Album \"{alias}\" not found for user \"{username}\".");
            }

            // Return a list containing only one entry.
            var album = this.albumFactory.New(albumEntity);
            return new PaginatedResponse<Album>
            {
                Offset = 0,
                Limit = 1,
                Total = 1,
                Results = new List<Album>() { album },
            };
        }

        var total = await this.albumRepository.GetCountAsync(userId);
        var albumEntities = await this.albumRepository.FetchAsync(userId, offset, limit);

        var results = albumEntities.Select(this.albumFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Album>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    public async Task<Album> ReadAsync(int albumId, string expand = "")
    {
        var albumEntity = await this.albumRepository.ReadAsync(albumId);
        if (albumEntity == null)
        {
            throw new KeyNotFoundException($"Album {albumId} not found.");
        }

        var album = this.albumFactory.New(albumEntity);
        await album.ExpandAsync(expand);
        return album;
    }

    /// <inheritdoc />
    public async Task<Album> ReadByDisplayNameAsync(int userId, string displayName)
    {
        var albumEntity = await this.albumRepository.ReadByDisplayNameAsync(userId, displayName);
        if (albumEntity == null)
        {
            throw new KeyNotFoundException(
                $"Album with display name \"{displayName}\" not found for user {userId}.");
        }

        return this.albumFactory.New(albumEntity);
    }

    /// <inheritdoc />
    public async Task<Album> EnsureAsync(int userId, string displayName)
    {
        var albumEntity = await this.albumRepository.ReadByDisplayNameAsync(userId, displayName);
        if (albumEntity == null)
        {
            albumEntity = new AlbumEntity
            {
                Alias = await this.albumRepository.GenerateUniqueAliasAsync(userId, displayName),
                DisplayName = displayName,
                UserId = userId,
            };

            albumEntity.Id = await this.albumRepository.CreateAsync(albumEntity);
        }

        return this.albumFactory.New(albumEntity);
    }
}
