﻿// <copyright file="IImageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.DTOs;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Gallery.BLL.Models;

/// <summary>
/// Business logic for the gallery images.
/// </summary>
public interface IImageService : IDomainService<Image>
{
    /// <summary>
    /// Get a collection of gallery images.
    /// </summary>
    /// <param name="imageIds">Optionally specify an comma-separated list of image ids.</param>
    /// <param name="album">The albumId or alias to get images for.</param>
    /// <param name="username">The username of the user who owns the album (if albumId is unknown).</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|album] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of gallery images.</returns>
    Task<PaginatedResponse<Image>> FetchAsync(
        string imageIds = "",
        string album = "",
        string username = "",
        string expand = "",
        int offset = 0,
        int limit = 20);

    /// <summary>
    /// Creates and uploads an image.
    /// </summary>
    /// <param name="file">The image file to upload.</param>
    /// <param name="albumId">
    /// <para>
    /// The album to upload the image to.
    /// </para>
    /// <para>
    /// If omitted, the default album is used (eg: "Uploads" or "Profile Pictures", depending if <c>avatar</c> is
    /// <c>true</c>).
    /// </para>
    /// </param>
    /// <param name="userId">The user to upload for. Uses currently authenticated user if zero.</param>
    /// <param name="setAsAvatar">If <c>true</c>, the image is set to the user's avatar.</param>
    /// <returns>The newly uploaded image.</returns>
    Task<Image> UploadAsync(FileDto file, int albumId, int userId = 0, bool setAsAvatar = false);

    /// <summary>
    /// Creates and uploads an image from an external URL.
    /// </summary>
    /// <param name="url">The image URL to get and upload.</param>
    /// <param name="albumId">The album to upload to.</param>
    /// <param name="userId">The user to upload for. Uses currently authenticated user if zero.</param>
    /// <param name="caption">Optional caption for the image.</param>
    /// <returns>The newly uploaded image.</returns>
    Task<Image> UploadAsync(string url, int albumId, int userId = 0, string caption = "");

    /// <summary>
    /// Attempts to get an image by it's primary key.
    /// </summary>
    /// <param name="imageId">The image's primary key.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|image] .</para>
    /// </param>
    /// <returns>A single image.</returns>
    Task<Image> ReadAsync(int imageId, string expand = "");

    /// <summary>
    /// Deletes a set of images.
    /// </summary>
    /// <param name="imageIds">A list of image IDs representing the images to delete.</param>
    /// <returns>Nothing.</returns>
    Task DeleteAsync(int[] imageIds);

    /// <summary>
    /// Moves a set of images to an album.
    /// </summary>
    /// <param name="imageIds">A list of image IDs representing the images to move.</param>
    /// <param name="albumId">The album to move to.</param>
    /// <returns>Nothing.</returns>
    Task MoveAsync(int[] imageIds, int albumId);
}
