﻿// <copyright file="IAlbumService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Gallery.BLL.Models;

/// <summary>
/// Business logic for the gallery albums.
/// </summary>
public interface IAlbumService : IDomainService<Album>
{
    /// <summary>
    /// Get a collection of gallery albums.
    /// </summary>
    /// <param name="username">The username of a user to get albums for.</param>
    /// <param name="alias">The alias of the album if you want to get a specific one.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image|user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of gallery albums.</returns>
    Task<PaginatedResponse<Album>> FetchAsync(
        string username, string alias, string expand = "", int offset = 0, int limit = 20);

    /// <summary>
    /// Gets a single gallery album.
    /// </summary>
    /// <param name="albumId">The ID of the gallery album.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image|user] .</para>
    /// </param>
    /// <returns>A single gallery album.</returns>
    Task<Album> ReadAsync(int albumId, string expand = "");

    /// <summary>
    /// Gets a single gallery album.
    /// </summary>
    /// <param name="userId">The id of the owner of the gallery album.</param>
    /// <param name="displayName">The display name of the gallery album.</param>
    /// <returns>A single gallery album.</returns>
    Task<Album> ReadByDisplayNameAsync(int userId, string displayName);

    /// <summary>
    /// Finds or creates an album with the given display name.
    /// </summary>
    /// <param name="userId">The user the album belongs to.</param>
    /// <param name="displayName">The display name to find or create an album for.</param>
    /// <returns>The existing or newly created album.</returns>
    Task<Album> EnsureAsync(int userId, string displayName);
}
