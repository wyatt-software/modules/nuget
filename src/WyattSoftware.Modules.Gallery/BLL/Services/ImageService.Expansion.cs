﻿// <copyright file="ImageService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class ImageService
{
    /// <summary>
    /// Registers expanders used by the User model. This should be called once on app startup.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    internal static void RegisterExpanders(IUserAddOn userAddOn)
    {
        userAddOn.ModelListExpanders.Add(
            "image",
            async (IServiceScopeFactory serviceScopeFactory, List<User> users) =>
            {
                var imageIds = users
                    .Select(x => (int?)x.Properties["imageId"] ?? 0)
                    .Where(x => x > 0)
                    .Distinct()
                    .ToArray();

                if (!imageIds.Any())
                {
                    return users;
                }

                // This is a delegate, so it doesn't have access to dependency injection.
                // We can't use more specific method-injection, because the caller doesn't know what the delegate
                // will need, and probably doesn't have a reference to the assembly anyway.
                using var scope = serviceScopeFactory.CreateScope();
                var scopedImageRepository = scope.ServiceProvider.GetRequiredService<IImageRepository>();

                var imageEntities = await scopedImageRepository.FetchAsync(imageIds);
                if (!imageEntities.Any())
                {
                    return users;
                }

                var scopedImageFactory = scope.ServiceProvider.GetRequiredService<IImageFactory>();
                foreach (var user in users)
                {
                    var imageId = (int?)user.Properties["imageId"];
                    if (!imageId.HasValue)
                    {
                        continue;
                    }

                    var imageEntity = imageEntities.Find(x => x.Id == imageId);
                    if (imageEntity == null)
                    {
                        continue;
                    }

                    var image = scopedImageFactory.New(imageEntity);
                    user.NestedResources["image"] = image;
                }

                return users;
            });
    }

    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="images">The collection of gallery images to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|album] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The Images, expanded.</returns>
    private async Task<List<Image>> ExpandAsync(List<Image> images, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return images;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    images = await this.ExpandUsersAsync(images);
                    images = await this.ExpandAlbumsAsync(images);
                    break;

                case "user":
                    images = await this.ExpandUsersAsync(images);
                    break;

                case "album":
                    images = await this.ExpandAlbumsAsync(images);
                    break;
            }
        }

        return images;
    }

    /// <summary>
    /// Expands the users.
    /// </summary>
    /// <param name="images">The collection of gallery images to expand.</param>
    /// <returns>The images, with references expanded.</returns>
    private async Task<List<Image>> ExpandUsersAsync(List<Image> images)
    {
        var userIds = images
            .Select(x => x.UserId)
            .Distinct()
            .ToArray();

        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return images;
        }

        foreach (var image in images)
        {
            image.User = users.First(x => x.Id == image.UserId);
        }

        return images;
    }

    /// <summary>
    /// Expands the albums.
    /// </summary>
    /// <param name="images">The collection of gallery images to expand.</param>
    /// <returns>The images, with references expanded.</returns>
    private async Task<List<Image>> ExpandAlbumsAsync(List<Image> images)
    {
        var albumIds = images
            .Select(x => x.AlbumId)
            .Distinct()
            .ToArray();

        var albumEntities = await this.albumRepository.FetchAsync(albumIds);
        var albums = albumEntities.Select(this.albumFactory.New).ToList();
        if (!albums.Any())
        {
            return images;
        }

        foreach (var image in images)
        {
            image.Album = albums.Find(x => x.Id == image.AlbumId);
        }

        return images;
    }
}