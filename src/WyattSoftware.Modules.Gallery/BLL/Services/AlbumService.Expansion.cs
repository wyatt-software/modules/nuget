﻿// <copyright file="AlbumService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Gallery.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class AlbumService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="albums">The collection of gallery albums to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image|user] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The albums, expanded.</returns>
    public async Task<List<Album>> ExpandAsync(List<Album> albums, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return albums;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    albums = await this.ExpandUsersAsync(albums);
                    albums = await this.ExpandImagesAsync(albums);
                    break;

                case "image":
                    albums = await this.ExpandImagesAsync(albums);
                    break;

                case "user":
                    albums = await this.ExpandUsersAsync(albums);
                    break;
            }
        }

        return albums;
    }

    /// <summary>
    /// Expands the users.
    /// </summary>
    /// <param name="albums">The albums to expand.</param>
    /// <returns>The albums, expanded.</returns>
    private async Task<List<Album>> ExpandUsersAsync(List<Album> albums)
    {
        var userIds = albums
            .Select(x => x.UserId)
            .Distinct()
            .ToArray();

        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return albums;
        }

        foreach (var album in albums)
        {
            album.User = users.First(x => x.Id == album.UserId);
        }

        return albums;
    }

    /// <summary>
    /// Expands the images.
    /// </summary>
    /// <param name="albums">The albums to expand.</param>
    /// <returns>The albums, expanded.</returns>
    private async Task<List<Album>> ExpandImagesAsync(List<Album> albums)
    {
        var imageIds = albums
            .Where(x => x.ImageId.HasValue)
            .Select(x => x.ImageId ?? 0)
            .Distinct()
            .ToArray();

        var imageEntities = await this.imageRepository.FetchAsync(imageIds);
        var images = imageEntities.Select(this.imageFactory.New).ToList();
        if (!images.Any())
        {
            return albums;
        }

        foreach (var album in albums)
        {
            album.Image = images.Find(x => x.Id == album.ImageId);
        }

        return albums;
    }
}
