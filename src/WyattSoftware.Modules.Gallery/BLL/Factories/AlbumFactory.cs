﻿// <copyright file="AlbumFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Factories;

using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IAlbumFactory"/> interface.
/// </summary>
public class AlbumFactory : IAlbumFactory
{
    private readonly IUserRepository userRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly IImageRepository imageRepository;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<IImageFactory> imageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="AlbumFactory"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="forumAlbumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    public AlbumFactory(
        IUserRepository userRepository,
        IAlbumRepository forumAlbumRepository,
        IImageRepository imageRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<IImageFactory> imageFactory)
    {
        this.userRepository = userRepository;
        this.albumRepository = forumAlbumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
    }

    /// <inheritdoc />
    public Album New(int userId, string displayName, string description = "")
    {
        return new Album(
            this.userRepository,
            this.albumRepository,
            this.imageRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            userId,
            displayName,
            description);
    }

    /// <inheritdoc />
    public Album New(AlbumEntity albumEntity)
    {
        return new Album(
            this.userRepository,
            this.albumRepository,
            this.imageRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            albumEntity);
    }
}
