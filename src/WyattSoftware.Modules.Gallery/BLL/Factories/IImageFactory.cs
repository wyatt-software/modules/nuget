﻿// <copyright file="IImageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Provides a way to create images without passing all dependencies every time.
/// </summary>
public interface IImageFactory : IDomainFactory<Image, ImageEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Image"/> class.
    /// </summary>
    /// <param name="userId">The owner.</param>
    /// <param name="albumId">The album.</param>
    /// <returns>A new <see cref="Image"/>, injected with the dependencies it needs.</returns>.
    public Image New(int userId, int albumId);

    /// <summary>
    /// Initializes a new instance of the <see cref="Image"/> class.
    /// </summary>
    /// <param name="imageEntity">The entity to construct from.</param>
    /// <returns>
    /// An <see cref="Image"/> based on the <see cref="ImageEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    new Image New(ImageEntity imageEntity);
}
