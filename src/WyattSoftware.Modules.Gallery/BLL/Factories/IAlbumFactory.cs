﻿// <copyright file="IAlbumFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;

/// <summary>
/// Provides a way to create albums without passing all dependencies every time.
/// </summary>
public interface IAlbumFactory : IDomainFactory<Album, AlbumEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Album"/> class.
    /// </summary>
    /// <param name="userId">The owner.</param>
    /// <param name="displayName">The display name.</param>
    /// <param name="description">A brief description.</param>
    /// <returns>A new <see cref="Album"/>, injected with the dependencies it needs.</returns>.
    Album New(int userId, string displayName, string description = "");

    /// <summary>
    /// Initializes a new instance of the <see cref="Album"/> class.
    /// </summary>
    /// <param name="albumEntity">The entity to construct the user from.</param>
    /// <returns>
    /// An <see cref="Album"/> based on the <see cref="AlbumEntity"/>, injected with the dependencies it needs.
    /// </returns>
    new Album New(AlbumEntity albumEntity);
}
