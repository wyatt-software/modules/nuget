﻿// <copyright file="ImageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Factories;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.Features.Storage;
using WyattSoftware.Modules.Gallery.BLL.ImageProcessing;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Gallery.Options;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IImageFactory"/> interface.
/// </summary>
public class ImageFactory : IImageFactory
{
    private readonly IOptions<GalleryOptions> galleryOptions;
    private readonly IHttpClientFactory httpClientFactory;
    private readonly ILogger<Image> logger;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IImageProcessor imageProcessor;
    private readonly IStorageManager storageManager;
    private readonly IImageAddOn imageAddOn;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly Lazy<IAlbumFactory> albumFactory;
    private readonly Lazy<IUserFactory> userFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="ImageFactory"/> class.
    /// </summary>
    /// <param name="galleryOptions">Injected IOptions{GalleryOptions}.</param>
    /// <param name="httpClientFactory">Injected IhttpClientFactory.</param>
    /// <param name="logger">Injected ILogger{Image}.</param
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="imageProcessor">Injected IImageProcessor.</param>
    /// <param name="storageManager">Injected IStorageManager.</param>
    /// <param name="imageAddOn">Injected IImageAddOn.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    public ImageFactory(
        IOptions<GalleryOptions> galleryOptions,
        IHttpClientFactory httpClientFactory,
        ILogger<Image> logger,
        IServiceScopeFactory serviceScopeFactory,
        IImageProcessor imageProcessor,
        IStorageManager storageManager,
        IImageAddOn imageAddOn,
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<IAlbumFactory> albumFactory)
    {
        this.galleryOptions = galleryOptions;
        this.httpClientFactory = httpClientFactory;
        this.logger = logger;
        this.serviceScopeFactory = serviceScopeFactory;
        this.imageProcessor = imageProcessor;
        this.storageManager = storageManager;
        this.imageAddOn = imageAddOn;
        this.userRepository = userRepository;
        this.userFactory = userFactory;
        this.albumRepository = albumRepository;
        this.albumFactory = albumFactory;
        this.imageRepository = imageRepository;
    }

    /// <inheritdoc />
    public Image New(int userId, int albumId)
    {
        return new Image(
            this.galleryOptions,
            this.httpClientFactory,
            this.logger,
            this.serviceScopeFactory,
            this.imageProcessor,
            this.storageManager,
            this.imageAddOn,
            this.userRepository,
            this.albumRepository,
            this.imageRepository,
            this.userFactory.Value,
            this.albumFactory.Value,
            userId,
            albumId);
    }

    /// <inheritdoc />
    public Image New(ImageEntity imageEntity)
    {
        return new Image(
            this.galleryOptions,
            this.httpClientFactory,
            this.logger,
            this.serviceScopeFactory,
            this.imageProcessor,
            this.storageManager,
            this.imageAddOn,
            this.userRepository,
            this.albumRepository,
            this.imageRepository,
            this.userFactory.Value,
            this.albumFactory.Value,
            imageEntity);
    }
}
