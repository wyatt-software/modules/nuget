﻿// <copyright file="ImageResizeStrategy.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Enums;

/// <summary>
/// Defines the strategy to use when resizing an image.
/// </summary>
public enum ImageResizeStrategy
{
    /// <summary>
    /// Copies the image without changing it's dimensions.
    /// </summary>
    None,

    /// <summary>
    /// Resizes the image so that it fills the new size, cropping one dimension if necessary.
    /// </summary>
    Fill,

    /// <summary>
    /// Resizes the new image so that it fits within the new size.
    /// </summary>
    Fit,

    /// <summary>
    /// Distorts the image to the new dimensions.
    /// </summary>
    Stretch,
}
