﻿// <copyright file="Image.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Features.Storage;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.ImageProcessing;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Gallery.Options;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Gallery image domain model.
/// </summary>
public partial class Image : ImageEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly GalleryOptions galleryOptions;
    private readonly IHttpClientFactory httpClientFactory;
    private readonly ILogger<Image> logger;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IImageProcessor imageProcessor;
    private readonly IStorageManager storageManager;
    private readonly IImageAddOn imageAddOn;
    private readonly IUserRepository userRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly IImageRepository imageRepository;
    private readonly IUserFactory userFactory;
    private readonly IAlbumFactory albumFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Image"/> class.
    /// </summary>
    /// <param name="galleryOptions">Injected IOptions{GalleryOptions}.</param>
    /// <param name="httpClientFactory">Injected IHttpClientFactory.</param>
    /// <param name="logger">Injected ILogger{Image}.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="imageProcessor">Injected IImageProcessor.</param>
    /// <param name="storageManager">Injected IStorageManager.</param>
    /// <param name="imageAddOn">Injected IImageAddOn.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    /// <param name="userId">The user.</param>
    /// <param name="albumId">The album.</param>
    internal Image(
        IOptions<GalleryOptions> galleryOptions,
        IHttpClientFactory httpClientFactory,
        ILogger<Image> logger,
        IServiceScopeFactory serviceScopeFactory,
        IImageProcessor imageProcessor,
        IStorageManager storageManager,
        IImageAddOn imageAddOn,
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IAlbumFactory albumFactory,
        int userId,
        int albumId)
    {
        this.galleryOptions = galleryOptions.Value;
        this.httpClientFactory = httpClientFactory;
        this.logger = logger;
        this.serviceScopeFactory = serviceScopeFactory;
        this.imageProcessor = imageProcessor;
        this.storageManager = storageManager;
        this.imageAddOn = imageAddOn;
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.albumFactory = albumFactory;

        this.Uuid = Guid.NewGuid();
        this.UserId = userId;
        this.AlbumId = albumId;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Image"/> class.
    /// </summary>
    /// <param name="galleryOptions">Injected IOptions{GalleryOptions}.</param>
    /// <param name="httpClientFactory">Injected IHttpClientFactory.</param>
    /// <param name="logger">Injected ILogger{Image}.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="imageProcessor">Injected IImageProcessor.</param>
    /// <param name="storageManager">Injected IStorageManager.</param>
    /// <param name="imageAddOn">Injected IImageAddOn.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="albumFactory">Injected IAlbumFactory.</param>
    /// <param name="imageEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Image(
        IOptions<GalleryOptions> galleryOptions,
        IHttpClientFactory httpClientFactory,
        ILogger<Image> logger,
        IServiceScopeFactory serviceScopeFactory,
        IImageProcessor imageProcessor,
        IStorageManager storageManager,
        IImageAddOn imageAddOn,
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IAlbumFactory albumFactory,
        ImageEntity imageEntity)
    {
        this.galleryOptions = galleryOptions.Value;
        this.httpClientFactory = httpClientFactory;
        this.serviceScopeFactory = serviceScopeFactory;
        this.logger = logger;
        this.imageProcessor = imageProcessor;
        this.storageManager = storageManager;
        this.imageAddOn = imageAddOn;
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.albumFactory = albumFactory;

        this.Id = imageEntity.Id;
        this.Created = imageEntity.Created;
        this.Modified = imageEntity.Modified;
        this.Uuid = imageEntity.Uuid;

        this.UserId = imageEntity.UserId;
        this.AlbumId = imageEntity.AlbumId;
        this.Filename = imageEntity.Filename;
        this.Extension = imageEntity.Extension;
        this.Caption = imageEntity.Caption;
    }

    /// <summary>
    /// Creates a gallery image.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        await this.ValidateAsync();

        var imageEntity = Mapper.Map<ImageEntity>(this);
        this.Id = await this.imageRepository.CreateAsync(imageEntity);

        await this.MaintainAlbumCoverAsync(this.AlbumId);
    }

    /// <summary>
    /// Updates a gallery image.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var imageEntity = Mapper.Map<ImageEntity>(this);
        await this.imageRepository.UpdateAsync(imageEntity);

        await this.MaintainAlbumCoverAsync(this.AlbumId);
    }

    /// <summary>
    /// Attempts to delete an image by it's primary key.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        await this.DeleteFilesAsync();

        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity != null && ((int?)userEntity.Properties["imageId"]) == this.Id)
        {
            userEntity.Properties.Remove("imageId");
            await this.userRepository.UpdateAsync(userEntity);
        }

        var albumEntities = await this.albumRepository.FetchByCoverAsync(this.Id);
        var albums = albumEntities.Select(this.albumFactory.New);
        foreach (var album in albums)
        {
            album.ImageId = null;
            await album.UpdateAsync();
        }

        foreach (var asyncAction in this.imageAddOn.BeforeDeleteHandlers)
        {
            await asyncAction(this.serviceScopeFactory, this.Id);
        }

        await this.imageRepository.DeleteAsync(this.Id);

        foreach (var album in albums)
        {
            await album.EnsureCoverImageAsync();
        }
    }

    /// <summary>
    /// Sets the image as the cover of the album it belongs to.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task SetAsCoverAsync()
    {
        var albumEntity = await this.albumRepository.ReadAsync(this.AlbumId);
        if (albumEntity == null)
        {
            throw new Exception($"Album {this.AlbumId} not found.");
        }

        albumEntity.ImageId = this.Id;
        await this.albumRepository.UpdateAsync(albumEntity);
    }

    /// <summary>
    /// Sets the image as the avatar of the user it belongs to.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task SetAsAvatarAsync()
    {
        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity == null)
        {
            throw new Exception($"User {this.UserId} not found.");
        }

        userEntity.Properties["imageId"] = this.Id;
        await this.userRepository.UpdateAsync(userEntity);
    }

    /// <summary>
    /// Ensure the album's cover is valid.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task MaintainAlbumCoverAsync(int albumId)
    {
        var albumEntity = await this.albumRepository.ReadAsync(albumId);
        if (albumEntity != null)
        {
            var album = this.albumFactory.New(albumEntity);
            await album.EnsureCoverImageAsync();
        }
    }
}
