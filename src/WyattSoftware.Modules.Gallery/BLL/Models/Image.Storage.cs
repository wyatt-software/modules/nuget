﻿// <copyright file="Image.Storage.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using MimeKit;
using WyattSoftware.Modules.Core.BLL.DTOs;
using WyattSoftware.Modules.Gallery.BLL.Enums;

/// <summary>
/// Gallery image domain model.
/// </summary>
public partial class Image
{
    private const string OriginalSizeName = "original";

    private readonly ImageSize[] sizes =
    {
        new ImageSize("large", 1024, 1024, ImageResizeStrategy.Fit),
        new ImageSize("thumb", 160, 160, ImageResizeStrategy.Fill),
    };

    /// <summary>
    /// Gets the base filename without the extension.
    /// </summary>
    /// <remarks>We use the ImageGuid.</remarks>
    private string BaseFilename => this.Uuid.ToString();

    /// <summary>
    /// Gets the sub-folder name.
    /// </summary>
    /// <remarks>
    /// Under each size folder, images are further organised into folders for the first two chars.
    /// </remarks>
    private string SubFolder => this.BaseFilename[..2];

    /// <summary>
    /// Uploads the file to the server.
    /// </summary>
    /// <param name="fileDto">The file to upload.</param>
    /// <returns>Nothing.</returns>
    public async Task UploadAsync(FileDto fileDto)
    {
        // Invalid file type?
        if (!HasAllowedExtension(fileDto.Filename, this.galleryOptions.AllowedImageExtensions))
        {
            throw new ArgumentException(
                $"Please only upload files of type {this.galleryOptions.AllowedImageExtensions}");
        }

        // This is the filename stored in the database. Not the filesystem.
        this.Filename = fileDto.Filename;
        this.Extension = Path.GetExtension(fileDto.Filename);

        var uploadPath = $"{this.galleryOptions.GalleryFolderName}/{OriginalSizeName}/{this.SubFolder}/";

        this.logger.LogDebug("EnsureFolderAsync()...");
        await this.storageManager.EnsureFolderAsync(uploadPath);

        // Use UUID as actual filename in filesystem.
        fileDto.Filename = this.BaseFilename + Path.GetExtension(fileDto.Filename);

        // We need the data for multiple sizes, so we clone it to prevent the stream from being disposed.
        this.logger.LogDebug("SaveFileAsync()...");
        await this.storageManager.SaveFileAsync(fileDto, uploadPath);

        foreach (var size in this.sizes)
        {
            this.logger.LogDebug($"SaveFileAsync(..., \"{size}\")...");
            await this.CreateSizeAsync(fileDto, size);
        }
    }

    /// <summary>
    /// Uploads the image at the URL to the server.
    /// </summary>
    /// <param name="url">The URL of the image to get and upload.</param>
    /// <returns>Nothing.</returns>
    public async Task UploadAsync(string url)
    {
        var fileDto = await this.DownloadAsync(url);
        await this.UploadAsync(fileDto);
    }

    /// <summary>
    /// Returns true if the given file has the specified extension.
    /// </summary>
    /// <param name="filePath">filename to test.</param>
    /// <param name="allowedExtensions">Comma-separated list of extensions (with their dots).</param>
    /// <returns>true if the given file has the specified extension.</returns>
    private static bool HasAllowedExtension(string filePath, string allowedExtensions)
    {
        if (string.IsNullOrEmpty(allowedExtensions))
        {
            return true;
        }

        // Remove query paramteters when given a URL.
        if (filePath.Contains('?'))
        {
            filePath = filePath.Split('?')[0];
        }

        var allowedFileExtensions = allowedExtensions.Split(',');
        var extension = Path.GetExtension(filePath).ToLower();
        return allowedFileExtensions.Contains(extension);
    }

    /// <summary>
    /// Attempts to get the file at the given URL.
    /// </summary>
    /// <param name="url">The URL of the file to get.</param>
    /// <returns>A File DTO.</returns>
    private async Task<FileDto> DownloadAsync(string url)
    {
        var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, url.Replace(" ", "%20"))
        {
            Headers =
            {
                { HeaderNames.Accept, "image/*" },
            },
        };

        var httpClient = this.httpClientFactory.CreateClient();
        var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

        if (!httpResponseMessage.IsSuccessStatusCode)
        {
            throw new Exception(
                $"Failed to download \"{url}\". Returned HTTP status {httpResponseMessage.StatusCode}");
        }

        var filename = Path.GetFileName(url);
        using var data = await httpResponseMessage.Content.ReadAsStreamAsync();

        return new FileDto
        {
            Filename = filename,
            ContentType = MimeTypes.GetMimeType(filename),
            ContentLength = data.Length,
            Data = data,
        };
    }

    /// <summary>
    /// Generates a resized version of the image.
    /// </summary>
    /// <param name="sourceFile">The source image. Assumes the sourceFile data stream is at the beginning.</param>
    /// <param name="size">The size to create.</param>
    /// <returns>Nothing.</returns>
    private async Task CreateSizeAsync(FileDto sourceFile, ImageSize size)
    {
        if (sourceFile.Data == null || sourceFile.Data.Length == 0)
        {
            throw new Exception("Can't create size. Source file has no data.");
        }

        using var resizedData = new MemoryStream();
        sourceFile.Data.Seek(0, SeekOrigin.Begin);
        await this.imageProcessor.ResizeImageAsync(
            sourceFile.Data, resizedData, size.Width, size.Height, size.Strategy);

        var resizedFile = new FileDto()
        {
            ContentType = sourceFile.ContentType,
            ContentLength = resizedData.Length,
            Filename = sourceFile.Filename,
            Data = resizedData,
        };

        var sizeFolder = $"{this.galleryOptions.GalleryFolderName}/{size.Name}/{this.SubFolder}/";

        await this.storageManager.EnsureFolderAsync(sizeFolder);
        await this.storageManager.SaveFileAsync(resizedFile, sizeFolder);
    }

    /// <summary>
    /// Deletes all sizes for this image.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task DeleteFilesAsync()
    {
        foreach (var size in this.sizes)
        {
            await this.DeleteSizeAsync(size.Name);
        }

        await this.DeleteSizeAsync(OriginalSizeName);
    }

    /// <summary>
    /// Deletes a single size file.
    /// </summary>
    /// <param name="sizeName">The size to delete.</param>
    /// <returns>Nothing.</returns>
    private async Task DeleteSizeAsync(string sizeName)
    {
        var subFolder = $"{this.galleryOptions.GalleryFolderName}/{sizeName}/{this.SubFolder}/";
        var realFilename = $"{this.BaseFilename}{this.Extension}";

        await this.storageManager.DeleteAsync($"{subFolder}{realFilename}");
        await this.storageManager.RemoveFolderIfEmptyAsync(subFolder);
    }
}
