﻿// <copyright file="Album.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System.Threading.Tasks;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Gallery album domain model.
/// </summary>
public partial class Album
{
    /// <summary>
    /// Gets or sets the user who this album belongs to.
    /// </summary>
    /// <remarks>Use "expand=user".</remarks>
    public User? User { get; set; }

    /// <summary>
    /// Gets or sets the cover image for this album.
    /// </summary>
    /// <remarks>Use "expand=image".</remarks>
    public Image? Image { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user|image] .</para>
    /// </param>
    /// <returns>The album, with references expanded.</returns>
    public async Task<Album> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandUserAsync();
                    await this.ExpandImageAsync();
                    break;

                case "user":
                    await this.ExpandUserAsync();
                    break;

                case "image":
                    await this.ExpandImageAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Expands the user.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserAsync()
    {
        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.User = this.userFactory.New(userEntity);
    }

    /// <summary>
    /// Expands the image.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandImageAsync()
    {
        if (!this.ImageId.HasValue)
        {
            return;
        }

        var imageEntity = await this.imageRepository.ReadAsync(this.ImageId.Value);
        if (imageEntity == null)
        {
            return;
        }

        this.Image = this.imageFactory.New(imageEntity);
    }
}