﻿// <copyright file="Album.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System;
using System.Threading.Tasks;

/// <summary>
/// Gallery album domain model.
/// </summary>
public partial class Album
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidatateUserIdAsync();
        await this.ValidatateImageIdAsync();
        await this.ValidatateAliasAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateUserIdAsync()
    {
        var userExists = await this.userRepository.ExistsAsync(this.UserId);
        if (!userExists)
        {
            throw new ArgumentException($"User {this.UserId} not found.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateImageIdAsync()
    {
        if (!this.ImageId.HasValue)
        {
            return;
        }

        var imageEntity = await this.imageRepository.ReadAsync(this.ImageId.Value);
        if (imageEntity == null)
        {
            throw new ArgumentException($"Image {this.ImageId} not found.");
        }

        if (imageEntity.AlbumId != this.Id)
        {
            throw new ArgumentException($"Image {this.ImageId} doesn't belong to album {this.Id}.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateAliasAsync()
    {
        var existingAlbumEntity = await this.albumRepository.ReadAsync(this.UserId, this.Alias);
        if (existingAlbumEntity != null && existingAlbumEntity.Id != this.Id)
        {
            throw new ArgumentException($"Alias \"{this.Alias}\" is taken.");
        }
    }
}
