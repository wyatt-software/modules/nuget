﻿// <copyright file="Image.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System;
using System.IO;
using System.Threading.Tasks;

/// <summary>
/// Gallery image domain model.
/// </summary>
public partial class Image
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        this.ValidatateExtension();
        await this.ValidatateUserIdAsync();
        await this.ValidatateAlbumIdAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateUserIdAsync()
    {
        var userExists = await this.userRepository.ExistsAsync(this.UserId);
        if (!userExists)
        {
            throw new ArgumentException($"User {this.UserId} not found.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateAlbumIdAsync()
    {
        var albumEntity = await this.albumRepository.ReadAsync(this.AlbumId);
        if (albumEntity == null)
        {
            throw new ArgumentException($"Album {this.AlbumId} not found.");
        }

        if (albumEntity.UserId != this.UserId)
        {
            throw new ArgumentException(
                $"Album {this.AlbumId} doesn't belong to user {this.UserId}.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    private void ValidatateExtension()
    {
        if (this.Extension != Path.GetExtension(this.Filename))
        {
            // TODO: Make extension a derived field. Or possibly remove altogether.
            throw new ArgumentException(
                $"Extension \"{this.Extension}\" doesn't match filename \"{this.Filename}\".");
        }
    }
}
