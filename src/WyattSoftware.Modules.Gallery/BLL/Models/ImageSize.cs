﻿// <copyright file="ImageSize.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using WyattSoftware.Modules.Gallery.BLL.Enums;

/// <summary>
/// Defines the different size images to create and store from an uploaded image.
/// </summary>
public class ImageSize
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ImageSize"/> class.
    /// </summary>
    /// <param name="name">The name of the image size.</param>
    /// <param name="width">The width of the image size.</param>
    /// <param name="height">The height of the image size.</param>
    /// <param name="strategy">How to resize/crop the image.</param>
    public ImageSize(string name, int width, int height, ImageResizeStrategy strategy)
    {
        this.Name = name;
        this.Width = width;
        this.Height = height;
        this.Strategy = strategy;
    }

    /// <summary>
    /// Gets or sets the name of the image size.
    /// </summary>
    /// <remarks>
    /// Should not contain any spaces or special characters, as it's used as a folder in the filesystem.
    /// </remarks>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the width of the image size.
    /// </summary>
    public int Width { get; set; }

    /// <summary>
    /// Gets or sets the height of the image size.
    /// </summary>
    public int Height { get; set; }

    /// <summary>
    /// Gets or sets the resize strategy to use.
    /// </summary>
    public ImageResizeStrategy Strategy { get; set; }
}
