﻿// <copyright file="Album.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System;
using System.Linq;
using System.Threading.Tasks;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Gallery album domain model.
/// </summary>
public partial class Album : AlbumEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly IUserRepository userRepository;
    private readonly IAlbumRepository albumRepository;
    private readonly IImageRepository imageRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Album"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="userId">The owner.</param>
    /// <param name="displayName">The display name.</param>
    /// <param name="description">A brief description.</param>
    internal Album(
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        int userId,
        string displayName,
        string description = "")
    {
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;

        // Trigger validation by setting properties, not backing fields.
        this.UserId = userId;
        this.DisplayName = displayName;
        this.Description = description;

        // Alias is a special case.
        // It's automatically set by the Service when creating.
        // TODO: Make this feature of the service public, and call it from the API when constructing a new Topic for
        // creation.
        this.Alias = string.Empty;

        // This is always null on creation.
        this.ImageId = null;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Album"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="albumRepository">Injected IAlbumRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="albumEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Album(
        IUserRepository userRepository,
        IAlbumRepository albumRepository,
        IImageRepository imageRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        AlbumEntity albumEntity)
    {
        this.userRepository = userRepository;
        this.albumRepository = albumRepository;
        this.imageRepository = imageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;

        this.Id = albumEntity.Id;
        this.Created = albumEntity.Created;
        this.Modified = albumEntity.Modified;

        this.UserId = albumEntity.UserId;
        this.ImageId = albumEntity.ImageId;
        this.Alias = albumEntity.Alias;
        this.DisplayName = albumEntity.DisplayName;
        this.Description = albumEntity.Description;
    }

    /// <summary>
    /// Attempts to create an album and persist it.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        await this.ValidateAsync();

        this.Alias = await this.albumRepository.GenerateUniqueAliasAsync(this.UserId, this.DisplayName);

        var albumEntity = Mapper.Map<AlbumEntity>(this);
        this.Id = await this.albumRepository.CreateAsync(albumEntity);
    }

    /// <summary>
    /// Updates a gallery album.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var albumEntity = Mapper.Map<AlbumEntity>(this);
        await this.albumRepository.UpdateAsync(albumEntity);
    }

    /// <summary>
    /// Attempts to delete an album by it's primary key.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        var count = await this.imageRepository.GetCountAsync(this.UserId, this.Id);
        if (count > 0)
        {
            var s = count != 1 ? "s" : string.Empty;
            throw new InvalidOperationException(
                $"Can't delete album \"{this.DisplayName}\" because it still contains {count} image{s}.");
        }

        await this.albumRepository.DeleteAsync(this.Id);
    }

    /// <summary>
    /// If the album doesn't already have a cover image, this method assigns the first one in the album.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task EnsureCoverImageAsync()
    {
        var imageEntity = this.ImageId.HasValue
            ? await this.imageRepository.ReadAsync(this.ImageId.Value)
            : null;

        var albumAlreadyHasValidCover =
            imageEntity != null &&
            imageEntity.AlbumId == this.Id;

        if (albumAlreadyHasValidCover)
        {
            return;
        }

        var imageEntities = await this.imageRepository.FetchAsync(0, 1, 0, this.Id);
        this.ImageId = imageEntities.FirstOrDefault()?.Id;

        await this.UpdateAsync();
    }
}
