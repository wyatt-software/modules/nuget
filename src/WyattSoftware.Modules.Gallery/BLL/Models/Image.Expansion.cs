﻿// <copyright file="Image.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.Models;

using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Gallery image domain model.
/// </summary>
public partial class Image
{
    /// <summary>
    /// Gets or sets the album this image belongs to.
    /// </summary>
    /// <remarks>Use "expand=album".</remarks>
    public Album? Album { get; set; }

    /// <summary>
    /// Gets or sets the user who this album belongs to.
    /// </summary>
    /// <remarks>Use "expand=user".</remarks>
    public User? User { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|album|user] .</para>
    /// </param>
    /// <returns>The image, with references expanded.</returns>
    public async Task<Image> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandAlbumAsync();
                    await this.ExpandUserAsync();
                    break;

                case "album":
                    await this.ExpandAlbumAsync();
                    break;

                case "user":
                    await this.ExpandUserAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Registers expanders used by the User model. This should be called once on app startup.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    internal static void RegisterExpanders(IUserAddOn userAddOn)
    {
        userAddOn.ModelExpanders.Add(
            "image",
            async (IServiceScopeFactory serviceScopeFactory, User user) =>
            {
                var id = (int?)user.Properties["imageId"];
                if (!id.HasValue)
                {
                    return user;
                }

                // This is a delegate, so it doesn't have access to dependency injection.
                // We can't use more specific method-injection, because the caller doesn't know what the delegate
                // will need, and probably doesn't have a reference to the assembly anyway.
                using var scope = serviceScopeFactory.CreateScope();
                var scopedImageRepository = scope.ServiceProvider.GetRequiredService<IImageRepository>();

                var imageEntity = await scopedImageRepository.ReadAsync(id.Value);
                if (imageEntity == null)
                {
                    return user;
                }

                var scopedImageFactory = scope.ServiceProvider.GetRequiredService<IImageFactory>();
                var image = scopedImageFactory.New(imageEntity);

                user.NestedResources["image"] = image;
                return user;
            });
    }

    /// <summary>
    /// Expands the album.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandAlbumAsync()
    {
        var albumEntity = await this.albumRepository.ReadAsync(this.AlbumId);
        if (albumEntity == null)
        {
            return;
        }

        this.Album = this.albumFactory.New(albumEntity);
    }

    /// <summary>
    /// Expands the user.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserAsync()
    {
        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.User = this.userFactory.New(userEntity);
    }
}