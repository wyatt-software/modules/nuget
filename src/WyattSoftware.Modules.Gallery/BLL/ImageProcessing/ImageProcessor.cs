﻿// <copyright file="ImageProcessor.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.ImageProcessing;

using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Omu.ValueInjecter;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using WyattSoftware.Modules.Gallery.BLL.Enums;

/// <summary>
/// ImageSharp implementation of the image processor.
/// </summary>
public class ImageProcessor : IImageProcessor
{
    private readonly ILogger<ImageProcessor> logger;

    /// <summary>
    /// Initializes static members of the <see cref="ImageProcessor"/> class.
    /// </summary>
    static ImageProcessor()
    {
        InitMapping();
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ImageProcessor"/> class.
    /// </summary>
    /// <param name="logger">Injected ILogger{ImageProcessor}.</param>
    public ImageProcessor(ILogger<ImageProcessor> logger)
    {
        this.logger = logger;
    }

    /// <inheritdoc />
    public async Task ResizeImageAsync(
        Stream input,
        Stream output,
        int width,
        int height,
        ImageResizeStrategy strategy)
    {
        var size = new Size(width, height);
        var mode = Mapper.Map<ResizeMode>(strategy);

        try
        {
            var image = await Image.LoadAsync(input);
            var format = await Image.DetectFormatAsync(input);
            image.Mutate(x => x.AutoOrient().Resize(new ResizeOptions
            {
                Size = size,
                Mode = mode,
            }));

            await image.SaveAsync(output, format);
            image.Dispose();
        }
        catch (Exception ex)
        {
            this.logger.LogWarning($"Failed to resize image. {ex.Message}");
        }
    }

    private static void InitMapping()
    {
        // Map our internal `ImageResizeStrategy` enum to the external `ResizeMode` enum.
        Mapper.AddMap<ImageResizeStrategy, ResizeMode>(strategy =>
        {
            return strategy switch
            {
                ImageResizeStrategy.None => throw new NotImplementedException(),
                ImageResizeStrategy.Fill => ResizeMode.Crop,
                ImageResizeStrategy.Fit => ResizeMode.Max,
                ImageResizeStrategy.Stretch => ResizeMode.Stretch,
                _ => throw new NotImplementedException(),
            };
        });
    }
}
