﻿// <copyright file="IImageProcessor.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.BLL.ImageProcessing;

using System.IO;
using System.Threading.Tasks;
using WyattSoftware.Modules.Gallery.BLL.Enums;

/// <summary>
/// Provides a way to store files.
/// </summary>
public interface IImageProcessor
{
    /// <summary>
    /// Creates a resized/cropped image from an original.
    /// </summary>
    /// <param name="input">The raw input data of the source image.</param>
    /// <param name="output">The raw output data of the resized image.</param>
    /// <param name="width">Width in pixels.</param>
    /// <param name="height">Height in pixels.</param>
    /// <param name="strategy">How to resize/crop the image.</param>
    /// <returns>Nothing.</returns>
    Task ResizeImageAsync(
        Stream input,
        Stream output,
        int width,
        int height,
        ImageResizeStrategy strategy);
}
