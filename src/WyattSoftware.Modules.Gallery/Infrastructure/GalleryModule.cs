﻿// <copyright file="GalleryModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.Infrastructure;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Gallery.API.Controllers;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.ImageProcessing;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Services;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Gallery.Options;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Gallery module.
/// </summary>
public static class GalleryModule
{
    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddGalleryModule(
        this IServiceCollection services, IConfiguration configuration)
    {
        var gallerySection = configuration.GetSection("WyattSoftware:Modules:Gallery");

        // Make WyattSoftware.Modules.Gallery appsettings.json options available via dependency injection.
        services.Configure<GalleryOptions>(gallerySection);

        // Allow the gallery module to be extended by other modules.
        services.AddSingleton(typeof(IImageAddOn), typeof(ImageAddOn));

        // DAL
        services.AddScoped(typeof(IAlbumRepository), typeof(AlbumRepository));
        services.AddScoped(typeof(IImageRepository), typeof(ImageRepository));

        // BLL
        services.AddScoped(typeof(IAlbumFactory), typeof(AlbumFactory));
        services.AddScoped(typeof(IAlbumService), typeof(AlbumService));
        services.AddScoped(typeof(IImageFactory), typeof(ImageFactory));
        services.AddScoped(typeof(IImageService), typeof(ImageService));
        services.AddScoped(typeof(IImageProcessor), typeof(ImageProcessor));

        return services;
    }

    /// <summary>
    /// Configures the Gallery module.
    /// </summary>
    /// <param name="app">The app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseGalleryModule(this IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();
        var userAddOn = scope.ServiceProvider.GetService<IUserAddOn>();
        if (userAddOn == null)
        {
            throw new Exception("Couldn't get an IUserAddOn.");
        }

        // API
        AlbumsController.InitMapping();
        ImagesController.InitMapping(userAddOn);

        // Register delegates used to expand optional nested resources on the User.
        Image.RegisterExpanders(userAddOn);
        ImageService.RegisterExpanders(userAddOn);

        return app;
    }
}
