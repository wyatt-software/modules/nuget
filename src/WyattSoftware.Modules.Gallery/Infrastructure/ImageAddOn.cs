﻿// <copyright file="ImageAddOn.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.Infrastructure;

using Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Default implementation of <see cref="IImageAddOn"/>.
/// </summary>
public class ImageAddOn : IImageAddOn
{
    /// <inheritdoc/>
    public List<Func<IServiceScopeFactory, int, Task>> BeforeDeleteHandlers { get; } = new();
}
