﻿// <copyright file="IImageAddOn.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Gallery.Infrastructure;

using Microsoft.Extensions.DependencyInjection;

/// <summary>
/// The image add-on is basically just a collection of dictionaries, made available globally using the singleton
/// pattern. It allows other modules to extend the functionality of the gallery image, without the gallery module
/// having any dependency on those modules. It's done this way to keep the modules optional, and to avoid circular
/// dependencies.
/// </summary>
public interface IImageAddOn
{
    /// <summary>
    /// Gets the delegates used to handle preparations (like clearing topic thumbs) before deleting an image.
    /// </summary>
    /// <remarks>
    /// We don't do it as an event handler, because the async delegates need to be awaited.
    /// </remarks>
    List<Func<IServiceScopeFactory, int, Task>> BeforeDeleteHandlers { get; }
}
