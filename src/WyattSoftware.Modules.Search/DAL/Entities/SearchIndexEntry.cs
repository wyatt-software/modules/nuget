﻿// <copyright file="SearchIndexEntry.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.DAL.Entities;

using System.Collections.Generic;

/// <summary>
/// Search result document from the search index.
/// </summary>
public class SearchIndexEntry
{
    /// <summary>
    /// Gets or sets a unique key to identify the object this search document references.
    /// </summary>
    public string Key { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the type of document. Used when we want to only search pages, or posts etc.
    /// </summary>
    public string DocType { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the document title to use in search results (if there's no title available in the searchable
    /// data).
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a relative URL to use in search results.
    /// </summary>
    public string Link { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a dictionary of fieldName/value pairs.
    /// </summary>
    public Dictionary<string, string> SearchableData { get; set; } = new Dictionary<string, string>();
}
