﻿// <copyright file="LuceneSearchIndexFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.DAL.Factories;

using System.Collections.Generic;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Search.DAL.Services;
using WyattSoftware.Modules.Search.Options;

/// <summary>
/// Lucene implementation of the <see cref="ISearchIndexFactory"/>.
/// </summary>
public class LuceneSearchIndexFactory : ISearchIndexFactory
{
    private readonly SearchOptions searchOptions;

    /// <summary>
    /// Initializes a new instance of the <see cref="LuceneSearchIndexFactory"/> class.
    /// </summary>
    /// <param name="searchOptions">Injected IOptions{SearchOptions}.</param>
    public LuceneSearchIndexFactory(
        IOptions<SearchOptions> searchOptions)
    {
        this.searchOptions = searchOptions.Value;
    }

    /// <inheritdoc />
    public ISearchIndex New(string name, List<string> fieldNames)
    {
        return new LuceneSearchIndex(this.searchOptions.SearchIndexesPath, name, fieldNames);
    }
}
