﻿// <copyright file="ISearchIndexFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.DAL.Factories;

using WyattSoftware.Modules.Search.DAL.Services;

/// <summary>
/// Provides a way to create search indexes without knowing which implementation.
/// </summary>
public interface ISearchIndexFactory
{
    /// <summary>
    /// Creates a new seach index, and injects any dependencies it needs.
    /// </summary>
    /// <param name="name">The code name of the search index.</param>
    /// <param name="fieldNames">The field names that are searchable.</param>
    /// <returns>A new search index.</returns>
    ISearchIndex New(string name, List<string> fieldNames);
}
