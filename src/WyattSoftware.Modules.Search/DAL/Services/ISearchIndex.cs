﻿// <copyright file="ISearchIndex.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.DAL.Services;

using System;
using System.Collections.Generic;
using System.Threading;
using WyattSoftware.Modules.Search.DAL.Entities;

/// <summary>
/// Provides a way to index and search text data.
/// </summary>
public interface ISearchIndex
{
    /// <summary>
    /// Gets the code name of the search index.
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the field names that are searchable.
    /// </summary>
    List<string> FieldNames { get; }

    /// <summary>
    /// Gets the total number of results for a particular query.
    /// </summary>
    /// <param name="phrase">The search phrase.</param>
    /// <param name="docType">The type of documents to search. Blank = All.</param>
    /// <returns>The total number of results.</returns>
    int GetCount(string phrase, string docType = "");

    /// <summary>
    /// Searches the index.
    /// </summary>
    /// <param name="phrase">The search phrase.</param>
    /// <param name="docType">The type of documents to search. Blank = All.</param>
    /// <param name="offset">Offset the results (used for pagination).</param>
    /// <param name="limit">Limit the number of results.</param>
    /// <param name="fragmentSize">The max number of characters to return for each searchable field.</param>
    /// <returns>A list of keys.</returns>
    List<SearchIndexEntry> Fetch(
        string phrase, string docType = "", int offset = 0, int limit = 20, int fragmentSize = 200);

    /// <summary>
    /// Creates an entry in the search index.
    /// </summary>
    /// <param name="searchIndexEntry">The entry to add to the search index.</param>
    void Create(SearchIndexEntry searchIndexEntry);

    /// <summary>
    /// Creates multiple entries in the search index.
    /// </summary>
    /// <param name="searchIndexEntries">A list of searchDocs.</param>
    /// <param name="updateProgress">An action to call upon each created index.</param>
    /// <param name="ct">A cancellation token for cancelling long-running tasks.</param>
    /// <remarks>This should be more efficient than looping over the single Create() method.</remarks>
    void Create(
        List<SearchIndexEntry> searchIndexEntries,
        Action<string, decimal> updateProgress,
        CancellationToken ct);

    /// <summary>
    /// Updates an entry in the search index.
    /// </summary>
    /// <param name="searchIndexEntry">The entry to update in the search index.</param>
    void Update(SearchIndexEntry searchIndexEntry);

    /// <summary>
    /// Deletes an entry from the search index.
    /// </summary>
    /// <param name="key">The key of the SearchDoc to delete.</param>
    void Delete(string key);

    /// <summary>
    /// Clears the search index entirely.
    /// </summary>
    void DeleteAll();
}
