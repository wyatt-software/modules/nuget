﻿// <copyright file="LuceneSearchIndex.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.DAL.Services;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Search.Highlight;
using Lucene.Net.Store;
using Lucene.Net.Util;
using WyattSoftware.Modules.Search.DAL.Entities;

/// <summary>
/// Lucene implementation of the <see cref="ISearchIndex"/>.
/// </summary>
/// <remarks>See https://lucenenet.apache.org/ .</remarks>
public class LuceneSearchIndex : ISearchIndex
{
    private const string KeyFieldName = ".key";
    private const string DocTypeFieldName = ".doctype";
    private const string TitleFieldName = ".title";
    private const string LinkFieldName = ".link";
    private const LuceneVersion AppLuceneVersion = LuceneVersion.LUCENE_48;

    // Only update the user when this many entries are rebuilt (so that we don't spam SignalR too much).
    private const int UpdateBlock = 50;

    private readonly StandardAnalyzer analyzer;
    private readonly IndexWriter writer;

    /// <summary>
    /// Initializes a new instance of the <see cref="LuceneSearchIndex"/> class.
    /// </summary>
    /// <param name="searchIndexesPath">Absolute path of search indexes folder.</param>
    /// <param name="name">The code name of the search index.</param>
    /// <param name="fieldNames">The searchable field names.</param>
    internal LuceneSearchIndex(string searchIndexesPath, string name, List<string> fieldNames)
    {
        this.Name = name;
        this.FieldNames = fieldNames;

        var indexLocation = Path.Join(searchIndexesPath, name);
        var dir = FSDirectory.Open(indexLocation);

        this.analyzer = new StandardAnalyzer(AppLuceneVersion);

        var indexConfig = new IndexWriterConfig(AppLuceneVersion, this.analyzer);
        this.writer = new IndexWriter(dir, indexConfig);
    }

    /// <inheritdoc />
    public string Name { get; }

    /// <inheritdoc />
    public List<string> FieldNames { get; }

    /// <inheritdoc />
    public int GetCount(string phrase, string docType = "")
    {
        var query = this.GetQuery(phrase, docType);
        var collector = new TotalHitCountCollector();
        var searcher = new IndexSearcher(this.writer.GetReader(true));
        searcher.Search(query, collector);
        return collector.TotalHits;
    }

    /// <inheritdoc />
    public List<SearchIndexEntry> Fetch(
        string phrase, string docType = "", int offset = 0, int limit = 20, int fragmentSize = 500)
    {
        var query = this.GetQuery(phrase, docType);

        var searcher = new IndexSearcher(this.writer.GetReader(true));
        ScoreDoc[] hits;
        try
        {
            hits = searcher.Search(query, offset + limit).ScoreDocs;
        }
        catch (ArgumentException ex)
        {
            if (ex.Message.StartsWith("numHits must be > 0;"))
            {
                // The search index doesn't contain any entries.
                // If there actually are records in the database, this probably means you need to rebuild the
                // search index.
                return new List<SearchIndexEntry>();
            }

            throw;
        }

        var highlighter = new Highlighter(
            new SimpleHTMLFormatter("<mark>", "</mark>"),
            new QueryScorer(query))
        {
            TextFragmenter = new SimpleFragmenter(fragmentSize),
        };

        var results = hits
            .Skip(offset)
            .Take(limit)
            .Select(hit => this.Highlight(
                this.MapToSearchIndexEntry(searcher.Doc(hit.Doc)),
                highlighter,
                fragmentSize))
            .ToList();

        return results;
    }

    /// <inheritdoc />
    public void Create(SearchIndexEntry searchIndexEntry)
    {
        var doc = this.MapToLuceneDocument(searchIndexEntry);
        this.writer.AddDocument(doc);
        this.writer.Commit();
    }

    /// <inheritdoc />
    public void Create(
        List<SearchIndexEntry> searchIndexEntries, Action<string, decimal> updateProgress, CancellationToken ct)
    {
        var lastUpdatedIndex = -1;
        var processed = 0;
        var total = searchIndexEntries.Count;
        foreach (var searchIndexEntry in searchIndexEntries)
        {
            ct.ThrowIfCancellationRequested();

            var doc = this.MapToLuceneDocument(searchIndexEntry);
            this.writer.AddDocument(doc);

            // We report on the entry we're _about_ to process.
            // This way, if there's an error, the user will know what caused it.
            var currentIndex = processed + 1;

            var shouldUpdateProgress = lastUpdatedIndex == -1 || currentIndex - lastUpdatedIndex >= UpdateBlock;
            if (shouldUpdateProgress)
            {
                var message = $"Entry {currentIndex}/{total} ({searchIndexEntry.Key})";
                var percentage = Math.Round(100M * currentIndex / total, 1);
                updateProgress(message, percentage);
                lastUpdatedIndex = currentIndex;
            }

            processed++;
        }

        this.writer.Commit();
    }

    /// <inheritdoc />
    public void Update(SearchIndexEntry searchIndexEntry)
    {
        var doc = this.MapToLuceneDocument(searchIndexEntry);
        this.writer.UpdateDocument(new Term(KeyFieldName), doc);
        this.writer.Commit();
    }

    /// <inheritdoc />
    public void Delete(string key)
    {
        this.writer.DeleteDocuments(new Term(KeyFieldName, key));
        this.writer.Commit();
    }

    /// <inheritdoc />
    public void DeleteAll()
    {
        this.writer.DeleteAll();
        this.writer.Commit();
    }

    /// <summary>
    /// Gets the query used by GetCount() and Fetch().
    /// </summary>
    /// <param name="phrase">The search phrase.</param>
    /// <param name="docType">The type of documents to search. Blank = All.</param>
    /// <returns>The Lucene query.</returns>
    private Query GetQuery(string phrase, string docType)
    {
        var query = new BooleanQuery();

        // Phrase
        if (!string.IsNullOrEmpty(phrase))
        {
            var allKeywordsQuery = new BooleanQuery();
            var tokens = phrase.ToLowerInvariant().Split(' ', '\t').ToArray();
            foreach (var token in tokens)
            {
                var anyFieldQuery = new BooleanQuery();
                foreach (var fieldName in this.FieldNames)
                {
                    anyFieldQuery.Add(new TermQuery(new Term(fieldName, token)), Occur.SHOULD);
                }

                allKeywordsQuery.Add(anyFieldQuery, Occur.MUST);
            }

            query.Add(allKeywordsQuery, Occur.MUST);
        }

        // Doc type
        if (!string.IsNullOrEmpty(docType))
        {
            var docTypeQuery = new TermQuery(new Term(DocTypeFieldName, docType));
            query.Add(docTypeQuery, Occur.MUST);
        }

        return query;
    }

    /// <summary>
    /// Highlights the terms and gets the best fragment for the search result.
    /// </summary>
    /// <param name="searchIndexEntry">The SearchIndexEntry to highlight.</param>
    /// <param name="highlighter">The highlighter to use.</param>
    /// <param name="fragmentSize">The max length of characters to return.</param>
    /// <returns>A highlighted SearchIndexEntry.</returns>
    private SearchIndexEntry Highlight(SearchIndexEntry searchIndexEntry, Highlighter highlighter, int fragmentSize)
    {
        foreach (var fieldName in this.FieldNames)
        {
            var text = searchIndexEntry.SearchableData[fieldName] ?? string.Empty;
            var fragment = highlighter.GetBestFragment(this.analyzer, fieldName, text);

            searchIndexEntry.SearchableData[fieldName] =
                fragment ?? text[..Math.Min(text.Length, fragmentSize)];
        }

        return searchIndexEntry;
    }

    /// <summary>
    /// Maps a Lucene Document to a SearchIndexEntry.
    /// </summary>
    /// <param name="doc">The Lucene Document to map.</param>
    /// <returns>The mapped SearchIndexEntry.</returns>
    private SearchIndexEntry MapToSearchIndexEntry(Document doc)
    {
        var searchableData = this.FieldNames.ToDictionary(x => x, doc.Get);

        return new SearchIndexEntry
        {
            Key = doc.Get(KeyFieldName),
            DocType = doc.Get(DocTypeFieldName),
            Title = doc.Get(TitleFieldName),
            Link = doc.Get(LinkFieldName),
            SearchableData = searchableData,
        };
    }

    /// <summary>
    /// Maps a SearchIndexEntry to a Lucene Document.
    /// </summary>
    /// <param name="searchIndexEntry">The SearchIndexEntry to map.</param>
    /// <returns>The mapped Lucene Document.</returns>
    private Document MapToLuceneDocument(SearchIndexEntry searchIndexEntry)
    {
        var doc = new Document
        {
            // Metadata (string fields are case-sensitive, and must match whole strings).
            new StringField(KeyFieldName, searchIndexEntry.Key, Field.Store.YES),
            new StringField(DocTypeFieldName, searchIndexEntry.DocType, Field.Store.YES),
            new StringField(TitleFieldName, searchIndexEntry.Title, Field.Store.YES),
            new StringField(LinkFieldName, searchIndexEntry.Link, Field.Store.YES),
        };

        // Searchable fields (text fields are case-insensitive and fully searchable).
        foreach (var fieldName in this.FieldNames)
        {
            doc.Add(new TextField(fieldName, searchIndexEntry.SearchableData[fieldName], Field.Store.YES));
        }

        return doc;
    }
}
