﻿// <copyright file="SearchModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.Infrastructure;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Search.API.Controllers;
using WyattSoftware.Modules.Search.BLL.Factories;
using WyattSoftware.Modules.Search.BLL.Singletons;
using WyattSoftware.Modules.Search.DAL.Factories;
using WyattSoftware.Modules.Search.Options;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Search module.
/// </summary>
public static class SearchModule
{
    /// <summary>
    /// The name of the site-wide search index. Used when accessing <c>SearchEngine[]</c>.
    /// </summary>
    public const string SiteSearchIndexName = "site";

    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddSearchModule(this IServiceCollection services, IConfiguration configuration)
    {
        var searchSection = configuration.GetSection("WyattSoftware:Modules:Search");

        // Make WyattSoftware.Modules.Search appsettings.json options available via dependency injection.
        services.Configure<SearchOptions>(searchSection);

        // Note, we use factories because the SearchIndex and SearchService implementations may have constructors
        // that take more than just dependencies.

        // DAL
        services.AddScoped(typeof(ISearchIndexFactory), typeof(LuceneSearchIndexFactory));

        // BLL
        services.AddSingleton(typeof(ISearchEngine), typeof(SearchEngine));
        services.AddScoped(typeof(ISearchDocFactory), typeof(SearchDocFactory));
        services.AddScoped(typeof(ISearchServiceFactory), typeof(SearchServiceFactory));

        return services;
    }

    /// <summary>
    /// Configures the Search module.
    /// </summary>
    /// <param name="app">This app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseSearchModule(this IApplicationBuilder app)
    {
        // BLL
        using var scope = app.ApplicationServices.CreateScope();
        var searchServiceFactory = scope.ServiceProvider.GetService<ISearchServiceFactory>();
        if (searchServiceFactory == null)
        {
            throw new Exception("Couldn't get an ISearchServiceFactory.");
        }

        var searchEngine = scope.ServiceProvider.GetService<ISearchEngine>();
        if (searchEngine == null)
        {
            throw new Exception("Couldn't get an ISearchEngine.");
        }

        // Create the site-wide search service.
        searchEngine.AddSearchService(searchServiceFactory.New(
            SiteSearchIndexName,
            new List<string> { "title", "content" }));

        // API
        SearchController.InitMapping();

        return app;
    }
}
