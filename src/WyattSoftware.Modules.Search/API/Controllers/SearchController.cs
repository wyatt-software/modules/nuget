﻿// <copyright file="SearchController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.API.Controllers;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.API.DTOs;
using WyattSoftware.Modules.Core.API.Hubs;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Search.API.DTOs;
using WyattSoftware.Modules.Search.BLL.Models;
using WyattSoftware.Modules.Search.BLL.Singletons;

/// <summary>
/// Handles site-wide searches.
/// </summary>
[Route("search")]
public class SearchController : WsControllerBase
{
    private readonly IHubContext<CoreHub> coreHubContext;
    private readonly ISearchEngine searchEngine;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchController"/> class.
    /// </summary>
    /// <param name="coreHubContext">Injected IHubContext{CoreHub}.</param>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    public SearchController(
        IHubContext<CoreHub> coreHubContext,
        ISearchEngine searchEngine)
    {
        this.coreHubContext = coreHubContext;
        this.searchEngine = searchEngine;
    }

    /// <summary>
    /// Performs a site-wide search.
    /// </summary>
    /// <param name="searchIndexName">The code name of the search index to search.</param>
    /// <param name="q">The search phrase.</param>
    /// <param name="type">The type of document to limit the search to. Blank = all.</param>
    /// <param name="offset">Offset the results (used for pagination).</param>
    /// <param name="limit">Optionally limit the number of results.</param>
    /// <param name="fragmentSize">The max number of characters to return for each searchable field.</param>
    /// <returns>A collection of SiteSearchResults.</returns>
    [HttpHead]
    [HttpGet]
    [Route("{searchIndexName}")]
    public IActionResult Search(
        string searchIndexName, string q, string type = "", int offset = 0, int limit = 20, int fragmentSize = 500)
    {
        if (!this.searchEngine.ContainsIndex(searchIndexName))
        {
            return this.BadRequest(new GeneralResponse($"Search index \"{searchIndexName}\" not found."));
        }

        var searchService = this.searchEngine[searchIndexName];
        var searchDocs = searchService.Search(q, type, offset, limit, fragmentSize);

        var response = Mapper.Map<PaginatedResponse<SiteSearchResponse>>(searchDocs);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Clears and rebuilds the entire search index.
    /// </summary>
    /// <param name="searchIndexName">The code name of the search index to search.</param>
    /// <param name="request">The request object.</param>
    /// <param name="ct">A cancellation token for cancelling a long-running process.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{searchIndexName}/actions/rebuild")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Rebuild(
        string searchIndexName, [FromBody] SiteSearchRebuildRequest request, CancellationToken ct)
    {
        if (!this.searchEngine.ContainsIndex(searchIndexName))
        {
            return this.BadRequest(new GeneralResponse($"Search index \"{searchIndexName}\" not found."));
        }

        var searchService = this.searchEngine[searchIndexName];
        var count = await searchService.RebuildAsync(
            async (message, percentage) =>
            {
                if (request?.ConnectionId == null)
                {
                    return;
                }

                var data = new { message, percentage };
                await this.coreHubContext.Clients.Client(request.ConnectionId).SendAsync(
                    "progress", data, ct);
            },
            ct);

        var entries = count == 1 ? "entry" : "entries";
        var response = new GeneralResponse($"{count} {entries} rebuilt.");
        return this.Ok(response);
    }

    /// <summary>
    /// Maps the SearchDoc to the SiteSearchResult object.
    /// </summary>
    internal static void InitMapping()
    {
        Mapper.AddMap<SearchDoc, SiteSearchResponse>(searchDoc =>
        {
            // If the title wasn't a searchable field, use the document title for the search results.
            var title = !string.IsNullOrEmpty(searchDoc.SearchableData["title"])
                ? searchDoc.SearchableData["title"]
                : searchDoc.Title;

            return new SiteSearchResponse
            {
                Key = searchDoc.Key,
                DocType = searchDoc.DocType,
                Link = searchDoc.Link,
                Title = title,
                Content = searchDoc.SearchableData["content"],
            };
        });

        Mapper.AddMap<PaginatedResponse<SearchDoc>, PaginatedResponse<SiteSearchResponse>>(searchDocs =>
            new PaginatedResponse<SiteSearchResponse>()
            {
                Total = searchDocs.Total,
                Offset = searchDocs.Offset,
                Limit = searchDocs.Limit,
                Results = searchDocs.Results.Select(x => Mapper.Map<SiteSearchResponse>(x)).ToList(),
            });
    }
}
