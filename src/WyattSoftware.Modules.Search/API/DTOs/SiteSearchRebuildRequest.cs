﻿// <copyright file="SiteSearchRebuildRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.API.DTOs;

/// <summary>
/// Request object rebuild request.
/// </summary>
public class SiteSearchRebuildRequest
{
    /// <summary>
    /// Gets or sets the SignalR connection ID.
    /// </summary>
    public string ConnectionId { get; set; } = string.Empty;
}
