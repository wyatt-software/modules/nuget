﻿// <copyright file="SiteSearchResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.API.DTOs;

/// <summary>
/// Search result document from the search index.
/// </summary>
public class SiteSearchResponse
{
    /// <summary>
    /// Gets or sets a unique key to identify the object this search result references.
    /// </summary>
    public string Key { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the document type.
    /// </summary>
    public string DocType { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the link.
    /// </summary>
    public string Link { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the title.
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the content.
    /// </summary>
    public string Content { get; set; } = string.Empty;
}
