﻿// <copyright file="SearchOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Search section.
/// </summary>
public class SearchOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets the location of the search index files.
    /// </para>
    /// <para>
    /// Eg: <c>"D:\\wyatt-software\\example\\server\\temp\\search-indexes"</c> or
    /// <c>"/home/ec2-user/example/server/temp/search-indexes"</c>.
    /// </para>
    /// </summary>
    public string SearchIndexesPath { get; set; } = string.Empty;
}
