﻿// <copyright file="SearchService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Core.BLL.Helpers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Search.BLL.Factories;
using WyattSoftware.Modules.Search.BLL.Models;
using WyattSoftware.Modules.Search.DAL.Factories;
using WyattSoftware.Modules.Search.DAL.Services;

/// <summary>
/// Default implementation of the <see cref="ISearchService"/> interface.
/// </summary>
public class SearchService : ISearchService
{
    // The number of records to index per progress update.
    private const int BatchSize = 20;

    // Dependencies.
    private readonly IConfiguration configuration;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ISearchDocFactory searchDocFactory;

    // Backing field for lazy-loaded property.
    private IServiceScope? scope;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchService"/> class.
    /// </summary>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="searchIndexFactory">Injected ISearchIndexFactory.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="name">The code name of the search index.</param>
    /// <param name="fieldNames">The field names that are searchable.</param>
    internal SearchService(
        IConfiguration configuration,
        IServiceScopeFactory serviceScopeFactory,
        ISearchIndexFactory searchIndexFactory,
        ISearchDocFactory searchDocFactory,
        string name,
        List<string> fieldNames)
    {
        this.configuration = configuration;
        this.serviceScopeFactory = serviceScopeFactory;
        this.searchDocFactory = searchDocFactory;
        this.Name = name;

        this.SearchIndex = searchIndexFactory.New(name, fieldNames);
    }

    /// <inheritdoc />
    public string Name { get; }

    /// <inheritdoc />
    public ISearchIndex SearchIndex { get; }

    /// <summary>
    /// Gets a lazy-loaded scope for dynamically creating domain services.
    /// </summary>
    private IServiceScope Scope => this.scope ??= this.serviceScopeFactory.CreateScope();

    /// <inheritdoc />
    public PaginatedResponse<SearchDoc> Search(
        string phrase,
        string docType = "",
        int offset = 0,
        int limit = 20,
        int fragmentSize = 500)
    {
        var total = this.SearchIndex.GetCount(phrase, docType);
        var results = this.SearchIndex
            .Fetch(phrase, docType, offset, limit, fragmentSize)
            .Select(this.searchDocFactory.New)
            .ToList();

        return new PaginatedResponse<SearchDoc>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };
    }

    /// <inheritdoc />
    public async Task<int> RebuildAsync(Action<string, decimal> updateProgress, CancellationToken ct)
    {
        var totalProcessed = 0;
        this.SearchIndex.DeleteAll();

        var modelTypeInfos = await this.GetModelTypeInfosAsync();
        var totalCount = modelTypeInfos.Sum(x => x.Count);

        foreach (var modelTypeInfo in modelTypeInfos)
        {
            var processed = 0;
            while (processed < modelTypeInfo.Count)
            {
                // We update progress first, so that the message reflects what we're currently working on.
                var message =
                    $"{modelTypeInfo.ModelType.Name} {processed}/{modelTypeInfo.Count} " +
                    $"({totalProcessed}/{totalCount} total).";

                var percentage = 100M * totalProcessed / totalCount;
                updateProgress(message, percentage);

                // Process each model in the batch.
                var batchSize = Math.Min(modelTypeInfo.Count - processed, BatchSize);
                var models = await modelTypeInfo.Service.FetchAsync(processed, batchSize);
                foreach (var model in models.Results)
                {
                    ct.ThrowIfCancellationRequested();
                    var searchDoc = await model.MapToSearchDocAsync();
                    this.SearchIndex.Create(searchDoc);
                    processed++;
                    totalProcessed++;
                }
            }
        }

        updateProgress("Complete.", 100);

        return totalProcessed;
    }

    /// <summary>
    /// Gets any types in the application that implement IIndexableModel.
    /// </summary>
    /// <returns>A List of types.</returns>
    private static List<Type> GetIndexableModelTypes()
    {
        return ReflectionHelper.GetAllImplementationsOf(typeof(IIndexableModel));
    }

    /// <summary>
    /// Gets a list of model types with their service and count.
    /// </summary>
    /// <returns>A list of model type infos.</returns>
    private async Task<List<ModelTypeInfo>> GetModelTypeInfosAsync()
    {
        var modelTypes = GetIndexableModelTypes();
        var modelTypeInfos = new List<ModelTypeInfo>();
        foreach (var modelType in modelTypes)
        {
            var service = this.GetService(modelType);
            if (service == null)
            {
                continue;
            }

            var count = await service.GetCountAsync();
            var modelTypeInfo = new ModelTypeInfo(modelType, service, count);

            modelTypeInfos.Add(modelTypeInfo);
        }

        return modelTypeInfos;
    }

    /// <summary>
    /// Dynamically gets the IDomainService for the given IIndexableModel.
    /// </summary>
    /// <param name="modelType">The type of model (must implement IIndexableDomainModel).</param>
    /// <returns>An IDomainService.</returns>
    private dynamic? GetService(Type modelType)
    {
        var dataType = new Type[] { modelType };
        var genericBase = typeof(IDomainService<>);
        var serviceType = genericBase.MakeGenericType(dataType);

        var service = this.Scope.ServiceProvider.GetService(serviceType);
        return service;
    }
}
