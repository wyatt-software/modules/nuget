﻿// <copyright file="ISearchService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Services;

using System;
using System.Threading;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Search.BLL.Models;
using WyattSoftware.Modules.Search.DAL.Services;

/// <summary>
/// Business logic for the site-wide search.
/// </summary>
public interface ISearchService
{
    /// <summary>
    /// Gets the code name of the search service (and underlying index).
    /// </summary>
    string Name { get; }

    /// <summary>
    /// Gets the underlying Search Index.
    /// </summary>
    ISearchIndex SearchIndex { get; }

    /// <summary>
    /// Performs a site-wide search.
    /// </summary>
    /// <param name="phrase">The search phrase.</param>
    /// <param name="docType">The type of document to limit the search to. Blank = all.</param>
    /// <param name="offset">Offset the results (used for pagination).</param>
    /// <param name="limit">Optionally limit the number of results.</param>
    /// <param name="fragmentSize">The max number of characters to return for each searchable field.</param>
    /// <returns>A collection of SearchDocs.</returns>
    PaginatedResponse<SearchDoc> Search(
        string phrase,
        string docType = "",
        int offset = 0,
        int limit = 20,
        int fragmentSize = 200);

    /// <summary>
    /// Clears and rebuilds the search index.
    /// </summary>
    /// <param name="progress">An action to call upon each created index.</param>
    /// <param name="ct">A cancellation token for cancelling a long-running token.</param>
    /// <returns>The number of entries rebuilt.</returns>
    Task<int> RebuildAsync(Action<string, decimal> progress, CancellationToken ct);
}
