﻿// <copyright file="SearchEngine.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Singletons;

using System.Collections.Generic;
using WyattSoftware.Modules.Search.BLL.Services;

/// <summary>
/// The default implememntation of <see cref="ISearchEngine"/>.
/// </summary>
public class SearchEngine : ISearchEngine
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SearchEngine"/> class.
    /// </summary>
    public SearchEngine()
    {
        this.SearchServices = new Dictionary<string, ISearchService>();
    }

    /// <summary>
    /// Gets the collection of registered search indexes.
    /// </summary>
    private Dictionary<string, ISearchService> SearchServices { get; }

    /// <inheritdoc/>
    public ISearchService this[string name] => this.SearchServices[name];

    /// <inheritdoc/>
    public bool ContainsIndex(string name)
    {
        return this.SearchServices.ContainsKey(name);
    }

    /// <inheritdoc/>
    public ISearchService AddSearchService(ISearchService searchIndex)
    {
        this.SearchServices.Add(searchIndex.Name, searchIndex);
        return searchIndex;
    }
}
