﻿// <copyright file="ISearchEngine.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Singletons;

using WyattSoftware.Modules.Search.BLL.Services;

/// <summary>
/// The search engine is basically just a collection of search services.
/// </summary>
/// <remarks>It should be configured as a singleton.</remarks>
public interface ISearchEngine
{
    /// <summary>
    /// Access the registered search indexes by name.
    /// </summary>
    /// <param name="name">The name of the search index.</param>
    /// <returns>The search index.</returns>
    ISearchService this[string name] { get; }

    /// <summary>
    /// Determine if a search index exists.
    /// </summary>
    /// <param name="name">The name of the search index.</param>
    /// <returns><c>true</c> if the index exists.</returns>
    bool ContainsIndex(string name);

    /// <summary>
    /// Adds a search service to the engine and initializes it.
    /// </summary>
    /// <param name="searchIndex">The index to register.</param>
    /// <returns>The registered search index.</returns>
    ISearchService AddSearchService(ISearchService searchIndex);
}
