﻿// <copyright file="ModelTypeInfo.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Store info relating to search index rebuilds.
/// </summary>
internal class ModelTypeInfo
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ModelTypeInfo"/> class.
    /// </summary>
    /// <param name="modelType">The IDomainModel type.</param>
    /// <param name="service">The IDomainService that will have been dynamically generated at run-time.</param>
    /// <param name="count">The total number of records to be indexed.</param>
    public ModelTypeInfo(Type modelType, dynamic service, int count)
    {
        this.ModelType = modelType;
        this.Service = service;
        this.Count = count;
    }

    /// <summary>
    /// Gets the IDomainModel type.
    /// </summary>
    public Type ModelType { get; }

    /// <summary>
    /// Gets the IDomainService that will have been dynamically generated at run-time.
    /// </summary>
    public dynamic Service { get; }

    /// <summary>
    /// Gets the total number of records to be indexed.
    /// </summary>
    public int Count { get; }
}
