﻿// <copyright file="SearchDoc.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Models;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Search.BLL.Singletons;
using WyattSoftware.Modules.Search.DAL.Entities;
using WyattSoftware.Modules.Search.DAL.Services;
using WyattSoftware.Modules.Search.Infrastructure;

/// <summary>
/// Search result document from the search index.
/// </summary>
public class SearchDoc : SearchIndexEntry, IDomainModel
{
    private readonly ISearchIndex searchIndex;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchDoc"/> class.
    /// </summary>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    /// <param name="key">a unique key to identify the object this search document references.</param>
    /// <param name="docType">The type of document. Used when we want to only search pages, or posts etc.</param>
    /// <param name="title">
    /// The document title to use in search results (if there's no title available in the searchable data).
    /// </param>
    /// <param name="link">A relative URL to use in search results.</param>
    /// <param name="searchableData">A dictionary of fieldName/value pairs.</param>
    internal SearchDoc(
        ISearchEngine searchEngine,
        string key,
        string docType,
        string title,
        string link,
        Dictionary<string, string> searchableData)
    {
        // TODO: Determine which search indexes to use from data attributes?
        this.searchIndex = searchEngine[SearchModule.SiteSearchIndexName].SearchIndex;

        this.Key = key;
        this.DocType = docType;
        this.Title = title;
        this.Link = link;
        this.SearchableData = searchableData;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchDoc"/> class.
    /// </summary>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    /// <param name="searchIndexEntry">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal SearchDoc(
        ISearchEngine searchEngine,
        SearchIndexEntry searchIndexEntry)
    {
        // TODO: Determine which search indexes to use from data attributes?
        this.searchIndex = searchEngine[SearchModule.SiteSearchIndexName].SearchIndex;

        this.Key = searchIndexEntry.Key;
        this.DocType = searchIndexEntry.DocType;
        this.Title = searchIndexEntry.Title;
        this.Link = searchIndexEntry.Link;
        this.SearchableData = searchIndexEntry.SearchableData;
    }

    /// <inheritdoc />
    public Task CreateAsync()
    {
        this.searchIndex.Create(this);
        return Task.FromResult(0);
    }

    /// <inheritdoc />
    public Task UpdateAsync()
    {
        this.searchIndex.Update(this);
        return Task.FromResult(0);
    }

    /// <inheritdoc />
    public Task DeleteAsync()
    {
        this.searchIndex.Delete(this.Key);
        return Task.FromResult(0);
    }
}
