﻿// <copyright file="IIndexableModel.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Defines functionality that searchable/indexable domain models need.
/// </summary>
public interface IIndexableModel
{
    /// <summary>
    /// Maps this model to a search doc.
    /// </summary>
    /// <returns>A SearchDoc.</returns>
    Task<SearchDoc> MapToSearchDocAsync();
}
