﻿// <copyright file="ISearchDocFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Factories;

using WyattSoftware.Modules.Search.BLL.Models;
using WyattSoftware.Modules.Search.DAL.Entities;

/// <summary>
/// Provides a way to index and search text data.
/// </summary>
public interface ISearchDocFactory
{
    /// <summary>
    /// Creates a new SearchDoc, and injects it with the dependencies it needs.
    /// </summary>
    /// <param name="searchIndexEntry">The search index entry to map.</param>
    /// <returns>A new SearchDoc.</returns>
    SearchDoc New(SearchIndexEntry searchIndexEntry);

    /// <summary>
    /// Creates a new SearchDoc, and injects it with the dependencies it needs.
    /// </summary>
    /// <param name="key">a unique key to identify the object this search document references.</param>
    /// <param name="docType">The type of document. Used when we want to only search pages, or posts etc.</param>
    /// <param name="title">
    /// The document title to use in search results (if there's no title available in the searchable data).
    /// </param>
    /// <param name="link">A relative URL to use in search results.</param>
    /// <param name="searchableData">A dictionary of fieldName/value pairs.</param>
    /// <returns>A new SearchDoc.</returns>
    SearchDoc New(
        string key,
        string docType,
        string title,
        string link,
        Dictionary<string, string> searchableData);
}
