﻿// <copyright file="SearchServiceFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Factories;

using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Search.BLL.Services;
using WyattSoftware.Modules.Search.DAL.Factories;

/// <summary>
/// Lucene implementation of <see cref="ISearchServiceFactory"/>.
/// </summary>
public class SearchServiceFactory : ISearchServiceFactory
{
    private readonly IConfiguration configuration;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ISearchIndexFactory searchIndexFactory;
    private readonly ISearchDocFactory searchDocFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchServiceFactory"/> class.
    /// </summary>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="searchIndexFactory">Injected ISearchIndexFactory.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    public SearchServiceFactory(
        IConfiguration configuration,
        IServiceScopeFactory serviceScopeFactory,
        ISearchIndexFactory searchIndexFactory,
        ISearchDocFactory searchDocFactory)
    {
        this.configuration = configuration;
        this.serviceScopeFactory = serviceScopeFactory;
        this.searchIndexFactory = searchIndexFactory;
        this.searchDocFactory = searchDocFactory;
    }

    /// <inheritdoc />
    public ISearchService New(string name, List<string> fieldNames)
    {
        return new SearchService(
            this.configuration,
            this.serviceScopeFactory,
            this.searchIndexFactory,
            this.searchDocFactory,
            name,
            fieldNames);
    }
}
