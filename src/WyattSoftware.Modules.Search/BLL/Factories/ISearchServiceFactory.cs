﻿// <copyright file="ISearchServiceFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Factories;

using System.Collections.Generic;
using WyattSoftware.Modules.Search.BLL.Services;

/// <summary>
/// Lucene implementation of <see cref="ISearchServiceFactory"/>.
/// </summary>
public interface ISearchServiceFactory
{
    /// <summary>
    /// Creates a new SearchService, and injects it with the dependencies it needs.
    /// </summary>
    /// <param name="name">The name of the search service and underlying index.</param>
    /// <param name="fieldNames">The names of custom searchable fields.</param>
    /// <returns>A new ISearchIndex.</returns>
    ISearchService New(string name, List<string> fieldNames);
}
