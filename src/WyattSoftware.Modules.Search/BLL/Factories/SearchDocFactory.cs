﻿// <copyright file="SearchDocFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Search.BLL.Factories;

using System.Collections.Generic;
using WyattSoftware.Modules.Search.BLL.Models;
using WyattSoftware.Modules.Search.BLL.Singletons;
using WyattSoftware.Modules.Search.DAL.Entities;

/// <summary>
/// Lucene implementation of <see cref="ISearchIndexFactory"/>.
/// </summary>
public class SearchDocFactory : ISearchDocFactory
{
    private readonly ISearchEngine searchEngine;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchDocFactory"/> class.
    /// </summary>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    public SearchDocFactory(
        ISearchEngine searchEngine)
    {
        this.searchEngine = searchEngine;
    }

    /// <inheritdoc/>
    public SearchDoc New(SearchIndexEntry searchIndexEntry)
    {
        return new SearchDoc(this.searchEngine, searchIndexEntry);
    }

    /// <inheritdoc/>
    public SearchDoc New(
        string key, string docType, string title, string link, Dictionary<string, string> searchableData)
    {
        return new SearchDoc(this.searchEngine, key, docType, title, link, searchableData);
    }
}
