﻿// <copyright file="PageServiceTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Services
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Content.BLL.Services;
    using WyattSoftware.Modules.Content.DAL.Entities;

    /// <summary>
    /// Unit tests for the page service.
    /// </summary>
    internal class PageServiceTests
    {
        private List<PageEntity> pageEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="PageServiceTests"/> class.
        /// </summary>
        public PageServiceTests()
        {
            this.pageEntities = new List<PageEntity>();
        }

        /// <summary>
        /// Creates valid data.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.pageEntities = new List<PageEntity>()
            {
                new PageEntity()
                {
                    Id = 1,
                    Title = "Home",
                    SortOrder = 1,
                },
                new PageEntity()
                {
                    Id = 2,
                    Title = "About",
                    SortOrder = 2,
                },
                new PageEntity()
                {
                    Id = 3,
                    Title = "Contact",
                    SortOrder = 3,
                },
            };
        }

        /// <summary>
        /// Tests a newly created page.
        /// </summary>
        [Test]
        public void GetUnsortedPages_ShouldDoNothing_WhenGivenValidPages()
        {
            var modifiedPageEntities = PageService.GetUnsortedPages(this.pageEntities);

            modifiedPageEntities.Count.ShouldBe(0);
        }

        /// <summary>
        /// Tests a newly created page.
        /// </summary>
        [Test]
        public void GetUnsortedPages_ShouldModifySortOrder_WhenAddingAPage()
        {
            this.pageEntities.Add(new PageEntity()
            {
                Id = 4,
                Title = "Forum",
            });

            var modifiedPageEntities = PageService.GetUnsortedPages(this.pageEntities);

            modifiedPageEntities.Count.ShouldBe(1);
            modifiedPageEntities[0].Id.ShouldBe(4);
            modifiedPageEntities[0].SortOrder.ShouldBe(4);
        }

        /// <summary>
        /// Tests a newly deleted page.
        /// </summary>
        [Test]
        public void GetUnsortedPages_ShouldModifySortOrder_WhenDeletingAPageFromTheMiddle()
        {
            this.pageEntities.RemoveAt(1);

            var modifiedPageEntities = PageService.GetUnsortedPages(this.pageEntities);

            modifiedPageEntities.Count.ShouldBe(1);
            modifiedPageEntities[0].Id.ShouldBe(3);
            modifiedPageEntities[0].SortOrder.ShouldBe(2);
        }

        /// <summary>
        /// Tests a newly deleted page.
        /// </summary>
        [Test]
        public void GetUnsortedPages_ShouldModifySortOrder_WhenDeletingAPageFromTheStart()
        {
            this.pageEntities.RemoveAt(0);

            var modifiedPageEntities = PageService.GetUnsortedPages(this.pageEntities);

            modifiedPageEntities.Count.ShouldBe(2);
            modifiedPageEntities[0].Id.ShouldBe(2);
            modifiedPageEntities[0].SortOrder.ShouldBe(1);
            modifiedPageEntities[1].Id.ShouldBe(3);
            modifiedPageEntities[1].SortOrder.ShouldBe(2);
        }
    }
}
