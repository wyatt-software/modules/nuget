﻿// <copyright file="UserRoleRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Repositories;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Dapper implementation of the account user / security role binding class repository.
/// </summary>
public class UserRoleRepository : BaseRepository<UserRoleEntity>, IUserRoleRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UserRoleRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public UserRoleRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task<Role[]> FetchAsync(int userId)
    {
        var query = this.Db.Query(this.Table).Select("role").Where("identity_user_id", userId);
        var results = await query.GetAsync<int>();
        return results.Select(x => (Role)x).ToArray();
    }
}
