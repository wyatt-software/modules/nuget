﻿// <copyright file="IUserRoleRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Repositories;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Interface defining custom methods in the account user / security role binding class repository.
/// </summary>
public interface IUserRoleRepository : IRepository<UserRoleEntity>
{
    /// <summary>
    /// Fetch all roles this user has.
    /// </summary>
    /// <param name="userId">The user to fetch roles for.</param>
    /// <returns>An array of roles.</returns>
    Task<Role[]> FetchAsync(int userId);
}
