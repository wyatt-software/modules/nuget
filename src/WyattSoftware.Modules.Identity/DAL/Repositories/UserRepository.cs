﻿// <copyright file="UserRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Dapper implementation of the account user repository.
/// </summary>
public class UserRepository : BaseRepository<UserEntity>, IUserRepository
{
    private readonly IUserAddOn userAddOn;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    public UserRepository(
        IOptions<CoreOptions> coreOptions,
        IUserAddOn userAddOn)
        : base(coreOptions)
    {
        this.userAddOn = userAddOn;
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(string search = "")
    {
        var query = this.Db
            .Query(this.Table + " AS IU")
            .AsCount();

        query = FilterUsers(query, search);

        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<UserEntity>> FetchAsync(
        string search = "", int offset = 0, int limit = 20, string sort = "-lastloggedon")
    {
        var query =
            this.QueryWithDerivedFields()
            .Offset(offset)
            .Limit(limit)
            .OrderByRaw(this.OrderByColumn(sort));

        query = FilterUsers(query, search);

        var results = await this.Db.GetAsync<UserEntity>(query);
        return results.ToList();
    }

    /// <inheritdoc/>
    /// <remarks>
    /// We're replacing the base methox so that we can get the derived fields.
    /// </remarks>
    public new async Task<List<UserEntity>> FetchAsync(int[] ids)
    {
        var query =
            this.QueryWithDerivedFields()
            .WhereIn(GetIdentityColumn<int>(), ids);

        var results = await this.Db.GetAsync<UserEntity>(query);
        return results.ToList();
    }

    /// <inheritdoc/>
    public new async Task<UserEntity?> ReadAsync(string usernameOrEmail, bool usernameIsCaseSensitive = true)
    {
        var userEntity = await
            this.QueryWithDerivedFields()
            .Where("username", usernameOrEmail)
            .OrWhere("email", usernameOrEmail)
            .FirstOrDefaultAsync<UserEntity>();

        if (userEntity == null)
        {
            return null;
        }

        var matchedOnUsernameExactly = userEntity.Username.Equals(usernameOrEmail);
        var matchedOnUsernameIgnoreCase =
            userEntity.Username.Equals(usernameOrEmail, StringComparison.InvariantCultureIgnoreCase);

        if (matchedOnUsernameIgnoreCase && usernameIsCaseSensitive && !matchedOnUsernameExactly)
        {
            return null;
        }

        return userEntity;
    }

    /// <inheritdoc/>
    public async Task<bool> EmailIsUniqueAsync(string value, int userId)
    {
        var query = this.Db
            .Query(this.Table + " AS IU")
            .Where("email", value)
            .WhereNot("id", userId)
            .AsCount();

        var count = await this.Db.ExecuteScalarAsync<int>(query);

        return count == 0;
    }

    /// <inheritdoc/>
    public async Task<bool> UsernameIsUniqueAsync(string value, int userId)
    {
        var count = await this.Db
            .Query(this.Table + " AS IU")
            .AsCount()
            .Where("username", value)
            .WhereNot("id", userId)
            .FirstOrDefaultAsync<int>();

        return count == 0;
    }

    /// <inheritdoc/>
    public async Task UpdateLastLoggedOnAsync(int userId, DateTime? timestamp)
    {
        timestamp ??= DateTimeOffset.UtcNow.UtcDateTime;

        var query = this.Db
            .Query(this.Table + " AS UI")
            .Where("id", userId)
            .AsUpdate(new
            {
                modified = DateTimeOffset.UtcNow.UtcDateTime,
                last_logged_on = timestamp,
            });

        await this.Db.ExecuteAsync(query);
    }

    /// <summary>
    /// Adds clauses to the query, based on the search terms.
    /// </summary>
    /// <param name="query">The query to modify and return.</param>
    /// <param name="search">Searches users by username.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterUsers(Query query, string search = "")
    {
        return query
            .Join("identity_user_role AS IUR", "IUR.identity_user_id", "IU.id")
            .Where("IUR.role", (int)Role.Member)
            .When(
                !string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search),
                q => q.WhereLike("IU.username", $"%{search}%"));
    }

    /// <summary>
    /// Get the SQL table's ORDER BY column, based on the model's property name.
    /// </summary>
    /// <param name="sort">The field name (from the model, not the DB) prefixed with a '-' if descending.</param>
    /// <returns>The OrderBy clause.</returns>
    private string OrderByColumn(string sort)
    {
        sort = (sort ?? string.Empty).ToLower();

        if (string.IsNullOrEmpty(sort))
        {
            sort = "-lastloggedon";
        }

        bool desc;
        if (sort[..1] == "-")
        {
            desc = true;
            sort = sort[1..];
        }
        else
        {
            desc = false;
        }

        if (!this.userAddOn.SortClauses.ContainsKey(sort))
        {
            sort = "lastloggedon";
        }

        return this.userAddOn.SortClauses[sort] + (desc ? " DESC" : string.Empty);
    }

    /// <summary>
    /// Gets the SELECT FROM clause with additional/derived fields.
    /// </summary>
    /// <returns>A query builder object.</returns>
    private Query QueryWithDerivedFields()
    {
        var jsonObjectParameters =
            string.Join(", ", this.userAddOn.SelectClauses.Select(x => $"'{x.Key}', ({x.Value})"));

        // We're injecting un-escaped SQL here, which is usually a huge no-no, but since it's not coming from user
        // input, it can be considered safe.
        return this.Db.Query(this.Table + " AS IU")
            .SelectRaw($"*, JSON_OBJECT({jsonObjectParameters}) AS derived__properties");
    }
}
