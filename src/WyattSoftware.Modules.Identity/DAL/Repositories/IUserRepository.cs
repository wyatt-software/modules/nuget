﻿// <copyright file="IUserRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Identity.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the account user repository.
/// </summary>
public interface IUserRepository : IRepository<UserEntity>
{
    /// <summary>
    /// Gets the number of users for a given search.
    /// </summary>
    /// <param name="search">Searches by part of username.</param>
    /// <returns>The number of users.</returns>
    Task<int> GetCountAsync(string search = "");

    /// <summary>
    /// Get all users who's usernames match the given search string.
    /// </summary>
    /// <param name="search">Searches by part of username.</param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <param name="sort">Sorts results by a given field (with a '-' prefix when descending).</param>
    /// <returns>A collection of users.</returns>
    Task<List<UserEntity>> FetchAsync(
        string search = "", int offset = 0, int limit = 20, string sort = "-LastLoggedOn");

    /// <summary>
    /// Checks if an email address is free or taken by another user.
    /// </summary>
    /// <param name="value">The email to check.</param>
    /// <param name="userId">
    /// The user checking, to avoid false positives on their own email address.
    /// </param>
    /// <returns>A value indicating whether the email address is free.</returns>
    Task<bool> EmailIsUniqueAsync(string value, int userId);

    /// <summary>
    /// Checks if a username is free or taken by another user.
    /// </summary>
    /// <param name="value">The username to check.</param>
    /// <param name="userId">The user checking, to avoid false positives on their own username.</param>
    /// <returns>A value indicating whether the username is free.</returns>
    Task<bool> UsernameIsUniqueAsync(string value, int userId);

    /// <summary>
    /// Updates the last logged on timestamp.
    /// </summary>
    /// <param name="userId">The primary key of the user to update.</param>
    /// <param name="timestamp">Timestamp of last login. Default to now.</param>
    /// <returns>Nothing.</returns>
    Task UpdateLastLoggedOnAsync(int userId, DateTime? timestamp);
}
