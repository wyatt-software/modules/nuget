﻿// <copyright file="UserEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Entities;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json.Linq;
using WyattSoftware.Modules.Core.DAL.Attributes;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Account user entity.
/// </summary>
[Table("identity_user")]
public class UserEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Gets or sets the date the user last logged on.
    /// </summary>
    [Column("last_logged_on")]
    public DateTime? LastLoggedOn { get; set; }

    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    [Key]
    [Column("username")]
    [Required]
    [StringLength(50)]
    [RegularExpression(
        @"^[0-9A-Za-z-]+$",
        ErrorMessage = "Only letters, numbers, and the dash character are allowed.")]
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the hash of the user's password to compare to (instead of storing the actual password).
    /// </summary>
    [Column("password_hash")]
    [Required]
    [StringLength(255)]
    public string PasswordHash { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the method to use when hashing a password.
    /// </summary>
    [Column("password_format")]
    [Required]
    [EnumDataType(typeof(UserPasswordFormat))]
    public UserPasswordFormat PasswordFormat { get; set; } = UserPasswordFormat.SHA256;

    /// <summary>
    /// Gets or sets the code sent to the user when they want to reset their password.
    /// </summary>
    [Column("password_reset_code")]
    public string PasswordResetCode { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the date the password reset code was created.
    /// </summary>
    /// <remarks>This is used for expiration.</remarks>
    [Column("password_reset_code_created")]
    public DateTime? PasswordResetCodeCreated { get; set; }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    [Column("email")]
    [Required]
    [StringLength(255)]
    [EmailAddress]
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the email was verified.
    /// </summary>
    [Column("email_verified")]
    public bool EmailVerified { get; set; }

    /// <summary>
    /// Gets or sets the code sent to the user when they join to prove that they have access to their email address.
    /// </summary>
    [Column("email_verification_code")]
    public string EmailVerificationCode { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the date the verification code was created.
    /// </summary>
    /// <remarks>This is used for expiration.</remarks>
    [Column("email_verification_code_created")]
    public DateTime? EmailVerificationCodeCreated { get; set; }

    /// <summary>
    /// Gets or sets public custom properties.
    /// </summary>
    /// <remarks>
    /// <para>We do this because we don't know what modules will be included. Eg.</para>
    /// <list type="bullet">
    /// <item>If we don't include the Gallery module we won't need an ImageId, and</item>
    /// <item>If we don't include the Forum moddule we won't need the Signature or Location fields.</item>
    /// </list>
    /// <para>
    /// Adhering to the "Third normal form" database design approach, the User entity is still the best place to
    /// store those things. Luckily, MySQL supports JSON fields, so this is a good compromise.
    /// </para>
    /// </remarks>
    [Column("properties")]
    public JObject Properties { get; set; } = new JObject();

    /// <summary>
    /// Gets or sets private custom properties.
    /// </summary>
    [Column("preferences")]
    public JObject Preferences { get; set; } = new JObject();

    /// <summary>
    /// Gets or sets derived properties, added by the other modules.
    /// </summary>
    [Derived]
    [Column("derived__properties")]
    public JObject DerivedProperties { get; set; } = new JObject();
}
