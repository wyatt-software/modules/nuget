﻿// <copyright file="UserRoleEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Account user/role binding entity.
/// </summary>
[Table("identity_user_role")]
public class UserRoleEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Gets or sets the foreign key to the user.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets the role enum.
    /// </summary>
    [Column("role")]
    [Required]
    [EnumDataType(typeof(Role))]
    public Role Role { get; set; }
}
