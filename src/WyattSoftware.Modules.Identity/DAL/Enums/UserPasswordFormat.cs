﻿// <copyright file="UserPasswordFormat.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// The method to use when hashing a password.
/// </summary>
public enum UserPasswordFormat
{
    /// <summary>
    /// Plain text. Only used for debugging.
    /// </summary>
    PlainText = 1,

    /// <summary>
    /// MD5, used for passwords migrated from the old site.
    /// </summary>
    MD5 = 2,

    /// <summary>
    /// All new passwords should be hashed using this method.
    /// </summary>
    SHA256 = 3,
}
