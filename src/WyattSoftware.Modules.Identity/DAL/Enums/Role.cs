﻿// <copyright file="Role.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Authorization in this solution is entirely role-based. A user can have multiple roles, which are used to
/// grant and deny access to different API controllers and endpoints.
/// </summary>
/// <remarks>
/// <para>
/// String representations of these roles are used in the <c>[Authorize Roles="..."]</c> attributes in the
/// API controllers, and must match exactly.
/// </para>
/// </remarks>
public enum Role
{
    /// <summary>
    /// Email has been verified.
    /// </summary>
    Member = 1,

    /// <summary>
    /// Can maintain user content.
    /// </summary>
    Moderator = 2,

    /// <summary>
    /// Can manage site content and structure.
    /// </summary>
    Administrator = 3,
}
