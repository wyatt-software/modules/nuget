﻿// <copyright file="IdentityOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Identity section.
/// </summary>
public class IdentityOptions
{
    /// <summary>
    /// Gets or sets the URL of the authority that provides authentication.
    /// </summary>
    /// <remarks>
    /// The web app uses IdentityServer4 on the same server. This should match the standard `Urls` setting in
    /// appsettings.json.
    /// </remarks>
    public string Authority { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets email verification code options.
    /// </summary>
    public IdentityEmailVerificationCodeOptions EmailVerificationCode { get; set; } =
        new IdentityEmailVerificationCodeOptions();

    /// <summary>
    /// Gets or sets password options.
    /// </summary>
    public IdentityPasswordOptions Password { get; set; } = new IdentityPasswordOptions();
}
