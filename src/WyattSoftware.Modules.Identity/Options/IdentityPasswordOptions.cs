﻿// <copyright file="IdentityPasswordOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Identity.Password section.
/// </summary>
public class IdentityPasswordOptions
{
    /// <summary>
    /// Gets or sets the number of tries a user has before the account is locked.
    /// </summary>
    public int AllowedFailedAttempts { get; set; } = 3;

    /// <summary>
    /// Gets or sets the minimum length allowed for passwords.
    /// </summary>
    public int MinLength { get; set; } = 8;

    /// <summary>
    /// Gets or sets A string containing the valid chars to use in the code.
    /// </summary>
    public string ResetCodeChars { get; set; } = "0123456789";

    /// <summary>
    /// Gets or sets the number of characters to use when generating a code.
    /// </summary>
    public int ResetCodeLength { get; set; } = 6;

    /// <summary>
    /// Gets or sets the number of hours until the code can no longer be used.
    /// </summary>
    public int ResetCodeExpirationHours { get; set; } = 24;
}
