﻿// <copyright file="IdentityEmailVerificationCodeOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Identity.EmailVerificationCode section.
/// </summary>
public class IdentityEmailVerificationCodeOptions
{
    /// <summary>
    /// Gets or sets a string containing the valid chars to use in the code.
    /// </summary>
    public string Chars { get; set; } = "0123456789";

    /// <summary>
    /// Gets or sets the number of characters to use when generating a code.
    /// </summary>
    public int Length { get; set; } = 6;

    /// <summary>
    /// Gets or sets the number of hours until the code can no longer be used.
    /// </summary>
    public int ExpirationHours { get; set; } = 24;
}
