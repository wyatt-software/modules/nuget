﻿// <copyright file="IUserAddOn.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Infrastructure;

using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// The user add-on is basically just a collection of dictionaries, made available globally using the singleton
/// pattern. It allows other modules to extend the functionality of the identity user, without the identity module
/// having any dependency on those modules. It's done this way to keep the modules optional, and to avoid circular
/// dependencies.
/// </summary>
public interface IUserAddOn
{
    /// <summary>
    /// Gets the delegates used to map optional nested resources domain models to API response DTOs.
    /// </summary>
    Dictionary<string, Func<IDomainModel?, object?>> DtoMappers { get; }

    /// <summary>
    /// Gets the delegates used to expand the user's optional nested resources.
    /// </summary>
    Dictionary<string, Func<IServiceScopeFactory, User, Task<User>>> ModelExpanders { get; }

    /// <summary>
    /// Gets the delegates used to expand the lists of users.
    /// </summary>
    Dictionary<string, Func<IServiceScopeFactory, List<User>, Task<List<User>>>> ModelListExpanders { get; }

    /// <summary>
    /// Gets a dictionary of select clauses used to get additional properties.
    /// </summary>
    /// <remarks>The key will also be used as the field/property name.</remarks>
    Dictionary<string, string> SelectClauses { get; }

    /// <summary>
    /// Gets a dictionary mapping sort keys to column names (or raw SQL).
    /// </summary>
    /// <remarks>
    /// Can be added to by the other modules.
    /// </remarks>
    Dictionary<string, string> SortClauses { get; }
}
