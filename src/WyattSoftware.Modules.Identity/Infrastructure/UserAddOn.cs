﻿// <copyright file="UserAddOn.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Infrastructure;

using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Default implementation of <see cref="IUserAddOn"/>.
/// </summary>
public class UserAddOn : IUserAddOn
{
    /// <inheritdoc/>
    public Dictionary<string, Func<IDomainModel?, object?>> DtoMappers { get; } = new();

    /// <inheritdoc/>
    public Dictionary<string, Func<IServiceScopeFactory, User, Task<User>>> ModelExpanders { get; } = new();

    /// <inheritdoc/>
    public
        Dictionary<string, Func<IServiceScopeFactory, List<User>, Task<List<User>>>> ModelListExpanders
    { get; } =
            new();

    /// <inheritdoc/>
    public Dictionary<string, string> SelectClauses { get; } = new();

    /// <inheritdoc/>
    public Dictionary<string, string> SortClauses { get; } = new()
    {
        { "created", "IU.created" },
        { "lastloggedon", "IU.last_logged_on" },
        { "username", "IU.username" },
    };
}
