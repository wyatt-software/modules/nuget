﻿// <copyright file="IdentityModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.Infrastructure;

using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.API.Authentication;
using WyattSoftware.Modules.Identity.API.Controllers;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.BLL.Services;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Identity.Options;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Identity module.
/// </summary>
public static class IdentityModule
{
    /// <summary>
    /// Gets a collection of identity resource that are returned as part of the JWT access token.
    /// </summary>
    /// <remarks>
    /// The first three identity resources represent some standard OpenID Connect scopes you’ll want IdentityServer
    /// to support. For example, the email scope allows the email and email_verified claims to be returned. You are
    /// also creating a custom identity resource called role which returns any role claims for the authenticated
    /// user.
    /// </remarks>
    public static IEnumerable<IdentityResource> IdentityResources =>
        new[]
        {
            new IdentityResources.OpenId(),
            new IdentityResources.Profile(),
            new IdentityResources.Email(),
            new IdentityResource
            {
                Name = "role",
                UserClaims = new List<string> { "role" },
            },
        };

    /// <summary>
    /// Gets a collection of APIs.
    /// </summary>
    /// <remarks>
    /// An API resource models a single API that IdentityServer is protecting (a "protected resource" from the OAuth
    /// specification).
    /// </remarks>
    public static IEnumerable<ApiResource> ApiResources =>
        new[]
        {
            new ApiResource
            {
                Name = "default",
                DisplayName = "Default API",
                Description = "Allow the application to access the default API on your behalf.",
                Scopes = new List<string> { "default.read", "default.write" },
                UserClaims = new List<string> { "role" },
            },
        };

    /// <summary>
    /// Gets a collection of scopes.
    /// </summary>
    /// <remarks>
    /// An API scope is an individual authorization level on an API that a client application is allowed to request.
    /// For example, an API resource might be adminapi, with the scopes adminapi.read, adminapi.write, and
    /// adminapi.createuser. API scopes can be as fine-grained or as generic as you want.
    /// </remarks>
    public static IEnumerable<ApiScope> Scopes =>
        new[]
        {
            new ApiScope("default.read", "Read Access to default API"),
            new ApiScope("default.write", "Read Access to default API"),
        };

    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddIdentityModule(
        this IServiceCollection services, IConfiguration configuration)
    {
        var coreSection = configuration.GetSection("WyattSoftware:Modules:Core");
        var coreOptions = new CoreOptions();
        coreSection.Bind(coreOptions);

        var identitySection = configuration.GetSection("WyattSoftware:Modules:Identity");
        var identityOptions = new IdentityOptions();
        identitySection.Bind(identityOptions);

        // Make WyattSoftware.Modules.Identity appsettings.json options available via dependency injection.
        services.Configure<IdentityOptions>(identitySection);

        // Allow the identity module to be extended by other modules.
        services.AddSingleton(typeof(IUserAddOn), typeof(UserAddOn));

        // DAL
        services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
        services.AddScoped(typeof(IUserRoleRepository), typeof(UserRoleRepository));

        // BLL
        services.AddScoped(typeof(ICurrentSession), typeof(CurrentSession));
        services.AddScoped(typeof(IUserFactory), typeof(UserFactory));
        services.AddScoped(typeof(IUserService), typeof(UserService));
        services.AddScoped(typeof(IValidationService), typeof(ValidationService));

        // API
        IdentityModelEventSource.ShowPII = true;
        services.AddScoped<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator>();
        services.AddScoped<IProfileService, ProfileService>();

        // IdentityServer needs to know what client applications are allowed to use it. I like to think of this as a
        // list of applications that are allowed to use your system; your Access Control List (ACL). Each client
        // application is then configured to only be allowed to do certain things; for instance, they can only ask
        // for tokens to be returned to specific URLs, or they can only request certain information about the user.
        // They have scoped access.
        var clients = coreOptions.Clients.Select(CreateClientFromSection);

        services.AddIdentityServer()
            .AddInMemoryClients(clients)
            .AddInMemoryIdentityResources(IdentityResources)
            .AddInMemoryApiResources(ApiResources)
            .AddInMemoryApiScopes(Scopes)
            .AddDeveloperSigningCredential();

        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.Authority = identityOptions.Authority;
            options.Audience = "default";
            options.IncludeErrorDetails = true;
            options.Events = new JwtBearerEvents
            {
                OnAuthenticationFailed = context => Task.FromException(context.Exception),
            };
        });
        services.AddScoped(typeof(IUserClaimService), typeof(UserClaimService));

        return services;
    }

    /// <summary>
    /// <para>
    /// Configures the Identity module.
    /// </para>
    /// <para>
    /// This must be called after <c>.UseCoreModule()</c>, but before <c>.UseCoreModuleEndpoints()</c>.
    /// </para>
    /// </summary>
    /// <param name="app">This app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseIdentityModule(this IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();
        var userAddOn = scope.ServiceProvider.GetService<IUserAddOn>();
        if (userAddOn == null)
        {
            throw new Exception("Couldn't get an IUserAddOn.");
        }

        // API
        AccountController.InitMapping();
        UsersController.InitMapping(userAddOn);

        // In most cases IdentityServer infers the base hostname/URI from the incoming request.
        // When running on AWS EC2 behind a reverse proxy and load balancer, the incoming HTTPS requests are
        // forwaded to a local port over HTTP.
        // We need to UseForwardedHeaders here so that the URLs given in `/.well-known/openid-configuration`
        // use the correct protocol.
        var forwardOptions = new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
            RequireHeaderSymmetry = false,
        };

        forwardOptions.KnownNetworks.Clear();
        forwardOptions.KnownProxies.Clear();
        app.UseForwardedHeaders(forwardOptions);

        // ref: https://github.com/aspnet/Docs/issues/2384
        app.UseForwardedHeaders(forwardOptions);

        app.UseIdentityServer();
        app.UseAuthentication();
        app.UseAuthorization();

        return app;
    }

    /// <summary>
    /// Creates a client from an appsettings section.
    /// </summary>
    /// <param name="clientOptions">The appsettings.json client to create from.</param>
    /// <returns>An IdentityServer4 Client.</returns>
    private static Client CreateClientFromSection(CoreClientOptions clientOptions)
    {
        // See https://identityserver4.readthedocs.io/en/latest/reference/client.html
        return new Client
        {
            ClientId = clientOptions.ClientId,
            AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
            AllowOfflineAccess = true,
            RefreshTokenExpiration = TokenExpiration.Sliding,
            AccessTokenLifetime = clientOptions.AccessTokenLifetime,
            SlidingRefreshTokenLifetime = clientOptions.SlidingRefreshTokenLifetime,
            ClientSecrets =
            {
                new Secret(clientOptions.ClientSecret.Sha256()),
            },
            AllowedScopes =
            {
                IdentityServerConstants.StandardScopes.OpenId,
                IdentityServerConstants.StandardScopes.Profile,
                IdentityServerConstants.StandardScopes.OfflineAccess,
                IdentityServerConstants.StandardScopes.Email,
                "role",
                "default.read",
                "default.write",
            },
            AllowedCorsOrigins = clientOptions.CorsAllowedOrigins,
        };
    }
}
