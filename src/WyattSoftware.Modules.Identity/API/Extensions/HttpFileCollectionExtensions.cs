﻿// <copyright file="HttpFileCollectionExtensions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Extensions;

using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using WyattSoftware.Modules.Core.BLL.DTOs;

/// <summary>
/// Helper methods relating to HTTP request.
/// </summary>
public static class HttpFileCollectionExtensions
{
    /// <summary>
    /// Converts a collection of posted files to a list of FileDtos.
    /// </summary>
    /// <param name="postedFiles">The collection of posted files.</param>
    /// <returns>A collection of file DTOs.</returns>
    public static List<FileDto> AsFileDtos(this IFormFileCollection postedFiles)
    {
        return new List<IFormFile>(postedFiles)
            .Select(ConvertFormFileToFileDto)
            .ToList();
    }

    /// <summary>
    /// Converts a single posted file to a file DTO.
    /// </summary>
    /// <returns>A single File DTO.</returns>
    private static FileDto ConvertFormFileToFileDto(IFormFile postedFile)
    {
        return new FileDto
        {
            ContentLength = postedFile.Length,
            ContentType = postedFile.ContentType,
            Filename = postedFile.FileName,
            Data = postedFile.OpenReadStream(),
        };
    }
}
