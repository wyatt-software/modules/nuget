﻿// <copyright file="UserClaimService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Authentication;

using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Generates claims for the user.
/// </summary>
public class UserClaimService : IUserClaimService
{
    /// <summary>
    /// Build claims array from user data.
    /// </summary>
    /// <param name="user">The user to get claims for.</param>
    /// <returns>An array of claims.</returns>
    public async Task<Claim[]> FetchUserClaimsAsync(User user)
    {
        var claims = new List<Claim>(new[]
        {
            new Claim(JwtClaimTypes.Id, user.Id.ToString()),
            new Claim(JwtClaimTypes.Name, user.Username),
            new Claim(JwtClaimTypes.Email, user.Email),
        });

        var roles = await user.FetchRolesAsync();
        var roleClaims = roles.Select(role => new Claim(JwtClaimTypes.Role, role.ToString()));
        claims.AddRange(roleClaims);

        return claims.ToArray();
    }
}
