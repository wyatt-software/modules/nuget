﻿// <copyright file="ResourceOwnerPasswordValidator.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Authentication;

using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Validation;
using WyattSoftware.Modules.Identity.BLL.Services;

/// <summary>
/// Default implementation of <see cref="IResourceOwnerPasswordValidator"/>.
/// </summary>
public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
{
    private readonly IUserService userService;
    private readonly IUserClaimService userClaimService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ResourceOwnerPasswordValidator"/> class.
    /// </summary>
    /// <param name="userService">Injected UserService.</param>
    /// <param name="userClaimService">Injected IUserClaimService.</param>
    public ResourceOwnerPasswordValidator(
        IUserService userService,
        IUserClaimService userClaimService)
    {
        this.userService = userService;
        this.userClaimService = userClaimService;
    }

    /// <summary>
    /// This is used to validate your user account with provided grant at /connect/token.
    /// </summary>
    /// <param name="context">Injected ResourceOwnerPasswordValidationContext.</param>
    /// <returns>A task.</returns>
    public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
    {
        var user = await this.userService.GetByUsernameAndPasswordAsync(context.UserName, context.Password);
        if (user == null)
        {
            context.Result = new GrantValidationResult(
            TokenRequestErrors.InvalidGrant, "Invalid username or password.");
            return;
        }

        if (!user.EmailVerified)
        {
            context.Result = new GrantValidationResult(
            TokenRequestErrors.InvalidGrant, "Email address not verified.");
            return;
        }

        var claims = await this.userClaimService.FetchUserClaimsAsync(user);

        context.Result = new GrantValidationResult(user.Id.ToString(), "custom", claims);

        await user.UpdateLastLoggedOnAsync();
    }
}
