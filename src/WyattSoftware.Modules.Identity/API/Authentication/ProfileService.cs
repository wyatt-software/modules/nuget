﻿// <copyright file="ProfileService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Authentication;

using System.Linq;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using WyattSoftware.Modules.Identity.BLL.Services;

/// <summary>
/// Default implementation of <see cref="IProfileService"/>.
/// </summary>
public class ProfileService : IProfileService
{
    private readonly IUserService userService;
    private readonly IUserClaimService userServiceClaim;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProfileService"/> class.
    /// </summary>
    /// <param name="userService">Injected UserService.</param>
    /// <param name="userServiceClaim">Injected IUserClaimService.</param>
    public ProfileService(
        IUserService userService,
        IUserClaimService userServiceClaim)
    {
        this.userService = userService;
        this.userServiceClaim = userServiceClaim;
    }

    /// <summary>
    /// Get user profile date in terms of claims when calling /connect/userinfo.
    /// </summary>
    /// <param name="context">A ProfileDataRequestContext.</param>
    /// <returns>A task.</returns>
    public async Task GetProfileDataAsync(ProfileDataRequestContext context)
    {
        var usernameOrEmail = context.Subject.Identity?.Name;
        var user = !string.IsNullOrEmpty(usernameOrEmail)
            ? await this.userService.ReadAsync(usernameOrEmail)
            : await this.userService.ReadAsync(
                int.Parse(context.Subject.Claims.FirstOrDefault(x => x.Type == "sub")?.Value ?? string.Empty));

        if (user != null)
        {
            var claims = await this.userServiceClaim.FetchUserClaimsAsync(user);
            context.IssuedClaims = claims // .Where(x => context.RequestedClaimTypes.Contains(x.Type))
                .ToList();
        }
    }

    /// <summary>
    /// Check if user account is active.
    /// </summary>
    /// <param name="context">IsActiveContext.</param>
    /// <returns>A Task.</returns>
    public async Task IsActiveAsync(IsActiveContext context)
    {
        var userIdClaim = context.Subject.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.Id);

        var userId = int.Parse(userIdClaim?.Value ?? "0");
        if (userId > 0)
        {
            var user = await this.userService.ReadAsync(userId);
            if (user != null)
            {
                context.IsActive = user.EmailVerified;
            }
        }
    }
}
