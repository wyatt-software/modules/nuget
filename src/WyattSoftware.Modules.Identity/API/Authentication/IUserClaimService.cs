﻿// <copyright file="IUserClaimService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Authentication;

using System.Security.Claims;
using System.Threading.Tasks;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Generates claims for the user.
/// </summary>
public interface IUserClaimService
{
    /// <summary>
    /// Gets the claims for a user.
    /// </summary>
    /// <param name="user">The user to generate the claims for.</param>
    /// <returns>An array of claims.</returns>
    Task<Claim[]> FetchUserClaimsAsync(User user);
}
