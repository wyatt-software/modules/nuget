﻿// <copyright file="ValidationController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Controllers;

using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Services;

/// <summary>
/// Handles data validation (checking for unique usernames and passwords).
/// </summary>
[Route("identity/validation")]
public class ValidationController : ControllerBase
{
    private readonly IValidationService validationService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidationController"/> class.
    /// </summary>
    /// <param name="validationService">Injected ValidationService.</param>
    public ValidationController(IValidationService validationService)
    {
        this.validationService = validationService;
    }

    /// <summary>
    /// Checks if the email address is NOT taken (unless it belongs to the given user).
    /// </summary>
    /// <param name="value">The email address to check.</param>
    /// <param name="userId">The user to ignore when checking for emails that match.</param>
    /// <returns>A validation response object.</returns>
    [HttpGet]
    [Route("email")]
    public async Task<IActionResult> Email(string value, int userId = 0)
    {
        var (valid, message) = await this.validationService.ValidateEmailAsync(value, userId);

        var response = new ValidationResponse
        {
            Valid = valid,
            Data = new ValidationResponseData
            {
                Message = message,
            },
        };

        return this.Ok(response);
    }

    /// <summary>
    /// Checks if the username is NOT taken (unless it belongs to the given user).
    /// </summary>
    /// <param name="value">The username to check.</param>
    /// <param name="userId">The user to ignore when checking for usernames that match.</param>
    /// <returns>A validation response object.</returns>
    [HttpGet]
    [Route("username")]
    public async Task<IActionResult> Username(string value, int userId = 0)
    {
        var (valid, message) = await this.validationService.ValidateUsernameAsync(value, userId);

        var response = new ValidationResponse
        {
            Valid = valid,
            Data = new ValidationResponseData
            {
                Message = message,
            },
        };

        return this.Ok(response);
    }
}
