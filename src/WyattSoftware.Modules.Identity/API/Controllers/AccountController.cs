﻿// <copyright file="AccountController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Controllers;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.DTOs;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Services;

/// <summary>
/// Provides functionality for the currently authenticated user.
/// </summary>
[Route("identity/account")]
public class AccountController : ControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly IUserService userService;

    /// <summary>
    /// Initializes a new instance of the <see cref="AccountController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userService">Injected IUserService.</param>
    public AccountController(
        ICurrentSession currentSession,
        IUserService userService)
    {
        this.currentSession = currentSession;
        this.userService = userService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
    }

    /// <summary>
    /// Gets the currently authenticated user.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <returns>An HTTP action result: 200 or 404.</returns>
    [HttpGet]
    [Route("")]
    [Authorize]
    public async Task<IActionResult> Read(string expand = "")
    {
        var user = await this.userService.ReadAsync(this.currentSession.UserId, expand);
        if (user == null)
        {
            return this.Unauthorized(new GeneralResponse("No currently authenticated user."));
        }

        var response = Mapper.Map<AccountResponse>(user);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates the currently authenticated user.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200 or 404.</returns>
    [HttpPut]
    [Route("")]
    [Authorize]
    public async Task<IActionResult> Update([FromBody]AccountUpdateRequest request)
    {
        var user = await this.userService.ReadAsync(this.currentSession.UserId);
        if (user == null)
        {
            return this.Unauthorized(new GeneralResponse("No currently authenticated user."));
        }

        user.Email = request.Email;

        // We merge the existing properties and preferences with the incoming data, so that we don't lose anything
        // not mentioned in the incoming data.
        // But... we still want to overwrite values when the request value is null.
        var settings = new JsonMergeSettings()
        {
            MergeNullValueHandling = MergeNullValueHandling.Merge,
        };

        user.Properties.Merge(request.Properties, settings);
        user.Preferences.Merge(request.Preferences, settings);

        await user.UpdateAsync();
        await user.ExpandAsync("all");

        var response = Mapper.Map<AccountResponse>(user);
        return this.Ok(response);
    }

    /// <summary>
    /// Changes the user's password.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200, 400 or 404.</returns>
    [HttpPost]
    [Route("actions/change-password")]
    [Authorize]
    public async Task<IActionResult> ChangePassword([FromBody] AccountActionChangePasswordRequest request)
    {
        var user = await this.userService.ReadAsync(this.currentSession.UserId);
        if (user == null)
        {
            return this.Unauthorized(new GeneralResponse("No currently authenticated user."));
        }

        await user.ChangePasswordAsync(
            request.CurrentPassword,
            request.NewPassword);

        return this.Ok(null);
    }
}
