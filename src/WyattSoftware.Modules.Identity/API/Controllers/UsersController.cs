﻿// <copyright file="UsersController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.Controllers;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.API.Filters.ReCaptcha;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.BLL.Services;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Provides public access to the users collection, and provided functionality for unauthenticated users
/// (eg: password reset etc).
/// </summary>
[Route("identity/users")]
public class UsersController : WsControllerBase
{
    private readonly CoreOptions coreOptions;
    private readonly IUserFactory userFactory;
    private readonly IUserService userService;
    private readonly ReCaptchaValidator reCaptchaValidator;

    /// <summary>
    /// Initializes a new instance of the <see cref="UsersController"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="httpClientFactory">Injected IHttpClientFactory.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="userService">Injected UserService.</param>
    public UsersController(
        IOptions<CoreOptions> coreOptions,
        IHttpClientFactory httpClientFactory,
        IUserFactory userFactory,
        IUserService userService)
    {
        this.coreOptions = coreOptions.Value;
        this.userService = userService;
        this.userFactory = userFactory;

        this.reCaptchaValidator = new ReCaptchaValidator(
            httpClientFactory,
            this.coreOptions.Vendor.ReCaptcha.Enabled,
            this.coreOptions.Vendor.ReCaptcha.PrivateKey);
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    public static void InitMapping(IUserAddOn userAddOn)
    {
        Mapper.AddMap<User?, AccountResponse?>(user =>
        {
            if (user == null)
            {
                return null;
            }

            var response = new AccountResponse();
            response.InjectFrom(user);

            foreach (var item in user.NestedResources)
            {
                var mapper = userAddOn.DtoMappers[item.Key];
                if (mapper == null)
                {
                    continue;
                }

                var nestedObject = mapper(item.Value);

                response.NestedResources.Add(item.Key, nestedObject);
            }

            return response;
        });

        Mapper.AddMap<User?, UserResponse?>(user =>
        {
            if (user == null)
            {
                return null;
            }

            var response = new UserResponse();
            response.InjectFrom(user);

            foreach (var item in user.NestedResources)
            {
                var mapper = userAddOn.DtoMappers[item.Key];
                if (mapper == null)
                {
                    continue;
                }

                var nestedObject = mapper(item.Value);

                response.NestedResources.Add(item.Key, nestedObject);
            }

            return response;
        });

        Mapper.AddMap<PaginatedResponse<User>, PaginatedResponse<UserResponse>>((users) =>
            new PaginatedResponse<UserResponse>()
            {
                Total = users.Total,
                Offset = users.Offset,
                Limit = users.Limit,
                Results = users.Results.Select(x => Mapper.Map<UserResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get just the total count for the given request.
    /// </summary>
    /// <param name="q">Searches by part of username.</param>
    /// <returns>A response with an empty body, and only an "X-Total-Count" header.</returns>
    [HttpHead]
    [Route("")]
    public async Task<IActionResult> Metadata(string q = "")
    {
        var total = await this.userService.GetCountAsync(q);
        return this.TotalHeaderOnly(total);
    }

    /// <summary>
    /// Get all users who's usernames match the given search string.
    /// </summary>
    /// <param name="q">Searches by part of username.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <param name="sort">Sorts results by a given field (with a '-' prefix when descending).</param>
    /// <returns>A collection of users.</returns>
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(
        string q = "", string expand = "", int offset = 0, int limit = 20, string sort = "-lastloggedon")
    {
        var users = await this.userService.FetchAsync(q, expand, offset, limit, sort);
        var response = Mapper.Map<PaginatedResponse<UserResponse>>(users);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a user.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200 or 400.</returns>
    [HttpPost]
    [Route("")]
    public async Task<IActionResult> Create([FromBody] UserCreateRequest request)
    {
        var valid = await this.reCaptchaValidator.ValidateAsync(request.ReCaptcha);
        if (!valid)
        {
            throw new ArgumentException("Invalid recaptcha.");
        }

        // Try to construct a domain model to trigger validation.
        var user = this.userFactory.New(
            request.Username,
            request.Password,
            request.Email);

        await user.CreateAsync();
        await user.ExpandAsync("all");

        var response = Mapper.Map<AccountResponse>(user);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets publicly available info for a single user (by their username or email address).
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address to find the user by.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <returns>An HTTP action result: 200 or 404.</returns>
    [HttpGet]
    [Route("{usernameOrEmail}")]
    public async Task<IActionResult> Read(string usernameOrEmail, string expand = "")
    {
        var user = await this.userService.ReadAsync(usernameOrEmail, expand);
        var response = Mapper.Map<UserResponse>(user);
        return this.Ok(response);
    }

    /// <summary>
    /// Sends an email to the user with a link to resend their email verification code.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address to find the user by.</param>
    /// <returns>An HTTP action result: 200, 400 or 404.</returns>
    [HttpPost]
    [Route("{usernameOrEmail}/actions/resend-email-verification")]
    public async Task<IActionResult> ResendEmailVerification(string usernameOrEmail)
    {
        var user = await this.userService.ReadAsync(usernameOrEmail);
        await user.SendEmailVerificationAsync();
        return this.Ok(null);
    }

    /// <summary>
    /// Verifies the user's email address.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address to find the user by.</param>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200 or 400.</returns>
    [HttpPost]
    [Route("{usernameOrEmail}/actions/verify-email")]
    public async Task<IActionResult> VerifyEmail(
        string usernameOrEmail, [FromBody] UserActionVerifyEmailRequest request)
    {
        var user = await this.userService.ReadAsync(usernameOrEmail);
        await user.VerifyEmailAsync(request.VerificationCode);
        return this.Ok(null);
    }

    /// <summary>
    /// Sends an email to the user with a link to resend their email verification code.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address to find the user by.</param>
    /// <returns>An HTTP action result: 200 or 400.</returns>
    [HttpPost]
    [Route("{usernameOrEmail}/actions/forgot-password")]
    public async Task<IActionResult> ForgotPassword(string usernameOrEmail)
    {
        var user = await this.userService.ReadAsync(usernameOrEmail);
        await user.SendPasswordResetLinkAsync();
        return this.Ok(null);
    }

    /// <summary>
    /// Resets the user's email address.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address to find the user by.</param>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200 or 400.</returns>
    [HttpPost]
    [Route("{usernameOrEmail}/actions/reset-password")]
    public async Task<IActionResult> ResetPassword(
        string usernameOrEmail, [FromBody] UserActionResetPasswordRequest request)
    {
        var user = await this.userService.ReadAsync(usernameOrEmail);
        await user.ResetPasswordAndUpdateAsync(request.ResetCode, request.NewPassword);
        return this.Ok(null);
    }
}
