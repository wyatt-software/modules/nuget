﻿// <copyright file="UserResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

using System;
using Newtonsoft.Json.Linq;

/// <summary>
/// User response.
/// </summary>
/// <remarks>
/// Mapped from the <see cref="User" /> model.
/// </remarks>
public class UserResponse
{
    /// <summary>
    /// Gets or sets the username.
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the date the user was created or joined.
    /// </summary>
    public DateTime Created { get; set; }

    /// <summary>
    /// Gets or sets the date the user last logged on.
    /// </summary>
    public DateTime? LastLoggedOn { get; set; }

    /// <summary>
    /// Gets or sets public custom properties, added by the other modules.
    /// </summary>
    public JObject Properties { get; set; } = new JObject();

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets a collection of optional nested resources, populated via reference expansion.
    /// </summary>
    public Dictionary<string, object?> NestedResources { get; set; } = new Dictionary<string, object?>();
}
