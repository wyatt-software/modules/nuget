﻿// <copyright file="ValidationResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Validation response.
/// </summary>
public class ValidationResponse
{
    /// <summary>
    /// Gets or sets a value indicating whether the validation passed or failed.
    /// </summary>
    public bool Valid { get; set; }

    /// <summary>
    /// Gets or sets additional data relating to the validation.
    /// </summary>
    public ValidationResponseData? Data { get; set; }
}
