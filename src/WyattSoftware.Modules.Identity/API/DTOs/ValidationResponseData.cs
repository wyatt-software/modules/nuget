﻿// <copyright file="ValidationResponseData.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// The data part of a validation response.
/// </summary>
public class ValidationResponseData
{
    /// <summary>
    /// Gets or sets the reason validation failed.
    /// </summary>
    public string Message { get; set; } = string.Empty;
}
