﻿// <copyright file="AccountUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

using Newtonsoft.Json.Linq;

/// <summary>
/// Account update request.
/// </summary>
public class AccountUpdateRequest
{
    /// <summary>
    /// Gets or sets the user's contact email address.
    /// </summary>
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets public custom properties, added by the other modules.
    /// </summary>
    public JObject Properties { get; set; } = new JObject();

    /// <summary>
    /// Gets or sets private custom properties, added by the other modules.
    /// </summary>
    public JObject Preferences { get; set; } = new JObject();
}
