﻿// <copyright file="AccountResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

using Newtonsoft.Json.Linq;

/// <summary>
/// Account response.
/// </summary>
/// <remarks>
/// <para>Mapped from the <see cref="User" /> model.</para>
/// <para>
/// Inherits from <see cref="UserResponse" /> so that we can be confident in passing the
/// account object to the avatar Vue.js component (in the front end SPA).
/// </para>
/// <para>
/// <c>&lt;Avatar :user="user"/&gt;</c> behaves the same as <c>&lt;Avatar :user="account"/&gt;</c>.
/// </para>
/// </remarks>
public class AccountResponse : UserResponse
{
    /// <summary>
    /// Gets or sets the user's id.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the user's email address.
    /// </summary>
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the user's email address has been verified.
    /// </summary>
    public bool EmailVerified { get; set; }

    /// <summary>
    /// Gets or sets private custom properties, added by the other modules.
    /// </summary>
    public JObject Preferences { get; set; } = new JObject();
}
