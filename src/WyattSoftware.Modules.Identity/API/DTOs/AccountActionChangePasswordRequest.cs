﻿// <copyright file="AccountActionChangePasswordRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Change password request.
/// </summary>
public class AccountActionChangePasswordRequest
{
    /// <summary>
    /// Gets or sets the user's current password.
    /// </summary>
    public string CurrentPassword { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the user's new password.
    /// </summary>
    public string NewPassword { get; set; } = string.Empty;
}
