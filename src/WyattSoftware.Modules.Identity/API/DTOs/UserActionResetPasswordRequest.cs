﻿// <copyright file="UserActionResetPasswordRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Reset password request.
/// </summary>
public class UserActionResetPasswordRequest
{
    /// <summary>
    /// Gets or sets the reset code that was emailed to the user.
    /// </summary>
    public string ResetCode { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the user's new password.
    /// </summary>
    public string NewPassword { get; set; } = string.Empty;
}
