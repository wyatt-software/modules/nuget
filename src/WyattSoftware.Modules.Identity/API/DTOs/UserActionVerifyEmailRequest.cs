﻿// <copyright file="UserActionVerifyEmailRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Verify email request.
/// </summary>
public class UserActionVerifyEmailRequest
{
    /// <summary>
    /// Gets or sets the email verification code.
    /// </summary>
    public string VerificationCode { get; set; } = string.Empty;
}