﻿// <copyright file="UserCreateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Account create request.
/// </summary>
public class UserCreateRequest
{
    /// <summary>
    /// Gets or sets the user's login and display username.
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the user's contact email address.
    /// </summary>
    public string Email { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the user's login password.
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the token generated by the Google ReCaptcha client-side API.
    /// </summary>
    public string ReCaptcha { get; set; } = string.Empty;
}
