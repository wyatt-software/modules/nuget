﻿// <copyright file="User.Email.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using WyattSoftware.Modules.Core.BLL.Extensions;
using WyattSoftware.Modules.Core.BLL.Helpers;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Identity user domain model.
/// </summary>
public partial class User
{
    /// <summary>
    /// Sends an email to a user.
    /// </summary>
    /// <param name="templateName">The template to use. Macros are resolved on ALL fields.</param>
    /// <param name="data">A dictionary of objects to use as macro values.</param>
    /// <param name="useHtmlTemplate">
    /// Optionally choose whether to use the HTML or Plain Text template is. HTML is used by default if it exists,
    /// otherwise plain text is used as a fall-back.
    /// </param>
    /// <returns>Nothing.</returns>
    public async Task SendEmailAsync(
        string templateName, Dictionary<string, object?> data, bool? useHtmlTemplate = null)
    {
        var emailTemplateEntity = await this.emailTemplateRepository.ReadAsync(templateName);
        if (emailTemplateEntity == null)
        {
            throw new Exception($"Email template \"{templateName}\" not found.");
        }

        var from = new MailAddress(
            this.coreOptions.Email.From.Address,
            this.coreOptions.Email.From.DisplayName);

        var subject = this.macroResolver.Resolve(emailTemplateEntity.Subject, data);

        useHtmlTemplate ??= !string.IsNullOrEmpty(emailTemplateEntity.Html);
        var bodyTemplate = useHtmlTemplate.Value
            ? emailTemplateEntity.Html
            : emailTemplateEntity.PlainText;

        var body = this.macroResolver.Resolve(bodyTemplate, data);

        var mailMessage = new MailMessage
        {
            Subject = subject,
            Body = body,
            From = from,
            IsBodyHtml = useHtmlTemplate.Value,
        };

        mailMessage.To.Add(new MailAddress(this.Email));

        await this.emailSender.SendEmailAsync(mailMessage);
    }

    /// <summary>
    /// Sends an email verification code to a user.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task SendEmailVerificationAsync()
    {
        this.EmailVerificationCode = UniqueKeyGenerator.Generate(
            this.identityOptions.EmailVerificationCode.Length,
            this.identityOptions.EmailVerificationCode.Chars);

        this.EmailVerificationCodeCreated = DateTimeOffset.UtcNow.UtcDateTime;

        await this.UpdateAsync();

        // Add user properties, including `EmailVerificationCode`.
        var data = this.ToDictionary(camelCase: true);

        var encodedEmailVerificationCode = HttpUtility.UrlEncode(this.EmailVerificationCode);
        var link =
            $"{this.coreOptions.Macros["DomainName"]}account/email-verification" +
            $"?usernameOrEmail={this.Username}" +
            $"&code={encodedEmailVerificationCode}";

        data.Add("link", link);

        // Add all macros from appsettings.json.
        foreach (var macro in this.coreOptions.Macros)
        {
            if (data.ContainsKey(macro.Key))
            {
                continue;
            }

            data.Add(macro.Key, macro.Value);
        }

        await this.SendEmailAsync("IdentityUserSendEmailVerification", data);
    }

    /// <summary>
    /// Verifies the user's email address.
    /// </summary>
    /// <param name="verificationCode">The verification code that should have been emailed to the user.</param>
    /// <returns>Nothing.</returns>
    public async Task VerifyEmailAsync(string verificationCode)
    {
        if (string.IsNullOrEmpty(verificationCode))
        {
            throw new ArgumentException("Missing verification code.");
        }

        if (!verificationCode.Equals(this.EmailVerificationCode))
        {
            throw new ArgumentException("Invalid email verification reset code.");
        }

        if (!this.EmailVerificationCodeCreated.HasValue)
        {
            throw new ArgumentException("The email verification code created date was not set.");
        }

        var expiryDate = this.EmailVerificationCodeCreated.Value.AddHours(
            this.identityOptions.EmailVerificationCode.ExpirationHours);

        if (DateTimeOffset.UtcNow.UtcDateTime > expiryDate)
        {
            throw new ArgumentException("The email verification code has expired.");
        }

        this.EmailVerified = true;
        this.EmailVerificationCode = string.Empty;
        await this.AddRoleAsync(Role.Member);
        await this.UpdateAsync();
    }
}
