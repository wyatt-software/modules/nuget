﻿// <copyright file="User.Password.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Models;

using System;
using System.Threading.Tasks;
using System.Web;
using WyattSoftware.Modules.Core.BLL.Extensions;
using WyattSoftware.Modules.Core.BLL.Helpers;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Identity user domain model.
/// </summary>
public partial class User
{
    /// <summary>
    /// Checks a user's password.
    /// </summary>
    /// <param name="password">The password to check.</param>
    /// <returns><c>true</c> if correct.</returns>
    public bool CheckPassword(string password)
    {
        string hash;

        switch (this.PasswordFormat)
        {
            case UserPasswordFormat.PlainText:
                // TODO: Disable in production.
                hash = password;
                break;

            case UserPasswordFormat.MD5:
                // We don't use salt here, because it was never used in the old site.
                hash = HashGenerator.Md5(password);
                break;

            case UserPasswordFormat.SHA256:
                hash = HashGenerator.Sha256($"{this.Id}|{password}|{this.coreOptions.Encryption.HashStringSalt}");
                break;

            default:
                return false;
        }

        return this.PasswordHash.Equals(hash);
    }

    /// <summary>
    /// Sets the password on the user model.
    /// </summary>
    /// <param name="newPassword">The password to set.</param>
    /// <remarks>Does not persist the data.</remarks>
    public void SetPassword(string newPassword)
    {
        if (string.IsNullOrEmpty(newPassword))
        {
            throw new ArgumentException("You must provide a password.");
        }

        if (newPassword.Length < this.identityOptions.Password.MinLength)
        {
            throw new ArgumentException(
                $"Password must be at least {this.identityOptions.Password.MinLength} characters long.");
        }

        // Include the user's id in the hash, so that a hacker can't copy and paste password hashes between users.
        this.PasswordHash = HashGenerator.Sha256(
            $"{this.Id}|{newPassword}|{this.coreOptions.Encryption.HashStringSalt}");

        this.PasswordFormat = UserPasswordFormat.SHA256;
        this.PasswordResetCode = string.Empty;
    }

    /// <summary>
    /// Resets a user's password, checking that the expiry code hasn't expired yet.
    /// </summary>
    /// <param name="passwordResetCode">The password reset code emailed to the user..</param>
    /// <param name="newPassword">The password to set.</param>
    /// <returns>Nothing.</returns>
    public async Task ResetPasswordAndUpdateAsync(string passwordResetCode, string newPassword)
    {
        if (string.IsNullOrEmpty(passwordResetCode))
        {
            throw new ArgumentException("You must provide a password reset code.");
        }

        if (!passwordResetCode.Equals(this.PasswordResetCode))
        {
            throw new ArgumentException("Invalid password reset code.");
        }

        if (string.IsNullOrEmpty(newPassword))
        {
            throw new ArgumentException("You must provide a new password.");
        }

        if (!this.PasswordResetCodeCreated.HasValue)
        {
            throw new ArgumentException("The password reset code created date was not set.");
        }

        var expiryDate = this.PasswordResetCodeCreated.Value.AddHours(
            this.identityOptions.Password.ResetCodeExpirationHours);

        if (DateTimeOffset.UtcNow.UtcDateTime > expiryDate)
        {
            throw new ArgumentException("The password reset code has expired.");
        }

        this.SetPassword(newPassword);
        await this.UpdateAsync();
    }

    /// <summary>
    /// Sends the password reset link to a user.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task SendPasswordResetLinkAsync()
    {
        this.PasswordResetCode = UniqueKeyGenerator.Generate(
            this.identityOptions.Password.ResetCodeLength,
            this.identityOptions.Password.ResetCodeChars);

        this.PasswordResetCodeCreated = DateTimeOffset.UtcNow.UtcDateTime;

        await this.UpdateAsync();

        // Add user properties, including `PasswordResetCode`.
        var data = this.ToDictionary(camelCase: true);

        var encodedPasswordResetCode = HttpUtility.UrlEncode(this.PasswordResetCode);
        var link =
            $"{this.coreOptions.Macros["DomainName"]}account/reset-password" +
            $"?usernameOrEmail={this.Username}" +
            $"&code={encodedPasswordResetCode}";

        data.Add("link", link);

        // Add all macros from appsettings.json.
        foreach (var macro in this.coreOptions.Macros)
        {
            if (data.ContainsKey(macro.Key))
            {
                continue;
            }

            data.Add(macro.Key, macro.Value);
        }

        await this.SendEmailAsync("IdentityUserSendPasswordResetLink", data);
    }

    /// <summary>
    /// Changes the user's password.
    /// </summary>
    /// <param name="currentPassword">The user's current (old) password.</param>
    /// <param name="newPassword">The new password to set.</param>
    /// <returns>Nothing.</returns>
    public async Task ChangePasswordAsync(string currentPassword, string newPassword)
    {
        if (string.IsNullOrEmpty(currentPassword))
        {
            throw new ArgumentException("You must provide the old password.");
        }

        if (!this.CheckPassword(currentPassword))
        {
            // TODO: Implement a max number of failed attempts?
            throw new ArgumentException("Incorrect old password.");
        }

        this.SetPassword(newPassword);
        await this.UpdateAsync();
    }
}
