﻿// <copyright file="User.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Models;

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Features.Email;
using WyattSoftware.Modules.Core.BLL.Features.Macros;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Identity.Infrastructure;
using WyattSoftware.Modules.Identity.Options;

/// <summary>
/// Identity user domain model.
/// </summary>
public partial class User : UserEntity, IDomainModel
{
    /// <summary>
    /// Validation to use for usernames.
    /// </summary>
    public const string UsernameValidation = "^[0-9A-Za-z]+$";

    // We're using "fat models", so we have a bunch of dependencies.
    private readonly CoreOptions coreOptions;
    private readonly IdentityOptions identityOptions;
    private readonly IUserAddOn userAddOn;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IEmailSender emailSender;
    private readonly IMacroResolver macroResolver;
    private readonly IEmailTemplateRepository emailTemplateRepository;
    private readonly IUserRepository userRepository;
    private readonly IUserRoleRepository userRoleRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="User"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="identityOptions">Injected IOptions{IdentityOptions}.</param>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="emailSender">Injected IEmailSender.</param>
    /// <param name="macroResolver">Injected IMacroResolver.</param>
    /// <param name="emailTemplateRepository">Injected IEmailTemplateRepository.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="userRoleRepository">Injected ICommunityUserRoleRepository.</param>
    /// <param name="username">The username.</param>
    /// <param name="password">The password.</param>
    /// <param name="email">The email address.</param>
    /// <remarks>Required properties are validated upon construction.</remarks>
    internal User(
        IOptions<CoreOptions> coreOptions,
        IOptions<IdentityOptions> identityOptions,
        IUserAddOn userAddOn,
        IServiceScopeFactory serviceScopeFactory,
        IEmailSender emailSender,
        IMacroResolver macroResolver,
        IEmailTemplateRepository emailTemplateRepository,
        IUserRepository userRepository,
        IUserRoleRepository userRoleRepository,
        string username,
        string password,
        string email)
    {
        // Dependencies.
        this.coreOptions = coreOptions.Value;
        this.identityOptions = identityOptions.Value;
        this.userAddOn = userAddOn;
        this.serviceScopeFactory = serviceScopeFactory;
        this.emailTemplateRepository = emailTemplateRepository;
        this.emailSender = emailSender;
        this.macroResolver = macroResolver;
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;

        this.Username = username;
        this.SetPassword(password);
        this.Email = email;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="User"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="identityOptions">Injected IOptions{IdentityOptions}.</param>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="emailTemplateRepository">Injected IEmailTemplateRepository.</param>
    /// <param name="emailSender">Injected IEmailSender.</param>
    /// <param name="macroResolver">Injected IMacroResolver.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="userRoleRepository">Injected IUserRoleRepository.</param>
    /// <param name="userEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal User(
        IOptions<CoreOptions> coreOptions,
        IOptions<IdentityOptions> identityOptions,
        IUserAddOn userAddOn,
        IServiceScopeFactory serviceScopeFactory,
        IEmailSender emailSender,
        IMacroResolver macroResolver,
        IEmailTemplateRepository emailTemplateRepository,
        IUserRepository userRepository,
        IUserRoleRepository userRoleRepository,
        UserEntity userEntity)
    {
        // Dependencies.
        this.coreOptions = coreOptions.Value;
        this.identityOptions = identityOptions.Value;
        this.userAddOn = userAddOn;
        this.serviceScopeFactory = serviceScopeFactory;
        this.emailSender = emailSender;
        this.macroResolver = macroResolver;
        this.emailTemplateRepository = emailTemplateRepository;
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;

        this.Id = userEntity.Id;
        this.Created = userEntity.Created;
        this.Modified = userEntity.Modified;
        this.LastLoggedOn = userEntity.LastLoggedOn;

        this.Username = userEntity.Username;

        this.PasswordFormat = userEntity.PasswordFormat;
        this.PasswordHash = userEntity.PasswordHash;
        this.PasswordResetCode = userEntity.PasswordResetCode;
        this.PasswordResetCodeCreated = userEntity.PasswordResetCodeCreated;

        this.Email = userEntity.Email;
        this.EmailVerified = userEntity.EmailVerified;
        this.EmailVerificationCode = userEntity.EmailVerificationCode;
        this.EmailVerificationCodeCreated = userEntity.EmailVerificationCodeCreated;

        this.Properties = userEntity.Properties;

        // Merge the derived properties into the properties collection.
        this.Properties.Merge(
            userEntity.DerivedProperties,
            new JsonMergeSettings { MergeArrayHandling = MergeArrayHandling.Union });

        this.Preferences = userEntity.Preferences;
    }

    /// <summary>
    /// Inserts the user into the database.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        var userEntity = Mapper.Map<UserEntity>(this);
        await this.ValidateAsync();
        this.Id = await this.userRepository.CreateAsync(userEntity);
        await this.SendEmailVerificationAsync();
    }

    /// <summary>
    /// Attempts to update the user in the database.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var userEntity = Mapper.Map<UserEntity>(this);
        await this.ValidateAsync();
        await this.userRepository.UpdateAsync(userEntity);
    }

    /// <inheritdoc />
    public async Task DeleteAsync()
    {
        await this.userRepository.DeleteAsync(this.Id);
    }

    /// <summary>
    /// Fetches the roles that this user has.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task<Role[]> FetchRolesAsync()
    {
        return await this.userRoleRepository.FetchAsync(this.Id);
    }

    /// <summary>
    /// Adds a role to the user.
    /// </summary>
    /// <param name="role">The role to add.</param>
    /// <returns>Nothing.</returns>
    public async Task AddRoleAsync(Role role)
    {
        // TODO: Ensure role doesn't already exist?
        await this.userRoleRepository.CreateAsync(new UserRoleEntity
        {
            UserId = this.Id,
            Role = role,
        });
    }

    /// <summary>
    /// Updates the last logged on timestamp.
    /// </summary>
    /// <param name="timestamp">Timestamp of last login. Default to now.</param>
    /// <returns>Nothing.</returns>
    public async Task UpdateLastLoggedOnAsync(DateTime? timestamp = null)
    {
        await this.userRepository.UpdateLastLoggedOnAsync(this.Id, timestamp);
    }
}
