﻿// <copyright file="User.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Models;

using WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// Identity user domain model.
/// </summary>
public partial class User
{
    /// <summary>
    /// Gets or sets a collection of optional nested resources, populated via reference expansion.
    /// </summary>
    public Dictionary<string, IDomainModel> NestedResources { get; set; } = new Dictionary<string, IDomainModel>();

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: "all", or the key for any registered nested resource expander (eg: "image").</para>
    /// </param>
    /// <returns>The user, with references expanded.</returns>
    public async Task<User> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        var user = this;
        foreach (var reference in expand.Split(','))
        {
            var key = reference.ToLower();
            if (key.Equals("all"))
            {
                foreach (var expander in this.userAddOn.ModelExpanders)
                {
                    var expandAsync = expander.Value;
                    if (expandAsync == null)
                    {
                        continue;
                    }

                    user = await expandAsync(this.serviceScopeFactory, user);
                }
            }
            else
            {
                if (!this.userAddOn.ModelExpanders.ContainsKey(key))
                {
                    continue;
                }

                var expandAsync = this.userAddOn.ModelExpanders[key];
                if (expandAsync == null)
                {
                    continue;
                }

                user = await expandAsync(this.serviceScopeFactory, user);
            }
        }

        return user;
    }
}