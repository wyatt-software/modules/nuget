﻿// <copyright file="User.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Models;

using System;
using System.Threading.Tasks;

/// <summary>
/// Identity user domain model.
/// </summary>
public partial class User
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidateUsernameAsync();
        await this.ValidateEmailAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateUsernameAsync()
    {
        var unique = await this.userRepository.UsernameIsUniqueAsync(this.Username, this.Id);
        if (!unique)
        {
            throw new ArgumentException($"Username \"{this.Username}\" is taken.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateEmailAsync()
    {
        var unique = await this.userRepository.EmailIsUniqueAsync(this.Email, this.Id);
        if (!unique)
        {
            throw new ArgumentException($"Email \"{this.Email}\" is taken.");
        }
    }
}