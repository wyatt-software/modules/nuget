﻿// <copyright file="IValidationService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Services;

using System;
using System.Threading.Tasks;

/// <summary>
/// Validation business logic.
/// </summary>
public interface IValidationService
{
    /// <summary>
    /// Validates an email address.
    /// </summary>
    /// <param name="value">The email address to validate.</param>
    /// <param name="userId">The user the email address belongs to, if known.</param>
    /// <returns>A Tuple containing a success value and error message if invalid.</returns>
    Task<Tuple<bool, string>> ValidateEmailAsync(string value, int userId);

    /// <summary>
    /// Validates a username.
    /// </summary>
    /// <param name="value">The username to validate.</param>
    /// <param name="userId">The user the username belongs to, if known.</param>
    /// <returns>A Tuple containing a success value and error message if invalid.</returns>
    Task<Tuple<bool, string>> ValidateUsernameAsync(string value, int userId);
}
