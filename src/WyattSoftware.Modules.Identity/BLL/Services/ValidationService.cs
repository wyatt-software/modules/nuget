﻿// <copyright file="ValidationService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Services;

using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IValidationService"/> interface.
/// </summary>
public class ValidationService : IValidationService
{
    private readonly IUserRepository userRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="ValidationService"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    public ValidationService(IUserRepository userRepository)
    {
        this.userRepository = userRepository;
    }

    /// <inheritdoc />
    public async Task<Tuple<bool, string>> ValidateEmailAsync(string value, int userId)
    {
        var valid = EmailIsValid(value);
        var unique = await this.userRepository.EmailIsUniqueAsync(value, userId);

        var message = !valid
            ? "Email address is invalid"
            : !unique
                ? "Email address is already taken."
                : string.Empty;

        return new Tuple<bool, string>(valid && unique, message);
    }

    /// <inheritdoc />
    public async Task<Tuple<bool, string>> ValidateUsernameAsync(string value, int userId)
    {
        var valid = UsernameIsValid(value);
        var unique = await this.userRepository.UsernameIsUniqueAsync(value, userId);

        var message = !valid
            ? "Only letters and numbers allowed"
            : !unique
                ? "Username is already taken."
                : string.Empty;

        return new Tuple<bool, string>(valid && unique, message);
    }

    /// <summary>
    /// Returns true if the email address is valid.
    /// </summary>
    /// <param name="email">The email address to test.</param>
    /// <returns>A value indicating whether the email address is valid.</returns>
    private static bool EmailIsValid(string email)
    {
        try
        {
            var mailAddress = new MailAddress(email);
            return mailAddress.Address == email;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Determine if a username is valid.
    /// </summary>
    /// <param name="username">The username to check.</param>
    /// <returns><c>true</c> if valid.</returns>
    private static bool UsernameIsValid(string username)
    {
        var rgx = new Regex(User.UsernameValidation);
        return rgx.IsMatch(username);
    }
}
