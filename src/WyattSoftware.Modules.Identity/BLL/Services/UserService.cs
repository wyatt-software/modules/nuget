﻿// <copyright file="UserService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Default implementation of the <see cref="IUserService"/> interface.
/// </summary>
public partial class UserService : BaseDomainService<User, UserEntity>, IUserService
{
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IUserAddOn userAddOn;
    private readonly IUserRepository userRepository;
    private readonly IUserFactory userFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserService"/> class.
    /// </summary>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    public UserService(
        IServiceScopeFactory serviceScopeFactory,
        IUserAddOn userAddOn,
        IUserRepository userRepository,
        IUserFactory userFactory)
        : base(userRepository, userFactory)
    {
        this.serviceScopeFactory = serviceScopeFactory;
        this.userAddOn = userAddOn;
        this.userRepository = userRepository;
        this.userFactory = userFactory;
    }

    /// <inheritdoc/>
    /// <remarks>
    /// Use the override that takes a search string instead, to only count active users (with the ForumMember role).
    /// </remarks>
    public new Task<int> GetCountAsync()
    {
        throw new NotImplementedException();
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(string search = "")
    {
        return await this.userRepository.GetCountAsync(search);
    }

    /// <inheritdoc/>
    public async Task<PaginatedResponse<User>> FetchAsync(
        string search = "", string expand = "", int offset = 0, int limit = 20, string sort = "-lastloggedon")
    {
        var total = await this.userRepository.GetCountAsync(search);
        var userEntities = await this.userRepository.FetchAsync(search, offset, limit, sort);
        var results = userEntities.Select(this.userFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<User>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <summary>
    /// Attempts to get a user by thier primary key.
    /// </summary>
    /// <param name="userId">The primary key of the user.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <returns>A single user.</returns>
    public async Task<User> ReadAsync(int userId, string expand = "")
    {
        if (userId == 0)
        {
            throw new ArgumentException("You must provide a userId.");
        }

        var userEntity = await this.userRepository.ReadAsync(userId);
        if (userEntity == null)
        {
            throw new KeyNotFoundException($"User \"{userId}\" not found.");
        }

        var user = this.userFactory.New(userEntity);
        await user.ExpandAsync(expand);
        return user;
    }

    /// <inheritdoc/>
    public async Task<User> ReadAsync(
        string usernameOrEmail, string expand = "", bool usernameIsCaseSensitive = true)
    {
        if (string.IsNullOrEmpty(usernameOrEmail))
        {
            throw new ArgumentException("You must provide a username or email.");
        }

        var userEntity = await this.userRepository.ReadAsync(usernameOrEmail, usernameIsCaseSensitive);
        if (userEntity == null)
        {
            throw new KeyNotFoundException($"User \"{usernameOrEmail}\" not found.");
        }

        var user = this.userFactory.New(userEntity);
        await user.ExpandAsync(expand);
        return user;
    }

    /// <inheritdoc/>
    public async Task<User?> GetByUsernameAndPasswordAsync(string usernameOrEmail, string password)
    {
        if (string.IsNullOrEmpty(usernameOrEmail))
        {
            throw new ArgumentException("You must provide a username or email.");
        }

        var userEntity = await this.userRepository.ReadAsync(usernameOrEmail);
        if (userEntity == null)
        {
            // Don't throw an exception.
            // For security reasons, we don't want to let anyone know why it failed.
            return null;
        }

        var user = this.userFactory.New(userEntity);
        if (!user.CheckPassword(password))
        {
            // Don't throw an exception.
            // For security reasons, we don't want to let anyone know why it failed.
            return null;
        }

        return user;
    }
}
