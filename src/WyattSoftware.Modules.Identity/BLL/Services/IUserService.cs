﻿// <copyright file="IUserService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Business logic for the account user.
/// </summary>
public interface IUserService : IDomainService<User>
{
    /// <summary>
    /// Gets the total number of users that satisfy the given search criteria.
    /// </summary>
    /// <param name="search">Searches by part of username.</param>
    /// <returns>The number tof active users (with the ForumMember role).</returns>
    Task<int> GetCountAsync(string search = "");

    /// <summary>
    /// Get all users who's usernames match the given search string.
    /// </summary>
    /// <param name="search">Searches by part of username.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <param name="sort">Sorts results by a given field (with a '-' prefix when descending).</param>
    /// <returns>A collection of users.</returns>
    Task<PaginatedResponse<User>> FetchAsync(
        string search = "", string expand = "", int offset = 0, int limit = 20, string sort = "-lastloggedon");

    /// <summary>
    /// Attempts to get a user by thier primary key.
    /// </summary>
    /// <param name="userId">The primary key of the user.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <returns>A single user.</returns>
    Task<User> ReadAsync(int userId, string expand = "");

    /// <summary>
    /// Gets a single user.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email of the user.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|image] .</para>
    /// </param>
    /// <param name="usernameIsCaseSensitive">Match case.</param>
    /// /// <returns>The user.</returns>
    Task<User> ReadAsync(string usernameOrEmail, string expand = "", bool usernameIsCaseSensitive = true);

    /// <summary>
    /// Gets the specified user only if the password is correct.
    /// </summary>
    /// <param name="usernameOrEmail">The username or email address of the user to check.</param>
    /// <param name="password">The password to check.</param>
    /// <returns>The user data entity, or null if not found or the password was incorrect.</returns>
    Task<User?> GetByUsernameAndPasswordAsync(string usernameOrEmail, string password);
}
