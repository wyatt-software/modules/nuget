﻿// <copyright file="UserService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Services;

using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class UserService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="users">The collection of identity users to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all] or any key registered by other modules. Eg: "image" .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The users, expanded.</returns>
    private async Task<List<User>> ExpandAsync(List<User> users, string expand = "")
    {
        if (string.IsNullOrEmpty(expand))
        {
            return users;
        }

        foreach (var reference in expand.Split(','))
        {
            var key = reference.ToLower();
            if (key.Equals("all"))
            {
                foreach (var listExpander in this.userAddOn.ModelListExpanders)
                {
                    var expandAsync = listExpander.Value;
                    if (expandAsync == null)
                    {
                        continue;
                    }

                    users = await expandAsync(this.serviceScopeFactory, users);
                }
            }
            else
            {
                if (!this.userAddOn.ModelListExpanders.ContainsKey(key))
                {
                    continue;
                }

                var expandAsync = this.userAddOn.ModelListExpanders[key];
                if (expandAsync == null)
                {
                    continue;
                }

                users = await expandAsync(this.serviceScopeFactory, users);
            }
        }

        return users;
    }
}
