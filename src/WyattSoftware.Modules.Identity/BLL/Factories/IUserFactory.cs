﻿// <copyright file="IUserFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Entities;

/// <summary>
/// Provides a way to create users without passing all dependencies every time.
/// </summary>
public interface IUserFactory : IDomainFactory<User, UserEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="User"/> class.
    /// </summary>
    /// <param name="username">The username.</param>
    /// <param name="password">The password to create the new user with.</param>
    /// <param name="email">The email address.</param>
    /// <remarks>Required properties are validated upon construction.</remarks>
    /// <returns>A new <see cref="User"/>, injected with the dependencies it needs.</returns>.
    User New(string username, string password, string email);

    /// <summary>
    /// Initializes a new instance of the <see cref="User"/> class.
    /// </summary>
    /// <param name="userEntity">The entity to construct the user from.</param>
    /// <returns>
    /// A <see cref="User"/> based on the <see cref="UserEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new User New(UserEntity userEntity);
}
