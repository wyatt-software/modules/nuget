﻿// <copyright file="UserFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Factories;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.Features.Email;
using WyattSoftware.Modules.Core.BLL.Features.Macros;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Identity.Infrastructure;
using WyattSoftware.Modules.Identity.Options;

/// <summary>
/// Default implementation of the <see cref="IUserFactory"/> interface.
/// </summary>
public class UserFactory : IUserFactory
{
    private readonly IOptions<CoreOptions> coreOptions;
    private readonly IOptions<IdentityOptions> identityOptions;
    private readonly IUserAddOn userAddOn;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IEmailSender emailSender;
    private readonly IMacroResolver macroResolver;
    private readonly IEmailTemplateRepository emailTemplateRepository;
    private readonly IUserRepository userRepository;
    private readonly IUserRoleRepository userRoleRepository;

    /// <summary>
    /// Initializes a new instance of the <see cref="UserFactory"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="identityOptions">Injected IOptions{IdentityOptions}.</param>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="emailSender">Injected IEmailSender.</param>
    /// <param name="macroResolver">Injected IMacroResolver.</param>
    /// <param name="emailTemplateRepository">Injected IEmailTemplateRepository.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="userRoleRepository">Injected IUserRoleRepository.</param>
    public UserFactory(
        IOptions<CoreOptions> coreOptions,
        IOptions<IdentityOptions> identityOptions,
        IUserAddOn userAddOn,
        IServiceScopeFactory serviceScopeFactory,
        IEmailSender emailSender,
        IMacroResolver macroResolver,
        IEmailTemplateRepository emailTemplateRepository,
        IUserRepository userRepository,
        IUserRoleRepository userRoleRepository)
    {
        this.coreOptions = coreOptions;
        this.identityOptions = identityOptions;
        this.userAddOn = userAddOn;
        this.serviceScopeFactory = serviceScopeFactory;
        this.emailSender = emailSender;
        this.macroResolver = macroResolver;
        this.emailTemplateRepository = emailTemplateRepository;
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }

    /// <inheritdoc />
    public User New(string username, string password, string email)
    {
        return new User(
            this.coreOptions,
            this.identityOptions,
            this.userAddOn,
            this.serviceScopeFactory,
            this.emailSender,
            this.macroResolver,
            this.emailTemplateRepository,
            this.userRepository,
            this.userRoleRepository,
            username,
            password,
            email);
    }

    /// <inheritdoc />
    public User New(UserEntity userEntity)
    {
        return new User(
            this.coreOptions,
            this.identityOptions,
            this.userAddOn,
            this.serviceScopeFactory,
            this.emailSender,
            this.macroResolver,
            this.emailTemplateRepository,
            this.userRepository,
            this.userRoleRepository,
            userEntity);
    }
}
