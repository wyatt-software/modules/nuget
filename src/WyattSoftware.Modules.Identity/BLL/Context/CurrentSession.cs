﻿// <copyright file="CurrentSession.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Context;

using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

/// <summary>
/// Default implementation, based on HttpContext.
/// </summary>
public class CurrentSession : ICurrentSession
{
    private readonly IHttpContextAccessor httpContextAccessor;

    /// <summary>
    /// Initializes a new instance of the <see cref="CurrentSession"/> class.
    /// </summary>
    /// <param name="httpContextAccessor">Injected IHttpContextAccessor.</param>
    public CurrentSession(IHttpContextAccessor httpContextAccessor)
    {
        this.httpContextAccessor = httpContextAccessor;
    }

    /// <inheritdoc />
    public int UserId => int.Parse(this.HttpUser?
        .FindFirst(x => x.Type == ClaimTypes.NameIdentifier)?.Value ?? "0");

    /// <inheritdoc />
    public string[] Roles => this.HttpUser?.Claims != null
        ? this.HttpUser.Claims
            .Where(x => x.Type == ClaimTypes.Role)
            .Select(x => x.Value)
            .ToArray()
        : Array.Empty<string>();

    /// <summary>
    /// Gets the currently authenticated AUR's username (or an empty string).
    /// </summary>
    private ClaimsPrincipal? HttpUser => this.httpContextAccessor?.HttpContext?.User;

    /// <inheritdoc />
    public void AssertResourceOwnerOrRole(int userId, string roleCodeName, string? message = null)
    {
        if (userId != this.UserId && !this.Roles.Contains(roleCodeName))
        {
            throw new UnauthorizedAccessException(
                message ?? $"Must be either the resource owner or in the \"{roleCodeName}\" role.");
        }
    }
}
