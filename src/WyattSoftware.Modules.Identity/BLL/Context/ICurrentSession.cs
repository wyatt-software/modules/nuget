﻿// <copyright file="ICurrentSession.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Identity.BLL.Context;

using System;

/// <summary>
/// Provides context for the current session.
/// </summary>
public interface ICurrentSession
{
    /// <summary>
    /// Gets the authenticated user to attempt actions on behalf of.
    /// </summary>
    int UserId { get; }

    /// <summary>
    /// Gets an array of roles the authenticated user has.
    /// </summary>
    string[] Roles { get; }

    /// <summary>
    /// Throws an <see cref=" UnauthorizedAccessException" /> if not either the resource owner or in the given role.
    /// </summary>
    /// <param name="userId">The real owner of the resource being checked.</param>
    /// <param name="roleCodeName">A role that has permission to access the resource anyway.</param>
    /// <param name="message">An alternative message.</param>
    /// <remarks>
    /// Use this in the API layer when simply authorizing by role isn't enough. Eg: We don't want users deleting
    /// each other's images etc.
    /// </remarks>
    void AssertResourceOwnerOrRole(int userId, string roleCodeName, string? message = null);
}
