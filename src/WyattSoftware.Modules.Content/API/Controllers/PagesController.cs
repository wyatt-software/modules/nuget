﻿// <copyright file="PagesController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.API.Controllers;

using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Content.API.DTOs;
using WyattSoftware.Modules.Content.BLL.Factories;
using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Content.BLL.Services;

/// <summary>
/// Provides a way to retrieve an individual page with it's content.
/// </summary>
[Route("content/pages")]
public class PagesController : ControllerBase
{
    private readonly IPageService pageService;
    private readonly IPageFactory pageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="PagesController"/> class.
    /// </summary>
    /// <param name="pagesService">Injected PageService.</param>
    /// <param name="pageFactory">Injected IPageFactory.</param>
    public PagesController(
        IPageService pagesService,
        IPageFactory pageFactory)
    {
        this.pageService = pagesService;
        this.pageFactory = pageFactory;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Page, PageResponse>(message =>
        {
            var response = new PageResponse();
            response.InjectFrom(message);

            return response;
        });
    }

    /// <summary>
    /// Creates an individual page with it's content.
    /// </summary>
    /// <param name="request">The page request DTO.</param>
    /// <returns>The page.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Create([FromBody]PageCreateRequest request)
    {
        var page = this.pageFactory.New(
            request.ParentId,
            request.Alias,
            request.Title,
            request.IconClass,
            request.Component,
            request.Redirect,
            request.MenuItem,
            request.Content,
            request.RequiresAuthentication);

        await page.CreateAsync();

        var response = Mapper.Map<PageResponse>(page);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets an individual page with it's content.
    /// </summary>
    /// <param name="pageId">The Id (primary key) of the page.</param>
    /// <returns>The page.</returns>
    [HttpGet]
    [Route("{pageId:int}")]
    public async Task<IActionResult> Read(int pageId)
    {
        var page = await this.pageService.ReadAsync(pageId);
        var response = Mapper.Map<PageResponse>(page);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a content page.
    /// </summary>
    /// <param name="pageId">The primary key of the page.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated content page.</returns>
    [HttpPut]
    [Route("{pageId:int}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Update(int pageId, [FromBody] PageUpdateRequest request)
    {
        var page = await this.pageService.ReadAsync(pageId);

        page.Content = request.Content;
        page.ParentId = request.ParentId;
        page.Alias = request.Alias;
        page.Title = request.Title;
        page.IconClass = request.IconClass;
        page.Component = request.Component;
        page.Redirect = request.Redirect;
        page.MenuItem = request.MenuItem;
        page.Content = request.Content;
        page.RequiresAuthentication = request.RequiresAuthentication;

        await page.UpdateAsync();

        var response = Mapper.Map<PageResponse>(page);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a content page.
    /// </summary>
    /// <param name="pageId">The primary key of the page.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{pageId:int}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Delete(int pageId)
    {
        var page = await this.pageService.ReadAsync(pageId);
        await page.DeleteAsync();
        return this.Ok();
    }
}
