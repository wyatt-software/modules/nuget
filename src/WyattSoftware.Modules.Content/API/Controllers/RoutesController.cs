﻿// <copyright file="RoutesController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Content.API.DTOs;
using WyattSoftware.Modules.Content.BLL.Services;

/// <summary>
/// Provides a way to retrieve the page tree without any content.
/// </summary>
[Route("content/routes")]
public class RoutesController : ControllerBase
{
    private readonly IPageService pageService;

    /// <summary>
    /// Initializes a new instance of the <see cref="RoutesController"/> class.
    /// </summary>
    /// <param name="pageService">Injected PagesService.</param>
    public RoutesController(IPageService pageService)
    {
        this.pageService = pageService;
    }

    /// <summary>
    /// Get all pages in the page tree, without their content.
    /// </summary>
    /// <returns>A collection of page routes.</returns>
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch()
    {
        var routes = await this.pageService.FetchRoutesAsync();
        var response = routes.Select(route => Mapper.Map<RouteResponse>(route)).ToList();
        return this.Ok(response);
    }
}