﻿// <copyright file="RouteResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.API.DTOs;

/// <summary>
/// Route response.
/// </summary>
/// <remarks>Mapped from the <see cref="Route" /> model.</remarks>
public class RouteResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the the order of this page under its parent.
    /// </summary>
    public int SortOrder { get; set; }

    /// <summary>
    /// Gets or sets the code name of this page.
    /// </summary>
    /// <remarks>
    /// This must be a globally unique code name (for use in Vue-router named routes).
    /// </remarks>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the name of the parent page.
    /// </summary>
    public string Parent { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the relative path of this page (URL).
    /// </summary>
    public string Path { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the title of this page.
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether to show this page in menus.
    /// </summary>
    public bool MenuItem { get; set; }

    /// <summary>
    /// Gets or sets the fontawesome.com icon class to use for the page.
    /// </summary>
    public string IconClass { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    public string Component { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    /// <remarks>Optional.</remarks>
    public string Redirect { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether this page requires the user to be authenticated.
    /// </summary>
    public bool RequiresAuthentication { get; set; }

    /// <summary>
    /// Gets or sets a pip-separated list of roles the user is required to have to access this page.
    /// </summary>
    public string RequiredRoles { get; set; } = string.Empty;
}