﻿// <copyright file="PageUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.API.DTOs;

/// <summary>
/// Page update request.
/// </summary>
/// <remarks>Mapped to the <see cref="Page" /> model.</remarks>
public class PageUpdateRequest
{
    /// <summary>
    /// Gets or sets the parent page.
    /// </summary>
    public int? ParentId { get; set; }

    /// <summary>
    /// Gets or sets the alias of the page (used in URLs).
    /// </summary>
    public string Alias { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the title of the page.
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the fontawesome.com icon class to use for the page.
    /// </summary>
    public string IconClass { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the Vue.js component to use for the page.
    /// </summary>
    public string Component { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    /// <remarks>Optional.</remarks>
    public string Redirect { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether to show the page in auto-generated menus.
    /// </summary>
    public bool MenuItem { get; set; }

    /// <summary>
    /// Gets or sets the page content to use if using a Page component.
    /// </summary>
    public string Content { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether this page requires the user to be authenticated.
    /// </summary>
    public bool RequiresAuthentication { get; set; }
}