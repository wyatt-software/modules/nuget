﻿// <copyright file="IPageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Services;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;

/// <summary>
/// Business logic for the account user.
/// </summary>
public interface IPageService : IDomainService<Page>
{
    /// <summary>
    /// Fetches pages that satisfy conditions.
    /// </summary>
    /// <param name="requiresAuthentication">If provided, filters by <c>requires_authentication</c>.</param>
    /// <param name="component">If provided, filters by <c>component</c>.</param>
    /// <param name="hasContent">If provided, filters by whether <c>content</c> is empty or not.</param>
    /// <returns>A list of matching page entities.</returns>
    Task<List<Page>> FetchAsync(
        bool? requiresAuthentication = null, string? component = null, bool? hasContent = null);

    /// <summary>
    /// Get all pages in the page tree, without their content.
    /// </summary>
    /// <returns>A collection of page routes.</returns>
    Task<List<Route>> FetchRoutesAsync();

    /// <summary>
    /// Maintains the sort order values for all pages under the given parent (null for the root level).
    /// </summary>
    /// <param name="parentId">The ID of the parent to maintain all child pages for.</param>
    /// <returns>Nothing.</returns>
    Task MaintainSortOrderAsync(int? parentId);
}
