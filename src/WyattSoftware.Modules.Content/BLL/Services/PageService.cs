﻿// <copyright file="PageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("WyattSoftware.Modules.Content.Tests")]

namespace WyattSoftware.Modules.Content.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Content.BLL.Factories;
using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Content.DAL.Repositories;
using WyattSoftware.Modules.Core.BLL.Services;

/// <summary>
/// Default implementation of the <see cref="IPageService"/> interface.
/// </summary>
public class PageService : BaseDomainService<Page, PageEntity>, IPageService
{
    private readonly IPageRepository pageRepository;
    private readonly IPageFactory pageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="PageService"/> class.
    /// </summary>
    /// <param name="pageRepository">Injected IPageRepository.</param>
    /// <param name="pageFactory">Injected IPageFactory.</param>
    public PageService(
        IPageRepository pageRepository,
        IPageFactory pageFactory)
        : base(pageRepository, pageFactory)
    {
        this.pageRepository = pageRepository;
        this.pageFactory = pageFactory;
    }

    /// <inheritdoc />
    public async Task<List<Page>> FetchAsync(
        bool? requiresAuthentication = null, string? component = null, bool? hasContent = null)
    {
        var pageEntities = await this.pageRepository.FetchAsync(
            requiresAuthentication, component, hasContent);

        return pageEntities.Select(x => this.pageFactory.New(x)).ToList();
    }

    /// <inheritdoc />
    public async Task<List<Route>> FetchRoutesAsync()
    {
        var routeQueryResults = await this.pageRepository.FetchRoutesAsync();
        return routeQueryResults.Select(x => new Route(x)).ToList();
    }

    /// <inheritdoc />
    public async Task MaintainSortOrderAsync(int? parentId)
    {
        var children = await this.pageRepository.FetchChildPagesAsync(parentId);
        var modifiedChildren = GetUnsortedPages(children);
        foreach (var childPageEntity in modifiedChildren)
        {
            var page = this.pageFactory.New(childPageEntity);
            await page.UpdateAsync();
        }
    }

    /// <summary>
    /// Takes a list of pages, and returns the pages with sort orders that needed updating.
    /// </summary>
    /// <param name="pageEntites">The list of pages (must all belong to the same parent).</param>
    /// <returns>
    /// A list of pages that needed modifying, with their sortorders updated (but not yet persisted).
    /// </returns>
    /// <remarks>
    /// This is static so that it's unit testable.</remarks>
    internal static List<PageEntity> GetUnsortedPages(List<PageEntity> pageEntites)
    {
        var modifiedPageEntities = new List<PageEntity>();
        for (var index = 0; index < pageEntites.Count; index++)
        {
            var sortOrder = index + 1;
            var pageEntity = pageEntites[index];
            if (pageEntity.SortOrder != sortOrder)
            {
                pageEntity.SortOrder = sortOrder;
                modifiedPageEntities.Add(pageEntity);
            }
        }

        return modifiedPageEntities;
    }
}
