﻿// <copyright file="IPageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Factories;

using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Core.BLL.Factories;

/// <summary>
/// Provides a way to create pages without passing all dependencies every time.
/// </summary>
public interface IPageFactory : IDomainFactory<Page, PageEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Page"/> class.
    /// </summary>
    /// <param name="parentId">Foreign key to another page.</param>
    /// <param name="alias">Alias of the page (used in URLs).</param>
    /// <param name="title">Title of the page.</param>
    /// <param name="iconClass">fontawesome.com icon class to use for the page.</param>
    /// <param name="component">Vue.js component to use for the page.</param>
    /// <param name="redirect">URL to redirect to instead of a Vue.js component.</param>
    /// <param name="menuItem">Show the page in auto-generated menus.</param>
    /// <param name="content">Content to use if using a Page component.</param>
    /// <param name="requiresAuthentication">Requires the user to be authenticated.</param>
    /// <returns>A new <see cref="Page"/>, injected with the dependencies it needs.</returns>.
    Page New(
        int? parentId,
        string alias,
        string title,
        string iconClass,
        string component,
        string redirect,
        bool menuItem,
        string content,
        bool requiresAuthentication);

    /// <summary>
    /// Initializes a new instance of the <see cref="Page"/> class.
    /// </summary>
    /// <param name="categoryEntity">The entity to construct the page from.</param>
    /// <returns>
    /// A <see cref="Page"/> based on the <see cref="PageEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new Page New(PageEntity categoryEntity);
}