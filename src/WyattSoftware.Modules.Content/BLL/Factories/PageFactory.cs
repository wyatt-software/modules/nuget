﻿// <copyright file="PageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Factories;

using System;
using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Content.BLL.Services;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Content.DAL.Repositories;
using WyattSoftware.Modules.Search.BLL.Factories;

/// <summary>
/// Default implementation of the <see cref="IPageFactory"/> interface.
/// </summary>
public class PageFactory : IPageFactory
{
    private readonly IPageRepository pageRepository;
    private readonly Lazy<IPageService> pageService;
    private readonly ISearchDocFactory searchDocFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="PageFactory"/> class.
    /// </summary>
    /// <param name="pageRepository">Injected IPageRepository.</param>
    /// <param name="pageService">Injected IPageService.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    public PageFactory(
        IPageRepository pageRepository,
        Lazy<IPageService> pageService,
        ISearchDocFactory searchDocFactory)
    {
        this.pageRepository = pageRepository;
        this.pageService = pageService;
        this.searchDocFactory = searchDocFactory;
    }

    /// <inheritdoc />
    public Page New(
        int? parentId,
        string alias,
        string title,
        string iconClass,
        string component,
        string redirect,
        bool menuItem,
        string content,
        bool requiresAuthentication)
    {
        return new Page(
            this.pageRepository,
            this.pageService.Value,
            this.searchDocFactory,
            parentId,
            alias,
            title,
            iconClass,
            component,
            redirect,
            menuItem,
            content,
            requiresAuthentication);
    }

    /// <inheritdoc />
    public Page New(PageEntity pageEntity)
    {
        return new Page(this.pageRepository, this.pageService.Value, this.searchDocFactory, pageEntity);
    }
}