﻿// <copyright file="Page.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Models;

using System;
using System.Threading.Tasks;
using WyattSoftware.Modules.Content.DAL.QueryResults;
using WyattSoftware.Modules.Core.BLL.Helpers;

/// <summary>
/// Gallery album domain model.
/// </summary>
public partial class Page
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidatateParentIdAsync();
        await this.ValidatateAliasAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateParentIdAsync()
    {
        if (!this.ParentId.HasValue)
        {
            // This page is an orphan. No need to validate the parent.
            return;
        }

        var parentExists = await this.pageRepository.ExistsAsync(this.ParentId.Value);
        if (!parentExists)
        {
            throw new ArgumentException($"Parent page {this.ParentId.Value} not found.");
        }

        await this.CheckForCircularReference();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateAliasAsync()
    {
        var existingPageEntity = await this.pageRepository.ReadAsync(this.ParentId, this.Alias);
        if (existingPageEntity != null && existingPageEntity.Id != this.Id)
        {
            var parentText = "(root)";
            if (this.ParentId.HasValue)
            {
                var routes = await this.pageRepository.FetchRoutesAsync();
                var parentRoute = routes.Find(x => x.Id == this.ParentId);
                parentText = parentRoute != null ? parentRoute.Path : this.ParentId.ToString();
            }

            throw new ArgumentException($"Alias \"{this.Alias}\" is taken under parent \"{parentText}\".");
        }
    }

    /// <summary>
    /// Checks to see if the parent is a child of the current page.
    /// </summary>
    /// <returns>Nothing.</returns>
    /// <exception cref="ArgumentException">Thrown if a circular reference exists.</exception>
    private async Task CheckForCircularReference()
    {
        if (this.Id == 0)
        {
            // There's no chance of a circular reference when adding new pages, since they don't have children.
            return;
        }

        var routes = await this.pageRepository.FetchRoutesAsync();
        var thisRoute = routes.Find(x => x.Id == this.Id);
        if (thisRoute == null)
        {
            throw new ArgumentException("Couldn't find page within routes.");
        }

        if (HierarchyHelper.HasCircularReference(
            routes,
            thisRoute.Parent,
            route => route.Name,
            route => route.Parent,
            name => string.IsNullOrEmpty(name)))
        {
            throw new ArgumentException("Circular reference encountered when setting parent page.");
        }
    }
}