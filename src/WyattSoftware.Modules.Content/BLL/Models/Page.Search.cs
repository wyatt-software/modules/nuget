﻿// <copyright file="Page.Search.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Models;

using WyattSoftware.Modules.Core.BLL.Extensions;
using WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Content page domain model.
/// </summary>
public partial class Page
{
    /// <inheritdoc />
    public Task<SearchDoc> MapToSearchDocAsync()
    {
        var searchDoc = this.searchDocFactory.New(
            $"page|{this.Id}",
            "page",
            this.Title,
            this.Path,
            new Dictionary<string, string>()
            {
                { "title", this.Title },
                { "content", this.Content.ToPlainText() },
            });

        // The method signature accomodated async/await, but we don't need it for this simple case.
        return Task.FromResult(searchDoc);
    }
}
