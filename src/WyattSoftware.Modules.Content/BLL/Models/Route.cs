﻿// <copyright file="Route.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Models;

using WyattSoftware.Modules.Content.DAL.QueryResults;
using WyattSoftware.Modules.Core.BLL.Helpers;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Content route domain model.
/// </summary>
public class Route : RouteQueryResult
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Route"/> class.
    /// </summary>
    /// <param name="routeEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Route(
        RouteQueryResult routeEntity)
    {
        this.Id = routeEntity.Id;
        this.SortOrder = routeEntity.SortOrder;
        this.Name = routeEntity.Name;
        this.Parent = routeEntity.Parent;
        this.Path = routeEntity.Path;
        this.Title = routeEntity.Title;
        this.MenuItem = routeEntity.MenuItem;
        this.IconClass = routeEntity.IconClass;
        this.Component = routeEntity.Component;
        this.Redirect = routeEntity.Redirect;
        this.RequiresAuthentication = routeEntity.RequiresAuthentication;

        // Roles are read from the repository as a comma-separated list of values (eg: "1,2").
        // We convert them to their enum string representations (eg: "ForumMember,ForumModerator") so that
        // authorization attributes on controllers and endpoints in the API are more human readable.
        this.RequiredRoles = EnumHelper.ConvertValuesToNamed<Role>(routeEntity.RequiredRoles);
    }
}
