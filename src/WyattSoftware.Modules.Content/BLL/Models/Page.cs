﻿// <copyright file="Page.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.BLL.Models;

using System;
using System.Threading.Tasks;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Content.BLL.Services;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Content.DAL.Repositories;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Search.BLL.Factories;
using WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Content page domain model.
/// </summary>
public partial class Page : PageEntity, IDomainModel, IIndexableModel
{
    private readonly IPageRepository pageRepository;
    private readonly IPageService pageService;
    private readonly ISearchDocFactory searchDocFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Page"/> class.
    /// </summary>
    /// <param name="pageRepository">Injected IPageRepository.</param>
    /// <param name="pageService">Injected IPageService.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="parentId">Id of another page.</param>
    /// <param name="alias">Alias of the page (used in URLs). Generated from the name if omitted.</param>
    /// <param name="title">Name title of the page.</param>
    /// <param name="iconClass">fontawesome.com icon class to use for the page.</param>
    /// <param name="component">Vue.js component to use for the page.</param>
    /// <param name="redirect">URL to redirect to instead of a Vue.js component.</param>
    /// <param name="menuItem">Show the page in auto-generated menus.</param>
    /// <param name="content">Content to use if using a Page component.</param>
    /// <param name="requiresAuthentication">Requires the user to be authenticated.</param>
    internal Page(
        IPageRepository pageRepository,
        IPageService pageService,
        ISearchDocFactory searchDocFactory,
        int? parentId,
        string alias,
        string title,
        string iconClass,
        string component,
        string redirect,
        bool menuItem,
        string content,
        bool requiresAuthentication)
    {
        this.pageRepository = pageRepository;
        this.pageService = pageService;
        this.searchDocFactory = searchDocFactory;

        this.ParentId = parentId;

        this.Alias = alias;
        this.Title = title;
        this.IconClass = iconClass;
        this.Component = component;
        this.Redirect = redirect;
        this.MenuItem = menuItem;
        this.Content = content;
        this.RequiresAuthentication = requiresAuthentication;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Page"/> class.
    /// </summary>
    /// <param name="pageRepository">Injected IPageRepository.</param>
    /// <param name="pageService">Injected IPageService.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="pageEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Page(
        IPageRepository pageRepository,
        IPageService pageService,
        ISearchDocFactory searchDocFactory,
        PageEntity pageEntity)
    {
        this.pageRepository = pageRepository;
        this.pageService = pageService;
        this.searchDocFactory = searchDocFactory;

        this.Id = pageEntity.Id;
        this.Created = pageEntity.Created;
        this.Modified = pageEntity.Modified;

        this.ParentId = pageEntity.ParentId;

        this.Alias = pageEntity.Alias;
        this.Title = pageEntity.Title;
        this.IconClass = pageEntity.IconClass;
        this.Component = pageEntity.Component;
        this.Redirect = pageEntity.Redirect;
        this.MenuItem = pageEntity.MenuItem;
        this.Content = pageEntity.Content;
        this.RequiresAuthentication = pageEntity.RequiresAuthentication;
        this.SortOrder = pageEntity.SortOrder;

        this.Path = pageEntity.Path;
    }

    /// <summary>
    /// Creates a content page.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        if (string.IsNullOrWhiteSpace(this.Alias))
        {
            this.Alias = await this.pageRepository.GenerateUniqueAliasAsync(this.ParentId, this.Title);
        }

        await this.ValidateAsync();

        var pageEntity = Mapper.Map<PageEntity>(this);
        this.Id = await this.pageRepository.CreateAsync(pageEntity);

        await this.pageService.MaintainSortOrderAsync(this.ParentId);

        var freshData = await this.pageService.ReadAsync(this.Id);
        this.SortOrder = freshData.SortOrder;
    }

    /// <summary>
    /// Updates a content page.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var pageEntity = Mapper.Map<PageEntity>(this);
        await this.pageRepository.UpdateAsync(pageEntity);

        await this.pageService.MaintainSortOrderAsync(this.ParentId);
    }

    /// <summary>
    /// Attempts to delete a page by it's primary key.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        await this.pageRepository.DeleteAsync(this.Id);

        await this.pageService.MaintainSortOrderAsync(this.ParentId);
    }
}
