﻿// <copyright file="IPageRoleRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Repositories;

using System.Threading.Tasks;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Interface defining custom methods in the content page / security role binding class repository.
/// </summary>
public interface IPageRoleRepository : IRepository<PageRoleEntity>
{
    /// <summary>
    /// Gets the binding object for the given user and role.
    /// </summary>
    /// <param name="pageId">The content page.</param>
    /// <param name="contentRole">The role.</param>
    /// <returns>The binding object.</returns>
    Task<PageRoleEntity> ReadAsync(int pageId, Role contentRole);
}