﻿// <copyright file="PageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Repositories;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using SqlKata.Execution;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Content.DAL.QueryResults;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Dapper implementation of the content page repository.
/// </summary>
public class PageRepository : BaseRepository<PageEntity>, IPageRepository
{
    private readonly string selectWithDerivedFieldsSql = $"CP.*, V.alias_path AS derived__alias_path";

    /// <summary>
    /// Initializes a new instance of the <see cref="PageRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public PageRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc />
    public async Task<List<RouteQueryResult>> FetchRoutesAsync()
    {
        var results = await this.Db.Query("view_content_route").GetAsync<RouteQueryResult>();
        return results.ToList();
    }

    /// <inheritdoc />
    public async Task<List<PageEntity>> FetchAsync(
        bool? requiresAuthentication = null, string? component = null, bool? hasContent = null)
    {
        var query = this.Db.Query(this.Table + " AS CP")
            .SelectRaw(this.selectWithDerivedFieldsSql)
            .Join("view_content_route AS V", "V.id", "CP.id")
            .When(
                requiresAuthentication.HasValue,
                q => q.Where("CP.requires_authentication", requiresAuthentication))
            .When(
                component != null,
                q => q.Where("CP.component", component))
            .When(
                hasContent.HasValue,
                q => q.WhereNot("CP.content", string.Empty));

        var results = await query.GetAsync<PageEntity>();
        return results.ToList();
    }

    /// <inheritdoc />
    public async Task<PageEntity?> ReadAsync(int? parentId, string alias)
    {
        return await this.Db.Query(this.Table)
            .Where("parent_id", parentId)
            .Where("alias", alias)
            .FirstOrDefaultAsync<PageEntity>();
    }

    /// <inheritdoc />
    public async Task<RouteQueryResult?> ReadRouteAsync(int pageId)
    {
        return await this.Db.Query("view_content_route")
            .Where("id", pageId)
            .FirstOrDefaultAsync<RouteQueryResult>();
    }

    /// <inheritdoc />
    public async Task<List<PageEntity>> FetchChildPagesAsync(int? parentId)
    {
        var query = this.Db.Query(this.Table)
            .Where("parent_id", parentId)
            .OrderByRaw("sort_order = 0 DESC, sort_order");

        var results = await query.GetAsync<PageEntity>();
        return results.ToList();
    }

    /// <inheritdoc />
    public async Task<string> GenerateUniqueAliasAsync(int? parentId, string name)
    {
        return await this.Connection.ExecuteScalarAsync<string>(
            "SELECT func_content_page_get_unique_alias(@parentId, @name);", new { parentId, name });
    }
}
