﻿// <copyright file="PageRoleRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Repositories;

using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata.Execution;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Dapper implementation of the content page / security role binding class repository.
/// </summary>
public class PageRoleRepository : BaseRepository<PageRoleEntity>, IPageRoleRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PageRoleRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public PageRoleRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc />
    public async Task<PageRoleEntity> ReadAsync(int pageId, Role contentRole)
    {
        return await this.Db.Query(this.Table)
            .Where("content_page_id", pageId)
            .Where("role", contentRole)
            .FirstOrDefaultAsync();
    }
}