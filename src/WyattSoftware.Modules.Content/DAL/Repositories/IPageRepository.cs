﻿// <copyright file="IPageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Content.DAL.Entities;
using WyattSoftware.Modules.Content.DAL.QueryResults;
using WyattSoftware.Modules.Core.DAL.Repositories;

/// <summary>
/// The interface defining the pages repository.
/// </summary>
public interface IPageRepository : IRepository<PageEntity>
{
    /// <summary>
    /// Fetches the entire page tree.
    /// </summary>
    /// <returns>The entire page tree.</returns>
    Task<List<RouteQueryResult>> FetchRoutesAsync();

    /// <summary>
    /// Fetches pages that satisfy conditions.
    /// </summary>
    /// <param name="requiresAuthentication">If provided, filters by <c>requires_authentication</c>.</param>
    /// <param name="component">If provided, filters by <c>component</c>.</param>
    /// <param name="hasContent">If provided, filters by whether <c>content</c> is empty or not.</param>
    /// <returns>A list of matching page entities.</returns>
    Task<List<PageEntity>> FetchAsync(
        bool? requiresAuthentication = null, string? component = null, bool? hasContent = null);

    /// <summary>
    /// Gets a single page based on it's parent and alias.
    /// </summary>
    /// <param name="parentId">The ID of the page's parent.</param>
    /// <param name="alias">The alias of the page to get.</param>
    /// <returns>A single page.</returns>
    Task<PageEntity?> ReadAsync(int? parentId, string alias);

    /// <summary>
    /// Gets a single page route.
    /// </summary>
    /// <param name="pageId">The ID of the page to get the route for.</param>
    /// <returns>A single page route.</returns>
    Task<RouteQueryResult?> ReadRouteAsync(int pageId);

    /// <summary>
    /// Get all child pages for a given parent (use null for the top level), sorted by sort order (with unsorted
    /// pages at the end).
    /// </summary>
    /// <param name="parentId">The ID of the parent page, or null.</param>
    /// <returns>A list of pages.</returns>
    Task<List<PageEntity>> FetchChildPagesAsync(int? parentId);

    /// <summary>
    /// Gets an alias for the given name, that doesn't conflict with any existing aliases.
    /// </summary>
    /// <param name="parentId">The parent this page belongs to.</param>
    /// <param name="name">The name to generate the alias from.</param>
    /// <returns>An alias for the given name, that doesn't conflict with any existing aliases.</returns>
    Task<string> GenerateUniqueAliasAsync(int? parentId, string name);
}