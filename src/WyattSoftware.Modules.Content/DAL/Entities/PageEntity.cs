﻿// <copyright file="PageEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Attributes;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Content page entity.
/// </summary>
[Table("content_page")]
public class PageEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PageEntity"/> class.
    /// </summary>
    public PageEntity()
    {
        this.ParentId = null;
        this.Alias = string.Empty;
        this.Title = string.Empty;
        this.IconClass = string.Empty;
        this.Component = string.Empty;
        this.Redirect = string.Empty;
        this.MenuItem = false;
        this.Content = string.Empty;
        this.RequiresAuthentication = false;
        this.SortOrder = 0;

        this.Path = string.Empty;
    }

    /// <summary>
    /// Gets or sets the id of the parent page.
    /// </summary>
    [ForeignKey("content_page.id")]
    [Column("parent_id")]
    public int? ParentId { get; set; }

    /// <summary>
    /// Gets or sets the alias of the page (used in URLs).
    /// </summary>
    [Column("alias")]
    [Required]
    [StringLength(200)]
    [RegularExpression(
        @"^[0-9a-z-]+$",
        ErrorMessage = "Only lowercase letters, numbers, and the dash character are allowed.")]
    public string Alias { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the title of the page.
    /// </summary>
    [Column("title")]
    [Required]
    [StringLength(200)]
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the fontawesome.com icon class to use for the page.
    /// </summary>
    [Column("icon_class")]
    [StringLength(50)]
    public string IconClass { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the Vue.js component to use for the page.
    /// </summary>
    [Column("component")]
    [Required]
    [StringLength(50)]
    public string Component { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    /// <remarks>Optional.</remarks>
    [Column("redirect")]
    [StringLength(200)]
    public string Redirect { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether to show the page in auto-generated menus.
    /// </summary>
    [Column("menu_item")]
    public bool MenuItem { get; set; }

    /// <summary>
    /// Gets or sets the page content to use if using a Page component.
    /// </summary>
    [Column("content")]
    [StringLength(65535)] // MySQL text
    public string Content { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether this page requires the user to be authenticated.
    /// </summary>
    [Column("requires_authentication")]
    public bool RequiresAuthentication { get; set; }

    /// <summary>
    /// Gets or sets the order to list the page in, under it's parent.
    /// </summary>
    [Column("sort_order")]
    [Required]
    [Range(1, int.MaxValue)]
    public int SortOrder { get; set; }

    /// <summary>
    /// Gets or sets the relative path of the page within the page tree.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__path")]
    public string Path { get; set; } = string.Empty;
}