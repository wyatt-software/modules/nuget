﻿// <copyright file="PageRoleEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Identity.DAL.Enums;

/// <summary>
/// Content page / security role binding entity.
/// </summary>
[Table("content_page_role")]
public class PageRoleEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PageRoleEntity"/> class.
    /// </summary>
    public PageRoleEntity()
    {
        this.PageId = 0;
        this.Role = 0;
    }

    /// <summary>
    /// Gets or sets the foreign key to the page.
    /// </summary>
    [ForeignKey("content_page.id")]
    [Column("content_page_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int PageId { get; set; }

    /// <summary>
    /// Gets or sets the role.
    /// </summary>
    [Column("role")]
    [Required]
    [EnumDataType(typeof(Role))]
    public int Role { get; set; }
}