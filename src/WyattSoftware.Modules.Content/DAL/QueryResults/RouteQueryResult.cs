﻿// <copyright file="RouteQueryResult.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.DAL.QueryResults;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL;

/// <summary>
/// Content page route entity.
/// </summary>
/// <remarks>This is the result of a view.</remarks>
[Table("view_content_route")]
public class RouteQueryResult : IDataRecord
{
    /// <summary>
    /// Gets or sets the foreign key to the parent page.
    /// </summary>
    [Key]
    [Column("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the the order of this page under its parent.
    /// </summary>
    [Column("sort_order")]
    public int SortOrder { get; set; }

    /// <summary>
    /// Gets or sets a globally unique name for this page, derived from aliases (for use as Vue named routes).
    /// </summary>
    [Column("name")]
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the  globally unique name for this page's parent (or null if it's a root page).
    /// </summary>
    [Column("parent")]
    public string? Parent { get; set; }

    /// <summary>
    /// Gets or sets the relative path of this page (URL).
    /// </summary>
    [Column("path")]
    public string Path { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the title of this page.
    /// </summary>
    [Column("title")]
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether to show this page in menus.
    /// </summary>
    [Column("menu_item")]
    public bool MenuItem { get; set; }

    /// <summary>
    /// Gets or sets the fontawesome.com icon class to use for the page.
    /// </summary>
    [Column("icon_class")]
    public string IconClass { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    [Column("component")]
    public string Component { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the URL to redirect to instead of a Vue.js component.
    /// </summary>
    /// <remarks>Optional.</remarks>
    [Column("redirect")]
    public string Redirect { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether this page requires the user to be authenticated.
    /// </summary>
    [Column("requires_authentication")]
    public bool RequiresAuthentication { get; set; }

    /// <summary>
    /// Gets or sets the comma-separate list of role values. Eg: "1,2,3".
    /// </summary>
    [Column("required_roles")]
    public string RequiredRoles { get; set; } = string.Empty;
}