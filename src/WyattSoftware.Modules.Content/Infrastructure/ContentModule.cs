﻿// <copyright file="ContentModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Content.Infrastructure;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Content.API.Controllers;
using WyattSoftware.Modules.Content.BLL.Factories;
using WyattSoftware.Modules.Content.BLL.Models;
using WyattSoftware.Modules.Content.BLL.Services;
using WyattSoftware.Modules.Content.DAL.Repositories;
using WyattSoftware.Modules.Core.BLL.Services;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Content module.
/// </summary>
public static class ContentModule
{
    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddContentModule(this IServiceCollection services)
    {
        // DAL
        services.AddScoped(typeof(IPageRepository), typeof(PageRepository));
        services.AddScoped(typeof(IPageRoleRepository), typeof(PageRoleRepository));

        // BLL
        services.AddScoped(typeof(IPageFactory), typeof(PageFactory));
        services.AddScoped(typeof(IPageService), typeof(PageService));

        // We need to also register the generic form of PageService so that the base class can get it, while only
        // knowing the <c>Page</c> type.
        services.AddScoped(typeof(IDomainService<Page>), typeof(PageService));

        return services;
    }

    /// <summary>
    /// Configures the Content module.
    /// </summary>
    /// <param name="app">The app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseContentModule(this IApplicationBuilder app)
    {
        // API
        PagesController.InitMapping();

        return app;
    }
}
