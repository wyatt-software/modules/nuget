﻿// <copyright file="ITopicService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System;
using System.Threading;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Business logic for the forum topics.
/// </summary>
public interface ITopicService : IDomainService<Topic>
{
    /// <summary>
    /// Gets topics that satisfy the search criteria.
    /// </summary>
    /// <param name="sectionIdOrCodeName">The section to get topics in.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|section|user|lastpost|lastpost.user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="username">The user who created the topics.</param>
    /// <param name="subscribed">Get only topics the currently authenticated user is subscribed to.</param>
    /// <returns>Topics that satisfy the search criteria.</returns>
    Task<PaginatedResponse<Topic>> FetchAsync(
        string sectionIdOrCodeName = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        string username = "",
        bool? subscribed = null);

    /// <summary>
    /// Attempts to get a topic by either it's primary key, or it's alias.
    /// </summary>
    /// <param name="topicIdOrAlias">The primary key value, or the topic's unique alias.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|section|user|lastpost|lastpost.user] .</para>
    /// </param>
    /// <returns>A single topic.</returns>
    Task<Topic> ReadAsync(string topicIdOrAlias, string expand = "");

    /// <summary>
    /// <para>Tries to scrape the first image as the topic's cover image.</para>
    /// <para>
    /// If <c>ImageAuto</c> is set and the topic doesn't already have a thumb image, the posts are searched for
    /// images.
    /// </para>
    /// <para>If it's external, an attempt is made to upload it to the user's gallery.</para>
    /// <para>The first internal or successfully uploaded external image is set as the topic's cover.</para>
    /// </summary>
    /// <param name="topicId">The topic to scrape for images and set the cover for.</param>
    /// <returns>Nothing.</returns>
    Task EnsureThumbAsync(int topicId);

    /// <summary>
    /// Tries to ensure all topics have thumbnails.
    /// </summary>
    /// <param name="progress">
    /// An action to call to update the user with the current progress of the long-running task.
    /// </param>
    /// <param name="complete">An action to call to when the task is complete.</param>
    /// <param name="ct">The CancellationToken for cancelling this long-running task.</param>
    /// <returns>Nothing.</returns>
    Task EnsureThumbsAsync(Action<string, decimal> progress, Action<string> complete, CancellationToken ct);
}
