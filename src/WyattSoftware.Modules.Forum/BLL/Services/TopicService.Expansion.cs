﻿// <copyright file="TopicService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using WyattSoftware.Modules.Forum.BLL.Models;

    /// <summary>
    /// Reference expansion.
    /// </summary>
    public partial class TopicService
    {
        /// <summary>
        /// Only expand the reference objects if requested.
        /// The default behaviour is to save bandwidth by only returning the foreign key IDs.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <param name="expand">
        /// <para>A comma-separated list of child objects that can be expanded.</para>
        /// <para>Accepts: [all|section|user|lastpost|lastpost.user|image] .</para>
        /// </param>
        /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
        /// <returns>The topics, expanded.</returns>
        public async Task<List<Topic>> ExpandAsync(List<Topic> topics, string expand)
        {
            if (string.IsNullOrEmpty(expand))
            {
                return topics;
            }

            foreach (var reference in expand.Split(','))
            {
                switch (reference.ToLower())
                {
                    case "all":
                        topics = await this.ExpandSectionsAsync(topics);
                        topics = await this.ExpandUsersAsync(topics);
                        topics = await this.ExpandLastPostsAsync(topics);
                        topics = await this.ExpandLastPostUsersAsync(topics);
                        topics = await this.ExpandImagesAsync(topics);
                        break;

                    case "section":
                        topics = await this.ExpandSectionsAsync(topics);
                        break;

                    case "user":
                        topics = await this.ExpandUsersAsync(topics);
                        break;

                    case "lastpost":
                        topics = await this.ExpandLastPostsAsync(topics);
                        break;

                    case "lastpost.user":
                        topics = await this.ExpandLastPostUsersAsync(topics);
                        break;

                    case "image":
                        topics = await this.ExpandImagesAsync(topics);
                        break;
                }
            }

            return topics;
        }

        /// <summary>
        /// Expands the sections.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <returns>The topic, expanded.</returns>
        private async Task<List<Topic>> ExpandSectionsAsync(List<Topic> topics)
        {
            var sectionIds = topics
                .Select(x => x.SectionId)
                .Distinct()
                .ToArray();

            var sectionEntities = await this.sectionRepository.FetchAsync(sectionIds);
            var sections = sectionEntities.Select(this.sectionFactory.New).ToList();
            if (!sections.Any())
            {
                return topics;
            }

            foreach (var topic in topics)
            {
                topic.Section = sections.Find(x => x.Id == topic.SectionId);
            }

            return topics;
        }

        /// <summary>
        /// Expands the users.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <returns>The topics, expanded.</returns>
        private async Task<List<Topic>> ExpandUsersAsync(List<Topic> topics)
        {
            var userIds = topics
                .Select(x => x.UserId)
                .Distinct()
                .ToArray();

            var userEntities = await this.userRepository.FetchAsync(userIds);
            var users = userEntities.Select(this.userFactory.New).ToList();
            if (!users.Any())
            {
                return topics;
            }

            foreach (var topic in topics)
            {
                topic.User = users.Find(x => x.Id == topic.UserId);
            }

            return topics;
        }

        /// <summary>
        /// Expands the last posts.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <returns>The topic, expanded.</returns>
        private async Task<List<Topic>> ExpandLastPostsAsync(List<Topic> topics)
        {
            var postIds = topics
                .Select(x => x.LastPostId)
                .Distinct()
                .ToArray();

            var postEntities = await this.postRepository.FetchAsync(postIds);
            var posts = postEntities.Select(this.postFactory.New).ToList();
            if (!posts.Any())
            {
                return topics;
            }

            foreach (var topic in topics)
            {
                topic.LastPost = posts.Find(x => x.Id == topic.LastPostId);
            }

            return topics;
        }

        /// <summary>
        /// Expands the lastpost.user.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <returns>The topic, expanded.</returns>
        private async Task<List<Topic>> ExpandLastPostUsersAsync(List<Topic> topics)
        {
            var userIds = topics
                .Where(x => x.LastPost?.UserId != null)
                .Select(x => x.LastPost?.UserId ?? 0)
                .Distinct()
                .Where(x => x != 0)
                .ToArray();

            var userEntities = await this.userRepository.FetchAsync(userIds);
            var users = userEntities.Select(this.userFactory.New).ToList();
            if (!users.Any())
            {
                return topics;
            }

            foreach (var topic in topics)
            {
                if (topic.LastPost == null)
                {
                    continue;
                }

                topic.LastPost.User = users.Find(x => x.Id == topic.LastPost.UserId);
            }

            return topics;
        }

        /// <summary>
        /// Expands the images.
        /// </summary>
        /// <param name="topics">The collection of forum topics to expand.</param>
        /// <returns>The topics, expanded.</returns>
        private async Task<List<Topic>> ExpandImagesAsync(List<Topic> topics)
        {
            var imageIds = topics
                .Where(x => x.ImageId.HasValue)
                .Select(x => x.ImageId ?? 0)
                .Distinct()
                .ToArray();

            var imageEntities = await this.imageRepository.FetchAsync(imageIds);
            var images = imageEntities.Select(this.imageFactory.New).ToList();
            if (!images.Any())
            {
                return topics;
            }

            foreach (var topic in topics)
            {
                topic.Image = images.Find(x => x.Id == topic.ImageId);
            }

            return topics;
        }
    }
}
