﻿// <copyright file="ISectionService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Business logic for the forum sections.
/// </summary>
public interface ISectionService : IDomainService<Section>
{
    /// <summary>
    /// Gets a collection of forum sections.
    /// </summary>
    /// <param name="categoryId">A forum category to get sections for.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of forum sections.</returns>
    Task<PaginatedResponse<Section>> FetchAsync(int categoryId, string expand = "", int offset = 0, int limit = 20);

    /// <summary>
    /// Attempts to get a section by either it's primary key, or it's code name.
    /// </summary>
    /// <param name="sectionIdOrCodeName">The primary key value, or the section's unique code name.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <returns>A single data entity.</returns>
    Task<Section> ReadAsync(string sectionIdOrCodeName, string expand = "");
}
