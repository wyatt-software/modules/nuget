﻿// <copyright file="SectionService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class SectionService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="sections">The collection of forum sections to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <returns>The mutated sections collection.</returns>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    public async Task<List<Section>> ExpandAsync(List<Section> sections, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return sections;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    sections = await this.ExpandCategoriesAsync(sections);
                    sections = await this.ExpandLastPostsAsync(sections);
                    sections = await this.ExpandLastPostTopicsAsync(sections);
                    sections = await this.ExpandLastPostUsersAsync(sections);
                    break;

                case "category":
                    sections = await this.ExpandCategoriesAsync(sections);
                    break;

                case "lastpost":
                    sections = await this.ExpandLastPostsAsync(sections);
                    break;

                case "lastpost.topic":
                    sections = await this.ExpandLastPostTopicsAsync(sections);
                    break;

                case "lastpost.user":
                    sections = await this.ExpandLastPostUsersAsync(sections);
                    break;
            }
        }

        return sections;
    }

    /// <summary>
    /// Expands the categories for the given sections.
    /// </summary>
    /// <param name="sections">The collection of forum sections to expand.</param>
    private async Task<List<Section>> ExpandCategoriesAsync(List<Section> sections)
    {
        var categoryIds = sections
            .Select(x => x.CategoryId)
            .Distinct()
            .ToArray();

        var categoryEntities = await this.categoryRepository.FetchAsync(categoryIds);
        var categories = categoryEntities.Select(this.categoryFactory.New).ToList();
        if (!categories.Any())
        {
            return sections;
        }

        foreach (var section in sections)
        {
            section.Category = categories.First(x => x.Id == section.CategoryId);
        }

        return sections;
    }

    /// <summary>
    /// Expands the last posts for the given sections.
    /// </summary>
    /// <param name="sections">The collection of forum sections to expand.</param>
    private async Task<List<Section>> ExpandLastPostsAsync(List<Section> sections)
    {
        var postIds = sections
            .Select(x => x.LastPostId)
            .Distinct()
            .ToArray();

        var postEntities = await this.postRepository.FetchAsync(postIds);
        var posts = postEntities.Select(this.postFactory.New).ToList();
        if (!posts.Any())
        {
            return sections;
        }

        foreach (var section in sections)
        {
            section.LastPost = posts.First(x => x.Id == section.LastPostId);
        }

        return sections;
    }

    /// <summary>
    /// Expands the last post topics for the given sections.
    /// </summary>
    /// <param name="sections">The collection of forum sections to expand.</param>
    private async Task<List<Section>> ExpandLastPostTopicsAsync(List<Section> sections)
    {
        var topicIds = sections
            .Where(x => x.LastPost?.TopicId != null)
            .Select(x => x.LastPost?.TopicId ?? 0)
            .Distinct()
            .ToArray();

        var topicEntities = await this.topicRepository.FetchAsync(topicIds, this.currentSession.UserId);
        var topics = topicEntities.Select(this.topicFactory.New).ToList();
        if (!topics.Any())
        {
            return sections;
        }

        foreach (var section in sections)
        {
            if (section.LastPost == null)
            {
                continue;
            }

            section.LastPost.Topic = topics.First(x => x.Id == section.LastPost.TopicId);
        }

        return sections;
    }

    /// <summary>
    /// Expands the last post users for the given sections.
    /// </summary>
    /// <param name="sections">The collection of forum sections to expand.</param>
    private async Task<List<Section>> ExpandLastPostUsersAsync(List<Section> sections)
    {
        var userIds = sections
            .Where(x => x.LastPost?.UserId != null)
            .Select(x => x.LastPost?.UserId ?? 0)
            .Distinct()
            .ToArray();

        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return sections;
        }

        foreach (var section in sections)
        {
            if (section.LastPost == null)
            {
                continue;
            }

            section.LastPost.User = users.First(x => x.Id == section.LastPost.UserId);
        }

        return sections;
    }
}
