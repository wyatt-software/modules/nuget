﻿// <copyright file="PostService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class PostService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|user.image|editedbyuser] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The posts, expanded.</returns>
    private async Task<List<Post>> ExpandAsync(List<Post> posts, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return posts;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    posts = await this.ExpandTopicsAsync(posts);
                    posts = await this.ExpandTopicSectionsAsync(posts);
                    posts = await this.ExpandUsersAsync(posts);
                    posts = await this.ExpandUserImagesAsync(posts);
                    posts = await this.ExpandEditedByUsersAsync(posts);
                    break;

                case "topic":
                    posts = await this.ExpandTopicsAsync(posts);
                    break;

                case "topic.section":
                    posts = await this.ExpandTopicSectionsAsync(posts);
                    break;

                case "user":
                    posts = await this.ExpandUsersAsync(posts);
                    break;

                case "user.image":
                    posts = await this.ExpandUserImagesAsync(posts);
                    break;

                case "editedbyuser":
                    posts = await this.ExpandEditedByUsersAsync(posts);
                    break;
            }
        }

        return posts;
    }

    /// <summary>
    /// Expands the topics for the given posts.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <returns>The post, with topics expanded.</returns>
    private async Task<List<Post>> ExpandTopicsAsync(List<Post> posts)
    {
        var topicIds = posts
            .Select(x => x.TopicId)
            .Distinct()
            .ToArray();

        var topicEntities = await this.topicRepository.FetchAsync(topicIds, this.currentSession.UserId);
        var topics = topicEntities.Select(this.topicFactory.New).ToList();
        if (!topics.Any())
        {
            return posts;
        }

        foreach (var post in posts)
        {
            post.Topic = topics.Find(x => x.Id == post.TopicId);
        }

        return posts;
    }

    /// <summary>
    /// Gets the topic sections for the given posts.
    /// Assumes topics have already been expanded.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <returns>The post, with topics expanded.</returns>
    private async Task<List<Post>> ExpandTopicSectionsAsync(List<Post> posts)
    {
        var sectionIds = posts
            .Where(x => x.Topic?.SectionId != null)
            .Select(x => x.Topic?.SectionId ?? 0)
            .Distinct()
            .ToArray();

        var sectionEntities = await this.sectionRepository.FetchAsync(sectionIds);
        var sections = sectionEntities.Select(this.sectionFactory.New).ToList();
        if (!sections.Any())
        {
            return posts;
        }

        foreach (var post in posts)
        {
            if (post.Topic == null)
            {
                continue;
            }

            post.Topic.Section = sections.Find(x => x.Id == post.Topic.SectionId);
        }

        return posts;
    }

    /// <summary>
    /// Gets the users for the given posts.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <returns>The post, with topics expanded.</returns>
    private async Task<List<Post>> ExpandUsersAsync(List<Post> posts)
    {
        var userIds = posts
            .Select(x => x.UserId)
            .Distinct()
            .ToArray();

        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return posts;
        }

        foreach (var post in posts)
        {
            post.User = users.Find(x => x.Id == post.UserId);
        }

        return posts;
    }

    /// <summary>
    /// Gets the user images for the given posts.
    /// Assumes users have already been expanded.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <returns>The post, with topics expanded.</returns>
    private async Task<List<Post>> ExpandUserImagesAsync(List<Post> posts)
    {
        var imageIds = posts
            .Select(x => (int?)x?.User?.Properties["imageId"] ?? 0)
            .Where(x => x > 0)
            .Distinct()
            .ToArray();

        if (!imageIds.Any())
        {
            return posts;
        }

        var imageEntities = await this.imageRepository.FetchAsync(imageIds);
        foreach (var post in posts)
        {
            if (post.User == null)
            {
                continue;
            }

            var imageEntity = imageEntities
                .Where(x => x.Id == (int?)post.User.Properties["imageId"])
                ?.FirstOrDefault();

            if (imageEntity != null)
            {
                post.User.NestedResources["image"] = this.imageFactory.New(imageEntity);
            }
        }

        return posts;
    }

    /// <summary>
    /// Gets the 'edited by' users for the given posts.
    /// </summary>
    /// <param name="posts">The collection of forum posts to expand.</param>
    /// <returns>The post, with topics expanded.</returns>
    private async Task<List<Post>> ExpandEditedByUsersAsync(List<Post> posts)
    {
        var userIds = posts
            .Where(x => x.EditedByUserId != null)
            .Select(x => x.EditedByUserId ?? 0)
            .Distinct()
            .ToArray();

        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return posts;
        }

        foreach (var post in posts)
        {
            post.EditedByUser = users.Find(x => x.Id == post.EditedByUserId);
        }

        return posts;
    }
}
