﻿// <copyright file="TopicService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.Helpers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Forum.Options;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.BLL.Services;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="ITopicService"/> interface.
/// </summary>
public partial class TopicService : BaseDomainService<Topic, TopicEntity>, ITopicService
{
    private readonly ForumOptions forumOptions;
    private readonly ILogger<TopicService> logger;
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly ITopicFactory topicFactory;
    private readonly IPostFactory postFactory;
    private readonly IAlbumService albumService;
    private readonly IImageService imageService;

    /// <summary>
    /// Initializes a new instance of the <see cref="TopicService"/> class.
    /// </summary>
    /// <param name="forumOptions">Injected IOptions{ForumOptions}.</param>
    /// <param name="logger">Injected ILogger{TopicService}.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="albumService">Injected IAlbumService.</param>
    /// <param name="imageService">Injected IImageService.</param>
    public TopicService(
        IOptions<ForumOptions> forumOptions,
        ILogger<TopicService> logger,
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory,
        IAlbumService albumService,
        IImageService imageService)
        : base(topicRepository, topicFactory)
    {
        this.forumOptions = forumOptions.Value;
        this.logger = logger;
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;
        this.albumService = albumService;
        this.imageService = imageService;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Topic>> FetchAsync(
        string sectionIdOrCodeName = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        string username = "",
        bool? subscribed = null)
    {
        // Section specified?
        var sectionId = 0;
        if (!string.IsNullOrEmpty(sectionIdOrCodeName))
        {
            var sectionEntity = int.TryParse(sectionIdOrCodeName, out sectionId)
                ? await this.sectionRepository.ReadAsync(sectionId)
                : await this.sectionRepository.ReadAsync(sectionIdOrCodeName);

            if (sectionEntity == null)
            {
                throw new KeyNotFoundException($"Section \"{sectionIdOrCodeName}\" not found.");
            }

            sectionId = sectionEntity.Id;
        }

        // User specified?
        var topicUserId = 0;
        if (!string.IsNullOrEmpty(username))
        {
            var userEntity = await this.userRepository.ReadAsync(username);
            if (userEntity == null)
            {
                throw new KeyNotFoundException($"User \"{username}\" not found.");
            }

            topicUserId = userEntity.Id;
        }

        // Note: For the currentSession.UserId to be populated, the API request needs to include the Authorize
        // header, even though the endpoint doesn't require authorization.
        var total = await this.topicRepository.GetCountAsync(
            this.currentSession.UserId, sectionId, topicUserId, subscribed);

        var topicEntities = await this.topicRepository.FetchAsync(
            this.currentSession.UserId,
            sectionId,
            offset,
            limit,
            topicUserId,
            subscribed);

        var results = topicEntities.Select(this.topicFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Topic>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    /// <remarks>
    /// We override the base method so that we can pass the currently authenticated users id to determine if they're
    /// subscribed to this topic.
    /// </remarks>
    public new async Task<Topic> ReadAsync(int topicId)
    {
        // Note: For the currentSession.UserId to be populated, the API request needs to include the Authorize
        // header, even though the endpoint doesn't require authorization.
        var topicEntity = await this.topicRepository.ReadAsync(topicId, this.currentSession.UserId);
        if (topicEntity == null)
        {
            throw new KeyNotFoundException($"Topic {topicId} not found.");
        }

        var topic = this.topicFactory.New(topicEntity);
        return topic;
    }

    /// <inheritdoc />
    public async Task<Topic> ReadAsync(string topicIdOrAlias, string expand = "")
    {
        // Note: For the currentSession.UserId to be populated, the API request needs to include the Authorize
        // header, even though the endpoint doesn't require authorization.
        var topicEntity = int.TryParse(topicIdOrAlias, out var topicId)
            ? await this.topicRepository.ReadAsync(topicId, this.currentSession.UserId)
            : await this.topicRepository.ReadAsync(topicIdOrAlias, this.currentSession.UserId);

        if (topicEntity == null)
        {
            throw new KeyNotFoundException($"Topic \"{topicIdOrAlias}\" not found.");
        }

        var topic = this.topicFactory.New(topicEntity);
        await topic.ExpandAsync(expand);
        return topic;
    }

    /// <inheritdoc />
    public async Task EnsureThumbAsync(int topicId)
    {
        var topicEntity = await this.topicRepository.ReadAsync(topicId, this.currentSession.UserId);
        if (topicEntity == null)
        {
            this.logger.LogWarning($"Topic {topicId} not found.");
            return;
        }

        if (!topicEntity.ImageAuto || topicEntity.ImageId.HasValue)
        {
            return;
        }

        await this.EnsureThumbAsync(topicEntity);
    }

    /// <inheritdoc />
    public async Task EnsureThumbsAsync(
        Action<string, decimal> progress,
        Action<string> complete,
        CancellationToken ct)
    {
        // 0 = No limit. Get them all.
        var topicEntities = await this.topicRepository.FetchAsync(0, 0, 0, 0);
        topicEntities = topicEntities.Where(x => x.ImageAuto && !x.ImageId.HasValue).ToList();

        var total = topicEntities.Count;
        var processed = 0;
        var successful = 0;

        foreach (var topicEntity in topicEntities)
        {
            var imageId = await this.EnsureThumbAsync(topicEntity);

            successful += imageId.HasValue ? 1 : 0;
            processed++;

            var percentage = Math.Round(100M * processed / total, 1);
            var message = $"{processed}/{total}: {topicEntity.Title}";
            this.logger.LogInformation($"{percentage}%: {message}");
            progress(message, percentage);
        }

        var s = total != 1 ? "s" : string.Empty;
        var completeMessage = $"{successful}/{total} thumb image{s} set.";
        this.logger.LogInformation(completeMessage);
        complete($"{successful}/{total} thumb image{s} set.");
    }

    /// <summary>
    /// <para>Tries to scrape the first image as the topic's cover image.</para>
    /// <para>If it's external, an attempt is made to upload it to the user's gallery.</para>
    /// <para>The first internal or successfully uploaded external image is set as the topic's cover.</para>
    /// </summary>
    /// <param name="topicEntity">The topic to scrape for images and set the cover for.</param>
    /// <returns>Nothing.</returns>
    /// <remarks>TODO: Move to the migration module.</remarks>
    private async Task<int?> EnsureThumbAsync(TopicEntity topicEntity)
    {
        var postEntities = await this.postRepository.FetchAsync(topicEntity.Id);
        var giantBlockOfText = string.Join('\n', postEntities.Select(p => p.Content));
        var images = UserContentHelper.ScrapeImages(giantBlockOfText, this.forumOptions.TopicThumb.ScraperBaseUrl);

        foreach (var (altText, url) in images)
        {
            foreach (var pattern in this.forumOptions.TopicThumb.ScraperBlackList)
            {
                var regex = new Regex(pattern);
                if (regex.IsMatch(url))
                {
                    return null;
                }
            }

            this.logger.LogInformation(url);
            var imageId = await this.FindOrUploadImageAsync(url, topicEntity.UserId, altText);
            if (imageId.HasValue)
            {
                topicEntity.ImageId = imageId;
                await this.topicRepository.UpdateAsync(topicEntity);
                return imageId;
            }
        }

        return null;
    }

    /// <summary>
    /// <para>
    /// If the URL is internal, attempts to find an existing image by the UUID in it's url (regardless of which user
    /// it belongs to).
    /// </para>
    /// <para>If the URL is external, attempts to upload it to the user's gallery.</para>
    /// </summary>
    /// <param name="url">The URL of an image.</param>
    /// <param name="userId">The id of the user whose gallery to upload external images to.</param>
    /// <param name="caption">The alt-text of an image (to use if uploading an external image).</param>
    /// <returns>An image id or null.</returns>
    private async Task<int?> FindOrUploadImageAsync(string url, int userId, string caption)
    {
        var imageId = await this.FindInternalImageIdAsync(url);
        if (imageId.HasValue)
        {
            return imageId;
        }

        var album = await this.albumService.EnsureAsync(userId, this.forumOptions.TopicThumb.AlbumName);

        try
        {
            var image = await this.imageService.UploadAsync(url, album.Id, userId, caption);
            return image.Id;
        }
        catch (Exception ex)
        {
            this.logger.LogError(ex.Message);
            return null;
        }
    }

    /// <summary>
    /// Tries to find the internal image's Id if the URL matches.
    /// </summary>
    /// <param name="url">The URL of an image.</param>
    /// <returns>The image Id or null.</returns>
    private async Task<int?> FindInternalImageIdAsync(string url)
    {
        var regex = new Regex(@"{{StorageBaseUrl}}gallery\/([^\/]*)\/([0-9a-f]{2})\/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})(\..+)");
        var match = regex.Match(url);
        if (!match.Success)
        {
            return null;
        }

        var uuid = new Guid(match.Groups[3].Value);
        var image = await this.imageRepository.ReadAsync(uuid);
        if (image == null)
        {
            return null;
        }

        return image.Id;
    }
}
