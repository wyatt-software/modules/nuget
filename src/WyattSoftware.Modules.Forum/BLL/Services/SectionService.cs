﻿// <copyright file="SectionService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="ISectionService"/> interface.
/// </summary>
public partial class SectionService : BaseDomainService<Section, SectionEntity>, ISectionService
{
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly ICategoryRepository categoryRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly ICategoryFactory categoryFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly ITopicFactory topicFactory;
    private readonly IPostFactory postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="SectionService"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    public SectionService(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        ICategoryFactory categoryFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory)
        : base(sectionRepository, sectionFactory)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.categoryFactory = categoryFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Section>> FetchAsync(
        int categoryId, string expand = "", int offset = 0, int limit = 20)
    {
        var total = await this.sectionRepository.GetCountAsync(categoryId);
        var sectionEntities = await this.sectionRepository.FetchAsync(categoryId, offset, limit);

        var results = sectionEntities.Select(this.sectionFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Section>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        return response;
    }

    /// <inheritdoc />
    public async Task<Section> ReadAsync(string sectionIdOrCodeName, string expand = "")
    {
        var sectionEntity = int.TryParse(sectionIdOrCodeName, out var sectionId)
            ? await this.sectionRepository.ReadAsync(sectionId)
            : await this.sectionRepository.ReadAsync(sectionIdOrCodeName);

        if (sectionEntity == null)
        {
            throw new KeyNotFoundException($"Section \"{sectionIdOrCodeName}\" not found.");
        }

        var section = this.sectionFactory.New(sectionEntity);
        await section.ExpandAsync(expand);
        return section;
    }
}
