﻿// <copyright file="CategoryService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class CategoryService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="categories">The collection of forum categories to expand.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The categories, expanded.</returns>
    public async Task<List<Category>> ExpandAsync(List<Category> categories, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return categories;
        }

        for (var i = 0; i < categories.Count; i++)
        {
            categories[i] = await categories[i].ExpandAsync(expand);
        }

        return categories;
    }
}