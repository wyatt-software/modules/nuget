﻿// <copyright file="ICategoryService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Business logic for the forum categories.
/// </summary>
public interface ICategoryService : IDomainService<Category>
{
    /// <summary>
    /// Get a collection of models without any filtering of any kind.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of categories.</returns>
    Task<PaginatedResponse<Category>> FetchAsync(string expand, int offset, int limit);

    /// <summary>
    /// Gets a single category.
    /// </summary>
    /// <param name="categoryId">The primary key for the category.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic] .</para>
    /// </param>
    /// <returns>A single category.</returns>
    Task<Category> ReadAsync(int categoryId, string expand = "");
}
