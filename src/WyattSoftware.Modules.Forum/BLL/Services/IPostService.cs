﻿// <copyright file="IPostService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Models;

/// <summary>
/// Business logic for the forum posts.
/// </summary>
public interface IPostService : IDomainService<Post>
{
    /// <summary>
    /// Get a collection of forum posts.
    /// </summary>
    /// <param name="topicIdOrAlias">A TopicID or topic alias to filter by.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|editedbyuser] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="postId">
    /// If specified, the offset (page) is adjusted to ensure the post is within the results.
    /// </param>
    /// <param name="username">Username of the user to get posts for.</param>
    /// <param name="sort">The field to sort by (with a '-' prefix if descending).</param>
    /// <returns>A collection of forum posts.</returns>
    Task<PaginatedResponse<Post>> FetchAsync(
        string topicIdOrAlias = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        int postId = 0,
        string username = "",
        string sort = "created");

    /// <summary>
    /// Tries to get the post by its id.
    /// </summary>
    /// <param name="postId">The primary key of the post.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|user.image|editedbyuser] .</para>
    /// </param>
    /// <returns>A post.</returns>
    Task<Post> ReadAsync(int postId, string expand = "");
}
