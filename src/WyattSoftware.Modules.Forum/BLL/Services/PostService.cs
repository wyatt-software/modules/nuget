﻿// <copyright file="PostService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IPostService"/> interface.
/// </summary>
public partial class PostService : BaseDomainService<Post, PostEntity>, IPostService
{
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly ITopicFactory topicFactory;
    private readonly IPostFactory postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="PostService"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    public PostService(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory)
        : base(postRepository, postFactory)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Post>> FetchAsync(
        string topicIdOrAlias = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        int postId = 0,
        string username = "",
        string sort = "created")
    {
        // Topic specified?
        var topicId = 0;
        if (!string.IsNullOrEmpty(topicIdOrAlias))
        {
            var topicEntity = int.TryParse(topicIdOrAlias, out var topicID)
                ? await this.topicRepository.ReadAsync(topicID, this.currentSession.UserId)
                : await this.topicRepository.ReadAsync(topicIdOrAlias, this.currentSession.UserId);

            if (topicEntity == null)
            {
                throw new KeyNotFoundException($"Topic \"{topicIdOrAlias}\" not found.");
            }

            topicId = topicEntity.Id;
        }

        // User specified?
        var userId = 0;
        if (!string.IsNullOrEmpty(username))
        {
            var userEntity = await this.userRepository.ReadAsync(username);
            if (userEntity == null)
            {
                throw new KeyNotFoundException($"User \"{username}\" not found.");
            }

            userId = userEntity.Id;
        }

        // Post specified?
        if (postId > 0)
        {
            offset = await this.postRepository.GetOffsetAsync(topicId, postId, limit, userId);
        }

        var total = await this.postRepository.GetCountAsync(topicId, userId);
        var postEntities = await this.postRepository.FetchAsync(topicId, offset, limit, userId, sort);

        var results = postEntities.Select(this.postFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);
        results = SetPostNumbers(results, offset);

        var response = new PaginatedResponse<Post>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    public async Task<Post> ReadAsync(int postId, string expand = "")
    {
        var postEntity = await this.postRepository.ReadAsync(postId);
        if (postEntity == null)
        {
            throw new KeyNotFoundException($"Post {postId} not found.");
        }

        var post = this.postFactory.New(postEntity);
        post = await post.ExpandAsync(expand);
        return post;
    }

    /// <summary>
    /// Modifies a collection of forum post results, and sets their PostNumber (position in the results).
    /// </summary>
    /// <param name="posts">The collection of posts results.</param>
    /// <param name="offset">An offset to shift all PostNumbers by.</param>
    /// <returns>The post, with post numbers set.</returns>
    private static List<Post> SetPostNumbers(List<Post> posts, int offset)
    {
        for (var i = 0; i < posts.Count; i++)
        {
            var post = posts[i];
            post.PostNumber = i + offset + 1;
        }

        return posts;
    }
}
