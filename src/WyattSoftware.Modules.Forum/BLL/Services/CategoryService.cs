﻿// <copyright file="CategoryService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Services;

using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="ICategoryService"/> interface.
/// </summary>
public partial class CategoryService : BaseDomainService<Category, CategoryEntity>, ICategoryService
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryService"/> class.
    /// </summary>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    public CategoryService(
        ICategoryRepository categoryRepository,
        ICategoryFactory categoryFactory)
        : base(categoryRepository, categoryFactory)
    {
    }

    /// <inheritdoc/>
    public async Task<PaginatedResponse<Category>> FetchAsync(string expand, int offset, int limit)
    {
        var categories = await this.FetchAsync(offset, limit);
        categories.Results = await this.ExpandAsync(categories.Results, expand);
        return categories;
    }

    /// <inheritdoc />
    public async Task<Category> ReadAsync(int categoryId, string expand = "")
    {
        var category = await base.ReadAsync(categoryId);
        await category.ExpandAsync(expand);
        return category;
    }
}
