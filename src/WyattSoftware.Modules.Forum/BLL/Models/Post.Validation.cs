﻿// <copyright file="Post.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// Forum post domain model.
/// </summary>
public partial class Post
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidatateUserIdAsync();
        await this.ValidatateTopicIdAsync();
        await this.ValidatateEditedByUserIdAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateUserIdAsync()
    {
        var userExists = await this.userRepository.ExistsAsync(this.UserId);
        if (!userExists)
        {
            throw new ArgumentException($"User {this.UserId} not found.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateTopicIdAsync()
    {
        var topicEntity = await this.topicRepository.ReadAsync(this.TopicId, this.currentSession.UserId);
        if (topicEntity == null)
        {
            throw new KeyNotFoundException($"Topic {this.TopicId} not found.");
        }

        if (topicEntity.Locked)
        {
            throw new KeyNotFoundException($"Topic {this.TopicId} is locked.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateEditedByUserIdAsync()
    {
        if (this.EditedByUserId.HasValue)
        {
            var editedByUserExists = await this.userRepository.ExistsAsync(this.EditedByUserId.Value);
            if (!editedByUserExists)
            {
                throw new ArgumentException($"EditedByUserId {this.EditedByUserId.Value} not found.");
            }
        }
    }
}
