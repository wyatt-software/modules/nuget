﻿// <copyright file="Topic.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Threading.Tasks;
using WyattSoftware.Modules.Forum.BLL.Enums;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Forum topic domain model.
/// </summary>
public partial class Topic
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidatateUserIdAsync();
        await this.ValidatateAliasAsync();

        var section = await this.sectionRepository.ReadAsync(this.SectionId);
        if (section == null)
        {
            throw new Exception($"Section {this.SectionId} not found.");
        }

        this.ValidatateStatus(section);
        this.ValidatatePrice(section);
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateUserIdAsync()
    {
        var userExists = await this.userRepository.ExistsAsync(this.UserId);
        if (!userExists)
        {
            throw new ArgumentException($"User {this.UserId} not found.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateAliasAsync()
    {
        var existingTopicEntity = await this.topicRepository.ReadAsync(this.Alias, this.currentSession.UserId);
        if (existingTopicEntity != null && existingTopicEntity.Id != this.Id)
        {
            throw new ArgumentException($"Alias \"{this.Alias}\" is taken.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <param name="section">The section this topic belongs to.</param>
    private void ValidatateStatus(SectionEntity section)
    {
        if (section.Classifieds)
        {
            if (!Enum.IsDefined(typeof(TopicStatus), this.Status) || this.Status == (int)TopicStatus.None)
            {
                throw new ArgumentException($"Invalid status.");
            }
        }
        else
        {
            if (this.Status != (int)TopicStatus.None)
            {
                throw new ArgumentException($"Status must be None (0) for non-classifieds topics.");
            }
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <param name="section">The section this topic belongs to.</param>
    private void ValidatatePrice(SectionEntity section)
    {
        if (!section.Classifieds)
        {
            return;
        }

        if (this.Status == (int)TopicStatus.ForSale && this.Price == 0)
        {
            throw new ArgumentException($"You must provide a price for \"For Sale\" topics.");
        }
    }
}
