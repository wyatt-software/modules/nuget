﻿// <copyright file="Post.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Search.BLL.Factories;
using WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Forum post domain model.
/// </summary>
public partial class Post : PostEntity, IDomainModel, IIndexableModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly ISearchDocFactory searchDocFactory;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly ITopicFactory topicFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Post"/> class.
    /// </summary>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="userId">The author.</param>
    /// <param name="topicId">the topic.</param>
    /// <param name="content">The content.</param>
    internal Post(
        IServiceScopeFactory serviceScopeFactory,
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        ISearchDocFactory searchDocFactory,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        int userId,
        int topicId,
        string content)
    {
        this.serviceScopeFactory = serviceScopeFactory;
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.searchDocFactory = searchDocFactory;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;

        this.UserId = userId;
        this.TopicId = topicId;
        this.Content = content;

        // Always zero upon creation.
        this.EditedByUserId = null;
        this.PostNumber = 0;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Post"/> class.
    /// </summary>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Post(
        IServiceScopeFactory serviceScopeFactory,
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        ISearchDocFactory searchDocFactory,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        PostEntity postEntity)
    {
        this.serviceScopeFactory = serviceScopeFactory;
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.searchDocFactory = searchDocFactory;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;

        this.Id = postEntity.Id;
        this.Created = postEntity.Created;
        this.Modified = postEntity.Modified;

        this.UserId = postEntity.UserId;
        this.TopicId = postEntity.TopicId;
        this.Content = postEntity.Content;
        this.EditedByUserId = postEntity.EditedByUserId;

        this.Created = postEntity.Created;
        this.EditedDate = postEntity.EditedDate;
    }

    /// <summary>
    /// Gets the post's position in the search results (not the position in the topic).
    /// </summary>
    /// <remarks>Derived field. Set by PostsService.Fetch() instead of this.PopulateDerivedFieldsAsync().</remarks>
    public int PostNumber { get; internal set; }

    /// <summary>
    /// Attempts to create a forum post.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        await this.ValidateAsync();

        var postEntity = Mapper.Map<PostEntity>(this);
        this.Id = await this.postRepository.CreateAsync(postEntity);

        // Update topics's "Last post created" field.
        var topicEntity = await this.topicRepository.ReadAsync(this.TopicId, this.currentSession.UserId);
        if (topicEntity != null)
        {
            topicEntity.LastPostCreated = this.Created;
            await this.topicRepository.UpdateAsync(topicEntity);
        }

        // Search index
        var searchDoc = await this.MapToSearchDocAsync();
        await searchDoc.CreateAsync();

        this.EnsureCoverThumbInBackground(this.TopicId);
    }

    /// <summary>
    /// Attempts to update a post.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        this.EditedByUserId = this.currentSession.UserId;
        this.EditedDate = DateTimeOffset.UtcNow.UtcDateTime;

        var postEntity = Mapper.Map<PostEntity>(this);

        await this.postRepository.UpdateAsync(postEntity);

        // Search index
        var searchDoc = await this.MapToSearchDocAsync();
        await searchDoc.UpdateAsync();

        this.EnsureCoverThumbInBackground(this.TopicId);
    }

    /// <summary>
    /// Attempts to delete a post.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        await this.postRepository.DeleteAsync(this.Id);

        // Search index
        var searchDoc = await this.MapToSearchDocAsync();
        await searchDoc.DeleteAsync();

        await this.postRepository.DeleteAsync(this.Id);
    }

    /// <summary>
    /// Scrape thumb image (in a background task).
    /// </summary>
    /// <param name="topicId">The Id of the topic to scrape images for.</param>
    private void EnsureCoverThumbInBackground(int topicId)
    {
        _ = Task.Run(async () =>
        {
            // This task runs in the background while the action returns immediately.
            // We can't use any dependencies that were injected into the controller class, since they're disposed
            // with it. Instead, we need to create a new scope for the lifetime of the background task.
            using var scope = this.serviceScopeFactory.CreateScope();

            var scopedTopicService =
                scope.ServiceProvider.GetRequiredService<ITopicService>();

            await scopedTopicService.EnsureThumbAsync(topicId);
        });
    }
}