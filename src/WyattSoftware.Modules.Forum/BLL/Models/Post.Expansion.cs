﻿// <copyright file="Post.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System.Threading.Tasks;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Forum post domain model.
/// </summary>
public partial class Post
{
    /// <summary>
    /// Gets or sets the topic the post belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=topic".
    /// </remarks>
    public Topic? Topic { get; set; }

    /// <summary>
    /// Gets or sets the user who created the post.
    /// </summary>
    /// <remarks>Use "expand=user".</remarks>
    public User? User { get; set; }

    /// <summary>
    /// Gets or sets the user who last edited the post.
    /// </summary>
    /// <remarks>Use "expand=editedbyuser".</remarks>
    public User? EditedByUser { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|user.image|editedbyuser] .</para>
    /// </param>
    /// <returns>The post, with references expanded.</returns>
    public async Task<Post> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandTopicAsync();
                    await this.ExpandTopicSectionAsync();
                    await this.ExpandUserAsync();
                    await this.ExpandUserImageAsync();
                    await this.ExpandEditedByUserAsync();
                    break;

                case "topic":
                    await this.ExpandTopicAsync();
                    break;

                case "topic.section":
                    await this.ExpandTopicSectionAsync();
                    break;

                case "user":
                    await this.ExpandUserAsync();
                    break;

                case "user.image":
                    await this.ExpandUserImageAsync();
                    break;

                case "editedbyuser":
                    await this.ExpandEditedByUserAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Expands the topic for the given post.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandTopicAsync()
    {
        var topicEntity = await this.topicRepository.ReadAsync(this.TopicId, this.currentSession.UserId);
        if (topicEntity == null)
        {
            return;
        }

        this.Topic = this.topicFactory.New(topicEntity);
    }

    /// <summary>
    /// Expands the section this post belongs to.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandTopicSectionAsync()
    {
        if (this.Topic == null)
        {
            return;
        }

        var sectionEntity = await this.sectionRepository.ReadAsync(this.Topic.SectionId);
        if (sectionEntity == null)
        {
            return;
        }

        this.Topic.Section = this.sectionFactory.New(sectionEntity);
    }

    /// <summary>
    /// Expands the user that created this post.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserAsync()
    {
        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.User = this.userFactory.New(userEntity);
    }

    /// <summary>
    /// Expands the section this post belongs to.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserImageAsync()
    {
        if (this.User == null)
        {
            return;
        }

        var imageId = (int?)this.User.Properties["ImageId"];
        if (imageId == null)
        {
            return;
        }

        var imageEntity = await this.imageRepository.ReadAsync(imageId.Value);
        if (imageEntity == null)
        {
            return;
        }

        this.User.NestedResources["image"] = this.imageFactory.New(imageEntity);
    }

    /// <summary>
    /// Expands the user tha last edited the post.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandEditedByUserAsync()
    {
        if (!this.EditedByUserId.HasValue)
        {
            return;
        }

        var userEntity = await this.userRepository.ReadAsync(this.EditedByUserId.Value);
        if (userEntity == null)
        {
            return;
        }

        this.EditedByUser = this.userFactory.New(userEntity);
    }
}