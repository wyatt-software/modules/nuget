﻿// <copyright file="Post.Search.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using WyattSoftware.Modules.Core.BLL.Extensions;
using WyattSoftware.Modules.Search.BLL.Models;

/// <summary>
/// Forum post domain model.
/// </summary>
public partial class Post
{
    /// <inheritdoc />
    public async Task<SearchDoc> MapToSearchDocAsync()
    {
        var topic = await this.topicRepository.ReadAsync(this.TopicId, this.currentSession.UserId);
        if (topic == null)
        {
            throw new Exception($"Topic {this.TopicId} not found.");
        }

        var topicTitle = topic.Title ?? string.Empty;
        var topicSubHeading = topic.SubHeading ?? string.Empty;
        var postContent = this.Content.ToPlainText();

        var isFirstPost = this.Id == topic.FirstPostId;
        if (isFirstPost)
        {
            // First post.
            var searchDoc = this.searchDocFactory.New(
                $"topic|{this.TopicId}",
                "topic",
                topicTitle,
                string.Format($"/forum/topics/{topic.Alias}"),
                new Dictionary<string, string>
                {
                    // Include the topic title in searches.
                    { "title", topicTitle },

                    // Include the sub-heading and first post in searches.
                    { "content", topicSubHeading + "\n\n" + postContent },
                });

            return searchDoc;
        }
        else
        {
            // Reply post.
            var searchDoc = this.searchDocFactory.New(
                $"post|{this.Id}",
                "post",
                topicTitle,
                string.Format("/forum/topics/{1}?postid={0}#post{0}", this.Id, topic.Alias),
                new Dictionary<string, string>
                {
                    // We don't want posts to flood the results when it's just the topic title we're matching on.
                    { "title", string.Empty },

                    // Search the post content like normal.
                    { "content", postContent },
                });

            return searchDoc;
        }
    }
}
