﻿// <copyright file="Category.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Forum category domain model.
/// </summary>
public partial class Category
{
    /// <summary>
    /// Gets or sets a list of child sections. <see cref="SectionEntity" />.
    /// </summary>
    /// <remarks>
    /// This is a special case, because it's a collection of child objects. Usually we only expand single foreign
    /// keys and parent objects.
    /// </remarks>
    public List<Section>? Sections { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.user.image|sections.lastpost.topic] .</para>
    /// </param>
    /// <remarks>
    /// Because this is a static method, parameter injection is used on this method instead of constructor injection.
    /// </remarks>
    /// <returns>The category, with references expanded.</returns>
    public async Task<Category> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandSectionsAsync();
                    await this.ExpandSectionsLastPostsAsync();
                    await this.ExpandSectionsLastPostUsersAsync();
                    await this.ExpandSectionsLastPostTopicsAsync();
                    break;

                case "sections":
                    await this.ExpandSectionsAsync();
                    break;

                case "sections.lastpost":
                    await this.ExpandSectionsLastPostsAsync();
                    break;

                case "sections.lastpost.user":
                    await this.ExpandSectionsLastPostUsersAsync();
                    break;

                case "sections.lastpost.topic":
                    await this.ExpandSectionsLastPostTopicsAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Expands all "sections".
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandSectionsAsync()
    {
        var sectionEntities = await this.sectionRepository.FetchAsync(this.Id);
        this.Sections = sectionEntities.Select(this.sectionFactory.New).ToList();
    }

    /// <summary>
    /// Expands all "sections.lastpost".
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandSectionsLastPostsAsync()
    {
        if (this.Sections == null)
        {
            return;
        }

        var postIds = this.Sections.Select(x => x.LastPostId).Distinct().ToArray();
        var postEntities = await this.postRepository.FetchAsync(postIds);
        var posts = postEntities.Select(this.postFactory.New).ToList();
        if (!posts.Any())
        {
            return;
        }

        foreach (var section in this.Sections)
        {
            section.LastPost = posts.Find(x => x.Id == section.LastPostId);
        }
    }

    /// <summary>
    /// Expands all "sections.lastpost.topic".
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandSectionsLastPostTopicsAsync()
    {
        if (this.Sections == null)
        {
            return;
        }

        var lastPosts = this.Sections.Select(x => x.LastPost).ToList();
        if (lastPosts.Count == 0)
        {
            return;
        }

        var topicIds = lastPosts.Select(x => x?.TopicId ?? 0).Distinct().ToArray();
        var topicEntities = await this.topicRepository.FetchAsync(topicIds, this.currentSession.UserId);
        var topics = topicEntities.Select(this.topicFactory.New).ToList();
        if (!topics.Any())
        {
            return;
        }

        foreach (var section in this.Sections)
        {
            if (section.LastPost == null)
            {
                continue;
            }

            section.LastPost.Topic = topics.Find(x => x.Id == section.LastPost.TopicId);
        }
    }

    /// <summary>
    /// Expands all "sections.lastpost.user".
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandSectionsLastPostUsersAsync()
    {
        if (this.Sections == null)
        {
            return;
        }

        var lastPosts = this.Sections.Select(x => x.LastPost).Where(x => x != null).ToList();
        if (lastPosts.Count == 0)
        {
            return;
        }

        var userIds = lastPosts.Select(x => x?.UserId ?? 0).Distinct().ToArray();
        var userEntities = await this.userRepository.FetchAsync(userIds);
        var users = userEntities.Select(this.userFactory.New).ToList();
        if (!users.Any())
        {
            return;
        }

        foreach (var section in this.Sections)
        {
            if (section.LastPost == null)
            {
                continue;
            }

            section.LastPost.User = users.Find(x => x.Id == section.LastPost.UserId);
        }
    }
}
