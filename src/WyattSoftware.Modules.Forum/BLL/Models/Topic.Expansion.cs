﻿// <copyright file="Topic.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System.Threading.Tasks;
using WyattSoftware.Modules.Gallery.BLL.Models;
using WyattSoftware.Modules.Identity.BLL.Models;

/// <summary>
/// Forum topic domain model.
/// </summary>
public partial class Topic
{
    /// <summary>
    /// Gets or sets the section this topic belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=editedbyuser".
    /// </remarks>
    public Section? Section { get; set; }

    /// <summary>
    /// Gets or sets the user who created this topic.
    /// </summary>
    /// <remarks>
    /// Use "expand=user".
    /// </remarks>
    public User? User { get; set; }

    /// <summary>
    /// Gets or sets the user who last edited the post.
    /// </summary>
    /// <remarks>
    /// Use "expand=editedbyuser".
    /// </remarks>
    public Post? LastPost { get; set; }

    /// <summary>
    /// Gets or sets the thumbnail image for this topic.
    /// </summary>
    /// <remarks>Use "expand=image".</remarks>
    public Image? Image { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|section|user|lastpost|lastpost.user|image] .</para>
    /// </param>
    /// <returns>This topic, with references expanded.</returns>
    public async Task<Topic> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandSectionAsync();
                    await this.ExpandUserAsync();
                    await this.ExpandLastPostAsync();
                    await this.ExpandLastPostUserAsync();
                    await this.ExpandImageAsync();
                    break;

                case "section":
                    await this.ExpandSectionAsync();
                    break;

                case "user":
                    await this.ExpandUserAsync();
                    break;

                case "lastpost":
                    await this.ExpandLastPostAsync();
                    break;

                case "lastpost.user":
                    await this.ExpandLastPostUserAsync();
                    break;

                case "image":
                    await this.ExpandImageAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Expands the section.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandSectionAsync()
    {
        var sectionEntity = await this.sectionRepository.ReadAsync(this.SectionId);
        if (sectionEntity == null)
        {
            return;
        }

        this.Section = this.sectionFactory.New(sectionEntity);
    }

    /// <summary>
    /// Expands the user.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserAsync()
    {
        var userEntity = await this.userRepository.ReadAsync(this.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.User = this.userFactory.New(userEntity);
    }

    /// <summary>
    /// Expands the last post.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandLastPostAsync()
    {
        var postEntity = await this.postRepository.ReadAsync(this.LastPostId);
        if (postEntity == null)
        {
            return;
        }

        this.LastPost = this.postFactory.New(postEntity);
    }

    /// <summary>
    /// Expands the lastpost.user.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandLastPostUserAsync()
    {
        if (this.LastPost == null)
        {
            return;
        }

        var userEntity = await this.userRepository.ReadAsync(this.LastPost.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.LastPost.User = this.userFactory.New(userEntity);
    }

    /// <summary>
    /// Expands the image.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandImageAsync()
    {
        if (!this.ImageId.HasValue)
        {
            return;
        }

        var imageEntity = await this.imageRepository.ReadAsync(this.ImageId.Value);
        if (imageEntity == null)
        {
            return;
        }

        this.Image = this.imageFactory.New(imageEntity);
    }
}