﻿// <copyright file="Category.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Threading.Tasks;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Forum category domain model.
/// </summary>
public partial class Category : CategoryEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly ICategoryRepository categoryRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly ITopicFactory topicFactory;
    private readonly IPostFactory postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Category"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="displayName">The display name.</param>
    internal Category(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory,
        string displayName)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;

        // Trigger validation by setting properties, not backing fields.
        this.DisplayName = displayName;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Category"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="categoryEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Category(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        ISectionFactory sectionFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory,
        CategoryEntity categoryEntity)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;

        this.DisplayName = categoryEntity.DisplayName;

        this.Id = categoryEntity.Id;
        this.Created = categoryEntity.Created;
        this.Modified = categoryEntity.Modified;
        this.SortOrder = categoryEntity.SortOrder;
    }

    /// <summary>
    /// Creates a category.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        var count = await this.categoryRepository.GetCountAsync();
        this.SortOrder = count + 1;

        var categoryEntity = Mapper.Map<CategoryEntity>(this);
        this.Id = await this.categoryRepository.CreateAsync(categoryEntity);
    }

    /// <summary>
    /// Updates a category.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var categoryEntity = Mapper.Map<CategoryEntity>(this);
        await this.categoryRepository.UpdateAsync(categoryEntity);
    }

    /// <summary>
    /// Attempts to delete a category.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        var sectionCount = await this.sectionRepository.GetCountAsync(this.Id);
        if (sectionCount != 0)
        {
            throw new InvalidOperationException(
                "This category still has sections. Only empty categories are allowed to be deleted.");
        }

        await this.categoryRepository.DeleteAsync(this.Id);
    }
}
