﻿// <copyright file="Section.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System.Threading.Tasks;

/// <summary>
/// Forum section domain model.
/// </summary>
public partial class Section
{
    /// <summary>
    /// Gets or sets the category the section belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=category".
    /// </remarks>
    public Category? Category { get; set; }

    /// <summary>
    /// Gets or sets the last post created in any topic under this section.
    /// </summary>
    /// <remarks>
    /// Use "expand=lastpost".
    /// </remarks>
    public Post? LastPost { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested. The default behaviour is to save bandwidth by only returning
    /// the foreign key IDs.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <returns>This response object, with references expanded.</returns>
    public async Task<Section> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                    await this.ExpandCategoryAsync();
                    await this.ExpandLastPostAsync();
                    await this.ExpandLastPostTopicAsync();
                    await this.ExpandLastPostUserAsync();
                    break;

                case "category":
                    await this.ExpandCategoryAsync();
                    break;

                case "lastpost":
                    await this.ExpandLastPostAsync();
                    break;

                case "lastpost.topic":
                    await this.ExpandLastPostTopicAsync();
                    break;

                case "lastpost.user":
                    await this.ExpandLastPostUserAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Expands the category for the given section.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandCategoryAsync()
    {
        var categoryEntity = await this.categoryRepository.ReadAsync(this.CategoryId);
        if (categoryEntity == null)
        {
            return;
        }

        this.Category = this.categoryFactory.New(categoryEntity);
    }

    /// <summary>
    /// Expands the last post for the given section.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandLastPostAsync()
    {
        var postEntity = await this.postRepository.ReadAsync(this.LastPostId);
        if (postEntity == null)
        {
            return;
        }

        this.LastPost = this.postFactory.New(postEntity);
    }

    /// <summary>
    /// Expands the lastpost.topic that posted the last post for the given topic.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandLastPostTopicAsync()
    {
        if (this.LastPost == null)
        {
            return;
        }

        var topicEntity = await this.topicRepository.ReadAsync(this.LastPost.TopicId, this.currentSession.UserId);
        if (topicEntity == null)
        {
            return;
        }

        this.LastPost.Topic = this.topicFactory.New(topicEntity);
    }

    /// <summary>
    /// Expands the lastpost.user that posted the last post for the given section.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandLastPostUserAsync()
    {
        if (this.LastPost == null)
        {
            return;
        }

        var userEntity = await this.userRepository.ReadAsync(this.LastPost.UserId);
        if (userEntity == null)
        {
            return;
        }

        this.LastPost.User = this.userFactory.New(userEntity);
    }
}