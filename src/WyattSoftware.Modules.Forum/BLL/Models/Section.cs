﻿// <copyright file="Section.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Forum section domain model.
/// </summary>
public partial class Section : SectionEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly ICategoryRepository categoryRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly ICategoryFactory categoryFactory;
    private readonly ITopicFactory topicFactory;
    private readonly IPostFactory postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Section"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="categoryId">The category.</param>
    /// <param name="codeName">The unique code name.</param>
    /// <param name="displayName">The display name.</param>
    /// <param name="description">The description.</param>
    internal Section(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        ICategoryFactory categoryFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory,
        int categoryId,
        string codeName,
        string displayName,
        string description)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.categoryFactory = categoryFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;

        this.CategoryId = categoryId;
        this.CodeName = codeName;
        this.DisplayName = displayName;
        this.Description = description;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Section"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="sectionEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Section(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        ICategoryFactory categoryFactory,
        ITopicFactory topicFactory,
        IPostFactory postFactory,
        SectionEntity sectionEntity)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.categoryFactory = categoryFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;

        this.Id = sectionEntity.Id;
        this.Created = sectionEntity.Created;
        this.Modified = sectionEntity.Modified;
        this.SortOrder = sectionEntity.SortOrder;

        this.CategoryId = sectionEntity.CategoryId;
        this.CodeName = sectionEntity.CodeName;
        this.DisplayName = sectionEntity.DisplayName;
        this.Description = sectionEntity.Description;

        this.Classifieds = sectionEntity.Classifieds;
        this.LastPostId = sectionEntity.LastPostId;
        this.TopicCount = sectionEntity.TopicCount;
    }

    /// <summary>
    /// Attempts to create a section and persist it.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        await this.ValidateAsync();

        var count = await this.sectionRepository.GetCountAsync(this.CategoryId);
        this.SortOrder = count + 1;

        var sectionEntity = Mapper.Map<SectionEntity>(this);
        this.Id = await this.sectionRepository.CreateAsync(sectionEntity);
    }

    /// <summary>
    /// Updates a forum section.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var sectionEntity = Mapper.Map<SectionEntity>(this);
        await this.sectionRepository.UpdateAsync(sectionEntity);
    }

    /// <summary>
    /// Updates a forum section.
    /// </summary>
    /// <returns>Nothing.</returns>
    public Task DeleteAsync()
    {
        throw new NotImplementedException("Use the \"moveOrphanedTopicsToSectionId\" overload instead.");
    }

    /// <summary>
    /// Deletes the given section, moving it's newly orphaned topics to another section.
    /// </summary>
    /// <param name="moveOrphanedTopicsToSectionId">The section to move the newly orphaned topics to.</param>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync(int moveOrphanedTopicsToSectionId)
    {
        var toSectionEntity = await this.sectionRepository.ReadAsync(moveOrphanedTopicsToSectionId);
        if (toSectionEntity == null)
        {
            throw new KeyNotFoundException(
                $"Section to move orphaned topics to \"{moveOrphanedTopicsToSectionId}\" not found.");
        }

        if (this.Id == toSectionEntity.Id)
        {
            throw new InvalidOperationException("Can't move orphaned topics to the same section being deleted.");
        }

        await this.sectionRepository.MoveTopicsToSectionAsync(this.Id, toSectionEntity.Id);
        await this.sectionRepository.DeleteAsync(this.Id);
    }
}
