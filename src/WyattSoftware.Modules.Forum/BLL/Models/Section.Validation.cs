﻿// <copyright file="Section.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Threading.Tasks;

/// <summary>
/// Forum section domain model.
/// </summary>
public partial class Section
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        await this.ValidatateCategoryIdAsync();
        await this.ValidatateCodeNameAsync();
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateCategoryIdAsync()
    {
        var categoryExists = await this.categoryRepository.ExistsAsync(this.CategoryId);
        if (!categoryExists)
        {
            throw new ArgumentException($"Category {this.CategoryId} not found.");
        }
    }

    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidatateCodeNameAsync()
    {
        var existingSection = await this.sectionRepository.ReadAsync(this.CodeName);
        if (existingSection != null && existingSection.Id != this.Id)
        {
            throw new ArgumentException($"CodeName \"{this.CodeName}\" is taken.");
        }
    }
}
