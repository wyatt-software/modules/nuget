﻿// <copyright file="Topic.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Models;

using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Search.BLL.Singletons;
using WyattSoftware.Modules.Search.DAL.Services;
using WyattSoftware.Modules.Search.Infrastructure;

/// <summary>
/// Forum topic domain model.
/// </summary>
public partial class Topic : TopicEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly ICurrentSession currentSession;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ISubscriptionRepository subscriptionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;
    private readonly ISectionFactory sectionFactory;
    private readonly IPostFactory postFactory;
    private readonly ISearchIndex searchIndex;

    /// <summary>
    /// Initializes a new instance of the <see cref="Topic"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="subscriptionRepository">Injected ISubscriptionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="userId">The author.</param>
    /// <param name="sectionId">The section.</param>
    /// <param name="imageId">The image to use as the thumb.</param>
    /// <param name="title">The human readable title.</param>
    /// <param name="subHeading">A subheading to show in listings and at the top of the page.</param>
    /// <param name="imageAuto">A value indicating whether to scrape the thumb image from the content.</param>
    /// <param name="status">The status of a classifieds topic.</param>
    /// <param name="price">The price of a classifieds topic.</param>
    internal Topic(
        ICurrentSession currentSession,
        IServiceScopeFactory serviceScopeFactory,
        ISearchEngine searchEngine,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ISubscriptionRepository subscriptionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        IPostFactory postFactory,
        int userId,
        int sectionId,
        int? imageId,
        string title,
        string subHeading,
        bool imageAuto,
        int status,
        decimal price)
    {
        this.currentSession = currentSession;
        this.serviceScopeFactory = serviceScopeFactory;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.postFactory = postFactory;

        this.searchIndex = searchEngine[SearchModule.SiteSearchIndexName].SearchIndex;

        // Trigger validation by setting properties, not backing fields.
        this.UserId = userId;
        this.SectionId = sectionId;
        this.ImageId = imageId;
        this.Title = title;
        this.SubHeading = subHeading;
        this.ImageAuto = imageAuto;
        this.Status = status;
        this.Price = price;

        // Alias is a special case.
        // It's automatically set when creating.
        // TODO: Make this feature of the service public, and call it from the API when constructing a new Topic for
        // creation.
        this.Alias = string.Empty;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Topic"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="subscriptionRepository">Injected ISubscriptionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="topicEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Topic(
        ICurrentSession currentSession,
        IServiceScopeFactory serviceScopeFactory,
        ISearchEngine searchEngine,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ISubscriptionRepository subscriptionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        ISectionFactory sectionFactory,
        IPostFactory postFactory,
        TopicEntity topicEntity)
    {
        this.currentSession = currentSession;
        this.serviceScopeFactory = serviceScopeFactory;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.postFactory = postFactory;

        this.searchIndex = searchEngine[SearchModule.SiteSearchIndexName].SearchIndex;

        this.Id = topicEntity.Id;
        this.Created = topicEntity.Created;
        this.Modified = topicEntity.Modified;

        this.UserId = topicEntity.UserId;
        this.SectionId = topicEntity.SectionId;
        this.ImageId = topicEntity.ImageId;
        this.Title = topicEntity.Title;
        this.Alias = topicEntity.Alias;
        this.SubHeading = topicEntity.SubHeading;
        this.Status = topicEntity.Status;
        this.Price = topicEntity.Price;

        this.Stickied = topicEntity.Stickied;
        this.Locked = topicEntity.Locked;
        this.ImageAuto = topicEntity.ImageAuto;
        this.ViewCounter = topicEntity.ViewCounter;
        this.FirstPostId = topicEntity.FirstPostId;
        this.LastPostCreated = topicEntity.LastPostCreated;
        this.LastPostId = topicEntity.LastPostId;
        this.PostCount = topicEntity.PostCount;
        this.Subscribed = topicEntity.Subscribed;
    }

    /// <summary>
    /// Registers awaitable "event" handlers to call just before deleting a gallery image.
    /// </summary>
    /// <param name="imageAddOn">Injected IImageAddOn.</param>
    public static void RegisterBeforeDeleteHandlers(IImageAddOn imageAddOn)
    {
        imageAddOn.BeforeDeleteHandlers.Add(async (serviceScopeFactory, id) =>
        {
            // This is a delegate, so it doesn't have access to dependency injection.
            // We can't use more specific method-injection, because the caller doesn't know what the delegate will
            // need, and probably doesn't have a reference to the assembly anyway.
            var scope = serviceScopeFactory.CreateScope();

            var scopedTopicRepository = scope.ServiceProvider.GetRequiredService<ITopicRepository>();
            await scopedTopicRepository.ClearTopicThumbsForImageAsync(id);
        });
    }

    /// <inheritdoc />
    public Task CreateAsync()
    {
        throw new NotImplementedException("Use the \"content\" overload instead.");
    }

    /// <summary>
    /// Attempts to create a topic/first post/subscription and persist it.
    /// </summary>
    /// <param name="content">The content of the first post.</param>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync(string content)
    {
        await this.ValidateAsync();

        this.LastPostCreated = DateTimeOffset.UtcNow.UtcDateTime;
        this.Alias = await this.topicRepository.GenerateUniqueAliasAsync(this.Title);

        // Topic
        var topicEntity = Mapper.Map<TopicEntity>(this);
        this.Id = await this.topicRepository.CreateAsync(topicEntity);

        // First post
        var postEntity = new PostEntity
        {
            TopicId = this.Id,
            UserId = this.UserId,
            Created = DateTimeOffset.UtcNow.UtcDateTime,
            Modified = DateTimeOffset.UtcNow.UtcDateTime,
            Content = content,
        };

        postEntity.Id = await this.postRepository.CreateAsync(postEntity);
        var post = this.postFactory.New(postEntity);

        // Subscription
        await this.subscriptionRepository.CreateAsync(this.Id, topicEntity.UserId);

        // Search index
        var searchDoc = await post.MapToSearchDocAsync();
        this.searchIndex.Create(searchDoc);

        this.EnsureCoverThumbInBackground(this.Id);
    }

    /// <summary>
    /// Updates a forum topic.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task UpdateAsync()
    {
        await this.ValidateAsync();

        this.Modified = DateTimeOffset.UtcNow.UtcDateTime;

        var topicEntity = Mapper.Map<TopicEntity>(this);
        await this.topicRepository.UpdateAsync(topicEntity);

        this.EnsureCoverThumbInBackground(this.Id);
    }

    /// <summary>
    /// Deletes a forum topic.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task DeleteAsync()
    {
        await this.subscriptionRepository.DeleteAllSubscriptionsAsync(this.Id);
        await this.topicRepository.DeleteAsync(this.Id);
    }

    /// <summary>
    /// Subscribes a user to a topic.
    /// </summary>
    /// <param name="userId">The user the subscription is for.</param>
    /// <returns>Nothing.</returns>
    public async Task SubscribeAsync(int userId)
    {
        await this.subscriptionRepository.CreateAsync(this.Id, userId);
    }

    /// <summary>
    /// Unsubscribes a user from a topic.
    /// </summary>
    /// <param name="userId">The user the subscription is for.</param>
    /// <returns>Nothing.</returns>
    public async Task UnsubscribeAsync(int userId)
    {
        await this.subscriptionRepository.DeleteAsync(this.Id, userId);
    }

    /// <summary>
    /// Scrape thumb image (in a background task).
    /// </summary>
    /// <param name="topicId">The Id of the topic to scrape images for.</param>
    private void EnsureCoverThumbInBackground(int topicId)
    {
        _ = Task.Run(async () =>
        {
            // This task runs in the background while the action returns immediately.
            // We can't use any dependencies that were injected into the controller class, since they're disposed
            // with it. Instead, we need to create a new scope for the lifetime of the background task.
            using var scope = this.serviceScopeFactory.CreateScope();

            var scopedTopicService =
                scope.ServiceProvider.GetRequiredService<ITopicService>();

            await scopedTopicService.EnsureThumbAsync(topicId);
        });
    }
}
