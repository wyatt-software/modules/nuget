﻿// <copyright file="TopicStatus.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Enums;

/// <summary>
/// Valid statuses for classifieds topics.
/// </summary>
public enum TopicStatus
{
    /// <summary>
    /// This topic is not a classified post.
    /// </summary>
    None = 0,

    /// <summary>
    /// This topic is for a single item for sale.
    /// </summary>
    ForSale = 1,

    /// <summary>
    /// There are multiple items for sale within this one topic.
    /// </summary>
    MultipleItems = 2,

    /// <summary>
    /// Expressions of Interest. i.e.: Maybe for sale - without a price.
    /// </summary>
    Eoi = 3,

    /// <summary>
    /// This topic is for an item that is sold, pending payment.
    /// </summary>
    PendingPayment = 4,

    /// <summary>
    /// This topic is for an item that is sold.
    /// </summary>
    Sold = 5,
}
