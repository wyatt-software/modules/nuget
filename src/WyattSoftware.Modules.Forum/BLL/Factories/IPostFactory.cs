﻿// <copyright file="IPostFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Provides a way to create posts without passing all dependencies every time.
/// </summary>
public interface IPostFactory : IDomainFactory<Post, PostEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Post"/> class.
    /// </summary>
    /// <param name="userId">The author.</param>
    /// <param name="topicId">the topic.</param>
    /// <param name="content">The content.</param>
    /// <returns>A new <see cref="Post"/>, injected with the dependencies it needs.</returns>.
    Post New(int userId, int topicId, string content);

    /// <summary>
    /// Initializes a new instance of the <see cref="Post"/> class.
    /// </summary>
    /// <param name="postEntity">The entity to construct the post from.</param>
    /// <returns>
    /// A <see cref="Post"/> based on the <see cref="PostEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new Post New(PostEntity postEntity);
}
