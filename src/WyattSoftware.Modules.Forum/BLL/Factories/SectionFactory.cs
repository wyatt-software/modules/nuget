﻿// <copyright file="SectionFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="ISectionFactory"/> interface.
/// </summary>
public class SectionFactory : ISectionFactory
{
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly ICategoryRepository categoryRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<ICategoryFactory> categoryFactory;
    private readonly Lazy<ITopicFactory> topicFactory;
    private readonly Lazy<IPostFactory> postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="SectionFactory"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    public SectionFactory(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<ICategoryFactory> categoryFactory,
        Lazy<ITopicFactory> topicFactory,
        Lazy<IPostFactory> postFactory)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.categoryFactory = categoryFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;
    }

    /// <inheritdoc />
    public Section New(int categoryId, string codeName, string displayName, string description)
    {
        return new Section(
            this.currentSession,
            this.userRepository,
            this.categoryRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.categoryFactory.Value,
            this.topicFactory.Value,
            this.postFactory.Value,
            categoryId,
            codeName,
            displayName,
            description);
    }

    /// <inheritdoc />
    public Section New(SectionEntity sectionEntity)
    {
        return new Section(
            this.currentSession,
            this.userRepository,
            this.categoryRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.categoryFactory.Value,
            this.topicFactory.Value,
            this.postFactory.Value,
            sectionEntity);
    }
}
