﻿// <copyright file="PostFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Search.BLL.Factories;

/// <summary>
/// Default implementation of the <see cref="ICategoryFactory"/> interface.
/// </summary>
public class PostFactory : IPostFactory
{
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly ISearchDocFactory searchDocFactory;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<IImageFactory> imageFactory;
    private readonly Lazy<ISectionFactory> sectionFactory;
    private readonly Lazy<ITopicFactory> topicFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="PostFactory"/> class.
    /// </summary>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="searchDocFactory">Injected ISearchDocFactory.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    public PostFactory(
        IServiceScopeFactory serviceScopeFactory,
        ICurrentSession currentSession,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        ISearchDocFactory searchDocFactory,
        Lazy<IUserFactory> userFactory,
        Lazy<IImageFactory> imageFactory,
        Lazy<ISectionFactory> sectionFactory,
        Lazy<ITopicFactory> topicFactory)
    {
        this.serviceScopeFactory = serviceScopeFactory;
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.searchDocFactory = searchDocFactory;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
    }

    /// <inheritdoc />
    public Post New(int userId, int topicId, string content)
    {
        return new Post(
            this.serviceScopeFactory,
            this.currentSession,
            this.userRepository,
            this.imageRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.searchDocFactory,
            this.userFactory.Value,
            this.imageFactory.Value,
            this.sectionFactory.Value,
            this.topicFactory.Value,
            userId,
            topicId,
            content);
    }

    /// <inheritdoc />
    public Post New(PostEntity postEntity)
    {
        return new Post(
            this.serviceScopeFactory,
            this.currentSession,
            this.userRepository,
            this.imageRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.searchDocFactory,
            this.userFactory.Value,
            this.imageFactory.Value,
            this.sectionFactory.Value,
            this.topicFactory.Value,
            postEntity);
    }
}
