﻿// <copyright file="TopicFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using System;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Search.BLL.Singletons;

/// <summary>
/// Default implementation of the <see cref="ITopicFactory"/> interface.
/// </summary>
public class TopicFactory : ITopicFactory
{
    private readonly ICurrentSession currentSession;
    private readonly IServiceScopeFactory serviceScopeFactory;
    private readonly ISearchEngine searchEngine;
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ISubscriptionRepository subscriptionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<IImageFactory> imageFactory;
    private readonly Lazy<ISectionFactory> sectionFactory;
    private readonly Lazy<IPostFactory> postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="TopicFactory"/> class.
    /// </summary>
    /// <param name="serviceScopeFactory">Injected IServiceScopeFactory.</param>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="searchEngine">Injected ISearchEngine.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="subscriptionRepository">Injected ISubscriptionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    public TopicFactory(
        IServiceScopeFactory serviceScopeFactory,
        ICurrentSession currentSession,
        ISearchEngine searchEngine,
        IUserRepository userRepository,
        IImageRepository imageRepository,
        ISectionRepository sectionRepository,
        ISubscriptionRepository subscriptionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<IImageFactory> imageFactory,
        Lazy<ISectionFactory> sectionFactory,
        Lazy<IPostFactory> postFactory)
    {
        this.serviceScopeFactory = serviceScopeFactory;
        this.currentSession = currentSession;
        this.searchEngine = searchEngine;
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.sectionRepository = sectionRepository;
        this.subscriptionRepository = subscriptionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.sectionFactory = sectionFactory;
        this.postFactory = postFactory;
    }

    /// <inheritdoc />
    public Topic New(
        int userId,
        int sectionId,
        int? imageId,
        string title,
        string subHeading,
        bool imageAuto,
        int status,
        decimal price)
    {
        return new Topic(
            this.currentSession,
            this.serviceScopeFactory,
            this.searchEngine,
            this.userRepository,
            this.imageRepository,
            this.sectionRepository,
            this.subscriptionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            this.sectionFactory.Value,
            this.postFactory.Value,
            userId,
            sectionId,
            imageId,
            title,
            subHeading,
            imageAuto,
            status,
            price);
    }

    /// <inheritdoc />
    public Topic New(TopicEntity topicEntity)
    {
        return new Topic(
            this.currentSession,
            this.serviceScopeFactory,
            this.searchEngine,
            this.userRepository,
            this.imageRepository,
            this.sectionRepository,
            this.subscriptionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            this.sectionFactory.Value,
            this.postFactory.Value,
            topicEntity);
    }
}