﻿// <copyright file="ISectionFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Provides a way to create sections without passing all dependencies every time.
/// </summary>
public interface ISectionFactory : IDomainFactory<Section, SectionEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Section"/> class.
    /// </summary>
    /// <param name="categoryId">The category.</param>
    /// <param name="codeName">The unique code name.</param>
    /// <param name="displayName">The display name.</param>
    /// <param name="description">The description.</param>
    /// <returns>A new <see cref="Section"/>, injected with the dependencies it needs.</returns>.
    Section New(int categoryId, string codeName, string displayName, string description);

    /// <summary>
    /// Initializes a new instance of the <see cref="Section"/> class.
    /// </summary>
    /// <param name="sectionEntity">The entity to construct the section from.</param>
    /// <returns>
    /// A <see cref="Section"/> based on the <see cref="SectionEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new Section New(SectionEntity sectionEntity);
}
