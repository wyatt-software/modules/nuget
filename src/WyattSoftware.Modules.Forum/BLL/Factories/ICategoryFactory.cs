﻿// <copyright file="ICategoryFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Provides a way to create categories without passing all dependencies every time.
/// </summary>
public interface ICategoryFactory : IDomainFactory<Category, CategoryEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Category"/> class.
    /// </summary>
    /// <param name="displayName">The display name.</param>
    /// <returns>A new <see cref="Category"/>, injected with the dependencies it needs.</returns>.
    Category New(string displayName);

    /// <summary>
    /// Initializes a new instance of the <see cref="Category"/> class.
    /// </summary>
    /// <param name="categoryEntity">The entity to construct the category from.</param>
    /// <returns>
    /// A <see cref="Category"/> based on the <see cref="CategoryEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new Category New(CategoryEntity categoryEntity);
}
