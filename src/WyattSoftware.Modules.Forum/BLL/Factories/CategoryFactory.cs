﻿// <copyright file="CategoryFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="ICategoryFactory"/> interface.
/// </summary>
public class CategoryFactory : ICategoryFactory
{
    private readonly ICurrentSession currentSession;
    private readonly IUserRepository userRepository;
    private readonly ICategoryRepository categoryRepository;
    private readonly ISectionRepository sectionRepository;
    private readonly ITopicRepository topicRepository;
    private readonly IPostRepository postRepository;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<ISectionFactory> sectionFactory;
    private readonly Lazy<ITopicFactory> topicFactory;
    private readonly Lazy<IPostFactory> postFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryFactory"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="categoryRepository">Injected ICategoryRepository.</param>
    /// <param name="sectionRepository">Injected ISectionRepository.</param>
    /// <param name="topicRepository">Injected ITopicRepository.</param>
    /// <param name="postRepository">Injected IPostRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    public CategoryFactory(
        ICurrentSession currentSession,
        IUserRepository userRepository,
        ICategoryRepository categoryRepository,
        ISectionRepository sectionRepository,
        ITopicRepository topicRepository,
        IPostRepository postRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<ISectionFactory> sectionFactory,
        Lazy<ITopicFactory> topicFactory,
        Lazy<IPostFactory> postFactory)
    {
        this.currentSession = currentSession;
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        this.sectionRepository = sectionRepository;
        this.topicRepository = topicRepository;
        this.postRepository = postRepository;
        this.userFactory = userFactory;
        this.sectionFactory = sectionFactory;
        this.topicFactory = topicFactory;
        this.postFactory = postFactory;
    }

    /// <inheritdoc />
    public Category New(string displayName)
    {
        return new Category(
            this.currentSession,
            this.userRepository,
            this.categoryRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.sectionFactory.Value,
            this.topicFactory.Value,
            this.postFactory.Value,
            displayName);
    }

    /// <inheritdoc />
    public Category New(CategoryEntity categoryEntity)
    {
        return new Category(
            this.currentSession,
            this.userRepository,
            this.categoryRepository,
            this.sectionRepository,
            this.topicRepository,
            this.postRepository,
            this.userFactory.Value,
            this.sectionFactory.Value,
            this.topicFactory.Value,
            this.postFactory.Value,
            categoryEntity);
    }
}
