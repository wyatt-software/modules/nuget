﻿// <copyright file="ITopicFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Provides a way to create topics without passing all dependencies every time.
/// </summary>
public interface ITopicFactory : IDomainFactory<Topic, TopicEntity>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="Topic"/> class.
    /// </summary>
    /// <param name="userId">The author.</param>
    /// <param name="sectionId">The section.</param>
    /// <param name="imageId">The image to use as the thumb.</param>
    /// <param name="title">The human readable title.</param>
    /// <param name="subHeading">A subheading to show in listings and at the top of the page.</param>
    /// <param name="imageAuto">A value indicating whether to scrape the thumb image from the content.</param>
    /// <param name="status">The status of a classifieds topic.</param>
    /// <param name="price">The price of a classifieds topic.</param>
    /// <returns>A new <see cref="Topic"/>, injected with the dependencies it needs.</returns>.
    Topic New(
        int userId,
        int sectionId,
        int? imageId,
        string title,
        string subHeading,
        bool imageAuto,
        int status,
        decimal price);

    /// <summary>
    /// Initializes a new instance of the <see cref="Topic"/> class.
    /// </summary>
    /// <param name="topicEntity">The entity to construct the topic from.</param>
    /// <returns>
    /// A <see cref="Topic"/> based on the <see cref="TopicEntity"/>, injected with the dependencies it needs.
    /// </returns>
    /// <remarks>No validation is performed.</remarks>
    new Topic New(TopicEntity topicEntity);
}
