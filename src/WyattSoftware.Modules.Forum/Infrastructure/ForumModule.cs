﻿// <copyright file="ForumModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.Infrastructure;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Forum.API.Controllers;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Forum.DAL.Repositories;
using WyattSoftware.Modules.Forum.Options;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Forum module.
/// </summary>
public static class ForumModule
{
    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddForumModule(
        this IServiceCollection services, IConfiguration configuration)
    {
        var forumSection = configuration.GetSection("WyattSoftware:Modules:Forum");

        // Make WyattSoftware.Modules.Forum appsettings.json options available via dependency injection.
        services.Configure<ForumOptions>(forumSection);

        // DAL
        services.AddScoped(typeof(ICategoryRepository), typeof(CategoryRepository));
        services.AddScoped(typeof(IPostRepository), typeof(PostRepository));
        services.AddScoped(typeof(ISectionRepository), typeof(SectionRepository));
        services.AddScoped(typeof(ISubscriptionRepository), typeof(SubscriptionRepository));
        services.AddScoped(typeof(ITopicRepository), typeof(TopicRepository));

        // BLL
        services.AddScoped(typeof(ICategoryFactory), typeof(CategoryFactory));
        services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
        services.AddScoped(typeof(IPostFactory), typeof(PostFactory));
        services.AddScoped(typeof(IPostService), typeof(PostService));
        services.AddScoped(typeof(ISectionFactory), typeof(SectionFactory));
        services.AddScoped(typeof(ISectionService), typeof(SectionService));
        services.AddScoped(typeof(ITopicFactory), typeof(TopicFactory));
        services.AddScoped(typeof(ITopicService), typeof(TopicService));

        // We need to also register the generic form of PostService so that the base class can get it, while only
        // knowing the <c>Post</c> type.
        services.AddScoped(typeof(IDomainService<Post>), typeof(PostService));

        return services;
    }

    /// <summary>
    /// Configures the Forum module.
    /// </summary>
    /// <param name="app">The app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseForumModule(this IApplicationBuilder app)
    {
        using var scope = app.ApplicationServices.CreateScope();
        var userAddOn = scope.ServiceProvider.GetService<IUserAddOn>();
        if (userAddOn == null)
        {
            throw new Exception("Couldn't get an IUserAddOn.");
        }

        var imageAddOn = scope.ServiceProvider.GetService<IImageAddOn>();
        if (imageAddOn == null)
        {
            throw new Exception("Couldn't get an IImageAddOn.");
        }

        // DAL
        Topic.RegisterBeforeDeleteHandlers(imageAddOn);
        PostRepository.RegisterSelectClauses(userAddOn);
        PostRepository.RegisterSortClauses(userAddOn);

        // API
        CategoriesController.InitMapping();
        SectionsController.InitMapping();
        TopicsController.InitMapping();
        PostsController.InitMapping();

        return app;
    }
}