﻿// <copyright file="CategoriesController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.API.DTOs;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;

/// <summary>
/// Handles forum categories.
/// </summary>
[Route("forum/categories")]
public class CategoriesController : WsControllerBase
{
    private readonly ICategoryFactory categoryFactory;
    private readonly ICategoryService categoryService;

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoriesController"/> class.
    /// </summary>
    /// <param name="categoryFactory">Injected ICategoryFactory.</param>
    /// <param name="categoryService">Injected ICategoryService.</param>
    public CategoriesController(
        ICategoryFactory categoryFactory,
        ICategoryService categoryService)
    {
        this.categoryFactory = categoryFactory;
        this.categoryService = categoryService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Category, CategoryResponse>(category =>
        {
            var response = new CategoryResponse();
            response.InjectFrom(category);
            response.Sections = category.Sections?
                .Select(section => Mapper.Map<SectionResponse>(section))
                .ToList();

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Category>, PaginatedResponse<CategoryResponse>>(categories =>
            new PaginatedResponse<CategoryResponse>()
            {
                Total = categories.Total,
                Offset = categories.Offset,
                Limit = categories.Limit,
                Results = categories.Results.Select(x => Mapper.Map<CategoryResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get all forum categories.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic] .</para>
    /// </param>
    /// <returns>A collection of categories.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(string expand = "")
    {
        // Don't paginate. Always get them all.
        var categories = await this.categoryService.FetchAsync(expand, 0, 0);

        var response = Mapper.Map<PaginatedResponse<CategoryResponse>>(categories);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a category.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>The newly created category.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Create([FromBody]CategoryUpsertRequest request)
    {
        // Try to construct a domain model to trigger validation.
        var category = this.categoryFactory.New(request.DisplayName);

        await category.CreateAsync();
        await category.ExpandAsync("all");

        var response = Mapper.Map<CategoryResponse>(category);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single category.
    /// </summary>
    /// <param name="categoryId">The primary key for the category.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|sections|sections.lastpost|sections.lastpost.user|sections.lastpost.topic] .</para>
    /// </param>
    /// <returns>A single category.</returns>
    [HttpGet]
    [Route("{categoryId:int}")]
    public async Task<IActionResult> Read(int categoryId, string expand = "")
    {
        var category = await this.categoryService.ReadAsync(categoryId, expand);

        var response = Mapper.Map<CategoryResponse>(category);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a category.
    /// </summary>
    /// <param name="categoryId">The primary key for the category.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The newly updated category.</returns>
    [HttpPut]
    [Route("{categoryId:int}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Update(int categoryId, [FromBody]CategoryUpsertRequest request)
    {
        var category = await this.categoryService.ReadAsync(categoryId);
        category.DisplayName = request.DisplayName;

        await category.UpdateAsync();
        await category.ExpandAsync("all");

        var response = Mapper.Map<CategoryResponse>(category);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a category.
    /// </summary>
    /// <param name="categoryId">The primary key for the category.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{categoryId:int}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Delete(int categoryId)
    {
        var category = await this.categoryService.ReadAsync(categoryId);

        await category.DeleteAsync();

        return this.Ok(null);
    }
}
