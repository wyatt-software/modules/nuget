﻿// <copyright file="TopicsController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.API.DTOs;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Gallery.API.DTOs;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Context;

/// <summary>
/// Handles forum topics.
/// </summary>
[Route("forum/topics")]
public class TopicsController : WsControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly ITopicFactory topicFactory;
    private readonly ITopicService topicService;

    /// <summary>
    /// Initializes a new instance of the <see cref="TopicsController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="topicFactory">Injected ITopicFactory.</param>
    /// <param name="topicService">Injected ITopicsService.</param>
    public TopicsController(
        ICurrentSession currentSession,
        ITopicFactory topicFactory,
        ITopicService topicService)
    {
        this.currentSession = currentSession;
        this.topicFactory = topicFactory;
        this.topicService = topicService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Topic?, TopicResponse?>(topic =>
        {
            if (topic == null)
            {
                return null;
            }

            var response = new TopicResponse();
            response.InjectFrom(topic);

            response.Section = topic.Section != null
                ? Mapper.Map<SectionResponse>(topic.Section)
                : null;

            response.User = topic.User != null
                ? Mapper.Map<UserResponse>(topic.User)
                : null;

            response.LastPost = topic.LastPost != null
                ? Mapper.Map<PostResponse>(topic.LastPost)
                : null;

            response.Image = topic.Image != null
                ? Mapper.Map<ImageResponse>(topic.Image)
                : null;

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Topic>, PaginatedResponse<TopicResponse>>(topics =>
            new PaginatedResponse<TopicResponse>()
            {
                Total = topics.Total,
                Offset = topics.Offset,
                Limit = topics.Limit,
                Results = topics.Results.Select(x => Mapper.Map<TopicResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get a collection of forum topics.
    /// </summary>
    /// <param name="section">A sectionId or section code name to filter by.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|section|user|lastpost|lastpost.user|image] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="username">Username of the user to get topics for.</param>
    /// <param name="subscribed">Get only topics the currently authenticated user is subscribed to.</param>
    /// <returns>A collection of forum topics.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(
        string section = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        string username = "",
        bool? subscribed = null)
    {
        // Note: For the 'subscribed' filter to work the API request needs to include the Authorize header, even
        // though the endpoint doesn't require authorization.
        var topics = await this.topicService.FetchAsync(section, expand, offset, limit, username, subscribed);

        var response = Mapper.Map<PaginatedResponse<TopicResponse>>(topics);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a forum topic.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>The newly created forum topic.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Create([FromBody]TopicCreateRequest request)
    {
        // Try to construct a domain model to trigger validation.
        var topic = this.topicFactory.New(
            this.currentSession.UserId,
            request.SectionId,
            request.ImageId,
            request.Title,
            request.SubHeading,
            request.ImageAuto,
            request.Status,
            request.Price);

        await topic.CreateAsync(request.Content);
        await topic.ExpandAsync("all");

        var response = Mapper.Map<TopicResponse>(topic);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single forum topic.
    /// </summary>
    /// <param name="topicIdOrAlias">The topicId or alias of the topic.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|section|user|lastpost|lastpost.user|image] .</para>
    /// </param>
    /// <returns>A single forum topic.</returns>
    [HttpGet]
    [Route("{topicIdOrAlias}")]
    public async Task<IActionResult> Read(string topicIdOrAlias, string expand = "")
    {
        var topic = await this.topicService.ReadAsync(topicIdOrAlias, expand);
        var response = Mapper.Map<TopicResponse>(topic);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a forum topic.
    /// </summary>
    /// <param name="topicIdOrAlias">The topicId or alias of the topic.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated forum topic.</returns>
    [HttpPut]
    [Route("{topicIdOrAlias}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Update(string topicIdOrAlias, [FromBody]TopicUpdateRequest request)
    {
        var topic = await this.topicService.ReadAsync(topicIdOrAlias);

        this.currentSession.AssertResourceOwnerOrRole(topic.UserId, "Moderator");

        topic.SectionId = request.SectionId;
        topic.ImageId = request.ImageId;
        topic.Title = request.Title;
        topic.SubHeading = request.SubHeading;
        topic.ImageAuto = request.ImageAuto;
        topic.Status = request.Status;
        topic.Price = request.Price;

        await topic.UpdateAsync();
        await topic.ExpandAsync("all");

        var response = Mapper.Map<TopicResponse>(topic);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a topic.
    /// </summary>
    /// <param name="topicIdOrAlias">The topicId or alias of the topic.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{topicIdOrAlias}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Delete(string topicIdOrAlias)
    {
        var topic = await this.topicService.ReadAsync(topicIdOrAlias);

        this.currentSession.AssertResourceOwnerOrRole(topic.UserId, "Moderator");

        await topic.DeleteAsync();

        return this.Ok(null);
    }

    /// <summary>
    /// Subscribes the currently authenticated user to a forum topic.
    /// </summary>
    /// <param name="topicIdOrAlias">The topicId or alias of the topic.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{topicIdOrAlias}/actions/subscribe")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Subscribe(string topicIdOrAlias)
    {
        var topic = await this.topicService.ReadAsync(topicIdOrAlias);
        await topic.SubscribeAsync(this.currentSession.UserId);
        return this.Ok(null);
    }

    /// <summary>
    /// Unsubscribes the currently authenticated user to a forum topic.
    /// </summary>
    /// <param name="topicIdOrAlias">The topicId or alias of the topic.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{topicIdOrAlias}/actions/unsubscribe")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Unsubscribe(string topicIdOrAlias)
    {
        var topic = await this.topicService.ReadAsync(topicIdOrAlias);
        await topic.UnsubscribeAsync(this.currentSession.UserId);
        return this.Ok(null);
    }
}
