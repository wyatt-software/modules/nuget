﻿// <copyright file="PostsController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.API.DTOs;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Context;

/// <summary>
/// Handles forum posts.
/// </summary>
[Route("forum/posts")]
public class PostsController : WsControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly IPostFactory postFactory;
    private readonly IPostService postService;
    private readonly ITopicService topicService;

    /// <summary>
    /// Initializes a new instance of the <see cref="PostsController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="postFactory">Injected IPostFactory.</param>
    /// <param name="postService">Injected IPostsService.</param>
    /// <param name="topicService">Injected ITopicsService.</param>
    public PostsController(
        ICurrentSession currentSession,
        IPostFactory postFactory,
        IPostService postService,
        ITopicService topicService)
    {
        this.currentSession = currentSession;
        this.postFactory = postFactory;
        this.postService = postService;
        this.topicService = topicService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Post?, PostResponse?>(post =>
        {
            if (post == null)
            {
                return null;
            }

            var response = new PostResponse();
            response.InjectFrom(post);

            response.Topic = post.Topic != null
                ? Mapper.Map<TopicResponse>(post.Topic)
                : null;

            response.User = post.User != null
                ? Mapper.Map<UserResponse>(post.User)
                : null;

            response.EditedByUser = post.EditedByUser != null
                ? Mapper.Map<UserResponse>(post.EditedByUser)
                : null;

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Post>, PaginatedResponse<PostResponse>>(posts =>
            new PaginatedResponse<PostResponse>()
            {
                Total = posts.Total,
                Offset = posts.Offset,
                Limit = posts.Limit,
                Results = posts.Results.Select(x => Mapper.Map<PostResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get a collection of forum posts.
    /// </summary>
    /// <param name="topic">A TopicId or topic alias to filter by.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|editedbyuser] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="postId">If specified, the offset (page) is adjusted to ensure the post is within the results.</param>
    /// <param name="username">Username of the user to get posts for.</param>
    /// <param name="sort">The field to sort by (with a '-' prefix if descending).</param>
    /// <returns>A collection of forum posts.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(
        string topic = "",
        string expand = "",
        int offset = 0,
        int limit = 20,
        int postId = 0,
        string username = "",
        string sort = "created")
    {
        var posts = await this.postService.FetchAsync(topic, expand, offset, limit, postId, username, sort);
        var response = Mapper.Map<PaginatedResponse<PostResponse>>(posts);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a forum post.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>The newly created post.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Create([FromBody]PostCreateRequest request)
    {
        // Try to construct a domain model to trigger validation.
        var post = this.postFactory.New(
            this.currentSession.UserId,
            request.TopicId,
            request.Content);

        await post.CreateAsync();
        await post.ExpandAsync("all");

        // Subscription
        if (request.Subscribe && post.Topic != null)
        {
            await post.Topic.SubscribeAsync(this.currentSession.UserId);
        }

        var response = Mapper.Map<PostResponse>(post);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single forum post.
    /// </summary>
    /// <param name="postId">The primary key of the post.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|topic|topic.section|user|editedbyuser] .</para>
    /// </param>
    /// <returns>A single forum post.</returns>
    [HttpGet]
    [Route("{postId:int}")]
    public async Task<IActionResult> Read(int postId, string expand = "")
    {
        var post = await this.postService.ReadAsync(postId, expand);

        var response = Mapper.Map<PostResponse>(post);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a forum post.
    /// </summary>
    /// <param name="postId">The primary key of the post.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated forum post.</returns>
    [HttpPut]
    [Route("{postId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Update(int postId, [FromBody]PostUpdateRequest request)
    {
        var post = await this.postService.ReadAsync(postId);

        this.currentSession.AssertResourceOwnerOrRole(post.UserId, "Moderator");

        post.Content = request.Content;

        await post.UpdateAsync();
        await post.ExpandAsync("all");

        var response = Mapper.Map<PostResponse>(post);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a forum post.
    /// </summary>
    /// <param name="postId">The primary key of the post.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{postId:int}")]
    [Authorize(Roles = "Member")]
    public async Task<IActionResult> Delete(int postId)
    {
        var post = await this.postService.ReadAsync(postId);

        this.currentSession.AssertResourceOwnerOrRole(post.UserId, "Moderator");

        await post.DeleteAsync();

        return this.Ok(null);
    }
}
