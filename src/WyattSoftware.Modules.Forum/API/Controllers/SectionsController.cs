﻿// <copyright file="SectionsController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Forum.API.DTOs;
using WyattSoftware.Modules.Forum.BLL.Factories;
using WyattSoftware.Modules.Forum.BLL.Models;
using WyattSoftware.Modules.Forum.BLL.Services;

/// <summary>
/// Handles forum sections.
/// </summary>
[Route("forum/sections")]
public class SectionsController : WsControllerBase
{
    private readonly ISectionFactory sectionFactory;
    private readonly ISectionService sectionService;

    /// <summary>
    /// Initializes a new instance of the <see cref="SectionsController"/> class.
    /// </summary>
    /// <param name="sectionFactory">Injected ISectionFactory.</param>
    /// <param name="sectionService">Injected ISectionService.</param>
    public SectionsController(
        ISectionFactory sectionFactory,
        ISectionService sectionService)
    {
        this.sectionFactory = sectionFactory;
        this.sectionService = sectionService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Section?, SectionResponse?>(section =>
        {
            if (section == null)
            {
                return null;
            }

            var response = new SectionResponse();
            response.InjectFrom(section);

            response.Category = section.Category != null
                ? Mapper.Map<CategoryResponse>(section.Category)
                : null;

            response.LastPost = section.LastPost != null
                ? Mapper.Map<PostResponse>(section.LastPost)
                : null;

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Section>, PaginatedResponse<SectionResponse>>(posts =>
            new PaginatedResponse<SectionResponse>()
            {
                Total = posts.Total,
                Offset = posts.Offset,
                Limit = posts.Limit,
                Results = posts.Results.Select(x => Mapper.Map<SectionResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Gets a collection of forum sections.
    /// </summary>
    /// <param name="categoryId">A forum category to get sections for.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of forum sections.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    public async Task<IActionResult> Fetch(int categoryId = 0, string expand = "", int offset = 0, int limit = 20)
    {
        var sections = await this.sectionService.FetchAsync(categoryId, expand, offset, limit);

        var response = Mapper.Map<PaginatedResponse<SectionResponse>>(sections);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Creates a forum section.
    /// </summary>
    /// <param name="request">The request object.</param>
    /// <returns>The newly created section.</returns>
    [HttpPost]
    [Route("")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Create([FromBody]SectionUpsertRequest request)
    {
        // Try to construct a domain model to trigger validation.
        var section = this.sectionFactory.New(
            request.CategoryId,
            request.CodeName,
            request.DisplayName,
            request.Description);

        await section.CreateAsync();
        await section.ExpandAsync("all");

        var response = Mapper.Map<SectionResponse>(section);
        return this.Ok(response);
    }

    /// <summary>
    /// Gets a single forum section.
    /// </summary>
    /// <param name="sectionIdOrCodeName">The sectionId or codename of the section.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|category|lastpost|lastpost.topic|lastpost.user] .</para>
    /// </param>
    /// <returns>A single forum section.</returns>
    [HttpGet]
    [Route("{sectionIdOrCodeName}")]
    public async Task<IActionResult> Read(string sectionIdOrCodeName, string expand = "")
    {
        var section = await this.sectionService.ReadAsync(sectionIdOrCodeName, expand);

        var response = Mapper.Map<SectionResponse>(section);
        return this.Ok(response);
    }

    /// <summary>
    /// Updates a forum section.
    /// </summary>
    /// <param name="sectionIdOrCodeName">The sectionId or codename of the section.</param>
    /// <param name="request">The request object.</param>
    /// <returns>The updated forum section.</returns>
    [HttpPut]
    [Route("{sectionIdOrCodeName}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Update(string sectionIdOrCodeName, [FromBody]SectionUpsertRequest request)
    {
        var section = await this.sectionService.ReadAsync(sectionIdOrCodeName);

        section.CategoryId = request.CategoryId;
        section.CodeName = request.CodeName;
        section.DisplayName = request.DisplayName;
        section.Description = request.Description;
        section.Classifieds = request.Classifieds;

        await section.UpdateAsync();
        await section.ExpandAsync("all");

        var response = Mapper.Map<SectionResponse>(section);
        return this.Ok(response);
    }

    /// <summary>
    /// Deletes a forum section.
    /// </summary>
    /// <param name="sectionIdOrCodeName">The sectionId or codename of the section.</param>
    /// <param name="request">The request object.</param>
    /// <returns>An empty response body.</returns>
    [HttpDelete]
    [Route("{sectionIdOrCodeName}")]
    [Authorize(Roles = "Administrator")]
    public async Task<IActionResult> Delete(string sectionIdOrCodeName, [FromBody]SectionDeleteRequest request)
    {
        var section = await this.sectionService.ReadAsync(sectionIdOrCodeName);

        await section.DeleteAsync(request.MoveOrphanedTopicsToSectionId);

        return this.Ok(null);
    }
}
