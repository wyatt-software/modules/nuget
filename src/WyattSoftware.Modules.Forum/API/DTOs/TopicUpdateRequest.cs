﻿// <copyright file="TopicUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Topic update request.
/// </summary>
/// <remarks>Mapped from the <see cref="Topic" /> model.</remarks>
public class TopicUpdateRequest
{
    /// <summary>
    /// Gets or sets a foreign key to the section this topic belongs to.
    /// </summary>
    public int SectionId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the thumb image for this topic.
    /// </summary>
    public int? ImageId { get; set; }

    /// <summary>
    /// Gets or sets the title of the topic.
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a sub-heading for this topic.
    /// </summary>
    public string SubHeading { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the thumb image for this topic is to be automatically taken from the
    /// first image in the first post.
    /// </summary>
    public bool ImageAuto { get; set; }

    /// <summary>
    /// Gets or sets the status of the classifieds topic.
    /// </summary>
    /// <remarks>
    /// <list type="number">
    /// <item>For Sale</item>
    /// <item>Multiple Items</item>
    /// <item>E.O.I</item>
    /// <item>Pending Payment</item>
    /// <item>Sold</item>
    /// </list>
    /// </remarks>
    public int Status { get; set; }

    /// <summary>
    /// Gets or sets the asking price of the classifieds topic.
    /// </summary>
    public decimal Price { get; set; }
}
