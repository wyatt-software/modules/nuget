﻿// <copyright file="PostResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

using System;
using WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Post response.
/// </summary>
/// <remarks>Mapped from the <see cref="Post" /> model.</remarks>
public class PostResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the date this post was created.
    /// </summary>
    public DateTime Created { get; set; }

    /// <summary>
    /// Gets or sets the date the post was last inserted or updated.
    /// </summary>
    public DateTime Modified { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the community user that created the post.
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the community user that last edited the post.
    /// </summary>
    public int? EditedByUserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the topic this post belongs to.
    /// </summary>
    public int TopicId { get; set; }

    /// <summary>
    /// Gets or sets the content of the post.
    /// </summary>
    public string Content { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the date the post was last edited.
    /// </summary>
    /// <remarks>
    /// This is probably redundant, since <c>Modified</c> should hold the same value, but we're keeping it because
    /// it's meaning is explicit.
    /// </remarks>
    public DateTime? EditedDate { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Derived fields

    /// <summary>
    /// <para>Gets or sets the posts position in the topic.</para>
    /// <para>The first post is post #1. The first reply is post #2.</para>
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public int PostNumber { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the topic the post belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=topic".
    /// </remarks>
    public TopicResponse? Topic { get; set; }

    /// <summary>
    /// Gets or sets the user who created the post.
    /// </summary>
    /// <remarks>
    /// Use "expand=user".
    /// </remarks>
    public UserResponse? User { get; set; }

    /// <summary>
    /// Gets or sets the user who last edited the post.
    /// </summary>
    /// <remarks>
    /// Use "expand=editedbyuser".
    /// </remarks>
    public UserResponse? EditedByUser { get; set; }
}
