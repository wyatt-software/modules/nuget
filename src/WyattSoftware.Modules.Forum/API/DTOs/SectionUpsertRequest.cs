﻿// <copyright file="SectionUpsertRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Section create or update request.
/// </summary>
/// <remarks>Mapped from the <see cref="Section" /> model.</remarks>
public class SectionUpsertRequest
{
    /// <summary>
    /// Gets or sets a foreign key to the forum category this section belongs to.
    /// </summary>
    public int CategoryId { get; set; }

    /// <summary>
    /// Gets or sets a unique code name that can be used in URLs.
    /// </summary>
    public string CodeName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name of the section.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a brief description for the section.
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the section is for classifieds ("for sale") topics.
    /// </summary>
    public bool Classifieds { get; set; }
}
