﻿// <copyright file="CategoryUpsertRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Category create or update request.
/// </summary>
/// <remarks>Mapped from the <see cref="Category" /> model.</remarks>
public class CategoryUpsertRequest
{
    /// <summary>
    /// Gets or sets the name of the category.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;
}
