﻿// <copyright file="PostUpdateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Post update request.
/// </summary>
/// <remarks>Mapped to the <see cref="Post" /> model.</remarks>
public class PostUpdateRequest
{
    /// <summary>
    /// Gets or sets the content of the post.
    /// </summary>
    public string Content { get; set; } = string.Empty;
}
