﻿// <copyright file="TopicResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

using System;
using WyattSoftware.Modules.Gallery.API.DTOs;
using WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// Topic response.
/// </summary>
/// <remarks>Mapped from the <see cref="Topic" /> model.</remarks>
public class TopicResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the section this topic belongs to.
    /// </summary>
    public int SectionId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the community user that created the topic.
    /// </summary>
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the gallery image thumbnail for the topic.
    /// </summary>
    public int? ImageId { get; set; }

    /// <summary>
    /// Gets or sets the title of the topic.
    /// </summary>
    public string Title { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a unique URL-friendly alias for this topic.
    /// </summary>
    public string Alias { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a sub-heading for this topic.
    /// </summary>
    public string SubHeading { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the topic is "stickied" to the top of the section.
    /// </summary>
    public bool Stickied { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the topic is locked (meaning no posts can be created or edited).
    /// </summary>
    public bool Locked { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the thumb image for this topic is to be automatically taken from the
    /// first image in the first post.
    /// </summary>
    public bool ImageAuto { get; set; }

    /// <summary>
    /// Gets or sets the number of times the topic has been viewed.
    /// </summary>
    public int ViewCounter { get; set; }

    /// <summary>
    /// Gets or sets the status of the classifieds topic.
    /// </summary>
    /// <remarks>
    /// <list type="number">
    /// <item>For Sale</item>
    /// <item>Multiple Items</item>
    /// <item>E.O.I</item>
    /// <item>Pending Payment</item>
    /// <item>Sold</item>
    /// </list>
    /// </remarks>
    public int Status { get; set; }

    /// <summary>
    /// Gets or sets the asking price of the classifieds topic.
    /// </summary>
    public decimal Price { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Derived fields

    /// <summary>
    /// Gets or sets a foreign key to the last post created in this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public int LastPostId { get; set; }

    /// <summary>
    /// Gets or sets the date the last post was created in this topic.
    /// </summary>
    /// <remarks>Redundant field, stored in the DB so that we can sort on it.</remarks>
    public DateTime? LastPostCreated { get; set; }

    /// <summary>
    /// Gets or sets the number of posts in this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public int PostCount { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the currently authenticated user is subscribed to this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public bool Subscribed { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the section this topic belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=section".
    /// </remarks>
    public SectionResponse? Section { get; set; }

    /// <summary>
    /// Gets or sets the user who created this topic.
    /// </summary>
    /// <remarks>
    /// Use "expand=user".
    /// </remarks>
    public UserResponse? User { get; set; }

    /// <summary>
    /// Gets or sets the user who last edited the post.
    /// </summary>
    /// <remarks>
    /// Use "expand=lastpost".
    /// </remarks>
    public PostResponse? LastPost { get; set; }

    /// <summary>
    /// Gets or sets the cover image for this album.
    /// </summary>
    /// <remarks>
    /// Use "expand=image".
    /// </remarks>
    public ImageResponse? Image { get; set; }
}
