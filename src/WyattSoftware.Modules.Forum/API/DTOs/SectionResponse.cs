﻿// <copyright file="SectionResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Section response.
/// </summary>
/// <remarks>Mapped from the <see cref="Section" /> model.</remarks>
public class SectionResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the forum category this section belongs to.
    /// </summary>
    public int CategoryId { get; set; }

    /// <summary>
    /// Gets or sets a unique code name that can be used in URLs.
    /// </summary>
    public string CodeName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name of the section.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a brief description for the section.
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the section is for classifieds ("for sale") topics.
    /// </summary>
    public bool Classifieds { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Derived fields

    /// <summary>
    /// Gets or sets a foreign key to the last post created in any topic under this section.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public int LastPostId { get; set; }

    /// <summary>
    /// Gets or sets the total number of posts in any topic under this section.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    public int TopicCount { get; set; }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the category the section belongs to.
    /// </summary>
    /// <remarks>
    /// Use "expand=category".
    /// </remarks>
    public CategoryResponse? Category { get; set; }

    /// <summary>
    /// Gets or sets the last post created in any topic under this section.
    /// </summary>
    /// <remarks>
    /// Use "expand=lastpost".
    /// </remarks>
    public PostResponse? LastPost { get; set; }
}
