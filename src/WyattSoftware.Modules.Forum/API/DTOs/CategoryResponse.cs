﻿// <copyright file="CategoryResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Category response.
/// </summary>
/// <remarks>Mapped from the <see cref="Category" /> model.</remarks>
public class CategoryResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the name of the category.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets a list of child sections.
    /// </summary>
    /// <remarks>
    /// This is a special case, because it's a collection of child objects. Usually we only expand single foreign
    /// keys and parent objects.
    /// </remarks>
    public List<SectionResponse>? Sections { get; set; }
}
