﻿// <copyright file="PostCreateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Post create request.
/// </summary>
/// <remarks>Mapped to the <see cref="Post" /> model.</remarks>
public class PostCreateRequest
{
    /// <summary>
    /// Gets or sets the topic to add the post to.
    /// </summary>
    public int TopicId { get; set; }

    /// <summary>
    /// Gets or sets the content of the post.
    /// </summary>
    public string Content { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the user should be subscribed to the topic.
    /// </summary>
    public bool Subscribe { get; set; }
}
