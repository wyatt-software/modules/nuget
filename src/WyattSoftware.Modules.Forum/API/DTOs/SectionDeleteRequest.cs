﻿// <copyright file="SectionDeleteRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.API.DTOs;

/// <summary>
/// Section delete request.
/// </summary>
public class SectionDeleteRequest
{
    /// <summary>
    /// Gets or sets the section to move orphaned topics when this section is deleted.
    /// </summary>
    public int MoveOrphanedTopicsToSectionId { get; set; }
}
