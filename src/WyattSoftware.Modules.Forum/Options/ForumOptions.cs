﻿// <copyright file="ForumOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Forum section.
/// </summary>
public class ForumOptions
{
    /// <summary>
    /// Gets or sets the TopicThumb options.
    /// </summary>
    public ForumTopicThumbOptions TopicThumb { get; set; } = new ForumTopicThumbOptions();
}
