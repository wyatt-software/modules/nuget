﻿// <copyright file="ForumTopicThumbOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Forum.TopicThumb section.
/// </summary>
public class ForumTopicThumbOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets the name of the user gallery folder to create and automatically upload external images to for
    /// topic thumbnails.
    /// </para>
    /// <para>
    /// Eg: <c>"Topic Thumbs"</c>.
    /// </para>
    /// </summary>
    public string AlbumName { get; set; } = "Topic Thumbs";

    /// <summary>
    /// Gets or sets the Base absolute URL to use when scraping images with relative URLs.
    /// </summary>
    public string ScraperBaseUrl { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets an array of regex patterns that descript blacklisted URLs.
    /// </summary>
    public string[] ScraperBlackList { get; set; } = Array.Empty<string>();
}