﻿// <copyright file="PostRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Forum.DAL.Entities;
using WyattSoftware.Modules.Identity.Infrastructure;

/// <summary>
/// Dapper implementation of the forum post repository.
/// </summary>
public class PostRepository : BaseRepository<PostEntity>, IPostRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PostRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public PostRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <summary>
    /// Registers sort clauses used in the UserRepository.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    public static void RegisterSelectClauses(IUserAddOn userAddOn)
    {
        userAddOn.SelectClauses.Add(
            "postCount",
            "SELECT COUNT(FP.id) FROM forum_post FP WHERE FP.identity_user_id = IU.id");
    }

    /// <summary>
    /// Registers sort clauses used in the UserRepository.
    /// </summary>
    /// <param name="userAddOn">Injected IUserAddOn.</param>
    public static void RegisterSortClauses(IUserAddOn userAddOn)
    {
        userAddOn.SortClauses.Add(
            "postcount",
            "(SELECT COUNT(FP.id) FROM forum_post FP WHERE FP.identity_user_id = IU.id)");

        // This isn't really related to forum posts, but there's no good place for it and we're registering sort
        // clauses here anyway.
        userAddOn.SortClauses.Add(
            "location",
            "IU.properties->>'$.location'");
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(int topicId = 0, int userId = 0)
    {
        var query = this.Db.Query(this.Table).AsCount();
        query = FilterPosts(query, topicId, userId);
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<int> GetOffsetAsync(int topicId, int postId, int limit, int userId)
    {
        // Don't divide by zero.
        if (limit == 0)
        {
            return 0;
        }

        var query = this.Db.Query()
            .Select("post_number")
            .From(this.Db.Query()
                .SelectRaw("ROW_NUMBER() OVER (ORDER BY created) AS post_number")
                .Select("id")
                .From(this.Table)
                .When(topicId > 0, q => q.Where("forum_topic_id", topicId))
                .When(userId > 0, q => q.Where("identity_user_id", userId))
                .As("X"))
            .Where("id", postId);

        // Get the post's position in the topic.
        var postNumber = await this.Db.ExecuteScalarAsync<int>(query);

        // Post not found in this topic?
        if (postNumber == 0)
        {
            return 0;
        }

        // Return the offset, based on page sizes defined by 'limit'.
        var page = (int)Math.Floor((decimal)(postNumber - 1) / limit);
        return page * limit;
    }

    /// <inheritdoc/>
    public async Task<List<PostEntity>> FetchAsync(
        int topicId = 0, int offset = 0, int limit = 20, int userId = 0, string sort = "created")
    {
        var query = this.Db.Query(this.Table)
            .OrderByRaw(OrderByColumn(sort))
            .Offset(offset)
            .Limit(limit);

        query = FilterPosts(query, topicId, userId);
        var results = await query.GetAsync<PostEntity>();
        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<PostEntity> ReadLastPostBySectionIdAsync(int sectionId)
    {
        return await this.Db.Query(this.Table)
            .WhereIn("forum_topic_id", q => q
                .Select("id")
                .From("forum_topic")
                .Where("forum_section_id", sectionId))
            .OrderByDesc("id")
            .FirstOrDefaultAsync<PostEntity>();
    }

    /// <inheritdoc/>
    public async Task<PostEntity> ReadLastPostByTopicIdAsync(int topicId)
    {
        return await this.Db.Query(this.Table)
            .Where("forum_topic_id", topicId)
            .OrderByDesc("id")
            .FirstOrDefaultAsync<PostEntity>();
    }

    /// <summary>
    /// Modifies the query to filter by the given search criteria.
    /// </summary>
    /// <param name="query">The query to modify.</param>
    /// <param name="topicId">Get only posts within this topic.</param>
    /// <param name="userId">Get only posts created by this user.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterPosts(Query query, int topicId = 0, int userId = 0)
    {
        return query
            .When(topicId > 0, q => q.Where("forum_topic_id", topicId))
            .When(userId > 0, q => q.Where("identity_user_id", userId));
    }

    /// <summary>
    /// Get the SQL table's ORDER BY column, based on the model's property name.
    /// </summary>
    /// <param name="sort">The model field to sort by, with a '-' prefix if descending.</param>
    /// <returns>An ORDER BY clause.</returns>
    private static string OrderByColumn(string sort)
    {
        sort = sort.ToLower();

        if (string.IsNullOrEmpty(sort))
        {
            sort = "created";
        }

        bool desc;
        if (sort[..1] == "-")
        {
            desc = true;
            sort = sort[1..];
        }
        else
        {
            desc = false;
        }

        var columns = new Dictionary<string, string>
        {
            { "created", "created" },
        };

        if (!columns.ContainsKey(sort))
        {
            sort = "created";
        }

        return columns[sort] + (desc ? " DESC" : string.Empty);
    }
}
