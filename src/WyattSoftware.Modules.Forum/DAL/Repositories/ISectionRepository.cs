﻿// <copyright file="ISectionRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the forum section repository.
/// </summary>
public interface ISectionRepository : IRepository<SectionEntity>
{
    /// <summary>
    /// Gets the total number of sections in this category.
    /// </summary>
    /// <param name="categoryId">The category to counts sections in.</param>
    /// <returns>The total number of sections in this category.</returns>
    Task<int> GetCountAsync(int categoryId);

    /// <summary>
    /// Get the sections in a specific category.
    /// </summary>
    /// <param name="categoryId">The category to get the sections from.</param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>The sections in a specific category.</returns>
    Task<List<SectionEntity>> FetchAsync(int categoryId, int offset = 0, int limit = 20);

    /// <summary>
    /// Moves all child topics from one section to another.
    /// </summary>
    /// <param name="fromSectionId">The section to move from.</param>
    /// <param name="toSectionId">The section to move to.</param>
    /// <returns>Nothing.</returns>
    Task MoveTopicsToSectionAsync(int fromSectionId, int toSectionId);
}
