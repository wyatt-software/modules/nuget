﻿// <copyright file="TopicRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Dapper implementation of the forum topic repository.
/// </summary>
public class TopicRepository : BaseRepository<TopicEntity>, ITopicRepository
{
    private const string SelectWithDerivedFieldsSql =
@"FT.*,
(
	SELECT id FROM forum_post WHERE forum_topic_id = FT.id ORDER BY id LIMIT 1
) AS derived__first_post_id,
(
	SELECT id FROM forum_post WHERE forum_topic_id = FT.id ORDER BY id DESC LIMIT 1
) AS derived__last_post_id,
(
	SELECT COUNT(id) FROM forum_post WHERE forum_topic_id = FT.id
) AS derived__post_count,
CASE WHEN FS.id IS NULL THEN 0 ELSE 1 END AS derived__subscribed";

    /// <summary>
    /// Initializes a new instance of the <see cref="TopicRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public TopicRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(
        int authenticatedUserId = 0, int sectionId = 0, int topicUserId = 0, bool? subscribed = null)
    {
        var query = this.SelectWithDerivedFields(authenticatedUserId).AsCount();
        query = FilterTopics(query, authenticatedUserId, sectionId, topicUserId, subscribed);
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public new Task<List<TopicEntity>> FetchAsync(int[] ids)
    {
        throw new Exception("Use `FetchAsync(int[] ids, int authenticatedUserId)` instead.");
    }

    /// <inheritdoc/>
    public async Task<List<TopicEntity>> FetchAsync(int[] ids, int authenticatedUserId)
    {
        var results = await this.SelectWithDerivedFields(authenticatedUserId)
            .WhereIn("FT.id", ids)
            .GetAsync<TopicEntity>();

        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<List<TopicEntity>> FetchAsync(
        int authenticatedUserId = 0,
        int sectionId = 0,
        int offset = 0,
        int limit = 20,
        int topicUserId = 0,
        bool? subscribed = null)
    {
        var query = this.SelectWithDerivedFields(authenticatedUserId);

        query = FilterTopics(query, authenticatedUserId, sectionId, topicUserId, subscribed);

        query = query
            .OrderByDesc("FT.stickied", "FT.last_post_created")
            .Offset(offset)
            .Limit(limit);

        var results = await query.GetAsync<TopicEntity>();

        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task ClearTopicThumbsForImageAsync(int imageId)
    {
        var query = this.Db
            .Query(this.Table)
            .Where("gallery_image_id", imageId)
            .AsUpdate(new { gallery_image_id = (int?)null });

        await this.Db.ExecuteAsync(query);
    }

    /// <inheritdoc/>
    public new Task<TopicEntity?> ReadAsync(int id)
    {
        throw new Exception("Use `ReadAsync(int id, int authenticatedUserId)` instead.");
    }

    /// <inheritdoc/>
    public async Task<TopicEntity?> ReadAsync(int id, int authenticatedUserId)
    {
        return await this.SelectWithDerivedFields(authenticatedUserId)
            .Where("FT.id", id)
            .FirstOrDefaultAsync<TopicEntity>();
    }

    /// <inheritdoc/>
    public new Task<TopicEntity?> ReadAsync(string alias, bool caseSensitive = true)
    {
        throw new Exception("Use `ReadAsync(string alias, int authenticatedUserId)` instead.");
    }

    /// <inheritdoc/>
    public async Task<TopicEntity?> ReadAsync(string alias, int authenticatedUserId)
    {
        var query = this.SelectWithDerivedFields(authenticatedUserId).Where("FT.alias", alias);
        _ = this.Db.Compiler.Compile(query);

        return await query.FirstOrDefaultAsync<TopicEntity>();
    }

    /// <inheritdoc/>
    public async Task<string> GenerateUniqueAliasAsync(string title)
    {
        return await this.Connection.ExecuteScalarAsync<string>(
            "SELECT func_forum_topic_get_unique_alias(@title);", new { title });
    }

    /// <summary>
    /// Modifies the query to filter by the given search criteria.
    /// </summary>
    /// <param name="query">The query to modify.</param>
    /// <param name="authenticatedUserId">
    /// Specifies the currently authenticated user, for determining if they're subscribed to topics.
    /// </param>
    /// <param name="sectionId">Get only topics within this section.</param>
    /// <param name="topicUserId">Get only topics created by this user.</param>
    /// <param name="subscribed">Only show subscribed topic.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterTopics(
        Query query,
        int authenticatedUserId = 0,
        int sectionId = 0,
        int topicUserId = 0,
        bool? subscribed = null)
    {
        return query
            .When(sectionId > 0, q => q.Where("FT.forum_section_id", sectionId))
            .When(topicUserId > 0, q => q.Where("FT.identity_user_id", topicUserId))
            .When(authenticatedUserId > 0 && subscribed.HasValue, q => q.When(
                subscribed ?? true,
                t => t.WhereNotNull("FS.id"),
                f => f.WhereNull("FS.id")));
    }

    /// <summary>
    /// Gets the a query that includes the fields and derived fields.
    /// </summary>
    private Query SelectWithDerivedFields(int authenticatedUserId)
    {
        return this.Db
            .Query(this.Table + " AS FT")
            .LeftJoin("forum_subscription AS FS", j => j
                .On("FS.forum_topic_id", "FT.id")
                .Where("FS.identity_user_id", authenticatedUserId))
            .SelectRaw(SelectWithDerivedFieldsSql);
    }
}