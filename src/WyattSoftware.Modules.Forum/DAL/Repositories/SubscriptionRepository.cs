﻿// <copyright file="SubscriptionRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Dapper implementation of the forum subscription repository.
/// </summary>
public class SubscriptionRepository : BaseRepository<SubscriptionEntity>, ISubscriptionRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SubscriptionRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public SubscriptionRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task CreateAsync(int topicId, int userId)
    {
        var readQuery = this.Db.Query("forum_subscription")
            .Select("id")
            .Where("forum_topic_id", topicId)
            .Where("identity_user_id", userId);

        var subscriptionId = this.Db.ExecuteScalar<int>(readQuery);
        if (subscriptionId != 0)
        {
            return;
        }

        var createQuery = this.Db.Query("forum_subscription").AsInsert(new
        {
            forum_topic_id = topicId,
            identity_user_id = userId,
        });

        await this.Db.ExecuteAsync(createQuery);
    }

    /// <inheritdoc/>
    public async Task DeleteAsync(int topicId, int userId)
    {
        var query = this.Db.Query("forum_subscription")
            .Where("forum_topic_id", topicId)
            .Where("identity_user_id", userId)
            .AsDelete();

        await this.Db.ExecuteAsync(query);
    }

    /// <inheritdoc/>
    public async Task DeleteAllSubscriptionsAsync(int topicId)
    {
        var query = this.Db.Query("forum_subscription")
            .Where("forum_topic_id", topicId)
            .AsDelete();

        await this.Db.ExecuteAsync(query);
    }
}
