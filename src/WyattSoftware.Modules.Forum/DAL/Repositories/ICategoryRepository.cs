﻿// <copyright file="ICategoryRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the forum category repository.
/// </summary>
public interface ICategoryRepository : IRepository<CategoryEntity>
{
}
