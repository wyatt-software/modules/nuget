﻿// <copyright file="SectionRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Dapper implementation of the forum section repository.
/// </summary>
public class SectionRepository : BaseRepository<SectionEntity>, ISectionRepository
{
    private const string SelectWithDerivedFieldsSql =
@"FS.*,
(
    SELECT id FROM forum_post WHERE forum_topic_id IN (
        SELECT id FROM forum_topic WHERE forum_section_id = FS.id
    )
    ORDER BY id DESC LIMIT 1
) AS derived__last_post_id,
(
    SELECT COUNT(id) FROM forum_topic WHERE forum_section_id = FS.id
) AS derived__topic_count";

    /// <summary>
    /// Initializes a new instance of the <see cref="SectionRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public SectionRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <summary>
    /// Gets the a query that includes the fields and derived fields.
    /// </summary>
    private Query SelectWithDerivedFields => this.Db
        .Query(this.Table + " AS FS")
        .SelectRaw(SelectWithDerivedFieldsSql);

    /// <inheritdoc/>
    public async Task<int> GetCountAsync(int categoryId)
    {
        var query = this.Db.Query(this.Table).AsCount();
        query = FilterSections(query, categoryId);
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<SectionEntity>> FetchAsync(int categoryId, int offset = 0, int limit = 20)
    {
        var query = this.SelectWithDerivedFields
            .OrderBy("forum_category_id", "sort_order")
            .Offset(offset)
            .Limit(limit);

        query = FilterSections(query, categoryId);
        var results = await query.GetAsync<SectionEntity>();
        return results.ToList();
    }

    /// <inheritdoc />
    public new async Task DeleteAsync(int id)
    {
        // TODO: Perhaps implement in MySql as a trigger?
        var section = await this.ReadAsync(id);
        if (section == null)
        {
            return;
        }

        const string sql =
            "UPDATE forum_section SET sort_order = sort_order - 1 " +
            "WHERE forum_category_id = @CategoryId AND sort_order >= @SortOrder;";

        await this.Connection.ExecuteAsync(sql, new { section.CategoryId, section.SortOrder });

        await base.DeleteAsync(id);
    }

    /// <inheritdoc/>
    public async Task MoveTopicsToSectionAsync(int fromSectionId, int toSectionId)
    {
        var query = this.Db.Query("forum_topic")
            .Where("forum_section_id", fromSectionId)
            .AsUpdate(new { TopicSectionID = toSectionId });

        await this.Db.ExecuteAsync(query);
    }

    /// <summary>
    /// Modifies the query to filter by the given search criteria.
    /// </summary>
    /// <param name="query">The query to modify.</param>
    /// <param name="categoryId">Get only sections within this category.</param>
    /// <returns>The query with optional conditions added.</returns>
    private static Query FilterSections(Query query, int categoryId)
    {
        return query
            .When(categoryId > 0, q => q.Where("forum_category_id", categoryId));
    }
}
