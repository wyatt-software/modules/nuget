﻿// <copyright file="ISubscriptionRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the forum subscription repository.
/// </summary>
public interface ISubscriptionRepository : IRepository<SubscriptionEntity>
{
    /// <summary>
    /// Subscribes a user to a topic.
    /// </summary>
    /// <param name="topicId">The topic to subscribe to.</param>
    /// <param name="userId">The user the subscription is for.</param>
    /// <returns>Nothing.</returns>
    Task CreateAsync(int topicId, int userId);

    /// <summary>
    /// Unsubscribes a user from a topic.
    /// </summary>
    /// <param name="topicId">The topic to unsubscribe from.</param>
    /// <param name="userId">The user the subscription is for.</param>
    /// <returns>Nothing.</returns>
    Task DeleteAsync(int topicId, int userId);

    /// <summary>
    /// Deletes all subscriptions to the specified topic.
    /// </summary>
    /// <param name="topicId">The topic to delete all subscriptions to.</param>
    /// <returns>Nothing.</returns>
    Task DeleteAllSubscriptionsAsync(int topicId);
}
