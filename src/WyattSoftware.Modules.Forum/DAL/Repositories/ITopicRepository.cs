﻿// <copyright file="ITopicRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the forum topic repository.
/// </summary>
public interface ITopicRepository : IRepository<TopicEntity>
{
    /// <summary>
    /// Gets the total number of topics that satisfy the search criteria.
    /// </summary>
    /// <param name="authenticatedUserId">
    /// The currently authenticated user (for determining if the topics are subscribed to).
    /// </param>
    /// <param name="sectionId">The section to get topics in.</param>
    /// <param name="topicUserId">The user who created the topics.</param>
    /// <param name="subscribed">Get only topics the currently authenticated user is subscribed to.</param>
    /// <returns>The total number of topics that satisfy the search criteria.</returns>
    Task<int> GetCountAsync(
        int authenticatedUserId = 0, int sectionId = 0, int topicUserId = 0, bool? subscribed = null);

    /// <summary>
    /// Gets the topics who's primary key are in the list of IDs provided.
    /// </summary>
    /// <param name="ids">A list of topic IDs.</param>
    /// <param name="authenticatedUserId">
    /// The currently authenticated user's id (to determine if subscribed).
    /// </param>
    /// <returns>The topics who's primary key are in the list of IDs provided.</returns>
    Task<List<TopicEntity>> FetchAsync(int[] ids, int authenticatedUserId);

    /// <summary>
    /// Gets topics that satisfy the search criteria.
    /// </summary>
    /// <param name="authenticatedUserId">
    /// The currently authenticated user (for determining if the topics are subscribed to).
    /// </param>
    /// <param name="sectionId">The section to get topics in.</param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="topicUserId">The user who created the topics.</param>
    /// <param name="subscribed">Get only topics the currently authenticated user is subscribed to.</param>
    /// <returns>A list of topics.</returns>
    Task<List<TopicEntity>> FetchAsync(
        int authenticatedUserId = 0,
        int sectionId = 0,
        int offset = 0,
        int limit = 20,
        int topicUserId = 0,
        bool? subscribed = null);

    /// <summary>
    /// Clear the thumb for any topics that use the given image.
    /// </summary>
    /// <param name="imageId">The image id.</param>
    /// <returns>Nothing.</returns>
    public Task ClearTopicThumbsForImageAsync(int imageId);

    /// <summary>
    /// Gets the topic with derived fields.
    /// </summary>
    /// <param name="id">The topic ID.</param>
    /// <param name="authenticatedUserId">
    /// The currently authenticated user's id (to determine if subscribed).
    /// </param>
    /// <returns>A topic entity or null.</returns>
    Task<TopicEntity?> ReadAsync(int id, int authenticatedUserId);

    /// <summary>
    /// Gets the topic with derived fields.
    /// </summary>
    /// <param name="alias">The topic alias.</param>
    /// <param name="authenticatedUserId">
    /// The currently authenticated user's id (to determine if subscribed).
    /// </param>
    /// <returns>A topic entity or null.</returns>
    Task<TopicEntity?> ReadAsync(string alias, int authenticatedUserId);

    /// <summary>
    /// Gets an alias for the given title, that doesn't conflict with any existing aliases.
    /// </summary>
    /// <param name="title">The title to generate the alias from.</param>
    /// <returns>An alias for the given title, that doesn't conflict with any existing aliases.</returns>
    Task<string> GenerateUniqueAliasAsync(string title);
}
