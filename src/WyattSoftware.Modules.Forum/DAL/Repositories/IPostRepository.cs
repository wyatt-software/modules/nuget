﻿// <copyright file="IPostRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the forum post repository.
/// </summary>
public interface IPostRepository : IRepository<PostEntity>
{
    /// <summary>
    /// Gets the total number of posts for this topic and/or user.
    /// </summary>
    /// <param name="topicId">The topic to counts posts for.</param>
    /// <param name="userId">The user to count posts for.</param>
    /// <returns>The total number of posts for this topic and/or user.</returns>
    Task<int> GetCountAsync(int topicId = 0, int userId = 0);

    /// <summary>
    /// Get the "page" a given post is on.
    /// </summary>
    /// <param name="topicId">The topic to consider posts for.</param>
    /// <param name="postId">The post to search for.</param>
    /// <param name="limit">The size (number of records) of each page.</param>
    /// <param name="userId">The user to consider posts for.</param>
    /// <returns>The "page" a given post is on.</returns>
    Task<int> GetOffsetAsync(int topicId, int postId, int limit, int userId);

    /// <summary>
    /// Get the posts for a specific topic and/or user.
    /// </summary>
    /// <param name="topicId">The topic to get posts for.</param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <param name="userId">The user to get posts for.</param>
    /// <param name="sort">The field to sort by (with a '-' prefix if descending).</param>
    /// <returns>The posts for a specific topic and/or user.</returns>
    /// <remarks>We don't use this for searching. That should be done with Lucene.</remarks>
    Task<List<PostEntity>> FetchAsync(
        int topicId = 0, int offset = 0, int limit = 20, int userId = 0, string sort = "Created");

    /// <summary>
    /// A derived field helper to get the last post created in a particular section.
    /// </summary>
    /// <param name="sectionId">The section in question.</param>
    /// <returns>The last post created in a particular section.</returns>
    Task<PostEntity> ReadLastPostBySectionIdAsync(int sectionId);

    /// <summary>
    /// A derived field helper to get the last post created in a particular topic.
    /// </summary>
    /// <param name="topicId">The topic in question.</param>
    /// <returns>The last post created in a particular topic.</returns>
    Task<PostEntity> ReadLastPostByTopicIdAsync(int topicId);
}
