﻿// <copyright file="CategoryRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Repositories;

using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Forum.DAL.Entities;

/// <summary>
/// Dapper implementation of the forum category repository.
/// </summary>
public class CategoryRepository : BaseRepository<CategoryEntity>, ICategoryRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public CategoryRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc />
    public new async Task DeleteAsync(int id)
    {
        var categoryEntity = await this.ReadAsync(id);
        if (categoryEntity == null)
        {
            return;
        }

        await this.Connection.ExecuteAsync(
            "UPDATE forum_category SET sort_order = sort_order - 1 WHERE sort_order >= @SortOrder;",
            new { id, categoryEntity.SortOrder });

        await base.DeleteAsync(id);
    }
}
