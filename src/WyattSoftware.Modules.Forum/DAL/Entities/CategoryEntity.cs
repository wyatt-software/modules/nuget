﻿// <copyright file="CategoryEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Forum category entity.
/// </summary>
[Table("forum_category")]
public class CategoryEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryEntity"/> class.
    /// </summary>
    public CategoryEntity()
    {
        this.DisplayName = string.Empty;
        this.SortOrder = 0;
    }

    /// <summary>
    /// Gets or sets the name of the category.
    /// </summary>
    [Column("display_name")]
    [Required]
    [StringLength(200)]
    public string DisplayName { get; set; }

    /// <summary>
    /// Gets or sets the order to list the categories in.
    /// </summary>
    [Column("sort_order")]
    [Required]
    [Range(1, int.MaxValue)]
    public int SortOrder { get; set; }
}
