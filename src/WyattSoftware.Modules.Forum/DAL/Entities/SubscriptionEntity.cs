﻿// <copyright file="SubscriptionEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Forum subscription entity.
/// </summary>
[Table("forum_subscription")]
public class SubscriptionEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SubscriptionEntity"/> class.
    /// </summary>
    public SubscriptionEntity()
    {
        this.UserId = 0;
        this.TopicId = 0;
    }

    /// <summary>
    /// Gets or sets a foreign key to the account user this subscription is for.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the topic this subscription is for.
    /// </summary>
    [ForeignKey("forum_topic.id")]
    [Column("forum_topic_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int TopicId { get; set; }
}
