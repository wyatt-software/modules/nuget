﻿// <copyright file="SectionEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Attributes;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Forum section entity.
/// </summary>
[Table("forum_section")]
public class SectionEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="SectionEntity"/> class.
    /// </summary>
    public SectionEntity()
    {
        this.CategoryId = 0;

        this.CodeName = string.Empty;
        this.DisplayName = string.Empty;
        this.Description = string.Empty;
        this.Classifieds = false;
        this.SortOrder = 0;
    }

    /// <summary>
    /// Gets or sets a foreign key to the forum category this section belongs to.
    /// </summary>
    [ForeignKey("forum_category.id")]
    [Column("forum_category_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int CategoryId { get; set; }

    /// <summary>
    /// Gets or sets a unique code name that can be used in URLs.
    /// </summary>
    [Key]
    [Column("code_name")]
    [Required]
    [StringLength(50)]
    [RegularExpression(
        @"^[0-9a-z-]+$",
        ErrorMessage = "Only lowercase letters, numbers, and the dash character are allowed.")]
    public string CodeName { get; set; }

    /// <summary>
    /// Gets or sets the display name of the section.
    /// </summary>
    [Column("display_name")]
    [Required]
    [StringLength(200)]
    public string DisplayName { get; set; }

    /// <summary>
    /// Gets or sets a brief description for the section.
    /// </summary>
    [Column("description")]
    [StringLength(200)]
    public string Description { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the section is for classifieds ("for sale") topics.
    /// </summary>
    [Column("classifieds")]
    public bool Classifieds { get; set; }

    /// <summary>
    /// Gets or sets the order the section appears within it's parent category.
    /// </summary>
    [Column("sort_order")]
    [Required]
    [Range(1, int.MaxValue)]
    public int SortOrder { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the last post created in any topic under this section.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__last_post_id")]
    public int LastPostId { get; set; }

    /// <summary>
    /// Gets or sets the total number of posts in any topic under this section.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__topic_count")]
    public int TopicCount { get; set; }
}
