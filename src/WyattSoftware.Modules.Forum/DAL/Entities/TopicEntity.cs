﻿// <copyright file="TopicEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Entities;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Attributes;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Forum topic entity.
/// </summary>
[Table("forum_topic")]
public class TopicEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="TopicEntity"/> class.
    /// </summary>
    public TopicEntity()
    {
        this.UserId = 0;
        this.SectionId = 0;
        this.ImageId = null;

        this.Alias = string.Empty;
        this.Title = string.Empty;
        this.SubHeading = string.Empty;
        this.Stickied = false;
        this.Locked = false;
        this.ImageAuto = true;
        this.ViewCounter = 0;
        this.Status = 0;
        this.Price = 0;

        this.LastPostCreated = null;
        this.LastPostId = 0;
        this.PostCount = 0;
        this.Subscribed = false;
    }

    /// <summary>
    /// Gets or sets a foreign key to the account user that created the topic.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the section this topic belongs to.
    /// </summary>
    [ForeignKey("forum_section.id")]
    [Column("forum_section_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int SectionId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the thumb image for this topic.
    /// </summary>
    [ForeignKey("gallery_image.id")]
    [Column("gallery_image_id")]
    public int? ImageId { get; set; }

    /// <summary>
    /// Gets or sets a unique URL-friendly alias for this topic.
    /// </summary>
    [Key]
    [Column("alias")]
    [Required]
    [StringLength(200)]
    [RegularExpression(
        @"^[0-9a-z-]+$",
        ErrorMessage = "Only lowercase letters, numbers, and the dash character are allowed.")]
    public string Alias { get; set; }

    /// <summary>
    /// Gets or sets the title of the topic.
    /// </summary>
    [Column("title")]
    [Required]
    [StringLength(200)]
    public string Title { get; set; }

    /// <summary>
    /// Gets or sets a sub-heading for this topic.
    /// </summary>
    [Column("sub_heading")]
    [StringLength(200)]
    public string SubHeading { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the topic is "stickied" to the top of the section.
    /// </summary>
    [Column("stickied")]
    public bool Stickied { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the topic is locked (meaning no posts can be created or edited).
    /// </summary>
    [Column("locked")]
    public bool Locked { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the thumb image for this topic is to be automatically scraped from
    /// the first image in the first post.
    /// </summary>
    [Column("image_auto")]
    public bool ImageAuto { get; set; }

    /// <summary>
    /// Gets or sets the number of times the topic has been viewed.
    /// </summary>
    [Column("view_counter")]
    [Range(0, int.MaxValue)]
    public int ViewCounter { get; set; }

    /// <summary>
    /// Gets or sets the status of the classifieds topic.
    /// </summary>
    /// <remarks>
    /// <list type="table">
    /// <item>
    /// <term>0</term>
    /// <description>None</description>
    /// </item>
    /// <item>
    /// <term>1</term>
    /// <description>For Sale</description>
    /// </item>
    /// <item>
    /// <term>2</term>
    /// <description>Multiple Items</description>
    /// </item>
    /// <item>
    /// <term>3</term>
    /// <description>E.O.I</description>
    /// </item>
    /// <item>
    /// <term>4</term>
    /// <description>Pending Payment</description>
    /// </item>
    /// <item>
    /// <term>5</term>
    /// <description>Sold</description>
    /// </item>
    /// </list>
    /// </remarks>
    [Column("status")]
    [Required]
    [Range(0, 5)]
    public int Status { get; set; }

    /// <summary>
    /// Gets or sets the asking price of the classifieds topic.
    /// </summary>
    [Column("price")]
    [Range(0.00, 1000000000.00)] // "one BILLION dollars!" 🤙
    public decimal Price { get; set; }

    /// <summary>
    /// Gets or sets the date the last post was created in this topic.
    /// </summary>
    /// <remarks>
    /// This is a derived field, but it's actually stored in the database, for more performant sorting. It must be
    /// updated whenever a child post is created.
    /// </remarks>
    [Column("last_post_created")]
    public DateTime? LastPostCreated { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the first post created in this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__first_post_id")]
    public int FirstPostId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the last post created in this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__last_post_id")]
    public int LastPostId { get; set; }

    /// <summary>
    /// Gets or sets the number of posts in this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__post_count")]
    public int PostCount { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the currently authenticated user is subscribed to this topic.
    /// </summary>
    /// <remarks>Derived field.</remarks>
    [Derived]
    [Column("derived__subscribed")]
    public bool Subscribed { get; set; }
}
