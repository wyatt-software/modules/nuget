﻿// <copyright file="PostEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Forum.DAL.Entities;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Forum post entity.
/// </summary>
[Table("forum_post")]
public class PostEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PostEntity"/> class.
    /// </summary>
    public PostEntity()
    {
        this.UserId = 0;
        this.TopicId = 0;
        this.EditedByUserId = null;

        this.EditedDate = null;
        this.Content = string.Empty;
    }

    /// <summary>
    /// Gets or sets a foreign key to the community user that created the post.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("identity_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int UserId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the topic this post belongs to.
    /// </summary>
    [ForeignKey("forum_topic.id")]
    [Column("forum_topic_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int TopicId { get; set; }

    /// <summary>
    /// Gets or sets a foreign key to the account user that last edited the post.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("edited_by_user_id")]
    public int? EditedByUserId { get; set; }

    /// <summary>
    /// Gets or sets the date the post was last edited.
    /// </summary>
    /// <remarks>
    /// This is probably redundant, since <c>Modified</c> should hold the same value, but we're keeping it because
    /// it's meaning is explicit.
    /// </remarks>
    [Column("edited_date")]
    public DateTime? EditedDate { get; set; }

    /// <summary>
    /// Gets or sets the content of the post.
    /// </summary>
    [Column("content")]
    [Required]
    [StringLength(16777215)] // MySQL mediumtext
    public string Content { get; set; }
}
