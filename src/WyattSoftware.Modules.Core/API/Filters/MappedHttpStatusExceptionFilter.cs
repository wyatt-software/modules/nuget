﻿// <copyright file="MappedHttpStatusExceptionFilter.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Filters;

using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

/// <summary>
/// Performs the mapping and error response creation.
/// </summary>
public class MappedHttpStatusExceptionFilter : IActionFilter
{
    // Exceptions --> HTTP Code mappings.
    private readonly Dictionary<Type, HttpStatusCode> mappings =
        new()
        {
            { typeof(ArgumentException), HttpStatusCode.BadRequest },
            { typeof(ArgumentNullException), HttpStatusCode.BadRequest },
            { typeof(InvalidOperationException), HttpStatusCode.Forbidden },
            { typeof(UnauthorizedAccessException), HttpStatusCode.Unauthorized },

            // Normally it's impossible to distinguish between the different causes of a 404 error ("endpoint
            // not found" vs "entity not found").
            //
            // Instead, we consider 404 as "Endpoint not found" and don't use it here. We return 400 - meaning:
            // "requested entity not found".
            { typeof(KeyNotFoundException), HttpStatusCode.BadRequest },
        };

    /// <inheritdoc/>
    public void OnActionExecuting(ActionExecutingContext context)
    {
    }

    /// <inheritdoc/>
    public void OnActionExecuted(ActionExecutedContext context)
    {
        if (context.Exception == null)
        {
            return;
        }

        var type = context.Exception.GetType();
        var code = this.mappings.ContainsKey(type)
            ? this.mappings[type]
            : HttpStatusCode.InternalServerError;

        context.Result = new JsonResult(new { context.Exception.Message })
        {
            StatusCode = (int)code,
        };

        context.ExceptionHandled = true;
    }
}