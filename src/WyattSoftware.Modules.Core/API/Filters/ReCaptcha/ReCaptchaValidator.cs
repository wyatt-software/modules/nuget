﻿// <copyright file="ReCaptchaValidator.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Filters.ReCaptcha;

using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

/// <summary>
/// Validates a token using google ReCaptcha.
/// </summary>
public class ReCaptchaValidator
{
    /// <summary>
    /// The Google ReCaptcha API base URL.
    /// </summary>
    private const string RequestUri = "https://www.google.com/recaptcha/api/siteverify";

    private readonly IHttpClientFactory httpClientFactory;
    private readonly string privateKey;
    private readonly bool enabled;

    /// <summary>
    /// Initializes a new instance of the <see cref="ReCaptchaValidator"/> class.
    /// </summary>
    /// <param name="httpClientFactory">Injected IHttpClientFactory.</param>
    /// <param name="enabled">If disabled, ReCaptchas will always be evaluated as valid.</param>
    /// <param name="privateKey">Your site's ReCaptcha private key.</param>
    public ReCaptchaValidator(
        IHttpClientFactory httpClientFactory,
        bool enabled,
        string privateKey)
    {
        this.httpClientFactory = httpClientFactory;
        this.enabled = enabled;
        this.privateKey = privateKey;
    }

    /// <summary>
    /// Makes an external API call to Google ReCaptcha to validate a token.
    /// </summary>
    /// <param name="token">The ReCaptcha token to validate.</param>
    /// <returns>true if the token was valid.</returns>
    public async Task<bool> ValidateAsync(string token)
    {
        if (!this.enabled)
        {
            return true;
        }

        var values = new Dictionary<string, string>
        {
            { "secret", this.privateKey },
            { "response", token },
        };

        var httpClient = this.httpClientFactory.CreateClient();
        var formUrlEncodedContent = new FormUrlEncodedContent(values);
        var httpResponseMessage = await httpClient.PostAsync(RequestUri, formUrlEncodedContent);
        var responseContent = await httpResponseMessage.Content.ReadAsStringAsync();
        var response = JsonConvert.DeserializeObject<SiteVerifyResponse>(responseContent);

        return response?.Success ?? false;
    }
}