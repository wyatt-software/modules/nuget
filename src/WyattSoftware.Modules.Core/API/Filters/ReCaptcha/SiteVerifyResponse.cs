﻿// <copyright file="SiteVerifyResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Filters.ReCaptcha;

using System.Collections.Generic;
using Newtonsoft.Json;

/// <summary>
/// This is the response we expect from the Google ReCaptcha API.
/// </summary>
public class SiteVerifyResponse
{
    /// <summary>
    /// Gets or sets a value indicating whether the verification was successful.
    /// </summary>
    [JsonProperty("success")]
    public bool Success { get; set; }

    /// <summary>
    /// Gets or sets the timestamp of the challenge (ISO format yyyy-MM-dd'T'HH:mm:ssZZ).
    /// </summary>
    [JsonProperty("challenge_ts")]
    public string ChallengeTimestamp { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the hostname of the site where the reCAPTCHA was solved.
    /// </summary>
    [JsonProperty("hostname")]
    public string Hostname { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets any error codes.
    /// </summary>
    /// <remarks>
    /// Optional.
    /// </remarks>
    [JsonProperty("error-codes")]
    public List<string> ErrorCodes { get; set; } = new List<string>();
}