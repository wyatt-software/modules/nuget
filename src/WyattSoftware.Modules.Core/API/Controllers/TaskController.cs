﻿// <copyright file="TaskController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Controllers;

using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.DTOs;
using WyattSoftware.Modules.Core.BLL.Features.BackgroundTasks;

/// <summary>
/// Handles background tasks.
/// </summary>
[Route("core/tasks")]
public class TaskController : ControllerBase
{
    private readonly ICancellableTaskManager cancellableTaskManager;

    /// <summary>
    /// Initializes a new instance of the <see cref="TaskController"/> class.
    /// </summary>
    /// <param name="cancellableTaskManager">Injected ICancellableTaskManager.</param>
    public TaskController(
        ICancellableTaskManager cancellableTaskManager)
    {
        this.cancellableTaskManager = cancellableTaskManager;
    }

    /// <summary>
    /// Lists cancellable tasks that have been registered.
    /// </summary>
    /// <returns>A list of tasks.</returns>
    [HttpGet]
    [Route("")]
    [Authorize(Roles = "Administrator")]
    public IActionResult Fetch()
    {
        var backgroundTasks = this.cancellableTaskManager.FetchAll();
        var response = backgroundTasks.Select(x => Mapper.Map<CancellableTaskResponse>(x));
        return this.Ok(response);
    }

    /// <summary>
    /// Cancels the task.
    /// </summary>
    /// <param name="key">The Guid of the cancellable task to cancel.</param>
    /// <returns>An empty response body.</returns>
    [HttpGet]
    [Route("{key:Guid}")]
    [Authorize(Roles = "Administrator")]
    public IActionResult Get(Guid key)
    {
        var backgroundTask = this.cancellableTaskManager.Read(key);
        var response = Mapper.Map<CancellableTaskResponse>(backgroundTask);
        return this.Ok(response);
    }

    /// <summary>
    /// Cancels the background task.
    /// </summary>
    /// <param name="key">The Guid of the task to cancel.</param>
    /// <returns>An empty response body.</returns>
    [HttpPost]
    [Route("{key:Guid}/actions/cancel")]
    [Authorize(Roles = "Administrator")]
    public IActionResult Cancel(Guid key)
    {
        this.cancellableTaskManager.Cancel(key);
        return this.Ok(null);
    }
}