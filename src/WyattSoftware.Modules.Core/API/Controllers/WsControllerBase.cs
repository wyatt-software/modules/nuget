﻿// <copyright file="WsControllerBase.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Controllers;

using System.Net;
using Microsoft.AspNetCore.Mvc;
using WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// Adds an additional response methods for dealing with pagination.
/// </summary>
public class WsControllerBase : ControllerBase
{
    /// <summary>
    /// Returns the a total as an "X-Total-Count" header only (no body).
    /// </summary>
    /// <param name="total">The total to return.</param>
    /// <returns>The response.</returns>
    protected IActionResult TotalHeaderOnly(int total)
    {
        var response = new ObjectResult(null)
        {
            StatusCode = (int)HttpStatusCode.NoContent,
        };

        this.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count");
        this.Response.Headers.Add("X-Total-Count", total.ToString());

        return response;
    }

    /// <summary>
    /// Returns the results from a paginated response as the body of an OK response, with the pagination metadata as
    /// headers ("X-Total-Count", "Offset", and "Limit").
    /// </summary>
    /// <typeparam name="T">The type of result.</typeparam>
    /// <param name="paginatedResponse">The paginated response.</param>
    /// <returns>The response.</returns>
    protected IActionResult ResultsWithPaginationHeaders<T>(PaginatedResponse<T> paginatedResponse)
    {
        var response = new ObjectResult(paginatedResponse.Results)
        {
            StatusCode = (int)HttpStatusCode.OK,
        };

        this.Response.Headers.Add("Access-Control-Expose-Headers", "X-Total-Count,Offset,Limit");
        this.Response.Headers.Add("X-Total-Count", paginatedResponse.Total.ToString());
        this.Response.Headers.Add("Offset", paginatedResponse.Offset.ToString());
        this.Response.Headers.Add("Limit", paginatedResponse.Limit.ToString());

        return response;
    }
}
