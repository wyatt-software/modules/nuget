﻿// <copyright file="ICoreHub.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Hubs;

/// <summary>
/// Defines the Remote Procedure Calls (functions) on the client that can be called from the server.
/// </summary>
public interface ICoreHub
{
    /// <summary>
    /// Updates the user of the progress of a long-running task.
    /// </summary>
    /// <param name="message">A message to display for this progress update.</param>
    /// <param name="percentage">How far along the process is.</param>
    void UpdateProgress(string message, decimal percentage);
}
