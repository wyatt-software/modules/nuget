﻿// <copyright file="CoreHub.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.Hubs;

using Microsoft.AspNetCore.SignalR;

/// <summary>
/// SignalR hub for core push notifications. />.
/// </summary>
public class CoreHub : Hub<ICoreHub>
{
}
