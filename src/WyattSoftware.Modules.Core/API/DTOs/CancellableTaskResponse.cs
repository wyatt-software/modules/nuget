﻿// <copyright file="CancellableTaskResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.DTOs;

using System;

/// <summary>
/// Represents the background task info that's exposed on the API.
/// </summary>
public class CancellableTaskResponse
{
    /// <summary>
    /// Gets or sets the key that this task is referenced by.
    /// </summary>
    public Guid Key { get; set; }

    /// <summary>
    /// Gets or sets the name of the task. This is only used for human readability.
    /// </summary>
    public string Name { get; set; } = string.Empty;
}
