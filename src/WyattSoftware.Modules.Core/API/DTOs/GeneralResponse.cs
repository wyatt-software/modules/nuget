﻿// <copyright file="GeneralResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.API.DTOs;

/// <summary>
/// Provides a way to return a general response message.
/// </summary>
public class GeneralResponse
{
    /// <summary>
    /// Initializes a new instance of the <see cref="GeneralResponse"/> class.
    /// </summary>
    /// <param name="message">A message to return.</param>
    public GeneralResponse(string message = "")
    {
        this.Success = true;
        this.Message = message;
        this.Data = null;
    }

    /// <summary>
    /// Gets or sets a value indicating whether some operation was successful.
    /// </summary>
    public bool Success { get; set; }

    /// <summary>
    /// Gets or sets the message to return.
    /// </summary>
    public string Message { get; set; }

    /// <summary>
    /// Gets or sets any additional data we want to return.
    /// </summary>
    public object? Data { get; set; }
}