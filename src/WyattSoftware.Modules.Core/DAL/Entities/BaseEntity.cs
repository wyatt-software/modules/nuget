﻿// <copyright file="BaseEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Entities;

using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Defines the base class that all data entities should inherit from.
/// </summary>
public class BaseEntity : IEntity
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    [Key]
    [Column("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the date the user was created or joined.
    /// </summary>
    [Column("created")]
    public DateTime Created { get; set; } = DateTimeOffset.UtcNow.UtcDateTime;

    /// <summary>
    /// Gets or sets the date last inserted or updated.
    /// </summary>
    [Column("modified")]
    public DateTime Modified { get; set; } = DateTimeOffset.UtcNow.UtcDateTime;
}
