﻿// <copyright file="EmailTemplateEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

/// <summary>
/// Email template entity.
/// </summary>
[Table("core_email_template")]
public class EmailTemplateEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Initializes a new instance of the <see cref="EmailTemplateEntity"/> class.
    /// </summary>
    public EmailTemplateEntity()
    {
        this.CodeName = string.Empty;
        this.DisplayName = string.Empty;
        this.Description = string.Empty;
        this.Subject = string.Empty;
        this.PlainText = string.Empty;
        this.Html = string.Empty;
    }

    /// <summary>
    /// Gets or sets the code name of the email template.
    /// </summary>
    [Key]
    [Column("code_name")]
    [Required]
    [StringLength(200)]
    public string CodeName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name.
    /// </summary>
    [Column("display_name")]
    [Required]
    [StringLength(200)]
    public string DisplayName { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the description.
    /// </summary>
    [Column("description")]
    [Required]
    [StringLength(200)]
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the subject to use for the email.
    /// </summary>
    [Column("subject")]
    [Required]
    [StringLength(200)]
    public string Subject { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the plain text version of the template.
    /// </summary>
    [Column("plain_text")]
    [StringLength(65535)] // MySQL text
    public string PlainText { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the HTML version fo the template.
    /// </summary>
    [Column("html")]
    [StringLength(65535)] // MySQL text
    public string Html { get; set; } = string.Empty;
}
