﻿// <copyright file="IEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Entities;

using System;

/// <summary>
/// Defines the properties that all data entities should inherit from.
/// </summary>
public interface IEntity : IDataRecord
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    int Id { get; set; }

    /// <summary>
    /// Gets or sets the date the user was created or joined.
    /// </summary>
    DateTime Created { get; set; }

    /// <summary>
    /// Gets or sets the date last inserted or updated.
    /// </summary>
    DateTime Modified { get; set; }
}
