﻿// <copyright file="DerivedAttribute.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Attributes;

using System;

/// <summary>
/// Indicates that a property in an entity is a derived field, and should not be mapped when creating or updating.
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class DerivedAttribute : Attribute
{
}
