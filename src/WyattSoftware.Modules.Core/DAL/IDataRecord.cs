﻿// <copyright file="IDataRecord.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL;

/// <summary>
/// All classes that represent data originating from the database should implement this interface, so that
/// the <c>[Column]</c> attributes are mapped to the Dapper <c>CustomPropertyTypeMap</c> on startup.
/// </summary>
public interface IDataRecord
{
}
