﻿// <copyright file="GuidHandler.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.TypeHandlers;

using System;
using System.Data;
using global::Dapper;

/// <summary>
/// Custom type handler for storing Guids in MySQL as VARCHAR(36)'s.
/// </summary>
public class GuidHandler : SqlMapper.TypeHandler<Guid>
{
    /// <summary>
    /// Writes the Guid value to the database as a VARCHAR(36).
    /// </summary>
    /// <param name="parameter">The parameter being written.</param>
    /// <param name="value">The value being written.</param>
    public override void SetValue(IDbDataParameter parameter, Guid value)
    {
        parameter.Value = value.ToString();
    }

    /// <summary>
    /// Reads the Guid from the database as a VARCHAR(36), then tries to parse it as a Guid.
    /// </summary>
    /// <param name="value">The raw value.</param>
    /// <returns>The DateTime values as UTC.</returns>
    public override Guid Parse(object value)
    {
        if (Guid.TryParse(value as string, out var stronglyTypedValue))
        {
            return stronglyTypedValue;
        }

        // TODO: Throw an exception?
        return Guid.Empty;
    }
}
