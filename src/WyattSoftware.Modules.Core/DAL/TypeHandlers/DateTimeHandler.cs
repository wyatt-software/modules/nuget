﻿// <copyright file="DateTimeHandler.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.TypeHandlers;

using System;
using System.Data;
using global::Dapper;

/// <summary>
/// Custom type handler for treating DateTimes in the database as UTC.
/// </summary>
/// <remarks>See https://stackoverflow.com/questions/12510299/get-datetime-as-utc-with-dapper .</remarks>
public class DateTimeHandler : SqlMapper.TypeHandler<DateTime>
{
    /// <summary>
    /// Writes the Datetime value to the database, unmodified.
    /// </summary>
    /// <param name="parameter">The parameter being written.</param>
    /// <param name="value">The value being written.</param>
    public override void SetValue(IDbDataParameter parameter, DateTime value)
    {
        parameter.Value = value;
    }

    /// <summary>
    /// Reads the datetime from the database, then specifies it's kind as UTC.
    /// </summary>
    /// <param name="value">The raw value.</param>
    /// <returns>The DateTime values as UTC.</returns>
    public override DateTime Parse(object value)
    {
        return DateTime.SpecifyKind((DateTime)value, DateTimeKind.Utc);
    }
}
