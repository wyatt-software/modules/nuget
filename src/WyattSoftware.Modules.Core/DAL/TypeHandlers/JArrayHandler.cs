﻿// <copyright file="JArrayHandler.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.TypeHandlers;

using System.Data;
using global::Dapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

/// <summary>
/// Custom type handler for storing JObjects in MySQL as JSON.
/// </summary>
public class JArrayHandler : SqlMapper.TypeHandler<JArray>
{
    /// <summary>
    /// Writes the JArray value to the database as a a JSON string.
    /// </summary>
    /// <param name="parameter">The parameter being written.</param>
    /// <param name="value">The value being written.</param>
    public override void SetValue(IDbDataParameter parameter, JArray value)
    {
        // Note: When using BaseRepository.MappedDataForWriting(), this isn't used since we convert to string there.
        parameter.Value = value.ToString();
    }

    /// <summary>
    /// Reads the JArray from the database as a string.
    /// </summary>
    /// <param name="value">The raw value.</param>
    /// <returns>The DateTime values as UTC.</returns>
    public override JArray Parse(object value)
    {
        var json = value.ToString() ?? "[]";
        return JArray.Parse(json);
    }
}
