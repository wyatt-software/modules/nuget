﻿// <copyright file="DataHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Helpers;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Default implementation of the <see cref="IDataHelper"/> interface.
/// </summary>
public class DataHelper : BaseConnection, IDataHelper
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DataHelper"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public DataHelper(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc />
    public async Task<List<dynamic>> ExecuteQueryAsync(string sql, object parameters)
    {
        var results = await this.Db.SelectAsync(sql, parameters);
        return results.ToList();
    }

    /// <inheritdoc />
    public async Task<List<dynamic>> ExecuteStoredProcedureAsync(string procedureName, object parameters)
    {
        var paramArguments = string.Join(", ", GetParamNames(parameters).Select(x => $"@{x}"));
        var sql = $"CALL {procedureName}({paramArguments});";
        var results = await this.Db.SelectAsync(sql, parameters);
        return results.ToList();
    }

    /// <summary>
    /// Gets a list of property names from the given object or dictionary.
    /// </summary>
    /// <param name="parameters">
    /// An <c>object</c>, <c>ExpandoObject</c>, or <c>IDictionary&lt;string, object&gt;</c>.
    /// </param>
    /// <returns>A list of property names.</returns>
    private static List<string> GetParamNames(object parameters)
    {
        if (parameters == null)
        {
            return new List<string>();
        }

        if (parameters is DynamicParameters dynamicParams)
        {
            return dynamicParams.ParameterNames.ToList();
        }

        if (parameters is IDictionary<string, object> dict)
        {
            return dict.Keys.ToList();
        }

        return parameters.GetType().GetProperties().Select(prop => prop.Name).ToList();
    }
}
