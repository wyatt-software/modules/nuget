﻿// <copyright file="IDataHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Helpers;

using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// Provides a way to execute raw SQL queries and stored procedures.
/// </summary>
public interface IDataHelper
{
    /// <summary>
    /// Executes the raw SQL with the given parameters, and returns the results.
    /// </summary>
    /// <param name="sql">The raw SQL string.</param>
    /// <param name="parameters">The values of any parameters used in the SQL.</param>
    /// <returns>The results as a list of dynamic records.</returns>
    Task<List<dynamic>> ExecuteQueryAsync(string sql, object parameters);

    /// <summary>
    /// Executes the stored procedure with the given parameters, and returns the results.
    /// </summary>
    /// <param name="procedureName">The name of the stored procedure.</param>
    /// <param name="parameters">The values of any parameters used in the stored procedure.</param>
    /// <returns>The results of the first query (if any) as a list of dynamic records.</returns>
    /// <remarks>Only supports a single record set as results.</remarks>
    Task<List<dynamic>> ExecuteStoredProcedureAsync(string procedureName, object parameters);
}
