﻿// <copyright file="BaseRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using SqlKata.Compilers;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Attributes;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// The base class for all repositories, providing basic CRUD actions.
/// </summary>
/// <typeparam name="TEntity">A data entity.</typeparam>
public class BaseRepository<TEntity> : BaseConnection, IRepository<TEntity>
    where TEntity : IEntity
{
    private string? table;

    /// <summary>
    /// Initializes a new instance of the <see cref="BaseRepository{TEntity}"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public BaseRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <summary>
    /// Gets the table name to use in SQL queries.
    /// </summary>
    protected string Table => this.table ??= GetTable();

    /// <inheritdoc/>
    public async Task<int> GetCountAsync()
    {
        var query = this.Db.Query(this.Table).AsCount();
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<TEntity>> FetchAsync(int offset = 0, int limit = 20)
    {
        var results = await this.Db.Query(this.Table).GetAsync<TEntity>();
        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<List<TEntity>> FetchAsync(int[] ids)
    {
        var results = await this.Db.Query(this.Table)
            .WhereIn(GetIdentityColumn<int>(), ids)
            .GetAsync<TEntity>();

        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<int> CreateAsync(TEntity entity)
    {
        return await this.Db.Query(this.Table)
            .InsertGetIdAsync<int>(this.MappedDataForWriting(entity));
    }

    /// <inheritdoc/>
    public async Task<TEntity?> ReadAsync(int id)
    {
        return await this.Db.Query(this.Table)
            .Where(GetIdentityColumn<int>(), id)
            .FirstOrDefaultAsync<TEntity?>();
    }

    /// <inheritdoc/>
    public async Task<TEntity?> ReadAsync(string code, bool caseSensitive = true)
    {
        return await this.Db.Query(this.Table)
            .Where(GetIdentityColumn<string>(), code)
            .FirstOrDefaultAsync<TEntity?>();
    }

    /// <inheritdoc/>
    public async Task UpdateAsync(TEntity entity)
    {
        var idProp = GetIdentityProp<int>();
        if (idProp == null)
        {
            throw new Exception(
                "The entity's primary key property is missing a [Key] attribute or it isn't an <int>.");
        }

        var query = this.Db.Query(this.Table)
            .Where(GetColumnName(idProp), idProp.GetValue(entity))
            .AsUpdate(this.MappedDataForWriting(entity));

        await this.Db.ExecuteAsync(query);
    }

    /// <inheritdoc/>
    public async Task DeleteAsync(int id)
    {
        var query = this.Db.Query(this.Table)
            .Where(GetIdentityColumn<int>(), id)
            .AsDelete();

        // var compiler = new MySqlCompiler();
        // var sqlResult = compiler.Compile(query);
        // var sql = sqlResult.RawSql;
        await this.Db.ExecuteAsync(query);
    }

    /// <inheritdoc/>
    public async Task<bool> ExistsAsync(int id)
    {
        var idColumnName = GetIdentityColumn<int>();
        var query = this.Db.Query(this.Table)
            .WhereExists(q => q
                .Select(idColumnName)
                .From(this.Table)
                .Where(idColumnName, id));

        return await this.Db.ExecuteScalarAsync<bool>(query);
    }

    /// <summary>
    /// Gets the [column] name of the first property of type T from the entity with a [Key] attribute.
    /// </summary>
    /// <typeparam name="T">The type of key. Eg: int or string.</typeparam>
    /// <returns>The [Column] name.</returns>
    protected static string? GetIdentityColumn<T>()
    {
        var prop = GetIdentityProp<T>();
        return prop != null ? GetColumnName(prop) : null;
    }

    /// <summary>
    /// Gets the entity as a dictionary of column names except for the identity column.
    /// </summary>
    /// <param name="entity">The entity to convert.</param>
    /// <remarks>We assume the identity column is an int marked with the [Key] attribute.</remarks>
    /// <returns>The entity, prepared for writing to the database.</returns>
    protected IReadOnlyDictionary<string, object?> MappedDataForWriting(TEntity entity)
    {
        var identityColumn = GetIdentityColumn<int>();
        var props = typeof(TEntity).GetProperties();

        var data = props
            .Where(prop =>
            {
                var columnName = GetColumnName(prop);
                var derived = GetDerived(prop);
                var value = prop.GetValue(entity);

                // Remove:
                // * properties not mapped to database columns,
                // * properties marked as [Derived],
                // * integer primary keys,
                // * null values (so that we use the database defaults), with the exception of nullable types.
                //   ...otherwise we'd never be able to set anything to null ;)
                return
                    !string.IsNullOrEmpty(columnName) &&
                    !derived &&
                    !columnName.Equals(identityColumn) &&
                    (value != null || IsNullable(prop.PropertyType));
            })
            .ToDictionary(GetColumnName, prop => GetColumnValue(prop, entity));

        return data;
    }

    /// <summary>
    /// Determines if a type is nullable.
    /// </summary>
    /// <remarks>
    /// Don't use with <c>Object.GetType()</c>. Use <c>typeof()</c> instead.
    /// </remarks>
    /// <param name="type">The type being evaluated.</param>
    /// <returns>True if the type is nullable.</returns>
    private static bool IsNullable(Type type) => Nullable.GetUnderlyingType(type) != null;

    /// <summary>
    /// Gets the [Table] attribute from the entity.
    /// </summary>
    /// <returns>The attribute value.</returns>
    private static string GetTable()
    {
        var tableAttribute = (TableAttribute?)typeof(TEntity).GetCustomAttribute(typeof(TableAttribute), true);
        if (tableAttribute == null)
        {
            throw new Exception("The entity doesn't have a [Table] attribute.");
        }

        return tableAttribute.Name;
    }

    /// <summary>
    /// Gets the first property of type T from the entity with a [Key] attribute.
    /// </summary>
    /// <typeparam name="T">The type of property. Eg: int or string.</typeparam>
    /// <returns>The property.</returns>
    private static PropertyInfo? GetIdentityProp<T>()
    {
        return typeof(TEntity).GetProperties()
            .Where(prop =>
            {
                var isType = prop.PropertyType == typeof(T);
                var isKey = prop
                    .CustomAttributes
                    .Any(attr => attr.AttributeType == typeof(KeyAttribute));
                return isType && isKey;
            })
            .FirstOrDefault();
    }

    /// <summary>
    /// Gets the [column] name of the given property.
    /// </summary>
    /// <returns>The [Column] name.</returns>
    private static string GetColumnName(PropertyInfo prop)
    {
        var columnAttribute = (ColumnAttribute?)prop
            ?.GetCustomAttributes(typeof(ColumnAttribute), true)
            .FirstOrDefault();

        return columnAttribute?.Name ?? string.Empty;
    }

    /// <summary>
    /// Gets the value of the given property, converting JObjects and JArray's to JSON since Dappers custom
    /// typehandlers don't see to work on the values of dictionaries.
    /// </summary>
    /// <param name="prop">The property definition.</param>
    /// <param name="entity">The entity to get the value from.</param>
    /// <returns>The property value.</returns>
    private static object? GetColumnValue(PropertyInfo prop, TEntity entity)
    {
        var value = prop.GetValue(entity);
        if (value == null)
        {
            return null;
        }

        if (prop.PropertyType == typeof(JObject))
        {
            return ((JObject)value).ToString();
        }

        if (prop.PropertyType == typeof(JArray))
        {
            return ((JArray)value).ToString();
        }

        return value;
    }

    /// <summary>
    /// Indicates if a property is marked with the <c>[derived]</c> attribute.
    /// </summary>
    /// <returns>True if derived.</returns>
    private static bool GetDerived(PropertyInfo prop)
    {
        return Attribute.IsDefined(prop, typeof(DerivedAttribute));
    }
}
