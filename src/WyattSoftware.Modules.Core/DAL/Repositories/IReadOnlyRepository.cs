﻿// <copyright file="IReadOnlyRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// The interface defining the read operations on repositories.
/// </summary>
/// <typeparam name="TEntity">The data entity type that the repository manages.</typeparam>
public interface IReadOnlyRepository<TEntity>
    where TEntity : IEntity
{
    /// <summary>
    /// Gets the total number of entities in the repository.
    /// </summary>
    /// <returns>The total number of entities in the repository..</returns>
    Task<int> GetCountAsync();

    /// <summary>
    /// Gets a page of data entities.
    /// </summary>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>Some data entities, or an empty list.</returns>
    Task<List<TEntity>> FetchAsync(int offset = 0, int limit = 20);

    /// <summary>
    /// Gets the data entities who's primary key are in the list of IDs provided.
    /// </summary>
    /// <param name="ids">A list of primary key values.</param>
    /// <returns>The data entities who's primary key are in the list of IDs provided.</returns>
    Task<List<TEntity>> FetchAsync(int[] ids);

    /// <summary>
    /// Attempts to get a data entity by it's primary key.
    /// </summary>
    /// <param name="id">The primary key of the data entity.</param>
    /// <returns>A single data entity.</returns>
    Task<TEntity?> ReadAsync(int id);

    /// <summary>
    /// Attempts to get a data entity by it's code name.
    /// </summary>
    /// <param name="code">The data entity's unique code name.</param>
    /// <param name="caseSensitive">Match case.</param>
    /// <returns>A single data entity.</returns>
    Task<TEntity?> ReadAsync(string code, bool caseSensitive = true);

    /// <summary>
    /// Determines if the entity with the given id exists.
    /// </summary>
    /// <param name="id">The primary key of the data entity.</param>
    /// <returns>A value indicating whenther the entity exists.</returns>
    Task<bool> ExistsAsync(int id);
}
