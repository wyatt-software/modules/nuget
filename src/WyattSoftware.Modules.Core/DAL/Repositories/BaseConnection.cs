﻿// <copyright file="BaseConnection.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using SqlKata.Compilers;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// The base class providing it's child classes a connection to the database.
/// </summary>
public class BaseConnection
{
    private readonly CoreOptions coreOptions;

    private MySqlConnection? connection;
    private QueryFactory? db;

    /// <summary>
    /// Initializes a new instance of the <see cref="BaseConnection"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public BaseConnection(
        IOptions<CoreOptions> coreOptions)
    {
        this.coreOptions = coreOptions.Value;
    }

    /// <summary>
    /// Gets the connection to the database.
    /// The Connection is exposed to child classes for when we need to execute a stored procedure directly with
    /// Dapper instead of a query via SqlKata.
    /// </summary>
    protected MySqlConnection Connection =>
        this.connection ??= new MySqlConnection(this.GetConnectionString());

    /// <summary>
    /// Gets the SqlKata query factory (query builder and wrapper for Dapper).
    /// </summary>
    protected QueryFactory Db => this.db ??= new QueryFactory(this.Connection, new MySqlCompiler());

    /// <summary>
    /// Gets the connection string based on <c>appsettings.json</c>.
    /// </summary>
    private string GetConnectionString()
    {
        var db = this.coreOptions.Database;
        return $"server={db.Server};port={db.Port};user={db.User};password={db.Password};database={db.Database};";
    }
}
