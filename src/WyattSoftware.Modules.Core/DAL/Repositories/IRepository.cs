﻿// <copyright file="IRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// The interface defining the write operations on repositories.
/// </summary>
/// <typeparam name="TEntity">The data entity type that the repository manages.</typeparam>
public interface IRepository<TEntity> : IReadOnlyRepository<TEntity>
    where TEntity : IEntity
{
    /// <summary>
    /// Attempts to create a data entity and persist it.
    /// </summary>
    /// <param name="entity">The data entity to create.</param>
    /// <returns>The ID of the newly created entity.</returns>
    Task<int> CreateAsync(TEntity entity);

    /// <summary>
    /// Attempts to update a data entity.
    /// </summary>
    /// <param name="entity">The data entity to update.</param>
    /// <returns>Nothing.</returns>
    Task UpdateAsync(TEntity entity);

    /// <summary>
    /// Attempts to delete a data entity by it's primary key.
    /// </summary>
    /// <param name="id">The data entity's primary key.</param>
    /// <returns>Nothing.</returns>
    Task DeleteAsync(int id);
}
