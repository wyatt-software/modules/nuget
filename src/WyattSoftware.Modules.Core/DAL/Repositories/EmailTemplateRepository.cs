﻿// <copyright file="EmailTemplateRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Dapper implementation of the config email template repository.
/// </summary>
public class EmailTemplateRepository : BaseRepository<EmailTemplateEntity>, IEmailTemplateRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="EmailTemplateRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public EmailTemplateRepository(IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }
}
