﻿// <copyright file="IEmailTemplateRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.DAL.Repositories;

using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the config email template repository.
/// </summary>
public interface IEmailTemplateRepository : IRepository<EmailTemplateEntity>
{
}
