﻿// <copyright file="ObjectExtensions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Extensions;

using System.Collections.Generic;

/// <summary>
/// Extension methods for objects.
/// </summary>
public static class ObjectExtensions
{
    /// <summary>
    /// Creates a dictionary of objects from each property in the object.
    /// </summary>
    /// <param name="obj">The object to convert.</param>
    /// <param name="camelCase">Convert the keys to camel case?.</param>
    /// <returns>A dictionary of property names and values.</returns>
    public static Dictionary<string, object?> ToDictionary(this object obj, bool camelCase = false)
    {
        var dict = new Dictionary<string, object?>();
        foreach (var property in obj.GetType().GetProperties())
        {
            if (property == null)
            {
                continue;
            }

            var key = camelCase
                ? char.ToLowerInvariant(property.Name[0]) + property.Name[1..]
                : property.Name;

            dict.Add(key, property.GetValue(obj));
        }

        return dict;
    }

    /// <summary>
    /// Converts a dictionary to a class object.
    /// </summary>
    /// <typeparam name="T">The type of class to convert to.</typeparam>
    /// <param name="source">The dictionary to start with.</param>
    /// <returns>A class object.</returns>
    public static T ToObject<T>(this IDictionary<string, object?> source)
        where T : class, new()
    {
        var obj = new T();
        var type = obj.GetType();

        foreach (var item in source)
        {
            var prop = type.GetProperty(item.Key);
            if (prop == null)
            {
                continue;
            }

            prop.SetValue(obj, item.Value, null);
        }

        return obj;
    }
}
