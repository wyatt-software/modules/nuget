﻿// <copyright file="StringMarkdownExtensions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Extensions;

using System.Linq;
using System.Net;
using HtmlAgilityPack;
using MarkdownSharp;

/// <summary>
/// Provides a way to convert markdown to HTML and Plain Text.
/// </summary>
public static class StringMarkdownExtensions
{
    /// <summary>
    /// Formats the given input string to plain text.
    /// </summary>
    /// <param name="markdown">The input string.</param>
    /// <returns>A plain text string.</returns>
    public static string ToPlainText(this string markdown)
    {
        var html = markdown.ToHtml();

        var htmlDoc = new HtmlDocument();
        htmlDoc.LoadHtml(html);

        // Remove HTML comments.
        htmlDoc.DocumentNode
            .SelectNodes("//comment()")
            ?.ToList()
            .ForEach(c => c.Remove());

        // Strip HTML.
        var plainText = htmlDoc.DocumentNode.InnerText;

        // Convert HTML entities (like "&nbsp;").
        plainText = WebUtility.HtmlDecode(plainText).Trim();

        return plainText;
    }

    /// <summary>
    /// Formats the given input string to HTML.
    /// </summary>
    /// <param name="markdown">The input string.</param>
    /// <returns>An HTML string.</returns>
    public static string ToHtml(this string markdown)
    {
        return new Markdown().Transform(markdown).Trim();
    }
}
