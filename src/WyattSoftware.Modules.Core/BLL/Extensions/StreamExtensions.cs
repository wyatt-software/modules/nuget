﻿// <copyright file="StreamExtensions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Extensions;

using System.Collections.Generic;

/// <summary>
/// Extension methods for Streams.
/// </summary>
public static class StreamExtensions
{
    /// <summary>
    /// Reads the contents of a stream as a byte array.
    /// </summary>
    /// <param name="stream">The input stream.</param>
    /// <returns>The contents of the stream as a byte array.</returns>
    public static byte[] ToByteArray(this Stream stream)
    {
        using var ms = new MemoryStream();
        stream.CopyTo(ms);
        return ms.ToArray();
    }
}
