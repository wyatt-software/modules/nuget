﻿// <copyright file="IDomainService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Services;

using WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// Defines functionality that all services should have. This is relied on by the search module when dealing with
/// indexable models.
/// </summary>
/// <typeparam name="TModel">The model type managed by this service.</typeparam>
public interface IDomainService<TModel>
    where TModel : IDomainModel
{
    /// <summary>
    /// Gets the total count of all models.
    /// </summary>
    /// <returns>The count.</returns>
    Task<int> GetCountAsync();

    /// <summary>
    /// Get a collection of models without any filtering of any kind.
    /// </summary>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The maximum number of records to return.</param>
    /// <returns>A collection of models.</returns>
    /// <remarks>Useful for fetching batches of models when rebuilding search indexes.</remarks>
    Task<PaginatedResponse<TModel>> FetchAsync(int offset, int limit);

    /// <summary>
    /// Tries to get the domain model by its id.
    /// </summary>
    /// <param name="id">The primary key of the domain model.</param>
    /// <returns>A domain model.</returns>
    Task<TModel> ReadAsync(int id);

    /// <summary>
    /// Tries to get the domain model by its code name.
    /// </summary>
    /// <param name="code">The code name of the domain model.</param>
    /// <returns>A domain model.</returns>
    Task<TModel> ReadAsync(string code);
}
