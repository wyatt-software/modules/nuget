﻿// <copyright file="BaseDomainService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.DAL.Entities;
using WyattSoftware.Modules.Core.DAL.Repositories;

/// <summary>
/// Implements common functionality expected of all domain services.
/// </summary>
/// <typeparam name="TModel">The type of domain model.</typeparam>
/// <typeparam name="TEntity">The type of data entity.</typeparam>
/// <remarks>This functionality is mostly needed for rebuilding search indexes.</remarks>
public class BaseDomainService<TModel, TEntity> : IDomainService<TModel>
    where TModel : IDomainModel
    where TEntity : IEntity
{
    private readonly IRepository<TEntity> repository;
    private readonly IDomainFactory<TModel, TEntity> factory;

    /// <summary>
    /// Initializes a new instance of the <see cref="BaseDomainService{TModel, TEntity}"/> class.
    /// </summary>
    /// <param name="repository">Injected IRepository.</param>
    /// <param name="factory">Injected IDomainFactory.</param>
    public BaseDomainService(
        IRepository<TEntity> repository,
        IDomainFactory<TModel, TEntity> factory)
    {
        this.repository = repository;
        this.factory = factory;
    }

    /// <inheritdoc/>
    public async Task<int> GetCountAsync()
    {
        return await this.repository.GetCountAsync();
    }

    /// <inheritdoc/>
    public async Task<PaginatedResponse<TModel>> FetchAsync(int offset, int limit)
    {
        var count = await this.repository.GetCountAsync();
        var entities = await this.repository.FetchAsync(offset, limit);
        var models = entities.Select(this.factory.New).ToList();
        return new PaginatedResponse<TModel>()
        {
            Total = count,
            Offset = offset,
            Limit = limit,
            Results = models,
        };
    }

    /// <inheritdoc />
    public async Task<TModel> ReadAsync(int id)
    {
        var entity = await this.repository.ReadAsync(id);
        if (entity == null)
        {
            throw new KeyNotFoundException($"{typeof(TModel).Name} {id} not found.");
        }

        var model = this.factory.New(entity);
        return model;
    }

    /// <inheritdoc />
    public async Task<TModel> ReadAsync(string code)
    {
        var entity = await this.repository.ReadAsync(code);
        if (entity == null)
        {
            throw new KeyNotFoundException($"{typeof(TModel).Name} \"{code}\" not found.");
        }

        var model = this.factory.New(entity);
        return model;
    }
}
