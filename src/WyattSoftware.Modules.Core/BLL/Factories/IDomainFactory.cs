﻿// <copyright file="IDomainFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Defines functionality that all domain models should have.
/// </summary>
/// <typeparam name="TModel">The type of domain model.</typeparam>
/// <typeparam name="TEntity">The type of data entity.</typeparam>
public interface IDomainFactory<TModel, TEntity>
    where TModel : IDomainModel
    where TEntity : IEntity
{
    /// <summary>
    /// Create a new model, and inject it with the required dependencies.
    /// </summary>
    /// <param name="entity">The entity to create the model from.</param>
    /// <returns>A Domain model.</returns>
    TModel New(TEntity entity);
}
