﻿// <copyright file="LocalStorageManager.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
namespace WyattSoftware.Modules.Core.BLL.Features.Storage;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.DTOs;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Local file system implementation of the storage manager.
/// </summary>
public class LocalStorageManager : IStorageManager
{
    private readonly CoreOptions coreOptions;

    /// <summary>
    /// Initializes a new instance of the <see cref="LocalStorageManager"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public LocalStorageManager(
        IOptions<CoreOptions> coreOptions)
    {
        this.coreOptions = coreOptions.Value;
    }

    /// <inheritdoc />
    public async Task<List<string>> ListAsync(string folderPath)
    {
        var absoluteFolderPath = this.MapPath(folderPath);
        if (!Directory.Exists(absoluteFolderPath))
        {
            throw new DirectoryNotFoundException();
        }

        return Directory.GetFiles(absoluteFolderPath)
            .Select(x => Path.GetFileName(x) ?? string.Empty)
            .Where(x => !string.IsNullOrEmpty(x))
            .ToList();
    }

    /// <inheritdoc />
    public async Task<string> SaveFileAsync(FileDto file, string folderPath, bool overwrite = false)
    {
        if (!(folderPath.EndsWith("/") || folderPath.EndsWith("\\")))
        {
            folderPath += this.coreOptions.Storage.Local.FolderSeparator;
        }

        var absoluteFolderPath = this.MapPath(folderPath);
        if (!Directory.Exists(absoluteFolderPath))
        {
            throw new DirectoryNotFoundException($"Directory \"{folderPath}\" not found.");
        }

        // Null or empty?
        if (file == null || file.ContentLength == 0 || file.Data == null)
        {
            throw new ArgumentException($"{file}: File is empty.");
        }

        var filename = FilenameConverter.SafeFilename(file.Filename ?? string.Empty);
        filename = overwrite
            ? filename
            : UniqueFilename(absoluteFolderPath, filename);

        // Try to save the file to the filesystem.
        file.Data.Seek(0, SeekOrigin.Begin);
        using (var fileStream = File.Create(absoluteFolderPath + filename))
        {
            file.Data.CopyTo(fileStream);
        }

        // The saved filename may be different to the posted filename.
        return filename;
    }

    /// <inheritdoc />
    public async Task EnsureFolderAsync(string folderPath)
    {
        var absoluteFolderPath = this.MapPath(folderPath);
        Directory.CreateDirectory(absoluteFolderPath);
    }

    /// <inheritdoc />
    public async Task RemoveFolderIfEmptyAsync(string folderPath)
    {
        var absoluteFolderPath = this.MapPath(folderPath);
        if (!Directory.Exists(absoluteFolderPath))
        {
            return;
        }

        var hasFiles = Directory.GetFiles(absoluteFolderPath).Any();
        if (!hasFiles)
        {
            Directory.Delete(absoluteFolderPath);
        }
    }

    /// <inheritdoc />
    public async Task DeleteAsync(string filePath)
    {
        var absoluteFilePath = this.MapPath(filePath);
        if (!File.Exists(absoluteFilePath))
        {
            return;
        }

        File.Delete(absoluteFilePath);
    }

    /// <summary>
    /// Ensures a unique filename by concatenating an integer to the end.
    /// </summary>
    private static string UniqueFilename(string absoluteFolderPath, string filename)
    {
        var filenameBase = Path.GetFileNameWithoutExtension(filename);
        var extension = Path.GetExtension(filename); // Contains the dot.

        var uniqueFilename = filename;
        var i = 0;
        while (File.Exists(absoluteFolderPath + uniqueFilename))
        {
            i++;
            uniqueFilename = $"{filenameBase}_{i}{extension}";
        }

        return uniqueFilename;
    }

    /// <summary>
    /// Maps the relative path to an absolute path on the local filesystem.
    /// </summary>
    /// <param name="relativePath">The relative path.</param>
    /// <returns>The absolute path.</returns>
    private string MapPath(string relativePath)
    {
        var path = relativePath.Replace("~/", "/");
        if (this.coreOptions.Storage.Local.FolderSeparator.Equals("\\"))
        {
            path = path.Replace("/", "\\");
        }

        return Path.Join(this.coreOptions.Storage.Local.AbsolutePath, path);
    }
}
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously