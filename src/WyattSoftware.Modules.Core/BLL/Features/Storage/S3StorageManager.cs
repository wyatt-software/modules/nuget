﻿// <copyright file="S3StorageManager.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Storage;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon;
using Amazon.Internal;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.BLL.DTOs;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// AWS S3 implementation of the storage manager.
/// </summary>
public class S3StorageManager : IStorageManager
{
    private readonly ILogger<S3StorageManager> logger;
    private readonly CoreOptions coreOptions;

    private readonly AmazonS3Client amazonS3Client;

    /// <summary>
    /// Initializes a new instance of the <see cref="S3StorageManager"/> class.
    /// </summary>
    /// <param name="logger">Injected ILogger{S3StorageManager}.</param>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public S3StorageManager(
        ILogger<S3StorageManager> logger,
        IOptions<CoreOptions> coreOptions)
    {
        this.logger = logger;
        this.coreOptions = coreOptions.Value;

        // ServiceUrl is set instead when using an alternative like Digital Ocean Spaces.
        var serviceUrl = this.coreOptions.Storage.S3.ServiceUrl;
        if (!string.IsNullOrEmpty(serviceUrl))
        {
            this.amazonS3Client = new AmazonS3Client(
                this.coreOptions.Storage.S3.AccessKeyId,
                this.coreOptions.Storage.S3.SecretAccessKey,
                new AmazonS3Config()
                {
                    ServiceURL = this.coreOptions.Storage.S3.ServiceUrl,
                });
        }
        else
        {
            // Default to AWS S3.
            this.amazonS3Client = new AmazonS3Client(
                this.coreOptions.Storage.S3.AccessKeyId,
                this.coreOptions.Storage.S3.SecretAccessKey,
                RegionEndpoint.GetBySystemName(this.coreOptions.Storage.S3.Region));
        }
    }

    /// <inheritdoc />
    public async Task<List<string>> ListAsync(string folderPath)
    {
        var objects = new List<string>();

        var recievedAll = false;
        string? startAfter = null;
        while (!recievedAll)
        {
            var request = new ListObjectsV2Request()
            {
                BucketName = this.coreOptions.Storage.S3.Bucket,
                Prefix = folderPath,
                StartAfter = startAfter,
            };

            var response = await this.amazonS3Client.ListObjectsV2Async(request);
            if (response.HttpStatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Could not list objects in \"{folderPath}\".");
            }

            objects.AddRange(
                response.S3Objects
                    .Select(x => Path.GetFileName(x.Key))
                    .Where(x => !string.IsNullOrEmpty(x)));

            recievedAll = !response.IsTruncated;
            startAfter = response.S3Objects.LastOrDefault()?.Key;
        }

        return objects;
    }

    /// <inheritdoc />
    public async Task<string> SaveFileAsync(FileDto file, string folderPath, bool overwrite = false)
    {
        if (file == null || file.Filename == null)
        {
            throw new Exception($"Empty file or blank filename.");
        }

        var safeFilename = FilenameConverter.SafeFilename(file.Filename);
        var filePath = folderPath + safeFilename;

        var exists = await this.Exists(filePath);
        if (exists && !overwrite)
        {
            throw new Exception($"\"{filePath}\" already exists.");
        }

        var request = new PutObjectRequest()
        {
            InputStream = file.Data,
            BucketName = this.coreOptions.Storage.S3.Bucket,
            Key = filePath,
            CannedACL = S3CannedACL.PublicRead,
            AutoCloseStream = false, // Keep open for potentially creating different sizes.
        };

        var response = await this.amazonS3Client.PutObjectAsync(request);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new Exception($"Could not save file \"{filePath}\".");
        }

        return safeFilename;
    }

    /// <inheritdoc />
    public Task EnsureFolderAsync(string folderPath)
    {
        // There's no such thing as folders in S3.
        return Task.FromResult(0);
    }

    /// <inheritdoc />
    public Task RemoveFolderIfEmptyAsync(string folderPath)
    {
        // There's no such thing as folders in S3.
        return Task.FromResult(0);
    }

    /// <inheritdoc />
    public async Task DeleteAsync(string filePath)
    {
        var request = new DeleteObjectRequest
        {
            BucketName = this.coreOptions.Storage.S3.Bucket,
            Key = filePath,
        };

        await this.amazonS3Client.DeleteObjectAsync(request);

        /*
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new Exception($"Could not delete \"{this.PathPrefix + fileOrfolderPath}\".");
        }
        */
    }

    /// <summary>
    /// Determines if a file or folder already exists.
    /// </summary>
    /// <param name="filePath">The file or folder path, assumed to be valid (no leading or trailing slash).</param>
    /// <returns>A value indicating whether the file exists.</returns>
    private async Task<bool> Exists(string filePath)
    {
        var request = new ListObjectsV2Request()
        {
            BucketName = this.coreOptions.Storage.S3.Bucket,
            Prefix = filePath,
        };

        this.logger.LogDebug($"Checking if \"{filePath}\" already exists.");
        this.logger.LogDebug($"If this call hangs, you probably have the S3 credentials wrong.");

        var response = await this.amazonS3Client.ListObjectsV2Async(request);
        if (response.HttpStatusCode != HttpStatusCode.OK)
        {
            throw new Exception($"Could not determine if \"{filePath}\" exists.");
        }

        var exists = response.S3Objects.Any();

        this.logger.LogDebug(exists ? "Exists" : "Does not exist.");

        return exists;
    }
}
