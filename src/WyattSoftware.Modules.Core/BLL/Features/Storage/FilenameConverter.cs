﻿// <copyright file="FilenameConverter.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Storage;

/// <summary>
/// Provides functionality for ensuring filenames will be valid in the storage system.
/// </summary>
public static class FilenameConverter
{
    /// <summary>
    /// Takes a filename and converts it to something safe to use in the storage system.
    /// </summary>
    /// <param name="filename">The original filename.</param>
    /// <param name="validSpecialChars">Allowed special characters (additional to alphanumeric).</param>
    /// <param name="replaceWithChar">Character to replace invalid characters with.</param>
    /// <returns>A filename that's safe to use in the storage system.</returns>
    public static string SafeFilename(string filename, string validSpecialChars = "_-.", char replaceWithChar = '_')
    {
        var output = string.Empty;
        for (var i = 0; i < filename.Length; i++)
        {
            var c = filename[i];
            if (
                !(char.IsLetterOrDigit(c) || validSpecialChars.Contains(c))
                || (c == '.' && (i == 0 || i == filename.Length - 1)))
            {
                c = replaceWithChar;
            }

            output += c;
        }

        return output;
    }
}
