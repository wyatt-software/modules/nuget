﻿// <copyright file="IStorageManager.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Storage;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.DTOs;

/// <summary>
/// Provides a way to store files.
/// </summary>
public interface IStorageManager
{
    /// <summary>
    /// Lists file names within a specified path, excluding folders.
    /// </summary>
    /// <param name="folderPath">Relative folder path (eg: "/gallery/large/c0/").</param>
    /// <returns>A list of filenames (excluding folder paths).</returns>
    Task<List<string>> ListAsync(string folderPath);

    /// <summary>
    /// Saves a file.
    /// </summary>
    /// <param name="file">The file to upload.</param>
    /// <param name="folderPath">Relative folder path (eg: "/gallery/large/c0/").</param>
    /// <param name="overwrite">Overwrite, or generate a new filename when filename already exists.</param>
    /// <returns>The saved filename, which may be different to the posted filename.</returns>
    Task<string> SaveFileAsync(FileDto file, string folderPath, bool overwrite = false);

    /// <summary>
    /// Ensures that all folders exists in the given path.
    /// </summary>
    /// <param name="folderPath">Relative folder path (eg: "/gallery/large/c0/").</param>
    /// <returns>Nothing.</returns>
    Task EnsureFolderAsync(string folderPath);

    /// <summary>
    /// Attempts to remove the given folder.
    /// </summary>
    /// <param name="folderPath">Relative folder path (eg: "/gallery/large/c0/").</param>
    /// <returns>Nothing.</returns>
    Task RemoveFolderIfEmptyAsync(string folderPath);

    /// <summary>
    /// Delete the given file (if exists).
    /// </summary>
    /// <param name="filePath">Absolute file path.</param>
    /// <returns>Nothing.</returns>
    Task DeleteAsync(string filePath);
}