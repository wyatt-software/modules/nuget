﻿// <copyright file="CancellableTaskManager.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.BackgroundTasks;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// The default implementation of <see cref="ICancellableTaskManager"/>.
/// </summary>
public class CancellableTaskManager : ICancellableTaskManager
{
    /// <summary>
    /// The backing store in memory of the background tasks.
    /// </summary>
    private readonly List<CancellableTask> cancellableTasks;

    /// <summary>
    /// Initializes a new instance of the <see cref="CancellableTaskManager"/> class.
    /// </summary>
    public CancellableTaskManager()
    {
        this.cancellableTasks = new List<CancellableTask>();
    }

    /// <inheritdoc/>
    public List<CancellableTask> FetchAll()
    {
        return this.cancellableTasks;
    }

    /// <inheritdoc/>
    public CancellableTask Register(string name)
    {
        var cancellableTask = new CancellableTask
        {
            Key = Guid.NewGuid(),
            Name = name,
            Source = new CancellationTokenSource(),
        };

        this.cancellableTasks.Add(cancellableTask);
        return cancellableTask;
    }

    /// <inheritdoc/>
    public CancellableTask Read(Guid key)
    {
        var cancellableTask = this.cancellableTasks.FirstOrDefault(x => x.Key == key);
        if (cancellableTask == null)
        {
            throw new KeyNotFoundException($"Cancellable Task {key} not found.");
        }

        return cancellableTask;
    }

    /// <inheritdoc/>
    public void Cancel(Guid key)
    {
        var cancellableTask = this.Read(key);
        if (cancellableTask.Source != null)
        {
            cancellableTask.Source.Cancel();
            cancellableTask.Source.Dispose();
        }

        this.cancellableTasks.Remove(cancellableTask);
    }

    /// <inheritdoc/>
    public void Remove(Guid key)
    {
        var cancellableTask = this.Read(key);
        cancellableTask.Source?.Dispose();

        this.cancellableTasks.Remove(cancellableTask);
    }
}
