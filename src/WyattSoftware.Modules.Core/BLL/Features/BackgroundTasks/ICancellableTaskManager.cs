﻿// <copyright file="ICancellableTaskManager.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.BackgroundTasks;

using System;
using System.Collections.Generic;
using WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// Keeps track of cancellation tokens so that we can trigger them externally (i.e.: from a separate API call).
/// </summary>
/// <remarks>It should be configured as a singleton.</remarks>
public interface ICancellableTaskManager
{
    /// <summary>
    /// Lists all currently cancellable background tasks.
    /// </summary>
    /// <returns>The list of background tasks that can be cancelled.</returns>
    List<CancellableTask> FetchAll();

    /// <summary>
    /// Registers a new cancellable task.
    /// </summary>
    /// <param name="name">The human-readable display name for the task.</param>
    /// <returns>The newly created <c>CancellableTask</c>.</returns>
    CancellableTask Register(string name);

    /// <summary>
    /// Tries to get the cancellable task by it's GUID key.
    /// </summary>
    /// <param name="key">The guid key of the cancellable task.</param>
    /// <returns>The cancellable task, or throws an exception.</returns>
    /// <exception cref="KeyNotFoundException">Cancellable Task not found.</exception>
    CancellableTask Read(Guid key);

    /// <summary>
    /// Cancels and removes the specified <c>CancellableTask</c>.
    /// </summary>
    /// <param name="key">The <c>Guid</c> key for the <c>CancellableTask</c> to cancel and remove.</param>
    void Cancel(Guid key);

    /// <summary>
    /// Removes the specified <c>CancellableTask</c> without cancelling it.
    /// </summary>
    /// <param name="key">The <c>Guid</c> key for the <c>CancellableTask</c> to remove.</param>
    void Remove(Guid key);
}
