﻿// <copyright file="IMacroResolver.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Macros;

using System.Collections.Generic;

/// <summary>
/// Provides a way to resolve macros in templates.
/// </summary>
public interface IMacroResolver
{
    /// <summary>
    /// Resolve macros.
    /// </summary>
    /// <param name="templateBody">The template that contains the macros.</param>
    /// <param name="data">A collection of data to resolve macros with.</param>
    /// <returns>The resolved template.</returns>
    string Resolve(string templateBody, Dictionary<string, object?> data);
}
