﻿// <copyright file="HandlebarsMacroResolver.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Macros;

using System.Collections.Generic;
using HandlebarsDotNet;

/// <summary>
/// Standard implementation of the macro resolver.
/// </summary>
public class HandlebarsMacroResolver : IMacroResolver
{
    /// <inheritdoc />
    public string Resolve(string templateBody, Dictionary<string, object?> data)
    {
        var template = Handlebars.Compile(templateBody);
        return template(data);
    }
}
