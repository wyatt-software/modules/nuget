﻿// <copyright file="EmailSender.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Email;

using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Standard implementation of the email sender.
/// </summary>
public class EmailSender : IEmailSender
{
    private readonly CoreOptions coreOptions;
    private readonly IEmailService emailService;

    /// <summary>
    /// Initializes a new instance of the <see cref="EmailSender"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="emailService">Injected IEmailService.</param>
    public EmailSender(
        IOptions<CoreOptions> coreOptions,
        IEmailService emailService)
    {
        this.coreOptions = coreOptions.Value;
        this.emailService = emailService;
    }

    /// <inheritdoc />
    public async Task SendEmailAsync(MailMessage mailMessage)
    {
        if (!string.IsNullOrEmpty(this.coreOptions.Email.RedirectTo.Address))
        {
            var mailAddress = new MailAddress(
                this.coreOptions.Email.RedirectTo.Address,
                this.coreOptions.Email.RedirectTo.DisplayName);

            mailMessage = EmailRedirector.Redirect(mailMessage, mailAddress);
        }

        // It should be safe now that we've redirected if configured.
        await this.emailService.SendEmailUnsafeAsync(mailMessage);
    }
}
