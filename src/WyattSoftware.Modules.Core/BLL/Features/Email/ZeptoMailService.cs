﻿// <copyright file="ZeptoMailService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Email;

using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Zoho ZeptoMail implementation of the email service.
/// </summary>
internal class ZeptoMailService : IEmailService
{
    private readonly CoreOptions coreOptions;
    private readonly IHttpClientFactory httpClientFactory;
    private readonly ILogger<ZeptoMailService> logger;

    /// <summary>
    /// Initializes a new instance of the <see cref="ZeptoMailService"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    /// <param name="httpClientFactory">Injected IHttpClientFactory.</param>
    /// <param name="logger">Injected ILogger{ZeptoMailService}.</param>
    public ZeptoMailService(
        IOptions<CoreOptions> coreOptions,
        IHttpClientFactory httpClientFactory,
        ILogger<ZeptoMailService> logger)
    {
        this.coreOptions = coreOptions.Value;
        this.httpClientFactory = httpClientFactory;
        this.logger = logger;
    }

    /// <inheritdoc />
    public async Task SendEmailUnsafeAsync(MailMessage mailMessage)
    {
        var zeptoMailRequestBodyDto = new RequestBodyDto(
            this.coreOptions.Email.ZeptoMail.BounceAddress, mailMessage);

        var requestJson = JsonConvert.SerializeObject(zeptoMailRequestBodyDto);
        var request = new HttpRequestMessage(HttpMethod.Post, this.coreOptions.Email.ZeptoMail.ApiUrl)
        {
            Content = new StringContent(requestJson, Encoding.UTF8, "application/json"),
        };

        request.Headers.Authorization = new AuthenticationHeaderValue(
            this.coreOptions.Email.ZeptoMail.AuthorizationScheme,
            this.coreOptions.Email.ZeptoMail.ApiKey);

        var httpClient = this.httpClientFactory.CreateClient();
        var response = await httpClient.SendAsync(request);

        if (!response.IsSuccessStatusCode)
        {
            var responseJson = await response.Content.ReadAsStringAsync();
            this.logger.LogError(responseJson);
            throw new Exception("Could not sent email.");
        }
    }
}
