﻿// <copyright file="IEmailSender.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Email;

using System.Net.Mail;
using System.Threading.Tasks;

/// <summary>
/// Provides a way to send emails, redirecting outgoing emails to the override account on the dev environment.
/// </summary>
public interface IEmailSender
{
    /// <summary>
    /// Sends an email.
    /// </summary>
    /// <param name="mailMessage">The email message to send.</param>
    /// <returns>Nothing.</returns>
    Task SendEmailAsync(MailMessage mailMessage);
}
