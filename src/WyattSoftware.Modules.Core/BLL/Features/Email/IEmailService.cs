﻿// <copyright file="IEmailService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Email;

using System.Net.Mail;
using System.Threading.Tasks;

/// <summary>
/// Provides a way to send emails, with no outbound redirection.
/// </summary>
/// <remarks>
/// <para>Do not use this directly!</para>
/// <para>Use IEmailSender instead, otherwise you risk sending emails to real users when developing.</para>
/// </remarks>
public interface IEmailService
{
    /// <summary>
    /// Sends an email.
    /// </summary>
    /// <param name="mailMessage">The email message to send.</param>
    /// <returns>Nothing.</returns>
    /// <remarks>
    /// <para>
    /// This method is named "Unsafe" to remind the developer that no checking or redirecting is done on the email
    /// address. This should not be called directly since real emails are sent out - which could be bad in a
    /// development environment.
    /// </para>
    /// <para>
    /// Use <c>IEmailSender.SendEmailAsync()</c> instead.
    /// </para>
    /// </remarks>
    Task SendEmailUnsafeAsync(MailMessage mailMessage);
}
