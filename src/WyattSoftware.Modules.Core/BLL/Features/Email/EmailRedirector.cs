﻿// <copyright file="EmailRedirector.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Features.Email;

using System.Net.Mail;

/// <summary>
/// Provides functionality to sanitize mail message in the dev environment, so that we don't send emails to real
/// users' email addresses.
/// </summary>
public class EmailRedirector
{
    /// <summary>
    /// Modifies a mail message's To and Bcc properties to send only to the specified address.
    /// </summary>
    /// <param name="mailMessage">The mail message to modify.</param>
    /// <param name="redirectTo">The email to redirect to.</param>
    /// <returns>The mutated mail message.</returns>
    public static MailMessage Redirect(MailMessage mailMessage, MailAddress redirectTo)
    {
        var originalTo = mailMessage.To.FirstOrDefault();
        if (originalTo != null &&
            !string.IsNullOrEmpty(originalTo.Address) &&
            originalTo.Address != redirectTo.Address)
        {
            mailMessage.Subject = $"REDIRECTED {mailMessage.Subject} - originally to {originalTo}";
        }

        mailMessage.To.Clear();
        mailMessage.CC.Clear();
        mailMessage.Bcc.Clear();

        mailMessage.To.Add(redirectTo);

        return mailMessage;
    }
}
