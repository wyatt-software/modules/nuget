﻿// <copyright file="LazilyResolved.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL;

using System;
using Microsoft.Extensions.DependencyInjection;

/// <summary>
/// Allows for lazy-loading injected dependencies as a workaround to resolve circular dependencies.
/// </summary>
/// <typeparam name="T">The dependency type to lazy-load.</typeparam>
public class LazilyResolved<T> : Lazy<T>
    where T : notnull
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LazilyResolved{T}"/> class.
    /// </summary>
    /// <param name="serviceProvider">Injected IServiceProvider.</param>
    public LazilyResolved(IServiceProvider serviceProvider)
        : base(serviceProvider.GetRequiredService<T>)
    {
    }
}
