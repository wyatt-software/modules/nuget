﻿// <copyright file="FileDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs;

using System.IO;

/// <summary>
/// Object used to pass files around.
/// </summary>
/// <remarks>Used instead of the HttpPostedFile, to decouple business logic from the web layer.</remarks>
public class FileDto
{
    /// <summary>
    /// Gets or sets the size of a file, in bytes.
    /// </summary>
    public long ContentLength { get; set; }

    /// <summary>
    /// Gets or sets the MIME content type of a file.
    /// </summary>
    public string ContentType { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the fully qualified name of the file.
    /// </summary>
    public string Filename { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a stream of data that represents the file contents.
    /// </summary>
    public Stream? Data { get; set; }
}
