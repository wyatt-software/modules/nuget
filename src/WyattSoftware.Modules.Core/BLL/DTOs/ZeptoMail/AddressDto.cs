﻿// <copyright file="AddressDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using System.Net.Mail;
using Newtonsoft.Json;

/// <summary>
/// <para>C# class representation of the JSON email address object used by the ZeptoMail email endpoint.</para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class AddressDto
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AddressDto"/> class.
    /// </summary>
    /// <param name="mailAddress">The mail address to map.</param>
    public AddressDto(MailAddress mailAddress)
    {
        this.Address = mailAddress.Address;
        this.Name = mailAddress.DisplayName;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="AddressDto"/> class.
    /// </summary>
    /// <param name="address">The email address field.</param>
    /// <param name="name">The optional display name field.</param>
    public AddressDto(string address, string? name = null)
    {
        this.Address = address;
        this.Name = name;
    }

    /// <summary>
    /// Gets or sets a sender or recipient's email address field.
    /// </summary>
    [JsonProperty("address")]
    public string Address { get; set; }

    /// <summary>
    /// Gets or sets a sender or recipient's display name field.
    /// </summary>
    [JsonProperty("name")]
    public string? Name { get; set; }
}
