﻿// <copyright file="RequestBodyDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using System.Net.Mail;
using Newtonsoft.Json;

/// <summary>
/// <para>C# class representation of the JSON request body used by the ZeptoMail email endpoint.</para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class RequestBodyDto
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RequestBodyDto"/> class.
    /// </summary>
    /// <param name="bounceAddress">The email address to which bounced emails will be sent.</param>
    /// <param name="mailMessage">The mail message to map.</param>
    public RequestBodyDto(string bounceAddress, MailMessage mailMessage)
    {
        if (string.IsNullOrEmpty(bounceAddress))
        {
            throw new ArgumentException("\"bounceAddress\" is null or empty.");
        }

        if (mailMessage == null)
        {
            throw new ArgumentException("\"mailMessage\" is null.");
        }

        this.BounceAddress = bounceAddress;

        if (mailMessage.From != null)
        {
            this.From = new AddressDto(mailMessage.From);
        }

        if (mailMessage.To != null && mailMessage.To.Count > 0)
        {
            this.To = mailMessage.To.Select(x => new RequestToDto(x)).ToList();
        }

        if (mailMessage.ReplyToList != null && mailMessage.ReplyToList.Count > 0)
        {
            this.ReplyTo = mailMessage.ReplyToList.Select(x => new AddressDto(x)).ToList();
        }

        this.Subject = mailMessage.Subject;

        this.TextBody = mailMessage.IsBodyHtml ? null : mailMessage.Body;
        this.HtmlBody = mailMessage.IsBodyHtml ? mailMessage.Body : null;

        if (mailMessage.CC != null && mailMessage.CC.Count > 0)
        {
            this.CC = mailMessage.CC.Select(x => new AddressDto(x)).ToList();
        }

        if (mailMessage.Bcc != null && mailMessage.Bcc.Count > 0)
        {
            this.Bcc = mailMessage.Bcc.Select(x => new AddressDto(x)).ToList();
        }

        if (mailMessage.Headers != null && mailMessage.Headers.Count > 0)
        {
            this.MimeHeaders = new Dictionary<string, string>();
            foreach (var key in mailMessage.Headers.AllKeys)
            {
                if (key == null)
                {
                    continue;
                }

                this.MimeHeaders.Add(key, mailMessage.Headers[key] ?? string.Empty);
            }
        }

        if (mailMessage.Attachments != null && mailMessage.Attachments.Count > 0)
        {
            this.Attachments = mailMessage.Attachments.Select(x => new AttachmentDto(x)).ToList();
        }
    }

    /// <summary>
    /// <para>Gets or sets the email address to which bounced emails will be sent.</para>
    /// <para><b>Allowed value</b> - A valid bounce email address as configured in your Mail Agent.</para>
    /// </summary>
    [JsonRequired]
    [JsonProperty("bounce_address")]
    public string? BounceAddress { get; set; }

    /// <summary>
    /// Gets or sets a senders email address.
    /// </summary>
    [JsonRequired]
    [JsonProperty("from")]
    public AddressDto? From { get; set; }

    /// <summary>
    /// Gets or sets a list of recipients.
    /// </summary>
    [JsonRequired]
    [JsonProperty("to")]
    public List<RequestToDto>? To { get; set; }

    /// <summary>
    /// Gets or sets a list of reply to email addresses.
    /// </summary>
    [JsonProperty("reply_to")]
    public List<AddressDto>? ReplyTo { get; set; }

    /// <summary>
    /// Gets or sets the subject of the email to be sent.
    /// </summary>
    [JsonRequired]
    [JsonProperty("subject")]
    public string? Subject { get; set; }

    /// <summary>
    /// Gets or sets the body content for your email.
    /// </summary>
    /// <remarks>Your email body can either be a textbody or a htmlbody.</remarks>
    [JsonProperty("textbody")]
    public string? TextBody { get; set; }

    /// <summary>
    /// Gets or sets the body content for your email.
    /// </summary>
    /// <remarks>Your email body can either be a textbody or a htmlbody.</remarks>
    [JsonProperty("htmlbody")]
    public string? HtmlBody { get; set; }

    /// <summary>
    /// Gets or sets email addresses of cc'd recipients.
    /// </summary>
    [JsonProperty("cc")]
    public List<AddressDto>? CC { get; set; }

    /// <summary>
    /// Gets or sets email addresses of bcc'd recipients.
    /// </summary>
    [JsonProperty("bcc")]
    public List<AddressDto>? Bcc { get; set; }

    /// <summary>
    /// <para>Gets or sets a value indicating whether click tracking is enabled.</para>
    /// <para>You can also enable email click tracking in your Mail Agent under Email Tracking section.</para>
    /// <para>Note: The API setting will override the Mail Agent settings in your ZeptoMail account.</para>
    /// <para><b>Allowed value</b></para>
    /// <list type="bullet">
    /// <item>
    /// <term>True</term>
    /// <description>Enable email click tracking.</description>
    /// </item>
    /// <item>
    /// <term>False</term>
    /// <description>Disable email click tracking.</description>
    /// </item>
    /// </list>
    /// </summary>
    [JsonProperty("track_clicks")]
    public bool? TrackClicks { get; set; }

    /// <summary>
    /// <para>Gets or sets a value indicating whether open tracking is enabled.</para>
    /// <para>You can also enable email open tracking in your Mail Agent under Email Tracking section.</para>
    /// <para>Note: The API setting will override the Mail Agent settings in your ZeptoMail account.</para>
    /// <para><b>Allowed value</b></para>
    /// <list type="bullet">
    /// <item>
    /// <term>True</term>
    /// <description>Enable email open tracking.</description>
    /// </item>
    /// <item>
    /// <term>False</term>
    /// <description>Disable email open tracking.</description>
    /// </item>
    /// </list>
    /// </summary>
    [JsonProperty("track_opens")]
    public bool? TrackOpens { get; set; }

    /// <summary>
    /// Gets or sets an identifier set by the user to track a particular transaction.
    /// </summary>
    [JsonProperty("client_reference")]
    public string? ClientReference { get; set; }

    /// <summary>
    /// Gets or sets the additional headers to be sent in the email for your reference purposes.
    /// </summary>
    [JsonProperty("mime_headers")]
    public Dictionary<string, string>? MimeHeaders { get; set; }

    /// <summary>
    /// Gets or sets the attachments you want to add to your transactional emails.
    /// </summary>
    [JsonProperty("attachments")]
    public List<AttachmentDto>? Attachments { get; set; }

    /// <summary>
    /// Gets or sets the attachments you want to add to your transactional emails.
    /// </summary>
    [JsonProperty("inline_images")]
    public List<InlineImageDto>? InlineImages { get; set; }
}
