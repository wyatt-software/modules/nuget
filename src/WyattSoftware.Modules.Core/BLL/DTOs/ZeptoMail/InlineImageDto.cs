﻿// <copyright file="InlineImageDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using Newtonsoft.Json;

/// <summary>
/// <para>C# class representation of the JSON "inline_image" object used by the ZeptoMail email endpoint.</para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class InlineImageDto
{
    /// <summary>
    /// <para>Gets or sets the content of your attachment.</para>
    /// <para><b>Allowed value</b> - Base64 encoded value of a file.</para>
    /// </summary>
    [JsonProperty("content")]
    public string? Content { get; set; }

    /// <summary>
    /// <para>Gets or sets the content type in your attachment.</para>
    /// <para><b>Allowed value</b></para>
    /// <list type="bullet">
    /// <item>simple text message - plain / text</item>
    /// <item>image file - image / jpg</item>
    /// </list>
    /// </summary>
    [JsonProperty("mime_type")]
    public string? MimeType { get; set; }

    /// <summary>
    /// <para>Gets or sets the unique key for your attached files in a Mail Agent.</para>
    /// <para>Obtain file_cache_key from the File Cache section in your Mail Agent.</para>
    /// </summary>
    [JsonProperty("file_cache_key")]
    public string? FileCacheKey { get; set; }

    /// <summary>
    /// <para>
    /// Gets or sets he content id used by html body for content lookup. Each content should be given a separate cid
    /// value.
    /// </para>
    /// <para><b>Allowed value</b></para>
    /// <para><b>It can either a base64 encoded content or a file_cache_key or both.</b></para>
    /// </summary>
    [JsonProperty("cid")]
    public string? Cid { get; set; }
}
