﻿// <copyright file="AttachmentDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using System.Net.Mail;
using Newtonsoft.Json;
using WyattSoftware.Modules.Core.BLL.Extensions;

/// <summary>
/// <para>C# class representation of the JSON "attachment" object used by the ZeptoMail email endpoint.</para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class AttachmentDto
{
    /// <summary>
    /// Initializes a new instance of the <see cref="AttachmentDto"/> class.
    /// </summary>
    /// <param name="attachment">The attachment to map from.</param>
    public AttachmentDto(Attachment attachment)
    {
        if (attachment == null || attachment.ContentStream == null || attachment.ContentStream.Length == 0)
        {
            throw new ArgumentException("The attachment has no data.");
        }

        var buffer = attachment.ContentStream.ToByteArray();
        this.Content = Convert.ToBase64String(buffer, Base64FormattingOptions.InsertLineBreaks);

        this.Name = attachment.Name;
        this.MimeType = attachment.ContentType.MediaType;
    }

    /// <summary>
    /// <para>Gets or sets the content of your attachment.</para>
    /// <para><b>Allowed value</b> - Base64 encoded value of a file.</para>
    /// </summary>
    [JsonProperty("content")]
    public string? Content { get; set; }

    /// <summary>
    /// <para>Gets or sets the content type in your attachment.</para>
    /// <para><b>Allowed value</b></para>
    /// <list type="bullet">
    /// <item>simple text message - plain / text</item>
    /// <item>image file - image / jpg</item>
    /// </list>
    /// </summary>
    [JsonProperty("mime_type")]
    public string? MimeType { get; set; }

    /// <summary>
    /// Gets or sets the file name of your attachment.
    /// </summary>
    [JsonProperty("name")]
    public string? Name { get; set; }

    /// <summary>
    /// <para>Gets or sets the unique key for your attached files in a Mail Agent.</para>
    /// <para>Obtain file_cache_key from the File Cache section in your Mail Agent.</para>
    /// </summary>
    [JsonProperty("file_cache_key")]
    public string? FileCacheKey { get; set; }
}
