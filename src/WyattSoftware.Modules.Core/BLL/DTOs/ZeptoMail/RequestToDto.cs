﻿// <copyright file="RequestToDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using System.Net.Mail;
using Newtonsoft.Json;

/// <summary>
/// <para>C# class representation of the JSON "to" object used by the ZeptoMail email endpoint.</para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class RequestToDto
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RequestToDto"/> class.
    /// </summary>
    /// <param name="mailAddress">The mail address to map.</param>
    public RequestToDto(MailAddress mailAddress)
    {
        this.EmailAddress = new AddressDto(mailAddress);
    }

    /// <summary>
    /// Gets or sets a valid recipient email address with "address" and "name".
    /// </summary>
    [JsonRequired]
    [JsonProperty("email_address")]
    public AddressDto? EmailAddress { get; set; }
}
