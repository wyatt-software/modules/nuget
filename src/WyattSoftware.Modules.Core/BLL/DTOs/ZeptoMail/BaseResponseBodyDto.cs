﻿// <copyright file="BaseResponseBodyDto.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

using Newtonsoft.Json;

/// <summary>
/// <para>
/// C# class representation of common field in the JSON success and failure responses returned by the ZeptoMail
/// email endpoint.
/// </para>
/// <para>
/// See their <see href="https://www.zoho.com/zeptomail/help/api/email-sending.html">API Documentations</see>.
/// </para>
/// </summary>
public class BaseResponseBodyDto
{
    /// <summary>
    /// Gets or sets the status of the request made.
    /// </summary>
    [JsonProperty("message")]
    public string? Message { get; set; }

    /// <summary>
    /// Gets or sets a unique id which is generated for every request.
    /// </summary>
    [JsonProperty("request_id")]
    public string? RequestId { get; set; }
}
