﻿// <copyright file="CancellableTask.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Models;

using System;
using System.Threading;

/// <summary>
/// Represents a task that can be cancelled.
/// </summary>
public class CancellableTask
{
    /// <summary>
    /// Gets or sets the key that this task is referenced by.
    /// </summary>
    public Guid Key { get; set; }

    /// <summary>
    /// Gets or sets the name of the task. This is only used for human readability.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the source of the cancellation token.
    /// </summary>
    public CancellationTokenSource? Source { get; set; }
}
