﻿// <copyright file="PaginatedResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Models;

using System.Collections.Generic;

/// <summary>
/// Wraps a single "page" of results, with metadata describing their position in the total available.
/// </summary>
/// <typeparam name="T">The type of result.</typeparam>
public class PaginatedResponse<T>
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PaginatedResponse{T}"/> class.
    /// </summary>
    public PaginatedResponse()
    {
        this.Results = new List<T>();
    }

    /// <summary>
    /// Gets or sets the total number of results available.
    /// </summary>
    public int Total { get; set; }

    /// <summary>
    /// Gets or sets the offset number of records to start the limited results from.
    /// </summary>
    public int Offset { get; set; }

    /// <summary>
    /// Gets or sets the size of a "page" of results.
    /// </summary>
    public int Limit { get; set; }

    /// <summary>
    /// Gets or sets the limited collection of results.
    /// </summary>
    public List<T> Results { get; set; }
}
