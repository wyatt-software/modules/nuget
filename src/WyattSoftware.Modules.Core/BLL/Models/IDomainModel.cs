﻿// <copyright file="IDomainModel.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Models;

/// <summary>
/// All domain models should implement this interface.
/// </summary>
/// <remarks>Fetch and Read methods are part of the associated <c>IDomainService</c>.</remarks>
public interface IDomainModel
{
    /// <summary>
    /// Creates the domain model.
    /// </summary>
    /// <returns>Nothing.</returns>
    Task CreateAsync();

    /// <summary>
    /// Updates the domain model.
    /// </summary>
    /// <returns>Nothing.</returns>
    Task UpdateAsync();

    /// <summary>
    /// Deletes the domain model.
    /// </summary>
    /// <returns>Nothing.</returns>
    Task DeleteAsync();
}
