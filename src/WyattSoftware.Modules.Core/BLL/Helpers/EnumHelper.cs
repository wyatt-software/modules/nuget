﻿// <copyright file="EnumHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

using System.Linq;

/// <summary>
/// Provides helper functions for enums.
/// </summary>
public static class EnumHelper
{
    /// <summary>
    /// <para>
    /// Takes a comma-sepatated list of role values, and converts it to a comma-separated list of role string
    /// representations.
    /// </para>
    /// <para>
    /// Eg: "1,2" --> "ForumMember,ForumModerator".
    /// </para>
    /// </summary>
    /// <typeparam name="T">The enum type.</typeparam>
    /// <param name="values">A comma-sepatated list of role values. Eg: "1,2".</param>
    /// <param name="separator">Character to use as a separator.</param>
    /// <returns>A comma-separated list of role string representations. Eg: "ForumMember,ForumModerator".</returns>
    /// <remarks>Undefined enum values are passed through as integers unchanged.</remarks>
    public static string ConvertValuesToNamed<T>(string values, char separator = ',')
    {
        if (string.IsNullOrEmpty(values))
        {
            return string.Empty;
        }

        return string.Join(separator, values.Split(separator).Select(x => ((T)(object)int.Parse(x)).ToString()));
    }
}
