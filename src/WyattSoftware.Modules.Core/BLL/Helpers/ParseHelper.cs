﻿// <copyright file="ParseHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

/// <summary>
/// Provides functionality for converting strings to simple data types.
/// </summary>
public static class ParseHelper
{
    /// <summary>
    /// Parses a boolean.
    /// </summary>
    /// <param name="rawValue">The raw value to parse.</param>
    /// <param name="defaultValue">The value to use if the raw value is null or invalid.</param>
    /// <returns>The parsed value, or the default value if the raw value is null or invalid.</returns>
    public static bool GetBoolean(string rawValue, bool defaultValue)
    {
        if (rawValue == null)
        {
            return defaultValue;
        }

        return !bool.TryParse(rawValue, out var value)
            ? defaultValue
            : value;
    }

    /// <summary>
    /// Parses an integer.
    /// </summary>
    /// <param name="rawValue">The raw value to parse.</param>
    /// <param name="defaultValue">The value to use if the raw value is null or invalid.</param>
    /// <returns>The parsed value, or the default value if the raw value is null or invalid.</returns>
    public static int GetInteger(string rawValue, int defaultValue)
    {
        if (rawValue == null)
        {
            return defaultValue;
        }

        return !int.TryParse(rawValue, out var value)
            ? defaultValue
            : value;
    }

    /// <summary>
    /// Parses a string.
    /// </summary>
    /// <param name="rawValue">The raw value to parse.</param>
    /// <param name="defaultValue">The value to use if the raw value is null.</param>
    /// <returns>The parsed value, or the default value if the raw value is null.</returns>
    public static string GetString(string rawValue, string defaultValue)
    {
        if (rawValue == null)
        {
            return defaultValue;
        }

        return rawValue;
    }
}
