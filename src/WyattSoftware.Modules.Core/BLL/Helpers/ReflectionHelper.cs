﻿// <copyright file="ReflectionHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

/// <summary>
/// Provides functionality for dynamically finding types.
/// </summary>
public class ReflectionHelper
{
    /// <summary>
    /// Gets any types in the application that implement the given type.
    /// </summary>
    /// <param name="interfaceType">The interface to find implementations of.</param>
    /// <returns>A List of types.</returns>
    public static List<Type> GetAllImplementationsOf(Type interfaceType)
    {
        return AppDomain
            .CurrentDomain
            .GetAssemblies()
            .SelectMany(x => x.GetTypes())
            .Where(x =>
                interfaceType.IsAssignableFrom(x) &&
                !x.IsInterface &&
                !x.IsAbstract)
            .ToList();
    }
}
