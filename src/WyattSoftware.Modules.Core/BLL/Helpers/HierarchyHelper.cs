﻿// <copyright file="HierarchyHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

using System;
using System.Collections.Generic;

/// <summary>
/// Provides functionality for validating hierarchies.
/// </summary>
public class HierarchyHelper
{
    /// <summary>
    /// Returns true if the collection of elements contains a circular reference.
    /// </summary>
    /// <typeparam name="TElement">Type of element.</typeparam>
    /// <typeparam name="TKey">Type of Id and ParentId properties within element.</typeparam>
    /// <param name="elements">The list of elements.</param>
    /// <param name="initialId">The ID of the element to start on.</param>
    /// <param name="getId">A delegate to get the Id from the element.</param>
    /// <param name="getParentId">A delegate to get the ParentId from the element.</param>
    /// <param name="exitCondition">A delegate that returns <c>true</c> when the given id is still valid.</param>
    /// <returns><c>true</c> if a circular reference exists in the collection of elements.</returns>
    public static bool HasCircularReference<TElement, TKey>(
        List<TElement> elements,
        TKey initialId,
        Func<TElement, TKey> getId,
        Func<TElement, TKey> getParentId,
        Func<TKey, bool> exitCondition)
    {
        var id = initialId;
        while (exitCondition(id))
        {
            var element = elements.Find(x =>
            {
                var elementId = getId(x);
                return elementId != null && elementId.Equals(id);
            });

            if (element == null)
            {
                return false;
            }

            id = getParentId(element);
            if (id != null && id.Equals(initialId))
            {
                return true;
            }
        }

        return false;
    }
}
