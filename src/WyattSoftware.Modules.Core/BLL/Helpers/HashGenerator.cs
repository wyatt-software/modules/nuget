﻿// <copyright file="HashGenerator.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Generates hashes of inputs.
/// </summary>
public static class HashGenerator
{
    /// <summary>
    /// Gets a SHA256 hash of the given text.
    /// </summary>
    /// <param name="text">The text to hash.</param>
    /// <returns>A hash of the given text.</returns>
    public static string Sha256(string text)
    {
        var input = Encoding.ASCII.GetBytes(text);
        var output = SHA256.Create().ComputeHash(input);
        return ByteArrayToString(output);
    }

    /// <summary>
    /// Gets an MD5 hash of the given text.
    /// </summary>
    /// <param name="text">The text to hash.</param>
    /// <returns>A salted hash of the given text.</returns>
    /// <remarks>Passwords on the old site were in MD5 format.</remarks>
    public static string Md5(string text)
    {
        var input = Encoding.ASCII.GetBytes(text);
        var output = MD5.Create().ComputeHash(input);
        return ByteArrayToString(output);
    }

    /// <summary>
    /// Converts a byte array to hexadecimal string.
    /// </summary>
    /// <param name="ba">A byte array.</param>
    /// <returns>A hexadecimal string.</returns>
    /// <remarks>
    /// Taken from:
    /// https://stackoverflow.com/questions/311165/how-do-you-convert-a-byte-array-to-a-hexadecimal-string-and-vice-versa .
    /// </remarks>
    private static string ByteArrayToString(byte[] ba)
    {
        var hex = new StringBuilder(ba.Length * 2);
        foreach (var b in ba)
        {
            hex.AppendFormat("{0:x2}", b);
        }

        return hex.ToString();
    }
}
