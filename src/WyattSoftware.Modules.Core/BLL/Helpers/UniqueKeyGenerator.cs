﻿// <copyright file="UniqueKeyGenerator.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

using System.Security.Cryptography;
using System.Text;

/// <summary>
/// Generates a unique key of the specified length using only the valid chars.
/// </summary>
public static class UniqueKeyGenerator
{
    /// <summary>
    /// Generates a unique key of the specified length.
    /// </summary>
    /// <remarks>
    /// Taken from Peter's answer here:
    /// https://bytes.com/topic/c-sharp/answers/671528-c-random-alphanumeric-strings .
    /// </remarks>
    /// <param name="size">The length of the string to generate.</param>
    /// <param name="validChars">Valid chars to use. Alphanumeric by default.</param>
    /// <returns>a unique key of the specified length.</returns>
    public static string Generate(
        int size, string validChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
    {
        var data = new byte[size];
        using (var rng = RandomNumberGenerator.Create())
        {
            rng.GetBytes(data);
        }

        var result = new StringBuilder(size);
        foreach (var b in data)
        {
            result.Append(validChars[b % validChars.Length]);
        }

        return result.ToString();
    }
}
