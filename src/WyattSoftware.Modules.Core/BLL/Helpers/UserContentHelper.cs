﻿// <copyright file="UserContentHelper.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.BLL.Helpers;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

/// <summary>
/// Provides functionality for converting between the various user content formats.
/// </summary>
public static class UserContentHelper
{
    /// <summary>
    /// Converts Bbcode to Markdown.
    /// </summary>
    /// <param name="bbcode">The bbcode string to convert.</param>
    /// <returns>The converted markdown.</returns>
    public static string BbcodeToMarkdown(string bbcode)
    {
        if (string.IsNullOrEmpty(bbcode))
        {
            return string.Empty;
        }

        var markdown = bbcode;

        markdown = Regex.Replace(
            markdown,
            @"(?i)\[url\](.*?)\[\/url\]",
            x => $"<{x.Groups[1].Value.Replace(" ", "%20")}>");

        markdown = Regex.Replace(
            markdown,
            @"(?i)\[url=([^\]]+)\](.*?)\[\/url\]",
            x => $"[{x.Groups[2].Value}]({x.Groups[1].Value.Replace(" ", "%20")})");

        markdown = Regex.Replace(
            markdown,
            @"(?i)\[img\](.*?)\[\/img\]",
            x => $"![]({x.Groups[1].Value.Replace(" ", "%20")})");

        // [yt] is not a real bbcode tag. But was implemented as a custom tag in the old forum.
        markdown = Regex.Replace(
            markdown,
            @"(?i)\[yt\](.*?)\[\/yt\]",
            "[Youtube video](https://www.youtube.com/watch?v=$1)");

        markdown = Regex.Replace(markdown, @"(?i)\[b\](.*?)\[\/b\]", "**$1**");
        markdown = Regex.Replace(markdown, @"(?i)\[i\](.*?)\[\/i\]", "*$1*");
        markdown = Regex.Replace(markdown, @"(?i)\[u\](.*?)\[\/u\]", "$1");
        markdown = Regex.Replace(markdown, @"(?i)\[color=([^\]]*?)\]([^\]]*?)\[\/color\]", "$2");
        markdown = Regex.Replace(markdown, @"(?i)\[quote\](.*?)\[\/quote\]", "> $1");
        markdown = Regex.Replace(markdown, @"(?i)\[code\](.*?)\[\/code\]", "`$1`");

        markdown = markdown.Replace("[list]", string.Empty);
        markdown = markdown.Replace("[list=1]", string.Empty);
        markdown = markdown.Replace("[/list]", string.Empty);
        markdown = markdown.Replace("[*]", "* ");

        // Emojis are converted in two steps to avoid problems arising from the colon character.
        markdown = markdown.Replace(":)", "{{smile}}");
        markdown = markdown.Replace(";)", "{{wink}}");
        markdown = markdown.Replace(":D", "{{grin}}");
        markdown = markdown.Replace(":P", "{{stuck_out_tongue}}");
        markdown = markdown.Replace(":(", "{{frowning}}");
        markdown = markdown.Replace(":'(", "{{cry}}");
        markdown = markdown.Replace(":.", "{{hushed}}");
        markdown = markdown.Replace(":|", "{{neutral_face}}");
        markdown = markdown.Replace(":O", "{{open_mouth}}");
        markdown = markdown.Replace(":@", "{{angry}}");
        markdown = markdown.Replace(":S", "{{confused}}");
        markdown = markdown.Replace(":$", "{{blush}}");
        markdown = Regex.Replace(markdown, @"\{\{(.*?)\}\}", ":$1:");

        // Ensure CR+LF newlines.
        markdown = markdown.Replace("\r\n", "\n");
        markdown = markdown.Replace("\n", "\r\n");

        return markdown;
    }

    /// <summary>
    /// Converts absolute URLs for a domain to relative.
    /// </summary>
    /// <param name="text">The text to modify.</param>
    /// <param name="domain">The domain to convert URLs for.</param>
    /// <returns>The modified text.</returns>
    public static string ConvertAbsoluteUrlsToRelative(string text, string domain)
    {
        if (string.IsNullOrEmpty(text))
        {
            return string.Empty;
        }

        text = text.Replace($"http://{domain}/", "/");
        text = text.Replace($"http://www.{domain}/", "/");
        text = text.Replace($"https://{domain}/", "/");
        text = text.Replace($"https://www.{domain}/", "/");
        return text;
    }

    /// <summary>
    /// Finds all images from all posts in a given list.
    /// </summary>
    /// <param name="markdown">A block of markdown to scrape images from.</param>
    /// <param name="baseUrl">Prepend this to relative URLs.</param>
    /// <returns>A list of alt-text/URL pairs.</returns>
    public static List<Tuple<string, string>> ScrapeImages(string markdown, string baseUrl = "")
    {
        var matches = Regex.Matches(markdown, @"!\[([^\]]*)\]\((.*?)\s*(?:""(.*[^""])"")?\s*\)");

        var images = matches
            .OfType<Match>()
            .Select(m => new Tuple<string, string>(m.Groups[1].Value, m.Groups[2].Value))
            .ToList();

        var uniqueImages = images.GroupBy(x => x.Item2).Select(x => x.First()).ToList();

        if (string.IsNullOrEmpty(baseUrl))
        {
            return uniqueImages;
        }

        var absoluteImages = uniqueImages.Select(x => EnsureImageHasAbsoluteUrl(x, baseUrl)).ToList();

        return absoluteImages;
    }

    /// <summary>
    /// Prepends the base url to relative images.
    /// </summary>
    /// <param name="image">A tuple with alt-text and url parts.</param>
    /// <param name="baseUrl">The base URL to prepend to relative URLs.</param>
    /// <returns>An absolute URL.</returns>
    public static Tuple<string, string> EnsureImageHasAbsoluteUrl(Tuple<string, string> image, string baseUrl)
    {
        var (altText, url) = image;

        if (IsAbsoluteUrl(url))
        {
            return image;
        }

        if (url.StartsWith("data:", StringComparison.InvariantCultureIgnoreCase))
        {
            return image;
        }

        var baseUri = new Uri(baseUrl);
        var absoluteUri = new Uri(baseUri, url);

        return new Tuple<string, string>(altText, absoluteUri.ToString());
    }

    /// <summary>
    /// Determines if a URL is absolute.
    /// </summary>
    /// <param name="url">A relative or absolute URL.</param>
    /// <returns><c>true</c> if the URL is absolute.</returns>
    /// <remarks>
    /// Adapted from https://stackoverflow.com/questions/4002692/determine-if-absolute-or-relative-url .
    /// </remarks>
    public static bool IsAbsoluteUrl(string url)
    {
        return Uri.TryCreate(url, UriKind.Absolute, out _);
    }
}
