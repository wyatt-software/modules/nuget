﻿// <copyright file="CoreModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Infrastructure;

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using Dapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using WyattSoftware.Modules.Core.API.Filters;
using WyattSoftware.Modules.Core.API.Hubs;
using WyattSoftware.Modules.Core.BLL;
using WyattSoftware.Modules.Core.BLL.Features.BackgroundTasks;
using WyattSoftware.Modules.Core.BLL.Features.Email;
using WyattSoftware.Modules.Core.BLL.Features.Macros;
using WyattSoftware.Modules.Core.BLL.Features.Storage;
using WyattSoftware.Modules.Core.DAL;
using WyattSoftware.Modules.Core.DAL.Helpers;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.DAL.TypeHandlers;
using WyattSoftware.Modules.Core.Options;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Core module.
/// </summary>
public static class CoreModule
{
    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddCoreModule(this IServiceCollection services, IConfiguration configuration)
    {
        // Make WyattSoftware.Modules.Core appsettings.json options available via dependency injection.
        var coreSection = configuration.GetSection("WyattSoftware:Modules:Core");
        services.Configure<CoreOptions>(coreSection);

        var coreOptions = new CoreOptions();
        coreSection.Bind(coreOptions);

        // System
        services.AddHttpClient();
        services.AddMemoryCache();

        // DAL
        services.AddScoped(typeof(IDataHelper), typeof(DataHelper));
        services.AddScoped(typeof(IEmailTemplateRepository), typeof(EmailTemplateRepository));

        // BLL
        services.AddSingleton(typeof(ICancellableTaskManager), typeof(CancellableTaskManager));

        // Allow lazy-loading.
        // Used for factories with dependencies on other factories, to avoid circular dependencies.
        services.AddScoped(typeof(Lazy<>), typeof(LazilyResolved<>));

        services.AddScoped(typeof(IEmailSender), typeof(EmailSender));
        services.AddScoped(typeof(IEmailService), typeof(ZeptoMailService));
        services.AddScoped(typeof(IMacroResolver), typeof(HandlebarsMacroResolver));

        switch (coreOptions.Storage.Implementation)
        {
            case "Local":
                services.AddScoped(typeof(IStorageManager), typeof(LocalStorageManager));
                break;

            case "S3":
                services.AddScoped(typeof(IStorageManager), typeof(S3StorageManager));
                break;
        }

        // API
        services
            .AddHttpContextAccessor()
            .AddCors(corsOptions =>
            {
                corsOptions.AddDefaultPolicy(
                    builder =>
                    {
                        // Get all CorsAllowedOrigins from all clients.
                        var allOrigins = coreOptions?.Clients
                            .SelectMany(x => x.CorsAllowedOrigins)
                            .ToArray();

                        if (allOrigins == null)
                        {
                            return;
                        }

                        builder
                            .WithOrigins(allOrigins)
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials(); // <-- Needed for SignalR
                    });
            })
            .AddControllers(options =>
            {
                options.Filters.Add(new MappedHttpStatusExceptionFilter());
            })
            .AddNewtonsoftJson();

        services
            .AddSignalR(configure =>
            {
                configure.EnableDetailedErrors = true;
            })
            .AddJsonProtocol(options =>
            {
                options.PayloadSerializerOptions.WriteIndented = false;
            });

        return services;
    }

    /// <summary>
    /// <para>
    /// Begin configuring the core module.
    /// </para>
    /// <para>
    /// Don't forget to also call <c>.UseIdentityModuleEndpoints()</c> (after <c>.UseIdentityModule()</c> if using
    /// the Identity module).
    /// </para>
    /// </summary>
    /// <param name="app">This app builder.</param>
    /// <param name="env">Injected IWebHostEnvironment.</param>
    /// <param name="configuration">Injected IConfiguration.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseCoreModule(
        this IApplicationBuilder app,
        IWebHostEnvironment env,
        IConfiguration configuration)
    {
        // Make WyattSoftware.Modules.Core appsettings.json options available via dependency injection.
        var coreSection = configuration.GetSection("WyattSoftware:Modules:Core");
        var coreOptions = new CoreOptions();
        coreSection.Bind(coreOptions);

        // DAL
        ConfigureMapForAllImplementationsOf(typeof(IDataRecord));

        // Handle MySQL's quirks with data type conversion.
        SqlMapper.AddTypeHandler(new DateTimeHandler());
        SqlMapper.AddTypeHandler(new GuidHandler());
        SqlMapper.AddTypeHandler(new JObjectHandler());
        SqlMapper.AddTypeHandler(new JArrayHandler());

        // BLL
        if (coreOptions.Storage.Implementation.Equals("Local"))
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                // Map requests for gallery images to local files.
                // We don't simply store the images in wwwroot because its' contents are deleted and rebuilt when
                // published.
                RequestPath = new PathString(coreOptions.Storage.Local.RequestPath),
                FileProvider = new PhysicalFileProvider(coreOptions.Storage.Local.AbsolutePath),
            });
        }

        // API
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseRouting();
        app.UseCors();

        return app;
    }

    /// <summary>
    /// <para>
    /// Finish configuring the core module.
    /// </para>
    /// <para>
    /// If using the Identity module. this must be called after <c>.UseIdentityModule()</c> for authorization to
    /// work.
    /// </para>
    /// </summary>
    /// <param name="app">This app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseCoreModuleEndpoints(this IApplicationBuilder app)
    {
        // UseAuthentication() needs to be called between UseRouting() and UseEndpoints(...).
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.MapHub<CoreHub>("/core/hub");
        });

        return app;
    }

    /// <summary>
    /// Sets up the property mapping for all implementations of the given interface that have a table attribute.
    /// </summary>
    private static void ConfigureMapForAllImplementationsOf(Type interfaceType)
    {
        var types = AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(assembly => assembly.GetTypes())
            .Where(type =>
                interfaceType.IsAssignableFrom(type) &&
                type.GetCustomAttribute(typeof(TableAttribute), true) != null);

        foreach (var type in types)
        {
            ConfigureEntityMap(type);
        }
    }

    /// <summary>
    /// Sets up the property mapping for the given entity type.
    /// </summary>
    /// <remarks>Uses the [Column] attribute on the type's properties.</remarks>
    private static void ConfigureEntityMap(Type entityType)
    {
        SqlMapper.SetTypeMap(
            entityType,
            new CustomPropertyTypeMap(
                entityType,
                (type, columnName) =>
                    type.GetProperties().FirstOrDefault(prop =>
                        prop.GetCustomAttributes(true)
                            .OfType<ColumnAttribute>()
                            .Any(attr => attr.Name == columnName))));
    }
}
