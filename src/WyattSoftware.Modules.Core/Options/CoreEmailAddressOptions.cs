﻿// <copyright file="CoreEmailAddressOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from an address in the WyattSoftware.Modules.Core.Email section.
/// </summary>
public class CoreEmailAddressOptions
{
    /// <summary>
    /// Gets or sets the email address.
    /// </summary>
    public string Address { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the display name.
    /// </summary>
    public string DisplayName { get; set; } = string.Empty;
}
