﻿// <copyright file="CoreVendorOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Vendor section.
/// </summary>
public class CoreVendorOptions
{
    /// <summary>
    /// Gets or sets ReCaptcha options.
    /// </summary>
    public CoreVendorReCaptchaOptions ReCaptcha { get; set; } = new CoreVendorReCaptchaOptions();
}
