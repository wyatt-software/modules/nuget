﻿// <copyright file="CoreStorageOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Storage section.
/// </summary>
public class CoreStorageOptions
{
    /// <summary>
    /// Gets or sets the implementation to use for storage.
    /// </summary>
    /// <remarks>
    /// Valid options are <c>"Local"</c> or <c>"S3"</c>.
    /// </remarks>
    public string Implementation { get; set; } = "Local";

    /// <summary>
    /// Gets or sets options relevant to the <c>"Local"</c> storage implementation.
    /// </summary>
    public CoreStorageLocalOptions Local { get; set; } = new CoreStorageLocalOptions();

    /// <summary>
    /// Gets or sets options relevant to the <c>"S3"</c> storage implementation.
    /// </summary>
    public CoreStorageS3Options S3 { get; set; } = new CoreStorageS3Options();
}
