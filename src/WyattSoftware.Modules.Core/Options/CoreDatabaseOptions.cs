﻿// <copyright file="CoreDatabaseOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Database section.
/// </summary>
public class CoreDatabaseOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets the domain name of the server to connect to (without the protocol).
    /// </para>
    /// <para>
    /// Eg: <c>"localhost"</c>, <c>"db.example.com"</c>, or <c>"db-dev.example.com"</c>.
    /// </para>
    /// </summary>
    public string Server { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the port to use for the database connection.
    /// </para>
    /// <para>
    /// Eg: 3306, or 25060 (when using Digital Ocean).
    /// </para>
    /// </summary>
    public int Port { get; set; } = 3306;

    /// <summary>
    /// <para>
    /// Gets or sets the database user to connect with..
    /// </para>
    /// <para>
    /// Eg: <c>"db-user"</c>.
    /// </para>
    /// </summary>
    public string User { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the database user's password.
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the name of the database to use.
    /// </para>
    /// <para>
    /// Eg: <c>"ex"</c>, <c>"example.com"</c>, or <c>"dev.example.com"</c>.
    /// </para>
    /// </summary>
    public string Database { get; set; } = string.Empty;
}
