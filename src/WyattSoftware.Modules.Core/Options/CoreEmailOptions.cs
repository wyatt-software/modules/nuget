﻿// <copyright file="CoreEmailOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Email section.
/// </summary>
public class CoreEmailOptions
{
    /// <summary>
    /// <para>Gets or sets the email address the site sends emails from.</para>
    /// <para>You must verify with ZeptoMail that you own this email address.</para>
    /// </summary>
    public CoreEmailAddressOptions From { get; set; } = new CoreEmailAddressOptions();

    /// <summary>
    /// <para>
    /// Gets or sets an opitional email address to redirect all outbound emails to when running on anything other
    /// than production environment (as determined by the
    /// <seealso href="https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting.hostingenvironmentextensions.isproduction?view=dotnet-plat-ext-6.0">
    /// HostingEnvironmentExtensions.IsProduction(IHostingEnvironment)</seealso> Method).
    /// </para>
    /// <para>
    /// This prevents the site from sending real emails to real users when testing.
    /// </para>
    /// </summary>
    public CoreEmailAddressOptions RedirectTo { get; set; } = new CoreEmailAddressOptions();

    /// <summary>
    /// Gets or sets options for configuring Zoho's ZeptoMail API.
    /// </summary>
    /// <remarks>
    /// This is currently the only implementation for sending transactional emails.
    /// </remarks>
    public CoreEmailZeptoMailOptions ZeptoMail { get; set; } = new CoreEmailZeptoMailOptions();
}
