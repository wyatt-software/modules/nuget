﻿// <copyright file="CoreEmailZeptoMailOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.ZeptoMail section.
/// </summary>
public class CoreEmailZeptoMailOptions
{
    /// <summary>
    /// Gets or sets the URL of Zoho's ZeptoMail API.
    /// </summary>
    /// <remarks>This must be <c>"https://api.zeptomail.com/v1.1/email"</c>.</remarks>
    public string ApiUrl { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the scheme used in the Authorization header when sending API requests.
    /// </summary>
    /// <remarks>
    /// This must be <c>"Zoho-enczapikey"</c>.
    /// </remarks>
    public string AuthorizationScheme { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets your ZeptoMail API key, <i>without</i> the <c>"Zoho-enczapikey"</c> authorization scheme.
    /// </summary>
    public string ApiKey { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the email address to send bounce notifications to.
    /// </summary>
    /// <remarks>
    /// This must match the bounce address that's been set up in ZeptoMail.
    /// </remarks>
    public string BounceAddress { get; set; } = string.Empty;
}
