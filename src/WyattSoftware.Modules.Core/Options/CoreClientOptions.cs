﻿// <copyright file="CoreClientOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from a single element in the WyattSoftware.Modules.Core.Clients section.
/// </summary>
public class CoreClientOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets the codename given to the client app.
    /// </para>
    /// <para>
    /// It needs to also be configured on the client, and sent as the `client_id` when authenticating.
    /// </para>
    /// </summary>
    public string ClientId { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the public key assigned to the client app.
    /// </para>
    /// <para>
    /// It needs to also be configured on the client, and sent as the `client_secret` when authenticating.
    /// </para>
    /// </summary>
    public string ClientSecret { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the number of seconds until the access token expires.
    /// </para>
    /// <para>
    /// Eg: <c>3600</c> (1 hour).
    /// </para>
    /// </summary>
    public int AccessTokenLifetime { get; set; } = 3600;

    /// <summary>
    /// <para>
    /// Gets or sets the number of seconds until the refresh token expires.
    /// </para>
    /// <para>
    /// Eg: <c>1296000</c> (15 days).
    /// </para>
    /// </summary>
    public int SlidingRefreshTokenLifetime { get; set; } = 1296000;

    /// <summary>
    /// <para>
    /// Gets or sets an array of origin clients allowed to connect to this API from.
    /// </para>
    /// <para>
    /// We can configure multiple allowed origins per client.
    /// </para>
    /// <para>
    /// Each entry should be the full protocol, domain, and port of the client, <i>without</i> trailing slash.
    /// </para>
    /// <para>
    /// Eg: <c>"http://localhost:3000"</c>, <c>"https://example.com"</c> or <c>"https://dev.example.com"</c>.
    /// </para>
    /// </summary>
    public List<string> CorsAllowedOrigins { get; set; } = new List<string>();
}
