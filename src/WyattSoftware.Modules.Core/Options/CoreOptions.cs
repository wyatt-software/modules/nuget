﻿// <copyright file="CoreOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core section.
/// </summary>
public class CoreOptions
{
    /// <summary>
    /// Gets or sets the <c>Database</c> section.
    /// </summary>
    public CoreDatabaseOptions Database { get; set; } = new CoreDatabaseOptions();

    /// <summary>
    /// Gets or sets the <c>Clients</c> section.
    /// </summary>
    /// <remarks>
    /// We can configure multiple clients. i.e.: Web, iOS, and Android apps. For now, we only have the web app.
    /// </remarks>
    public List<CoreClientOptions> Clients { get; set; } = new List<CoreClientOptions>();

    /// <summary>
    /// Gets or sets the <c>Encryption</c> section.
    /// </summary>
    public CoreEncryptionOptions Encryption { get; set; } = new CoreEncryptionOptions();

    /// <summary>
    /// Gets or sets the <c>Email</c> section.
    /// </summary>
    public CoreEmailOptions Email { get; set; } = new CoreEmailOptions();

    /// <summary>
    /// Gets or sets global macros available in the site content pages and email templates.
    /// </summary>
    public Dictionary<string, object?> Macros { get; set; } = new Dictionary<string, object?>();

    /// <summary>
    /// Gets or sets the <c>Storage</c> section.
    /// </summary>
    public CoreStorageOptions Storage { get; set; } = new CoreStorageOptions();

    /// <summary>
    /// Gets or sets the <c>Vendor</c> section.
    /// </summary>
    public CoreVendorOptions Vendor { get; set; } = new CoreVendorOptions();
}
