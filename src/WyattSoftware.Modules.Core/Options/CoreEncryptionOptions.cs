﻿// <copyright file="CoreEncryptionOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Encryption section.
/// </summary>
public class CoreEncryptionOptions
{
    /// <summary>
    /// Gets or sets a secret string used when creating passwords hashes etc.
    /// </summary>
    /// <remarks>
    /// It must be kept in sync with the database, so make sure to keep a record of the hash used - especially if
    /// you migrate the database to a new environment.
    /// </remarks>
    public string HashStringSalt { get; set; } = string.Empty;
}
