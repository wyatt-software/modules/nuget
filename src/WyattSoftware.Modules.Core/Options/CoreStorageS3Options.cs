﻿// <copyright file="CoreStorageS3Options.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Storage.S3 section.
/// </summary>
public class CoreStorageS3Options
{
    /// <summary>
    /// <para>
    /// Gets or sets the name of the S3 bucket (or DigitalOcean "Space") used for image and file storage.
    /// </para>
    /// <para>
    /// Eg: <c>"storage-dev.wyatt-software.com"</c>.
    /// </para>
    /// </summary>
    public string Bucket { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets an Access key ID for an IAM user with the <c>AmazonS3FullAccess</c> permission.
    /// </para>
    /// <para>
    /// If using Digital Ocean Spaces, this is your space access key (not the name).
    /// </para>
    /// </summary>
    public string AccessKeyId { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the secret access key for the above Access key ID.
    /// </para>
    /// <para>
    /// If using Digital Ocean Spaces, this is your space secret key.
    /// </para>
    /// </summary>
    public string SecretAccessKey { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the Amazon AWS region to use for your connection to the API.
    /// </para>
    /// <para>
    /// Eg: <c>"ap-southeast-2"</c>.
    /// </para>
    /// </summary>
    public string Region { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the optional ServiceURL if not using AWS.
    /// </para>
    /// <para>
    /// Eg: <c>"https://sgp1.digitaloceanspaces.com"</c>.
    /// </para>
    /// </summary>
    public string ServiceUrl { get; set; } = string.Empty;
}
