﻿// <copyright file="CoreVendorReCaptchaOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Vendor.ReCaptcha section.
/// </summary>
public class CoreVendorReCaptchaOptions
{
    /// <summary>
    /// Gets or sets a value indicating whether ReCaptchas are enabled.
    /// </summary>
    /// <remarks>
    /// Allows you to disable recaptchas for testing locally via Postman etc.
    /// </remarks>
    public bool Enabled { get; set; }

    /// <summary>
    /// Gets or sets your ReCaptcha private key.
    /// </summary>
    /// <remarks>
    /// Your front end client will need to use the public key.
    /// </remarks>
    public string PrivateKey { get; set; } = string.Empty;
}
