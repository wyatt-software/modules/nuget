﻿// <copyright file="CoreStorageLocalOptions.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Options;

/// <summary>
/// appsettings.json options from the WyattSoftware.Modules.Core.Storage.Local section.
/// </summary>
public class CoreStorageLocalOptions
{
    /// <summary>
    /// <para>
    /// Gets or sets the full absolute path on the local filesystem of the storage folder. Don't use trailing
    /// slashes.
    /// </para>
    /// <para>
    /// Note the use of backslashes for Windows systems (escaped as double-backslashes, since this is JSON).
    /// Forward-slashes should be used on Linux systems.
    /// </para>
    /// <para>
    /// Eg: <c>"D:\\websites\\example\\example-server\\storage"</c> or
    /// <c>"/home/ec2-user/example/server/storage"</c>.
    /// </para>
    /// </summary>
    public string AbsolutePath { get; set; } = string.Empty;

    /// <summary>
    /// <para>
    /// Gets or sets the slash character used to separate folders in the current environment.
    /// </para>
    /// <para>
    /// For Windows systems, use <c>"\\"</c> (this is a single back-slash, but is escaped with a backslash prefix
    /// since this is JSON).For Linux systems, uses <c>"/"</c>.
    /// </para>
    /// </summary>
    public string FolderSeparator { get; set; } = "/";

    /// <summary>
    /// <para>
    /// Gets or sets the relative URL request path that should be mapped to the local filesystem.
    /// </para>
    /// <para>
    /// Eg: <c>"/storage"</c>.
    /// </para>
    /// </summary>
    public string RequestPath { get; set; } = "/storage";
}
