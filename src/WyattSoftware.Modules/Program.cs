// <copyright file="Program.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

using WyattSoftware.Modules.Content.Infrastructure;
using WyattSoftware.Modules.Core.Infrastructure;
using WyattSoftware.Modules.Forum.Infrastructure;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Identity.Infrastructure;
using WyattSoftware.Modules.Messaging.Infrastructure;
using WyattSoftware.Modules.Search.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

var env = builder.Environment;

var configuration = builder.Configuration;

builder.Services
    .AddCoreModule(configuration)
    .AddIdentityModule(configuration)
    .AddSearchModule(configuration)
    .AddContentModule()
    .AddGalleryModule(configuration)
    .AddMessagingModule()
    .AddForumModule(configuration);

var app = builder.Build();

app
    .UseCoreModule(env, configuration)
    .UseIdentityModule()
    .UseCoreModuleEndpoints() // <-- This must go after .UseIdentityModule()
    .UseSearchModule()
    .UseContentModule()
    .UseGalleryModule()
    .UseMessagingModule()
    .UseForumModule();

app.Run();
