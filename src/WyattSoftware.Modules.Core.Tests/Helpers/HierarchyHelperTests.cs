﻿// <copyright file="HierarchyHelperTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;
    using WyattSoftware.Modules.Core.Tests.Models;

    /// <summary>
    /// Unit tests for the <c>HierarchyHelper</c> class.
    /// </summary>
    public class HierarchyHelperTests
    {
        private static readonly List<HierarchicalTestClass> CircularElements = new()
        {
            new HierarchicalTestClass(1, null),
            new HierarchicalTestClass(2, 1),
            new HierarchicalTestClass(3, 2),
            new HierarchicalTestClass(4, 2),
            new HierarchicalTestClass(5, 7), // <-- uh oh...
            new HierarchicalTestClass(6, 5),
            new HierarchicalTestClass(7, 6),
        };

        private static readonly List<HierarchicalTestClass> ValidElements = new()
        {
            new HierarchicalTestClass(1, null),
            new HierarchicalTestClass(2, 1),
            new HierarchicalTestClass(3, 2),
            new HierarchicalTestClass(4, 2),
            new HierarchicalTestClass(5, 1),
            new HierarchicalTestClass(6, 5),
            new HierarchicalTestClass(7, 6),
        };

        /// <summary>
        /// Circular reference test.
        /// </summary>
        [Test]
        public void HasCircularReference_ShouldDetect_WhenGivenCircularData()
        {
            var detected = HierarchyHelper.HasCircularReference(
                CircularElements, 7, route => route.Id, route => route.ParentId, id => id.HasValue);

            detected.ShouldBe(true);
        }

        /// <summary>
        /// Circular reference test.
        /// </summary>
        [Test]
        public void HasCircularReference_ShouldNotDetect_WhenGivenValidData()
        {
            var detected = HierarchyHelper.HasCircularReference(
                ValidElements, 7, route => route.Id, route => route.ParentId, id => id.HasValue);

            detected.ShouldBe(false);
        }
    }
}
