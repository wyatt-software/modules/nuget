// <copyright file="HashGeneratorTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using System;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;

    /// <summary>
    /// Unit tests for the <c>HashGenerator</c> class.
    /// </summary>
    public class HashGeneratorTests
    {
        /// <summary>
        /// Valid SHA256 test.
        /// </summary>
        [Test]
        public void Sha256_ShouldHash_WhenGivenText()
        {
            var text = "Some secret text.";
            var hash = HashGenerator.Sha256(text);
            hash.ShouldBe("e7d82d1bb08295682389aa373b8542f73ccfa64298d4a0f7809c1fa375647f2f");
        }

        /// <summary>
        /// Blank SHA256 test.
        /// </summary>
        [Test]
        public void Sha256_ShouldHash_WhenGivenEmptyString()
        {
            var hash = HashGenerator.Sha256(string.Empty);
            hash.ShouldBe("e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
        }

        /// <summary>
        /// Invalid SHA256 test.
        /// </summary>
        [Test]
        public void Sha256_ShouldThrowException_WhenGivenNull()
        {
            Should.Throw<ArgumentNullException>(() => HashGenerator.Sha256(null));
        }

        /// <summary>
        /// Valid MD5 test.
        /// </summary>
        [Test]
        public void Md5_ShouldHash_WhenGivenText()
        {
            var text = "Some secret text.";
            var hash = HashGenerator.Md5(text);
            hash.ShouldBe("6c62f1d2c867d505f64938fd356feb13");
        }

        /// <summary>
        /// Blank MD5 test.
        /// </summary>
        [Test]
        public void Md5_ShouldHash_WhenGivenEmptyString()
        {
            var hash = HashGenerator.Md5(string.Empty);
            hash.ShouldBe("d41d8cd98f00b204e9800998ecf8427e");
        }

        /// <summary>
        /// Invalid MD5 test.
        /// </summary>
        [Test]
        public void Md5_ShouldThrowException_WhenGivenNull()
        {
            Should.Throw<ArgumentNullException>(() => HashGenerator.Md5(null));
        }
    }
}
