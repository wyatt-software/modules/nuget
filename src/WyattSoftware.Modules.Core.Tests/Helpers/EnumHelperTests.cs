// <copyright file="EnumHelperTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;
    using WyattSoftware.Modules.Core.Tests.Enums;

    /// <summary>
    /// Unit tests for the <c>EnumHelper</c> class.
    /// </summary>
    public class EnumHelperTests
    {
        /// <summary>
        /// Valid values.
        /// </summary>
        [Test]
        public void ConvertValuesToNamed_ShouldConvertValuesToNamed_WhenGivenValidValues()
        {
            var input = "1,2";
            var output = EnumHelper.ConvertValuesToNamed<TestEnum>(input);
            output.ShouldBe("ValueOne,ValueTwo");
        }

        /// <summary>
        /// Valid values with custom separator.
        /// </summary>
        [Test]
        public void ConvertValuesToNamed_ShouldConvertValuesToNamed_WhenGivenValidValuesWithCustomSeparator()
        {
            var separator = '|';
            var input = "1|2";
            var output = EnumHelper.ConvertValuesToNamed<TestEnum>(input, separator);
            output.ShouldBe("ValueOne|ValueTwo");
        }

        /// <summary>
        /// No values.
        /// </summary>
        [Test]
        public void ConvertValuesToNamed_ShouldReturnEmptyString_WhenGivenAnEmptyString()
        {
            var input = string.Empty;
            var output = EnumHelper.ConvertValuesToNamed<TestEnum>(input);
            output.ShouldBe(string.Empty);
        }

        /// <summary>
        /// Single value.
        /// </summary>
        [Test]
        public void ConvertValuesToNamed_ShouldReturnASingleName_WhenGivenASingleValue()
        {
            var input = "1";
            var output = EnumHelper.ConvertValuesToNamed<TestEnum>(input);
            output.ShouldBe("ValueOne");
        }

        /// <summary>
        /// Invalid value.
        /// </summary>
        [Test]
        public void ConvertValuesToNamed_ShouldPassInalidValuesThrough_WhenGivenInalidValues()
        {
            var input = "1,2,1000";
            var output = EnumHelper.ConvertValuesToNamed<TestEnum>(input);
            output.ShouldBe("ValueOne,ValueTwo,1000");
        }
    }
}