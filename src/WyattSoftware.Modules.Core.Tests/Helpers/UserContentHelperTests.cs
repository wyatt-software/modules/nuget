// <copyright file="UserContentHelperTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using System;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;

    /// <summary>
    /// Unit tests for the <c>UserContentHelper</c> class.
    /// </summary>
    public class UserContentHelperTests
    {
        private const string ThreeTotalTwoUnique =
@"I fixed up the wiring on my round guard indicator today.
The (+) signal wire was not allowing a circit so I replaced it.

[![alt]({{StorageBaseUrl}}gallery/thumb/e4/e4133c3b-1615-47fe-a110-a6f3d20643ad.jpg)]({{StorageBaseUrl}}gallery/original/e4/e4133c3b-1615-47fe-a110-a6f3d20643ad.jpg)
I drilled a small hole into a nut to make a mould for some solder to form around the end of my new wire.

[![]({{StorageBaseUrl}}gallery/thumb/1c/1cbb365c-25ca-44ac-bdd1-b1bb75105df0.jpg ""title"")]({{StorageBaseUrl}}gallery/original/1c/1cbb365c-25ca-44ac-bdd1-b1bb75105df0.jpg)
Here is a pic of the new wire with solder on the end to act as a contact point in my indicator.
I put it all together in an exploded layout to show what I mean.

[![unique alt]({{StorageBaseUrl}}gallery/thumb/1c/1cbb365c-25ca-44ac-bdd1-b1bb75105df0.jpg ""duplicate url"")]({{StorageBaseUrl}}gallery/original/1c/1cbb365c-25ca-44ac-bdd1-b1bb75105df0.jpg)";

        /// <summary>
        /// Tests BB code to markdown conversion.
        /// </summary>
        [Test]
        public void BbcodeToMarkdown_ShouldConvertToMarkdown_WhenGivenBbcode()
        {
            var bbcode = "This [b]user content[/b] was originally formatted in [i]bbcode[/i] :).";
            var markdown = UserContentHelper.BbcodeToMarkdown(bbcode);
            markdown.ShouldBe("This **user content** was originally formatted in *bbcode* :smile:.");
        }

        /// <summary>
        /// Tests that spaces are encoded within markdown links when converting from bbcode.
        /// </summary>
        [Test]
        public void BbcodeToMarkdown_ShouldEncodeSpaces_WhenGivenUrlWithSpaces()
        {
            var bbcode = "[url=link with spaces]Display text[/url]";
            var markdown = UserContentHelper.BbcodeToMarkdown(bbcode);
            markdown.ShouldBe("[Display text](link%20with%20spaces)");
        }

        /// <summary>
        /// Tests that no images are scraped from markdown containing no images.
        /// </summary>
        [Test]
        public void ScrapeImages_ShouldFindNothing_WhenGivenMarkdownWithoutImages()
        {
            var markdown = "[url=link with spaces]Display text[/url]";
            var images = UserContentHelper.ScrapeImages(markdown);
            images.ShouldBeEmpty();
        }

        /// <summary>
        /// Tests that images are scraped from markdown containing images.
        /// </summary>
        [Test]
        public void ScrapeImages_ShouldFindTwoImages_WhenGivenThreeTotalTwoUnique()
        {
            var images = UserContentHelper.ScrapeImages(ThreeTotalTwoUnique);
            images.Count.ShouldBe(2);

            var (alt0, url0) = images[0];
            alt0.ShouldBe("alt");
            url0.ShouldBe("{{StorageBaseUrl}}gallery/thumb/e4/e4133c3b-1615-47fe-a110-a6f3d20643ad.jpg");

            var (alt1, url1) = images[1];
            alt1.ShouldBe(string.Empty);
            url1.ShouldBe("{{StorageBaseUrl}}gallery/thumb/1c/1cbb365c-25ca-44ac-bdd1-b1bb75105df0.jpg");
        }

        /// <summary>
        /// Tests that absolute URLs aren't mutated.
        /// </summary>
        [Test]
        public void EnsureImageHasAbsoluteUrl_ShouldDoNothing_WhenGivenAbsoluteUrl()
        {
            var image = new Tuple<string, string>("Absolute test", "http://www.original.com/images/test.png");
            var output = UserContentHelper.EnsureImageHasAbsoluteUrl(image, "http://www.mutated.com/");
            var (altText, url) = output;

            altText.ShouldBe("Absolute test");
            url.ShouldBe("http://www.original.com/images/test.png");
        }

        /// <summary>
        /// Tests that relative URLs starting with a slash have the base URL preprended.
        /// </summary>
        [Test]
        public void EnsureImageHasAbsoluteUrl_ShouldPreprend_WhenGivenRelativeUrlWithSlash()
        {
            var image = new Tuple<string, string>("Relative test with slash", "/images/test.png");
            var output = UserContentHelper.EnsureImageHasAbsoluteUrl(image, "http://www.mutated.com/");
            var (altText, url) = output;

            altText.ShouldBe("Relative test with slash");
            url.ShouldBe("http://www.mutated.com/images/test.png");
        }

        /// <summary>
        /// Tests that relative URLs without a slash have the base URL preprended.
        /// </summary>
        [Test]
        public void EnsureImageHasAbsoluteUrl_ShouldPreprend_WhenGivenRelativeUrlWithoutSlash()
        {
            var image = new Tuple<string, string>("Relative test without slash", "images/test.png");
            var output = UserContentHelper.EnsureImageHasAbsoluteUrl(image, "http://www.mutated.com/");
            var (altText, url) = output;

            altText.ShouldBe("Relative test without slash");
            url.ShouldBe("http://www.mutated.com/images/test.png");
        }

        /// <summary>
        /// Tests inline data.
        /// </summary>
        [Test]
        public void EnsureImageHasAbsoluteUrl_ShouldDoNothing_WhenGivenInlineData()
        {
            var image = new Tuple<string, string>("Inline data", "data:image/jpeg, ...");
            var output = UserContentHelper.EnsureImageHasAbsoluteUrl(image, "http://www.mutated.com/");
            var (altText, url) = output;

            altText.ShouldBe("Inline data");
            url.ShouldBe("data:image/jpeg, ...");
        }

        /// <summary>
        /// Tests that absolute URLs are recognised.
        /// </summary>
        [Test]
        public void IsAbsoluteUrl_ShouldReturnTrue_WhenGivenAbsoluteeUrl()
        {
            var full = UserContentHelper.IsAbsoluteUrl("http://www.original.com/images/test.png");
            full.ShouldBe(true);

            var baseOnly = UserContentHelper.IsAbsoluteUrl("http://www.original.com");
            baseOnly.ShouldBe(true);

            var baseWithSlashOnly = UserContentHelper.IsAbsoluteUrl("http://www.original.com/");
            baseWithSlashOnly.ShouldBe(true);
        }

        /// <summary>
        /// Tests that relative URLs are recognised.
        /// </summary>
        [Test]
        public void IsAbsoluteUrl_ShouldReturnFalse_WhenGivenRelativeUrl()
        {
            var withslash = UserContentHelper.IsAbsoluteUrl("/images/test.png");
            withslash.ShouldBe(false);

            var withoutslash = UserContentHelper.IsAbsoluteUrl("images/test.png");
            withoutslash.ShouldBe(false);
        }
    }
}
