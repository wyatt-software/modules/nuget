﻿// <copyright file="ParseHelperTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;

    /// <summary>
    /// Unit tests for the <c>ParseHelper</c> class.
    /// </summary>
    public class ParseHelperTests
    {
        /// <summary>
        /// Valid integer.
        /// </summary>
        [Test]
        public void GetInteger_ShouldParse_WhenGivenValidInput()
        {
            var output = ParseHelper.GetInteger("1234", 5678);
            output.ShouldBe(1234);
        }

        /// <summary>
        /// Invalid integer.
        /// </summary>
        [Test]
        public void GetInteger_ShouldUseDefault_WhenGivenInvalidInput()
        {
            var output = ParseHelper.GetInteger("ABCD", 5678);
            output.ShouldBe(5678);
        }

        /// <summary>
        /// Empty string.
        /// </summary>
        [Test]
        public void GetInteger_ShouldUseDefault_WhenGivenAnEmptyString()
        {
            var output = ParseHelper.GetInteger(string.Empty, 5678);
            output.ShouldBe(5678);
        }

        /// <summary>
        /// null.
        /// </summary>
        [Test]
        public void GetInteger_ShouldUseDefault_WhenGivenNull()
        {
            var output = ParseHelper.GetInteger(null, 5678);
            output.ShouldBe(5678);
        }

        /// <summary>
        /// Valid string.
        /// </summary>
        [Test]
        public void GetString_ShouldParse_WhenGivenValidInput()
        {
            var output = ParseHelper.GetString("ABCD", "Default");
            output.ShouldBe("ABCD");
        }

        /// <summary>
        /// Empty string.
        /// </summary>
        [Test]
        public void GetString_ShouldParse_WhenGivenAnEmptyString()
        {
            var output = ParseHelper.GetString(string.Empty, "Default");
            output.ShouldBe(string.Empty);
        }

        /// <summary>
        /// null.
        /// </summary>
        [Test]
        public void GetString_ShouldUseDefault_WhenGivenNull()
        {
            var output = ParseHelper.GetString(null, "Default");
            output.ShouldBe("Default");
        }
    }
}
