﻿// <copyright file="ReflectionHelperTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;
    using WyattSoftware.Modules.Core.Tests.Models;

    /// <summary>
    /// Unit tests for the <c>EmailRedirector</c> class.
    /// </summary>
    public class ReflectionHelperTests
    {
        /// <summary>
        /// Tests that implementations are found.
        /// </summary>
        [Test]
        public void GetAllImplementationsOf_ShouldFindImplementations_WhenGivenAnInterface()
        {
            var implementations = ReflectionHelper.GetAllImplementationsOf(typeof(ITestClass));
            implementations.Count.ShouldBe(2);
            implementations.ShouldContain(typeof(SimpleTestClass));
            implementations.ShouldContain(typeof(HierarchicalTestClass));
        }
    }
}
