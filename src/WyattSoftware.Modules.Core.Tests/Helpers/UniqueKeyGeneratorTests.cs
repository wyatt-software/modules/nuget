﻿// <copyright file="UniqueKeyGeneratorTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using System.Text.RegularExpressions;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Helpers;

    /// <summary>
    /// Unit tests for the <c>UniqueKeyGenerator</c> class.
    /// </summary>
    public class UniqueKeyGeneratorTests
    {
        /// <summary>
        /// Tests generated key length and valid chars 100 times.
        /// </summary>
        [Test]
        public void Generate_ShouldBeSpecifiedSizeAndChars_WhenGenerated()
        {
            for (var i = 0; i < 100; i++)
            {
                var key = UniqueKeyGenerator.Generate(256, "0123456789abcdefghijklmnopqrstuvwxyz");

                var regex = new Regex(@"[0-9a-z]{256}");
                var match = regex.Match(key);
                match.Success.ShouldBe(true);
            }
        }
    }
}
