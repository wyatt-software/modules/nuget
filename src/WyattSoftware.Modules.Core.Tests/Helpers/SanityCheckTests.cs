﻿// <copyright file="SanityCheckTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using Shouldly;

    /// <summary>
    /// Unit tests to confirm behaviour of system and third party code.
    /// </summary>
    public class SanityCheckTests
    {
        /// <summary>
        /// Test Linq <c>Find()</c>, <c>First()</c> and <c>FirstOrDefault()</c> on a populated and empty arrays and
        /// lists.
        /// </summary>
        [Test]
        public void LinqFirst_ShouldReturnNull_WhenGivenAnEmptyArray()
        {
            var populatedArray = new[] { "A", "B", "C" };
            var emptyArray = Array.Empty<string>();
            var populatedList = new List<string>() { "A", "B", "C" };
            var emptyList = new List<string>();

            // Populated array.
            Array.Find(populatedArray, x => x.Equals("B")).ShouldBe("B");
            populatedArray.First().ShouldBe("A");
            populatedArray.FirstOrDefault().ShouldBe("A");

            // Populated list.
            populatedList.Find(x => x.Equals("B")).ShouldBe("B");
            populatedList.First().ShouldBe("A");
            populatedList.FirstOrDefault().ShouldBe("A");

            // Empty array.
            Array.Find(emptyArray, x => x.Equals("B")).ShouldBe(null);
            Should.Throw(() => emptyArray.First(), typeof(InvalidOperationException));
            emptyArray.FirstOrDefault().ShouldBe(null);

            // Empty list.
            emptyList.Find(x => x.Equals("B")).ShouldBe(null);
            Should.Throw(() => emptyList.First(), typeof(InvalidOperationException));
            emptyList.FirstOrDefault().ShouldBe(null);

            // Empty array, guarded with a null conditional operator.
            Should.Throw(() => emptyArray?.First(), typeof(InvalidOperationException));
            emptyArray?.FirstOrDefault().ShouldBe(null);

            // Empty list, guarded with a null conditional operator.
            Should.Throw(() => emptyList?.First(), typeof(InvalidOperationException));
            emptyList?.FirstOrDefault().ShouldBe(null);
        }
    }
}