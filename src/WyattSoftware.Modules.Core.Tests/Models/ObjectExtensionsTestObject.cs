﻿// <copyright file="ObjectExtensionsTestObject.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Models
{
    using System;

    /// <summary>
    /// POCO to use in unit tests.
    /// </summary>
    public class ObjectExtensionsTestObject
    {
        /// <summary>
        /// Gets or sets a test integer parameter.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a test string parameter.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets a test DateTime parameter.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Gets or sets a test property with a multiple-word PascalCase name.
        /// </summary>
        public string MultiWordProperty { get; set; }

        /// <summary>
        /// Gets or sets a test property with only one character.
        /// </summary>
        public string X { get; set; }
    }
}
