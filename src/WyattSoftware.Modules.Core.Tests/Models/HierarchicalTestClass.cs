﻿// <copyright file="HierarchicalTestClass.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Models
{
    /// <summary>
    /// Represents an element in a hierarchy (for testing).
    /// </summary>
    internal class HierarchicalTestClass : ITestClass
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchicalTestClass"/> class.
        /// </summary>
        /// <param name="id">Primary key.</param>
        /// <param name="parentId">
        /// Foreign key to another test element. Set to <c>null</c> if this is the root element.
        /// </param>
        public HierarchicalTestClass(int id, int? parentId)
        {
            this.Id = id;
            this.ParentId = parentId;
        }

        /// <inheritdoc/>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the foreign key to another test element. Set to <c>null</c> if this is the root element.
        /// </summary>
        public int? ParentId { get; set; }
    }
}
