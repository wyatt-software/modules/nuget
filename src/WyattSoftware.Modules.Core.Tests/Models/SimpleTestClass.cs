﻿// <copyright file="SimpleTestClass.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Models
{
    /// <summary>
    /// Represents an element in a hierarchy (for testing).
    /// </summary>
    internal class SimpleTestClass : ITestClass
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleTestClass"/> class.
        /// </summary>
        /// <param name="id">Primary key.</param>
        public SimpleTestClass(int id)
        {
            this.Id = id;
        }

        /// <inheritdoc/>
        public int Id { get; set; }
    }
}
