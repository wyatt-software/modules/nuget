﻿// <copyright file="ITestClass.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Models
{
    /// <summary>
    /// Defines a test interface.
    /// </summary>
    internal interface ITestClass
    {
        /// <summary>
        /// Gets or sets the primary key for this element.
        /// </summary>
        int Id { get; set; }
    }
}
