﻿// <copyright file="JObjectTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.ModelTests
{
    using Newtonsoft.Json.Linq;
    using NUnit.Framework;
    using Shouldly;

    /// <summary>
    /// Unit tests for the <c>JObject</c> model.
    /// </summary>
    /// <remarks>These aren't really neccessary, but were written as a sanity-check.</remarks>
    public class JObjectTests
    {
        /// <summary>
        /// Test that JObjects can be constructed/parsed from JSON strings.
        /// </summary>
        [Test]
        public void JObject_ShouldParse_WhenGivenASimpleJsonString()
        {
            var jobject = JObject.Parse(@"{
  ""imageId"": 1,
  ""location"": ""Gold Coast, Australia"",
  ""signature"": "":smiley:"",
  ""emailNewMassages"": true,
  ""emailNewPosts"": false
}");
            var imageId = (int)jobject["imageId"];
            var location = (string)jobject["location"];
            var signature = (string)jobject["signature"];
            var emailNewMassages = (bool)jobject["emailNewMassages"];
            var emailNewPosts = (bool)jobject["emailNewPosts"];

            imageId.ShouldBe(1);
            location.ShouldBe("Gold Coast, Australia");
            signature.ShouldBe(":smiley:");
            emailNewMassages.ShouldBe(true);
            emailNewPosts.ShouldBe(false);
        }

        /// <summary>
        /// Test that JObjects can be constructed/parsed from JSON strings.
        /// </summary>
        [Test]
        public void JObject_ShouldSerialize_WhenSettingRootProperties()
        {
            var jobject = new JObject();
            jobject["imageId"] = 1;
            jobject["location"] = "Gold Coast, Australia";
            jobject["signature"] = ":smiley:";
            jobject["emailNewMassages"] = true;
            jobject["emailNewPosts"] = false;

            var json = jobject.ToString();
            json.ShouldBe(@"{
  ""imageId"": 1,
  ""location"": ""Gold Coast, Australia"",
  ""signature"": "":smiley:"",
  ""emailNewMassages"": true,
  ""emailNewPosts"": false
}");
        }
    }
}
