﻿// <copyright file="ZeptoMailRequestBodyDtoTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Static
{
    using System;
    using System.IO;
    using System.Net.Mail;
    using System.Text;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.DTOs.ZeptoMail;

    /// <summary>
    /// Unit tests for the <c>ZeptoMailRequestBodyDto</c> model.
    /// </summary>
    public class ZeptoMailRequestBodyDtoTests
    {
        /// <summary>
        /// Tests constructing from a null bounce address.
        /// </summary>
        [Test]
        public void Constructor_ShouldThrowException_WhenGivenANullOrEmptyBounceAddress()
        {
            var mailMessage = new MailMessage();
            Should.Throw<ArgumentException>(() => new RequestBodyDto(string.Empty, mailMessage));
        }

        /// <summary>
        /// Tests constructing from a null MailMessage.
        /// </summary>
        [Test]
        public void Constructor_ShouldThrowException_WhenGivenANullMailMessage()
        {
            Should.Throw<ArgumentException>(() => new RequestBodyDto("bounces@info.zylker.com", null));
        }

        /// <summary>
        /// Tests constructing from an empty MailMessage.
        /// </summary>
        [Test]
        public void Constructor_ShouldConstructEmptyModel_WhenGivenAnEmptyMailMessage()
        {
            var mailMessage = new MailMessage();
            var zeptoMailRequestBodyDto = new RequestBodyDto("bounces@info.zylker.com", mailMessage);

            zeptoMailRequestBodyDto.BounceAddress.ShouldBe("bounces@info.zylker.com");
            zeptoMailRequestBodyDto.From.ShouldBeNull();
            zeptoMailRequestBodyDto.To.ShouldBeNull();
            zeptoMailRequestBodyDto.ReplyTo.ShouldBeNull();
            zeptoMailRequestBodyDto.Subject.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.TextBody.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.HtmlBody.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.CC.ShouldBeNull();
            zeptoMailRequestBodyDto.Bcc.ShouldBeNull();
            zeptoMailRequestBodyDto.TrackClicks.ShouldBeNull();
            zeptoMailRequestBodyDto.TrackOpens.ShouldBeNull();
            zeptoMailRequestBodyDto.ClientReference.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.MimeHeaders.ShouldBeNull();
            zeptoMailRequestBodyDto.Attachments.ShouldBeNull();
            zeptoMailRequestBodyDto.InlineImages.ShouldBeNull();
        }

        /// <summary>
        /// Tests constructing from an empty MailMessage.
        /// </summary>
        [Test]
        public void Constructor_ShouldConstructValidModel_WhenGivenAValidMailMessage()
        {
            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("accounts@info.zylker.com", "Accounts");
            mailMessage.To.Add(new MailAddress("rudra.d@zylker.com", "Rudra"));
            mailMessage.ReplyToList.Add(new MailAddress("paula.x@info.zylker.com", "Paula"));

            mailMessage.IsBodyHtml = true;
            mailMessage.Body =
                "<html>" +
                "<body>" +
                "Hi, Verify your account to start receiving reports" +
                "<img src=\"cid:img-welcome-design\">scdjsncj<img src=\"cid:img-CTA\">" +
                "<h1><a href=\"http://www.zylker.com/cache-bin\">Verify Account</a></h1>" +
                "</body>" +
                "</html>";

            mailMessage.Subject = "Account Confirmation";

            mailMessage.CC.Add(new MailAddress("rebecca@zylker.com", "Rebecca"));
            mailMessage.Bcc.Add(new MailAddress("helen@zylker.com", "Helen"));
            mailMessage.Headers.Add("X-Zylker-User", "rebecca-72893");

            mailMessage.Attachments.Add(
                new Attachment(
                    new MemoryStream(Encoding.ASCII.GetBytes("Hello there!")),
                    "DM-welcome-guide",
                    "text/plain"));

            var zeptoMailRequestBodyDto = new RequestBodyDto("bounces@info.zylker.com", mailMessage);

            zeptoMailRequestBodyDto.BounceAddress.ShouldBe("bounces@info.zylker.com");
            zeptoMailRequestBodyDto.From.Address.ShouldBe("accounts@info.zylker.com");
            zeptoMailRequestBodyDto.From.Name.ShouldBe("Accounts");
            zeptoMailRequestBodyDto.To.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.To[0].EmailAddress.Address.ShouldBe("rudra.d@zylker.com");
            zeptoMailRequestBodyDto.To[0].EmailAddress.Name.ShouldBe("Rudra");
            zeptoMailRequestBodyDto.ReplyTo.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.ReplyTo[0].Address.ShouldBe("paula.x@info.zylker.com");
            zeptoMailRequestBodyDto.ReplyTo[0].Name.ShouldBe("Paula");
            zeptoMailRequestBodyDto.Subject.ShouldBe("Account Confirmation");
            zeptoMailRequestBodyDto.TextBody.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.HtmlBody.ShouldContain("<html>");
            zeptoMailRequestBodyDto.HtmlBody.ShouldContain("Verify your account to start receiving reports");
            zeptoMailRequestBodyDto.CC.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.CC[0].Address.ShouldBe("rebecca@zylker.com");
            zeptoMailRequestBodyDto.CC[0].Name.ShouldBe("Rebecca");
            zeptoMailRequestBodyDto.Bcc.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.Bcc[0].Address.ShouldBe("helen@zylker.com");
            zeptoMailRequestBodyDto.Bcc[0].Name.ShouldBe("Helen");
            zeptoMailRequestBodyDto.TrackClicks.ShouldBeNull();
            zeptoMailRequestBodyDto.TrackOpens.ShouldBeNull();
            zeptoMailRequestBodyDto.ClientReference.ShouldBeNullOrEmpty();
            zeptoMailRequestBodyDto.MimeHeaders.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.MimeHeaders["X-Zylker-User"].ShouldBe("rebecca-72893");
            zeptoMailRequestBodyDto.MimeHeaders.ShouldNotBeNull();
            zeptoMailRequestBodyDto.Attachments.Count.ShouldBe(1);
            zeptoMailRequestBodyDto.Attachments[0].Content.ShouldBe("SGVsbG8gdGhlcmUh"); // https://www.base64encode.org/
            zeptoMailRequestBodyDto.Attachments[0].Name.ShouldBe("DM-welcome-guide");
            zeptoMailRequestBodyDto.Attachments[0].MimeType.ShouldBe("text/plain");
            zeptoMailRequestBodyDto.InlineImages.ShouldBeNull();
        }
    }
}
