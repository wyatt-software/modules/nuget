﻿// <copyright file="TestEnum.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Enums
{
    /// <summary>
    /// A test enum for the EnumHelper unit tests.
    /// </summary>
    public enum TestEnum
    {
        /// <summary>
        /// First dummy value.
        /// </summary>
        ValueOne = 1,

        /// <summary>
        /// Second dummy value.
        /// </summary>
        ValueTwo = 2,
    }
}
