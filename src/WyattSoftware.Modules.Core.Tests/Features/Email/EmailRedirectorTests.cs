﻿// <copyright file="EmailRedirectorTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Features.Email
{
    using System.Net.Mail;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Features.Email;

    /// <summary>
    /// Unit tests for the <c>EmailRedirector</c> class.
    /// </summary>
    public class EmailRedirectorTests
    {
        /// <summary>
        /// Tests the ideal simple case.
        /// </summary>
        [Test]
        public void Redirect_ShouldRedirect_WhenGivenTheIdealSimpleCase()
        {
            var mailMessage = new MailMessage();
            mailMessage.Subject = "Redirect_ShouldRedirect_WhenGivenTheIdealSimpleCase";
            mailMessage.To.Add(new MailAddress("original@mailinator.com"));

            mailMessage = EmailRedirector.Redirect(mailMessage, new MailAddress("redirected@wyatt-software.com"));

            mailMessage.Subject.ShouldStartWith("REDIRECTED");
            mailMessage.To.Count.ShouldBe(1);
            mailMessage.To[0].Address.ShouldBe("redirected@wyatt-software.com");
            mailMessage.CC.ShouldBeEmpty();
            mailMessage.Bcc.ShouldBeEmpty();
        }

        /// <summary>
        /// Tests the ideal simple case.
        /// </summary>
        [Test]
        public void Redirect_ShouldRedirect_WhenAllRecipientListsArePopulated()
        {
            var mailMessage = new MailMessage();
            mailMessage.Subject = "Redirect_ShouldRedirect_WhenAllRecipientListsArePopulated";
            mailMessage.To.Add(new MailAddress("original.to.1@mailinator.com", "original.to.1"));
            mailMessage.To.Add(new MailAddress("original.to.2@mailinator.com", "original.to.2"));
            mailMessage.CC.Add(new MailAddress("original.cc.1@mailinator.com", "original.cc.1"));
            mailMessage.CC.Add(new MailAddress("original.cc.2@mailinator.com", "original.cc.2"));
            mailMessage.Bcc.Add(new MailAddress("original.bcc.1@mailinator.com", "original.bcc.1"));
            mailMessage.Bcc.Add(new MailAddress("original.bcc.2@mailinator.com", "original.bcc.2"));

            mailMessage = EmailRedirector.Redirect(mailMessage, new MailAddress("redirected@wyatt-software.com"));

            mailMessage.Subject.ShouldStartWith("REDIRECTED");
            mailMessage.To.Count.ShouldBe(1);
            mailMessage.To[0].Address.ShouldBe("redirected@wyatt-software.com");
            mailMessage.CC.ShouldBeEmpty();
            mailMessage.Bcc.ShouldBeEmpty();
        }

        /// <summary>
        /// Tests the ideal simple case.
        /// </summary>
        [Test]
        public void Redirect_ShouldRedirectWithoutChangingTheSubject_WhenRedirectAddressIsTheSame()
        {
            var mailMessage = new MailMessage();
            mailMessage.Subject = "Redirect_ShouldRedirectWithoutChangingTheSubject_WhenRedirectAddressIsTheSame";
            mailMessage.To.Add(new MailAddress("original.to.1@mailinator.com", "original.to.1"));
            mailMessage.To.Add(new MailAddress("original.to.2@mailinator.com", "original.to.2"));
            mailMessage.CC.Add(new MailAddress("original.cc.1@mailinator.com", "original.cc.1"));
            mailMessage.CC.Add(new MailAddress("original.cc.2@mailinator.com", "original.cc.2"));
            mailMessage.Bcc.Add(new MailAddress("original.bcc.1@mailinator.com", "original.bcc.1"));
            mailMessage.Bcc.Add(new MailAddress("original.bcc.2@mailinator.com", "original.bcc.2"));

            mailMessage = EmailRedirector.Redirect(mailMessage, new MailAddress("original.to.1@mailinator.com"));

            mailMessage.Subject.ShouldNotStartWith("REDIRECTED");
            mailMessage.To.Count.ShouldBe(1);
            mailMessage.To[0].Address.ShouldBe("original.to.1@mailinator.com");
            mailMessage.CC.ShouldBeEmpty();
            mailMessage.Bcc.ShouldBeEmpty();
        }
    }
}
