// <copyright file="FilenameConverterTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Core.Tests.Features.Storage
{
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Features.Storage;

    /// <summary>
    /// Unit tests for the <c>FilenameConverter</c> class.
    /// </summary>
    public class FilenameConverterTests
    {
        /// <summary>
        /// Tests valid filenames.
        /// </summary>
        [Test]
        public void SafeFilename_ShouldDoNothing_WhenGivenAValidFilename()
        {
            var filename = "boring.txt";
            var safeFilename = FilenameConverter.SafeFilename(filename);
            safeFilename.ShouldBe(filename);
        }

        /// <summary>
        /// Tests valid filenames.
        /// </summary>
        [Test]
        public void SafeFilename_ShouldChangeInvalidCharacters_WhenGivenAInvalidFilename()
        {
            var filename = "will this file-name pass?";
            var safeFilename = FilenameConverter.SafeFilename(filename);
            safeFilename.ShouldBe("will_this_file-name_pass_");
        }
    }
}
