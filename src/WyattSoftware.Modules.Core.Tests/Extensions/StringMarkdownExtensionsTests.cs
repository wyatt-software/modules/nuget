﻿// <copyright file="StringMarkdownExtensionsTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Extensions
{
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Extensions;

    /// <summary>
    /// Unit tests for the <c>StringMarkdownExtensions</c> class.
    /// </summary>
    public class StringMarkdownExtensionsTests
    {
        /// <summary>
        /// Tests stripping markdown from a string.
        /// </summary>
        [Test]
        public void ToPlainText_ShouldConvertToPlainText_WhenGivenMarkdown()
        {
            var markdown = "**bold** _italic_";
            var plainText = markdown.ToPlainText();
            plainText.ShouldBe("bold italic");
        }

        /// <summary>
        /// Tests stripping HTML from a string.
        /// </summary>
        /// <remarks>Notice how HTML entities (like <![CDATA["&nbsp;"]]>) are converted to unicode.</remarks>
        [Test]
        public void ToPlainText_ShouldConvertToPlainText_WhenGivenHtml()
        {
            var html = "<b>bold<b>&nbsp;<i>italic</i><!-- Test Comment -->";
            var plainText = html.ToPlainText();
            plainText.ShouldBe("bold\u00a0italic");
        }
    }
}
