﻿// <copyright file="StreamExtensionsTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Extensions
{
    using System.IO;
    using System.Text;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Extensions;

    /// <summary>
    /// Unit tests for the <c>StreamExtensions</c> class.
    /// </summary>
    public class StreamExtensionsTests
    {
        /// <summary>
        /// Tests converting a stream to a byte array.
        /// </summary>
        [Test]
        public void ToByteArray_ShouldConvert_WhenGivenAValidStream()
        {
            var inputString = "Oh, hi!";
            var inputByteArray = Encoding.ASCII.GetBytes(inputString);

            var stream = new MemoryStream(inputByteArray);
            var outputByteArray = stream.ToByteArray();
            outputByteArray.Length.ShouldBe(7);
            Assert.AreEqual(outputByteArray[0], (byte)'O');
            Assert.AreEqual(outputByteArray[1], (byte)'h');
            Assert.AreEqual(outputByteArray[2], (byte)',');
            Assert.AreEqual(outputByteArray[3], (byte)' ');
            Assert.AreEqual(outputByteArray[4], (byte)'h');
            Assert.AreEqual(outputByteArray[5], (byte)'i');
            Assert.AreEqual(outputByteArray[6], (byte)'!');
        }

        /// <summary>
        /// Tests converting a stream to a byte array.
        /// </summary>
        [Test]
        public void ToByteArray_ShouldReturnEmptyByteArray_WhenGivenAnEmptyStream()
        {
            var stream = new MemoryStream();
            var outputByteArray = stream.ToByteArray();
            outputByteArray.Length.ShouldBe(0);
        }
    }
}
