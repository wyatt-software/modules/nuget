// <copyright file="ObjectExtensionsTests.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>
namespace WyattSoftware.Modules.Core.Tests.Extensions
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Shouldly;
    using WyattSoftware.Modules.Core.BLL.Extensions;
    using WyattSoftware.Modules.Core.Tests.Models;

    /// <summary>
    /// Unit tests for the <c>ObjectExtensions</c> class.
    /// </summary>
    public class ObjectExtensionsTests
    {
        /// <summary>
        /// Valid values.
        /// </summary>
        [Test]
        public void ToDictionary_ShouldConvertObjectToDictionary_WhenGivenAnObject()
        {
            var data = new ObjectExtensionsTestObject
            {
                Id = 1234,
                Name = "Test object",
                Created = new DateTime(2021, 9, 15),
                MultiWordProperty = "Test value",
                X = "Y",
            };

            var output = data.ToDictionary();
            output.Count.ShouldBe(5);

            output["Id"].ShouldBe(1234);
            output["Name"].ShouldBe("Test object");

            var created = (DateTime)output["Created"];
            created.Year.ShouldBe(2021);
            created.Month.ShouldBe(9);
            created.Day.ShouldBe(15);

            output["MultiWordProperty"].ShouldBe("Test value");
            output["X"].ShouldBe("Y");
        }

        /// <summary>
        /// Valid values, converted to camel case.
        /// </summary>
        [Test]
        public void ToDictionary_ShouldConvertObjectToCamelCaseDictionary_WhenGivenAnObject()
        {
            var data = new ObjectExtensionsTestObject
            {
                Id = 1234,
                Name = "Test object",
                Created = new DateTime(2021, 9, 15),
                MultiWordProperty = "Test value",
                X = "Y",
            };

            var output = data.ToDictionary(camelCase: true);
            output.Count.ShouldBe(5);

            output["id"].ShouldBe(1234);
            output["name"].ShouldBe("Test object");

            var created = (DateTime)output["created"];
            created.Year.ShouldBe(2021);
            created.Month.ShouldBe(9);
            created.Day.ShouldBe(15);

            output["multiWordProperty"].ShouldBe("Test value");
            output["x"].ShouldBe("Y");
        }
    }
}
