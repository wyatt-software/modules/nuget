﻿// <copyright file="MessagesController.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.API.Controllers;

using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.API.Controllers;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Identity.API.DTOs;
using WyattSoftware.Modules.Identity.BLL.Context;
using WyattSoftware.Modules.Identity.BLL.Services;
using WyattSoftware.Modules.Messaging.API.DTOs;
using WyattSoftware.Modules.Messaging.BLL.Factories;
using WyattSoftware.Modules.Messaging.BLL.Models;
using WyattSoftware.Modules.Messaging.BLL.Services;

/// <summary>
/// Provides personal message (PM) functionality to authenticated users.
/// </summary>
[Route("messaging/messages")]
public class MessagesController : WsControllerBase
{
    private readonly ICurrentSession currentSession;
    private readonly IMessageFactory messageFactory;
    private readonly IMessageService messageService;
    private readonly IUserService userService;

    /// <summary>
    /// Initializes a new instance of the <see cref="MessagesController"/> class.
    /// </summary>
    /// <param name="currentSession">Injected ICurrentSession.</param>
    /// <param name="messageFactory">Injected IMessageFactory.</param>
    /// <param name="messageService">Injected IMessageService.</param>
    /// <param name="userService">Injected IUserService.</param>
    public MessagesController(
        ICurrentSession currentSession,
        IMessageFactory messageFactory,
        IMessageService messageService,
        IUserService userService)
    {
        this.currentSession = currentSession;
        this.messageFactory = messageFactory;
        this.messageService = messageService;
        this.userService = userService;
    }

    /// <summary>
    /// Maps the data entity to the response object.
    /// </summary>
    public static void InitMapping()
    {
        Mapper.AddMap<Message?, MessageResponse?>(message =>
        {
            if (message == null)
            {
                return null;
            }

            var response = new MessageResponse();
            response.InjectFrom(message);

            response.ToUser = message.ToUser != null
                ? Mapper.Map<UserResponse?>(message.ToUser)
                : null;

            response.FromUser = message.FromUser != null
                ? Mapper.Map<UserResponse?>(message.FromUser)
                : null;

            return response;
        });

        Mapper.AddMap<PaginatedResponse<Message>, PaginatedResponse<MessageResponse>>(messages =>
            new PaginatedResponse<MessageResponse>()
            {
                Total = messages.Total,
                Offset = messages.Offset,
                Limit = messages.Limit,
                Results = messages.Results.Select(x => Mapper.Map<MessageResponse>(x)).ToList(),
            });
    }

    /// <summary>
    /// Get the last messages from conversations with a given user.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user] .</para>
    /// </param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <returns>The first message of each conversation.</returns>
    [HttpHead]
    [HttpGet]
    [Route("")]
    [Authorize]
    public async Task<IActionResult> FetchConversations(string expand = "", int offset = 0, int limit = 20)
    {
        var messages = await this.messageService.FetchConversationsAsync(
            this.currentSession.UserId, expand, offset, limit);

        var response = Mapper.Map<PaginatedResponse<MessageResponse>>(messages);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Get the messages in the conversation between two users.
    /// </summary>
    /// <param name="username">The 'to' user.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user] .</para>
    /// </param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <returns>The messages.</returns>
    [HttpHead]
    [HttpGet]
    [Route("{username}")]
    [Authorize]
    public async Task<IActionResult> FetchMessages(
        string username, string expand = "", int offset = -1, int limit = 20)
    {
        var messages = await this.messageService.FetchMessagesAsync(
            this.currentSession.UserId, username, expand, offset, limit);

        var response = Mapper.Map<PaginatedResponse<MessageResponse>>(messages);
        return this.ResultsWithPaginationHeaders(response);
    }

    /// <summary>
    /// Sends a message from the currently authenticated user to another specified user.
    /// </summary>
    /// <param name="username">The user to send the message to.</param>
    /// <param name="request">The request object.</param>
    /// <returns>An HTTP action result: 200 400, 401, or 404.</returns>
    [HttpPost]
    [Route("{username}")]
    [Authorize]
    public async Task<IActionResult> Create(string username, [FromBody] MessageCreateRequest request)
    {
        var toUser = await this.userService.ReadAsync(username);

        var message = this.messageFactory.New(
            this.currentSession.UserId,
            toUser.Id,
            request.Body);

        await message.CreateAsync();
        await message.ExpandAsync("all");

        var response = Mapper.Map<MessageResponse>(message);
        return this.Ok(response);
    }
}
