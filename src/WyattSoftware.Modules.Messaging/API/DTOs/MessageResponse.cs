﻿// <copyright file="MessageResponse.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.API.DTOs;

using System;
using WyattSoftware.Modules.Identity.API.DTOs;

/// <summary>
/// API response mapped from the <see cref="Message" /> model.
/// </summary>
public class MessageResponse
{
    /// <summary>
    /// Gets or sets the primary key.
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the foreign key to the user who sent the message.
    /// </summary>
    public int FromUserId { get; set; }

    /// <summary>
    /// Gets or sets the foreign key to the user who should receive the message.
    /// </summary>
    public int ToUserId { get; set; }

    /// <summary>
    /// Gets or sets the date the message was create.
    /// </summary>
    public DateTime Created { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the message was read.
    /// </summary>
    public bool Read { get; set; }

    /// <summary>
    /// Gets or sets the body of the message.
    /// </summary>
    public string Body { get; set; } = string.Empty;

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Reference expansion

    /// <summary>
    /// Gets or sets the user the message was sent from.
    /// </summary>
    /// <remarks>
    /// Use "expand=fromUser".
    /// </remarks>
    public UserResponse? FromUser { get; set; }

    /// <summary>
    /// Gets or sets the user the message was sent to.
    /// </summary>
    /// <remarks>
    /// Use "expand=toUser".
    /// </remarks>
    public UserResponse? ToUser { get; set; }
}
