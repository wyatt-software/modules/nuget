﻿// <copyright file="MessageCreateRequest.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.API.DTOs;

/// <summary>
/// Create message request.
/// </summary>
public class MessageCreateRequest
{
    /// <summary>
    /// Gets or sets the message being sent.
    /// </summary>
    public string Body { get; set; } = string.Empty;
}
