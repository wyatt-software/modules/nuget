﻿// <copyright file="MessageEntity.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.DAL.Entities;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WyattSoftware.Modules.Core.DAL.Entities;

/// <summary>
/// Community message entity.
/// </summary>
[Table("messaging_message")]
public class MessageEntity : BaseEntity, IEntity
{
    /// <summary>
    /// Gets or sets the foreign key to the user who sent the message.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("from_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int FromUserId { get; set; }

    /// <summary>
    /// Gets or sets the foreign key to the user who should receive the message.
    /// </summary>
    [ForeignKey("identity_user.id")]
    [Column("to_user_id")]
    [Required]
    [Range(1, int.MaxValue)]
    public int ToUserId { get; set; }

    /// <summary>
    /// Gets or sets the body of the message.
    /// </summary>
    [Column("body")]
    [StringLength(16777215)] // MySQL mediumtext
    public string Body { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets a value indicating whether the message was read.
    /// </summary>
    [Column("read")]
    public bool Read { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the message was deleted from the recipient's Inbox.
    /// </summary>
    /// <remarks>No longer used, since we're using an SMS-style conversation format, rather than email-style.</remarks>
    [Column("deleted_from_inbox")]
    public bool DeletedFromInbox { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the message was deleted from the sender's Outbox.
    /// </summary>
    /// <remarks>No longer used, since we're using an SMS-style conversation format, rather than email-style.</remarks>
    [Column("deleted_from_outbox")]
    public bool DeletedFromOutbox { get; set; }
}
