﻿// <copyright file="MessageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.DAL.Repositories;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SqlKata;
using SqlKata.Execution;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Core.Options;
using WyattSoftware.Modules.Messaging.DAL.Entities;

/// <summary>
/// Dapper implementation of the community message repository.
/// </summary>
public class MessageRepository : BaseRepository<MessageEntity>, IMessageRepository
{
    /// <summary>
    /// Initializes a new instance of the <see cref="MessageRepository"/> class.
    /// </summary>
    /// <param name="coreOptions">Injected IOptions{CoreOptions}.</param>
    public MessageRepository(
        IOptions<CoreOptions> coreOptions)
        : base(coreOptions)
    {
    }

    /// <inheritdoc/>
    public async Task<int> GetConversationCountAsync(int userId)
    {
        var query = this.GetConversationsQuery(userId).AsCount();
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<int> GetMessageCountAsync(int firstUserId, int secondUserId)
    {
        var query = this.GetConversationQuery(firstUserId, secondUserId).AsCount();
        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<int> GetUnreadMessageCountAsync(int toUserId)
    {
        var query = this.Db.Query(this.Table)
            .Where("to_user_id", toUserId)
            .Where("read", false)
            .Where("deleted_from_inbox", false)
            .AsCount();

        return await this.Db.ExecuteScalarAsync<int>(query);
    }

    /// <inheritdoc/>
    public async Task<List<MessageEntity>> FetchConversationsAsync(int userId, int offset = 0, int limit = 20)
    {
        var results = await this.GetConversationsQuery(userId)
            .OrderByDesc("id")
            .Offset(offset)
            .Limit(limit)
            .GetAsync<MessageEntity>();

        return results.ToList();
    }

    /// <inheritdoc/>
    public async Task<List<MessageEntity>> FetchMessagesAsync(
        int toUserId, int fromUserId, int offset = 0, int limit = 20)
    {
        var results = await this.GetConversationQuery(toUserId, fromUserId)
            .OrderBy("id")
            .Offset(offset)
            .Limit(limit)
            .GetAsync<MessageEntity>();

        return results.ToList();
    }

    /// <summary>
    /// Get the query for messages in the conversation between two users.
    /// </summary>
    /// <param name="toUserId">The 'to' user.</param>
    /// <param name="fromUserId">The 'from' user.</param>
    private Query GetConversationQuery(int toUserId, int fromUserId)
    {
        return this.Db.Query(this.Table)
            .Where(x => x
                .Where(x1 => x1
                    .Where("from_user_id", fromUserId)
                    .Where("to_user_id", toUserId)
                    .Where("deleted_from_inbox", false))
                .OrWhere(x2 => x2
                    .Where("from_user_id", toUserId)
                    .Where("to_user_id", fromUserId)
                    .Where("deleted_from_outbox", false)));
    }

    /// <summary>
    /// Get the last messages from conversations with a given user.
    /// </summary>
    /// <param name="userId">The user to fetch conversations for.</param>
    private Query GetConversationsQuery(int userId)
    {
        const string fromSql =
            @"messaging_message MM1
                LEFT JOIN messaging_message MM2 ON
                    (MM1.from_user_id = MM2.from_user_id OR MM1.to_user_id = MM2.from_user_id)
                    AND (MM1.to_user_id = MM2.to_user_id OR MM1.from_user_id = MM2.to_user_id)
                    AND MM1.id < MM2.id";

        const string whereSql =
            @"MM2.id IS NULL
                AND ((MM1.from_user_id = ? AND MM1.deleted_from_outbox = 0)
	            OR (MM1.to_user_id = ? AND MM1.deleted_from_inbox = 0))";

        return this.Db.Query()
            .SelectRaw("MM1.*")
            .FromRaw(fromSql)
            .WhereRaw(whereSql, userId, userId);
    }
}
