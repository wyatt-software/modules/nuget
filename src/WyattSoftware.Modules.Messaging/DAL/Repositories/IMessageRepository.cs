﻿// <copyright file="IMessageRepository.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.DAL.Repositories;

using System.Collections.Generic;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.DAL.Repositories;
using WyattSoftware.Modules.Messaging.DAL.Entities;

/// <summary>
/// Interface defining custom methods in the community message repository.
/// </summary>
public interface IMessageRepository : IRepository<MessageEntity>
{
    /// <summary>
    /// Gets the number of conversations between this user and others.
    /// </summary>
    /// <param name="userId">The user to count conversations for.</param>
    /// <returns>The number of conversations.</returns>
    Task<int> GetConversationCountAsync(int userId);

    /// <summary>
    /// Gets the number of message between this user and another.
    /// </summary>
    /// <param name="firstUserId">The first user.</param>
    /// <param name="secondUserId">The second user.</param>
    /// <returns>The number of messages.</returns>
    Task<int> GetMessageCountAsync(int firstUserId, int secondUserId);

    /// <summary>
    /// Gets the number of message the user has that aren't read yet (that aren't deleted).
    /// </summary>
    /// <param name="toUserId">The user to count unread messages for.</param>
    /// <returns>The number of messages.</returns>
    Task<int> GetUnreadMessageCountAsync(int toUserId);

    /// <summary>
    /// Get the last messages from conversations with a given user.
    /// </summary>
    /// <param name="userId">The user to fetch conversations for.</param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The number of records to return, at most.</param>
    /// <returns>The first message of each conversation.</returns>
    Task<List<MessageEntity>> FetchConversationsAsync(int userId, int offset = 0, int limit = 20);

    /// <summary>
    /// Get the messages in the conversation between two users.
    /// </summary>
    /// <param name="firstUserId">The first user.</param>
    /// <param name="secondUserId">The second user.</param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <returns>The messages.</returns>
    Task<List<MessageEntity>> FetchMessagesAsync(int firstUserId, int secondUserId, int offset = 0, int limit = 20);
}
