﻿// <copyright file="Message.Validation.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Models;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

/// <summary>
/// Community message domain model.
/// </summary>
public partial class Message
{
    /// <summary>
    /// Throws exceptions if there's any invalid data.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ValidateAsync()
    {
        if (string.IsNullOrEmpty(this.Body))
        {
            throw new ArgumentException($"Body is required.");
        }

        var fromUserExists = await this.userRepository.ExistsAsync(this.FromUserId);
        if (!fromUserExists)
        {
            throw new KeyNotFoundException($"\"From\" user {this.FromUserId} not found.");
        }

        var toUserExists = await this.userRepository.ExistsAsync(this.ToUserId);
        if (!toUserExists)
        {
            throw new KeyNotFoundException($"\"To\" user {this.ToUserId} not found.");
        }
    }
}
