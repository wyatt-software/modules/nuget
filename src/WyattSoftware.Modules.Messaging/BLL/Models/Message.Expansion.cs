﻿// <copyright file="Message.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Models;

using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Entities;

/// <summary>
/// Community message domain model.
/// </summary>
public partial class Message
{
    /// <summary>
    /// Gets or sets the user the message was sent to.
    /// </summary>
    /// <remarks>
    /// Use "expand=touser".
    /// </remarks>
    public User? ToUser { get; set; }

    /// <summary>
    /// Gets or sets the user the message was sent from.
    /// </summary>
    /// <remarks>
    /// Use "expand=fromuser".
    /// </remarks>
    public User? FromUser { get; set; }

    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|users] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The mutated message collection, to make it explicit.</returns>
    public async Task<Message> ExpandAsync(string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return this;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                case "users":
                    await this.ExpandUserAsync();
                    break;
            }
        }

        return this;
    }

    /// <summary>
    /// Get the from and to users mentioned in the response in a single SQL query.
    /// </summary>
    /// <returns>Nothing.</returns>
    private async Task ExpandUserAsync()
    {
        var userIds = new[] { this.FromUserId, this.ToUserId };
        var userEntities = await this.userRepository.FetchAsync(userIds);
        if (!userEntities.Any())
        {
            return;
        }

        var fromUserEntity = userEntities.Where(x => x.Id == this.FromUserId)?.FirstOrDefault();
        var toUserEntity = userEntities.Where(x => x.Id == this.ToUserId)?.FirstOrDefault();

        var fromUserImageId = (int?)fromUserEntity?.Properties["imageId"] ?? 0;
        var toUserImageId = (int?)toUserEntity?.Properties["imageId"] ?? 0;
        var imageIds = new[] { fromUserImageId, toUserImageId }.Where(x => x > 0).ToArray();
        var imageEntities = await this.imageRepository.FetchAsync(imageIds);

        this.FromUser = this.ExpandUser(fromUserEntity, fromUserImageId, imageEntities);
        this.ToUser = this.ExpandUser(toUserEntity, toUserImageId, imageEntities);
    }

    /// <summary>
    /// Expands the user along with their image, using the pre-fetched image entities.
    /// </summary>
    /// <param name="fromUserEntity">The user entity to expand.</param>
    /// <param name="fromUserImageId">The user's image ID.</param>
    /// <param name="imageEntities">Pre-fetched image entities.</param>
    /// <returns>The user domain model.</returns>
    private User? ExpandUser(UserEntity? fromUserEntity, int fromUserImageId, List<ImageEntity> imageEntities)
    {
        User? user = null;
        if (fromUserEntity != null)
        {
            user = this.userFactory.New(fromUserEntity);

            var fromUserImage = fromUserImageId > 0
                ? imageEntities.Where(x => x.Id == fromUserImageId)?.FirstOrDefault()
                : null;

            if (fromUserImage != null)
            {
                user.NestedResources["image"] = this.imageFactory.New(fromUserImage);
            }
        }

        return user;
    }
}