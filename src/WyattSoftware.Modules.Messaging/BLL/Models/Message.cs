﻿// <copyright file="Message.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Models;

using System.Threading.Tasks;
using Omu.ValueInjecter;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Messaging.DAL.Entities;
using WyattSoftware.Modules.Messaging.DAL.Repositories;

/// <summary>
/// Community message domain model.
/// </summary>
public partial class Message : MessageEntity, IDomainModel
{
    // We're using "fat models", so we have a bunch of dependencies.
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly IMessageRepository messageRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="Message"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="messageRepository">Injected MessageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="fromUserId">The author.</param>
    /// <param name="toUserId">The recipient.</param>
    /// <param name="body">The body.</param>
    /// <remarks>Required properties are validated upon construction.</remarks>
    internal Message(
        IUserRepository userRepository,
        IImageRepository imageRepository,
        IMessageRepository messageRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        int fromUserId,
        int toUserId,
        string body)
    {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.messageRepository = messageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;

        // Trigger validation by setting properties, not backing fields.
        this.FromUserId = fromUserId;
        this.ToUserId = toUserId;
        this.Body = body;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Message"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="messageRepository">Injected MessageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="messageEntity">The entity to construct from.</param>
    /// <remarks>
    /// Models constructed from an entity are not validated. It's assumed they've come from valid data. This allows
    /// us to do validation in the setters without triggering costly validation methods for every model retrieved
    /// from the data access layer.
    /// </remarks>
    internal Message(
        IUserRepository userRepository,
        IImageRepository imageRepository,
        IMessageRepository messageRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        MessageEntity messageEntity)
    {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.messageRepository = messageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;

        this.Id = messageEntity.Id;
        this.Created = messageEntity.Created;
        this.Modified = messageEntity.Modified;

        this.FromUserId = messageEntity.FromUserId;
        this.ToUserId = messageEntity.ToUserId;
        this.Body = messageEntity.Body;
        this.Read = messageEntity.Read;
    }

    /// <summary>
    /// Attempts to create a message and persist it.
    /// </summary>
    /// <returns>Nothing.</returns>
    public async Task CreateAsync()
    {
        var messageEntity = Mapper.Map<MessageEntity>(this);
        await this.ValidateAsync();
        this.Id = await this.messageRepository.CreateAsync(messageEntity);
    }

    /// <inheritdoc />
    public async Task UpdateAsync()
    {
        await this.messageRepository.UpdateAsync(this);
    }

    /// <inheritdoc />
    public async Task DeleteAsync()
    {
        await this.messageRepository.DeleteAsync(this.Id);
    }
}
