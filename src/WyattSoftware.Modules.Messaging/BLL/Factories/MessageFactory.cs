﻿// <copyright file="MessageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Factories;

using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Messaging.BLL.Models;
using WyattSoftware.Modules.Messaging.DAL.Entities;
using WyattSoftware.Modules.Messaging.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IMessageFactory"/> interface.
/// </summary>
public class MessageFactory : IMessageFactory
{
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly IMessageRepository messageRepository;
    private readonly Lazy<IUserFactory> userFactory;
    private readonly Lazy<IImageFactory> imageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="MessageFactory"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="messageRepository">Injected IMessageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    public MessageFactory(
        IUserRepository userRepository,
        IImageRepository imageRepository,
        IMessageRepository messageRepository,
        Lazy<IUserFactory> userFactory,
        Lazy<IImageFactory> imageFactory)
    {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.messageRepository = messageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
    }

    /// <inheritdoc />
    public Message New(int fromUserId, int toUserId, string body)
    {
        return new Message(
            this.userRepository,
            this.imageRepository,
            this.messageRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            fromUserId,
            toUserId,
            body);
    }

    /// <inheritdoc />
    public Message New(MessageEntity messageEntity)
    {
        return new Message(
            this.userRepository,
            this.imageRepository,
            this.messageRepository,
            this.userFactory.Value,
            this.imageFactory.Value,
            messageEntity);
    }
}
