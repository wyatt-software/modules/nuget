﻿// <copyright file="IMessageFactory.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Factories;

using WyattSoftware.Modules.Core.BLL.Factories;
using WyattSoftware.Modules.Messaging.BLL.Models;
using WyattSoftware.Modules.Messaging.DAL.Entities;

/// <summary>
/// Provides a way to create messages without passing all dependencies every time.
/// </summary>
public interface IMessageFactory : IDomainFactory<Message, MessageEntity>
{
    /// <summary>
    /// Attempts to create a message and persist it.
    /// </summary>
    /// <param name="fromUserId">The user to send from.</param>
    /// <param name="toUserId">The user to send to.</param>
    /// <param name="body">The body of the message.</param>
    /// <returns>A new <see cref="Message"/>, injected with the dependencies it needs.</returns>.
    Message New(int fromUserId, int toUserId, string body);

    /// <summary>
    /// Attempts to create a message and persist it.
    /// </summary>
    /// <param name="messageEntity">The entity to construct from.</param>
    /// <returns>
    /// A <see cref="Message"/> based on the <see cref="MessageEntity"/>, injected with the dependencies it needs.
    /// </returns>
    new Message New(MessageEntity messageEntity);
}
