﻿// <copyright file="IMessageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Services;

using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Messaging.BLL.Models;

/// <summary>
/// Business logic for the community messages.
/// </summary>
public interface IMessageService : IDomainService<Message>
{
    /// <summary>
    /// Get the last messages from conversations with a given user.
    /// </summary>
    /// <param name="userId">The user to fetch conversations for.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|user] .</para>
    /// </param>
    /// <param name="offset">The number of records to offset the results by.</param>
    /// <param name="limit">The number of records to return, at most.</param>
    /// <returns>The first message of each conversation.</returns>
    Task<PaginatedResponse<Message>> FetchConversationsAsync(
        int userId, string expand, int offset = 0, int limit = 20);

    /// <summary>
    /// Get the messages in the conversation between two users.
    /// </summary>
    /// <param name="toUserId">The 'to' user.</param>
    /// <param name="fromUsername">The 'from' user.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|users] .</para>
    /// </param>
    /// <param name="offset">Offset the limited results by a number of records.</param>
    /// <param name="limit">Limits the number of records returned.</param>
    /// <returns>The messages.</returns>
    Task<PaginatedResponse<Message>> FetchMessagesAsync(
        int toUserId, string fromUsername, string expand = "", int offset = -1, int limit = 20);

    /// <summary>
    /// Gets the number of message the user has that aren't read yet (that aren't deleted).
    /// </summary>
    /// <param name="toUserId">The user to count unread messages for.</param>
    /// <returns>The number of messages.</returns>
    Task<int> GetUnreadMessageCountAsync(int toUserId);
}
