﻿// <copyright file="MessageService.Expansion.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Services;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Gallery.DAL.Entities;
using WyattSoftware.Modules.Identity.BLL.Models;
using WyattSoftware.Modules.Identity.DAL.Entities;
using WyattSoftware.Modules.Messaging.BLL.Models;

/// <summary>
/// Reference expansion.
/// </summary>
public partial class MessageService
{
    /// <summary>
    /// Only expand the reference objects if requested.
    /// </summary>
    /// <param name="messages">The messages which are mutated by this method.</param>
    /// <param name="expand">
    /// <para>A comma-separated list of child objects that can be expanded.</para>
    /// <para>Accepts: [all|users] .</para>
    /// </param>
    /// <remarks>The default behaviour is to save bandwidth by only returning the foreign key IDs.</remarks>
    /// <returns>The mutated messages collection, to make it explicit.</returns>
    private async Task<List<Message>> ExpandAsync(List<Message> messages, string expand)
    {
        if (string.IsNullOrEmpty(expand))
        {
            return messages;
        }

        foreach (var reference in expand.Split(','))
        {
            switch (reference.ToLower())
            {
                case "all":
                case "users":
                    messages = await this.ExpandUsersAsync(messages);
                    break;
            }
        }

        return messages;
    }

    /// <summary>
    /// Get all from and to users mentioned in the response in a single SQL query.
    /// </summary>
    /// <param name="messages">The messages which are mutated by this method.</param>
    /// <returns>The mutated messages collection, to make it explicit.</returns>
    private async Task<List<Message>> ExpandUsersAsync(List<Message> messages)
    {
        var fromUserIds = messages.Select(x => x.FromUserId);
        var toUserIds = messages.Select(x => x.ToUserId);
        var userIds = fromUserIds.Union(toUserIds).Distinct().ToArray();
        var userEntities = await this.userRepository.FetchAsync(userIds);
        if (!userEntities.Any())
        {
            return messages;
        }

        var imageIds = userEntities
            .Select(x => (int?)x.Properties["imageId"] ?? 0)
            .Where(x => x > 0)
            .ToArray();

        var imageEntities = await this.imageRepository.FetchAsync(imageIds);

        foreach (var message in messages)
        {
            message.FromUser = this.ExpandUser(message.FromUserId, userEntities, imageEntities);
            message.ToUser = this.ExpandUser(message.ToUserId, userEntities, imageEntities);
        }

        return messages;
    }

    /// <summary>
    /// Expands the user with their image.
    /// </summary>
    /// <param name="userId">The id of the user to expand.</param>
    /// <param name="userEntities">Pre-fetched user entities.</param>
    /// <param name="imageEntities">Pre-fetched image entities.</param>
    /// <returns>The User domain model.</returns>
    private User? ExpandUser(int userId, List<UserEntity> userEntities, List<ImageEntity> imageEntities)
    {
        User? user = null;
        var userEntity = userEntities.Where(x => x.Id == userId)?.FirstOrDefault();
        if (userEntity != null)
        {
            user = this.userFactory.New(userEntity);
            var userImageId = (int?)user.Properties["imageId"];
            if (userImageId != null)
            {
                var userImageEntity = imageEntities.Where(x => x.Id == userImageId)?.FirstOrDefault();
                if (userImageEntity != null)
                {
                    user.NestedResources["image"] = this.imageFactory.New(userImageEntity);
                }
            }
        }

        return user;
    }
}