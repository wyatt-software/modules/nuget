﻿// <copyright file="MessageService.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.BLL.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WyattSoftware.Modules.Core.BLL.Models;
using WyattSoftware.Modules.Core.BLL.Services;
using WyattSoftware.Modules.Gallery.BLL.Factories;
using WyattSoftware.Modules.Gallery.DAL.Repositories;
using WyattSoftware.Modules.Identity.BLL.Factories;
using WyattSoftware.Modules.Identity.DAL.Repositories;
using WyattSoftware.Modules.Messaging.BLL.Factories;
using WyattSoftware.Modules.Messaging.BLL.Models;
using WyattSoftware.Modules.Messaging.DAL.Entities;
using WyattSoftware.Modules.Messaging.DAL.Repositories;

/// <summary>
/// Default implementation of the <see cref="IMessageService"/> interface.
/// </summary>
public partial class MessageService : BaseDomainService<Message, MessageEntity>, IMessageService
{
    private readonly IUserRepository userRepository;
    private readonly IImageRepository imageRepository;
    private readonly IMessageRepository messageRepository;
    private readonly IUserFactory userFactory;
    private readonly IImageFactory imageFactory;
    private readonly IMessageFactory messageFactory;

    /// <summary>
    /// Initializes a new instance of the <see cref="MessageService"/> class.
    /// </summary>
    /// <param name="userRepository">Injected IUserRepository.</param>
    /// <param name="imageRepository">Injected IImageRepository.</param>
    /// <param name="messageRepository">Injected IMessageRepository.</param>
    /// <param name="userFactory">Injected IUserFactory.</param>
    /// <param name="imageFactory">Injected IImageFactory.</param>
    /// <param name="messageFactory">Injected IMessageFactory.</param>
    public MessageService(
        IUserRepository userRepository,
        IImageRepository imageRepository,
        IMessageRepository messageRepository,
        IUserFactory userFactory,
        IImageFactory imageFactory,
        IMessageFactory messageFactory)
        : base(messageRepository, messageFactory)
    {
        this.userRepository = userRepository;
        this.imageRepository = imageRepository;
        this.messageRepository = messageRepository;
        this.userFactory = userFactory;
        this.imageFactory = imageFactory;
        this.messageFactory = messageFactory;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Message>> FetchConversationsAsync(
        int userId, string expand = "", int offset = 0, int limit = 20)
    {
        if (userId == 0)
        {
            throw new ArgumentException($"You must specify a user to get conversations with.");
        }

        var total = await this.messageRepository.GetConversationCountAsync(userId);
        var messageEntities = await this.messageRepository.FetchConversationsAsync(userId, offset, limit);

        var results = messageEntities.Select(this.messageFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Message>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    public async Task<PaginatedResponse<Message>> FetchMessagesAsync(
        int toUserId, string fromUsername, string expand = "", int offset = -1, int limit = 20)
    {
        var toUserEntity = await this.userRepository.ReadAsync(toUserId);
        if (toUserEntity == null)
        {
            throw new KeyNotFoundException($"\"To\" user {toUserId} not found.");
        }

        var fromUserEntity = await this.userRepository.ReadAsync(fromUsername);
        if (fromUserEntity == null)
        {
            throw new KeyNotFoundException($"\"From\" user \"{fromUsername}\" not found.");
        }

        var fromUserId = fromUserEntity.Id;
        var total = await this.messageRepository.GetMessageCountAsync(toUserId, fromUserId);

        // Messages are a special case, where we want to see the last page by default.
        // 'last' is indicated by the special default value of '-1'.
        // -1' was chosen so that we can still specify the first page as '0'.
        if (offset == -1)
        {
            offset = LastPageOffset(total, limit);
        }

        var messageEntities = await this.messageRepository.FetchMessagesAsync(toUserId, fromUserId, offset, limit);
        var results = messageEntities.Select(this.messageFactory.New).ToList();
        results = await this.ExpandAsync(results, expand);

        var response = new PaginatedResponse<Message>
        {
            Offset = offset,
            Limit = limit,
            Total = total,
            Results = results,
        };

        // TODO: Cache
        return response;
    }

    /// <inheritdoc />
    public async Task<int> GetUnreadMessageCountAsync(int toUserId)
    {
        return await this.messageRepository.GetUnreadMessageCountAsync(toUserId);
    }

    /// <summary>
    /// Figure out what the offset would be for a given total and page size.
    /// </summary>
    /// <param name="total">Total number of pages.</param>
    /// <param name="pageSize">Number of messages is each "page".</param>
    /// <returns>The zero-based offset number of pages.</returns>
    private static int LastPageOffset(int total, int pageSize)
    {
        var pageCount = (int)Math.Ceiling((decimal)total / (decimal)pageSize);
        return (pageCount - 1) * pageSize;
    }
}
