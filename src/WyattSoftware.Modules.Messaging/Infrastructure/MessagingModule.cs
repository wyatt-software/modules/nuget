﻿// <copyright file="MessagingModule.cs" company="Wyatt Software">
// Copyright (c) Wyatt Software. All rights reserved.
// </copyright>

namespace WyattSoftware.Modules.Messaging.Infrastructure;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using WyattSoftware.Modules.Messaging.API.Controllers;
using WyattSoftware.Modules.Messaging.BLL.Factories;
using WyattSoftware.Modules.Messaging.BLL.Services;
using WyattSoftware.Modules.Messaging.DAL.Repositories;

/// <summary>
/// Dependency injection and application builder extension methods for the WyattSoftware.Modules.Messaging module.
/// </summary>
public static class MessagingModule
{
    /// <summary>
    /// Configures dependency injection.
    /// </summary>
    /// <param name="services">These services.</param>
    /// <returns>This.</returns>
    public static IServiceCollection AddMessagingModule(this IServiceCollection services)
    {
        // DAL
        services.AddScoped(typeof(IMessageRepository), typeof(MessageRepository));

        // BLL
        services.AddScoped(typeof(IMessageFactory), typeof(MessageFactory));
        services.AddScoped(typeof(IMessageService), typeof(MessageService));

        return services;
    }

    /// <summary>
    /// Configures the Messaging module.
    /// </summary>
    /// <param name="app">The app builder.</param>
    /// <returns>This.</returns>
    public static IApplicationBuilder UseMessagingModule(this IApplicationBuilder app)
    {
        // API
        MessagesController.InitMapping();

        return app;
    }
}
