/ [Home](../../../README.md) / [Database](../README.md) / Set up database users and privileges

# Set up database users and privileges

## Step 1

From the "Server" menu, choose "Users and Privileges".

![Users and Privileges](./images/users-and-privileges.png)

## Step 2

Click "Add Account" and under the "Login" tab, create a user called "db-user":
![db-user - Login](./images/db-user--login.png)

_**Note:** "Limit to Hosts Matching" should be set to "localhost" for local servers, and "%" for remote._

## Step 3

Click on the "Administrative Roles" tab, and ensure the `db-user` only has the following Global Privileges:

- CREATE
- DELETE
- EXECUTE
- INSERT
- SELECT
- UPDATE

![db-user - Administrative Roles](./images/db-user--administrative-roles.png)

## Step 4

Click "Apply"
