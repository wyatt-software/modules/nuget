/ [Home](../../../README.md) / [Database](../README.md) / Create a local database

# Create a local database

## Step 1

Ensure you have a connection to the local server:

- **Hostname:** either 127.0.0.1 or localhost
- **Port:** 3306
- **Username:** root
  ![Setup New Connection](./images/setup-new-connection.png)

## Step 2

Right-click in the "Schemas" section and choose "Create Schema..." from the context menu:

![Schemas context menu](./images/schemas-context-menu.png)

## Step 3

Name the schema and click "Apply":

![Create schema](./images/create-schema.png)

## Step 4

Click "Apply" again in the "Apply SQL Script to Database" dialog.

## Step 5

Finally, click "Finish" to close the dialog.
