/ [Home](../../../README.md) / [Database](../README.md) / Set up dev tools

# Set up dev tools

## Step 1

Download and install [MySQL and MySQL Workbench](https://dev.mysql.com/downloads/mysql/) (version 8 or newer).

## Step 2

Ensure the `bin` folder is part of your `PATH` environment variable. Eg: "C:\Program Files\MySQL\MySQL Server 8.0\bin".
