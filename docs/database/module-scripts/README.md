/ [Home](../../../README.md) / [Database](../README.md) / Install, reset, and uninstall SQL scripts per module

# Install, reset, and uninstall SQL scripts per module

Most modules rely on the database. The only exception is the Search module.

| Order | Module    | Install                                                               | Reset                                                             | Uninstall                                                                 |
| ----: | :-------- | :-------------------------------------------------------------------- | :---------------------------------------------------------------- | :------------------------------------------------------------------------ |
|     1 | Core      | [core.install.sql](../../../sql/core/core.install.sql)                | [core.reset.sql](../../../sql/core/core.reset.sql)                | [core.uninstall.sql](../../../sql/core/core.uninstall.sql)                |
|     2 | Identity  | [identity.install.sql](../../../sql/identity/identity.install.sql)    | [identity.reset.sql](../../../sql/identity/identity.reset.sql)    | [identity.uninstall.sql](../../../sql/identity/identity.uninstall.sql)    |
|     3 | Search    | -                                                                     | -                                                                 | -                                                                         |
|     4 | Content   | [content.install.sql](../../../sql/content/content.install.sql)       | [content.reset.sql](../../../sql/content/content.reset.sql)       | [content.uninstall.sql](../../../sql/content/content.uninstall.sql)       |
|     5 | Gallery   | [gallery.install.sql](../../../sql/gallery/gallery.install.sql)       | [gallery.reset.sql](../../../sql/gallery/gallery.reset.sql)       | [gallery.uninstall.sql](../../../sql/gallery/gallery.uninstall.sql)       |
|     6 | Messaging | [messaging.install.sql](../../../sql/messaging/messaging.install.sql) | [messaging.reset.sql](../../../sql/messaging/messaging.reset.sql) | [messaging.uninstall.sql](../../../sql/messaging/messaging.uninstall.sql) |
|     7 | Forum     | [forum.install.sql](../../../sql/forum/forum.install.sql)             | [forum.reset.sql](../../../sql/forum/forum.reset.sql)             | [forum.uninstall.sql](../../../sql/forum/forum.uninstall.sql)             |

## Installing

When setting up your site, you'll need to run the "install" script for each module you're using in the order shown above.

## Resetting

If you ever need to reset data, you'll need to run the "reset" script for each module in the **reverse** order to what's shown above.

## Uninstalling

Similarly, if you need to uninstall any modules, you'll need to run the "uninstall" script for each module in the **reverse** order to what's shown above.

---

## Default "admin" user account (Identity module)

When you install or reset the Identity module, a single default user is created: `"admin"` with the default password `"password"` (stored as plain text).

You should log into the site and change to a more secure password immediately.

All new passwords are hashed with the `HashStringSalt` (configured in the Core section of your [appsettings.json](../../config/appsettings-core.md)).

Make sure to keep a record of the `HashStringSalt` used with each database. Encryption relies on it.
