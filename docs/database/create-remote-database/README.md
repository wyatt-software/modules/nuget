/ [Home](../../../README.md) / [Database](../README.md) / Create a remote database

# Create a remote database

## Step 1

Log into the AWS Console and navigate to the RDS dashboard.

Ensure you're in the desired region, and click on the "Create database" button.

![RDS dashboard](./images/rds-dashboard.png)

## Step 2

Choose the "Easy create" database creation method.

![Database creation method](./images/database-creation-method.png)

## Step 3

Choose the database instance size. "Free tier" is fine for our purposes.

Give the database an instance identifier. Eg" ws-dev".

Create a root username and password (don't lose these).

![Configuration](./images/configuration.png)

## Step 4

Scroll to the bottom and click the "Create database" button.

## Step 5

Click on your database, then choose Instance actions -> Modify

Modify the security groups that are associated with the DB instance (add the new security group, remove the default security group).

Expand the "Connectivity" section and make the database publicly accessible.

## Step 6

Once the database has been created, test the connection in MySQL Workbench. (the connection details will be under the "Connectivity & security" tab when viewing the database).

## Step 7

You'll need to create a DB parameter group with `log_bin_trust_function_creators=1`.
See https://aws.amazon.com/premiumsupport/knowledge-center/rds-mysql-functions/

Otherwise, you'll see the following error when trying to create tables, views, functions, and stored procedures etc:

```
ERROR 1419 (HY000): You do not have the SUPER privilege and binary logging is enabled (you might want to use the less safe log_bin_trust_function_creators variable)
```
