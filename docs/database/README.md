/ [Home](../../README.md) / Database

# Database

- [Set up dev tools](./dev-tools/README.md)
- [Create a local database](./create-local-database/README.md)
- [Create a remote database](./create-remote-database/README.md)
- [Set up database users and privileges](./users-and-privileges/README.md)
- [Install, reset, and uninstall SQL scripts per module](./module-scripts/README.md)
- [MySQL via CLI](./mysql-via-cli/README.md)
