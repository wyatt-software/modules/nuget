/ [Home](../../../README.md) / [Database](../README.md) / MySQL via Command Line Interface (CLI)

# MySQL via Command Line Interface (CLI)

Below are some notes on accessing the database via command-line.

## Creating a "login path".

If you haven't already, you should create a "login path" so that you don't have to use your password on the command line. Open a Command Prompt, and enter the following command:

```
mysql_config_editor set --login-path=local --host=localhost --user=root --password
```

Enter the password for your local MySQL root password when prompted.

## Logging into MySQL CLI

Log into MySQL CLI using the following command:

```
mysql --login-path=local
```

## Ensuring your database already exists

Check if your database already exists by entering the following command into the MySQL CLI:

```
show databases;
```

If you don't see it in the list, you'll need to create it with:

```
create database yourdatabasename;
```

## Switching to your database

Enter the following command into the MySQL CLI:

```
use yourdatabasename;
```

## Running an SQL script

Enter the following command into the MySQL CLI:

```
source D:\full\path\to\your\script.sql
```
