/ [Home](../../README.md) / [Config files](./README.md) / [appsettings.json](./appsettings.md) / Forum

# Forum

## TopicThumb

| Key                  | Description                                                                                                     | Example value |
| :------------------- | :-------------------------------------------------------------------------------------------------------------- | :------------ |
| `"AlbumName"`        | The name of the user gallery folder to create and automatically upload external images to for topic thumbnails. |               |
| `"ScraperBaseUrl"`   | The Base absolute URL to use when scraping images with relative URLs.                                           |               |
| `"ScraperBlackList"` | An array of regex patterns that descript blacklisted URLs.                                                      |               |
