/ [Home](../../README.md) / [Config files](./README.md) / Bash terminal helpers

# Bash terminal helpers

## .bashrc

Add the following to your `.bashrc` file.

```
# Allow showing git branch on command prompt
git_branch() {
  branch=$(git symbolic-ref --short HEAD 2>/dev/null)
  if [ $branch ]; then
    printf "($branch)"
  fi
}

# Command promt
export PS1="\n\e[0;32m\u@\h\e[m \e[0;35mLOCAL\e[m \e[0;33m\w\e[m \e[0;36m\$(git_branch)\e[m"$'\n$ '

# Git shortcuts
alias gs='clear && clear && clear && git status'
alias gb='git branch'
```
