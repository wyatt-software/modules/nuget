/ [Home](../../README.md) / [Config files](./README.md) / Configuring the httpd.conf file

# Configuring the httpd.conf file

## Step 1: Navigate to the directory containing the `httpd.conf` file

```
cd /etc/httpd/conf
```

## Step 2: Edit the file

```
vim httpd.conf
```

## Step 3: Fill in important options

Listen on port 80:

```
Listen 80
```

The `html` sub-folder is where the default port `:80` usually maps to.

Add a rule for the `"/var/www"` directory to relax permissions.

```
DocumentRoot "/var/www/html"

#
# Relax access to content within /var/www.
#
<Directory "/var/www">
    AllowOverride None
    # Allow open access:
    Require all granted
</Directory>
```

## Step 4: Add virtual host

This maps our domain name to port 5000 of our localhost (apache).
See https://httpd.apache.org/docs/2.2/vhosts/name-based.html

The local port (5000 in this example) is set in [appsettings.json](./appsettings.md) and [launchsettings.json](./launchsettings.md)

```
<VirtualHost *:80>
    DocumentRoot "/home/ec2-user/example/server/dist"
    ServerName api-dev.example.com
    ProxyPass / http://localhost:5000/
    ProxyPassReverse / http://localhost:5000/
    TimeOut 30
    ErrorLog "logs/example-server-error_log"
    CustomLog "logs/example-server-access_log" common
</VirtualHost>
```

## Step 5: Ensure dist directories exist

Make sure the above DocumentRoot folders all exist.
