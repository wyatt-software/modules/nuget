/ [Home](../../README.md) / [Config files](./README.md) / appsettings.json

# appsettings.json

## Initial setup

1. Navigate to the app folder. Eg: `cd example-server/src/EX/`
2. Copy from the template with `cp appsettings.template.json appsettings.json`
3. Edit the settings with `vim appsettings.json`
   - vim starts in command mode. Hit `i` or `insert` to enter edit mode
   - `esc` gets back to command mode
   - when in command mode, `:wq` writes and quits
   - `:q!` quits without saving, even if there are changes

_Note: You don't have to use vim locally, but it's worth learning the basics for editing remote config files over ssh._

## App settings explained

| Key              | Description                                                                                                                                                                                                                                                | Example                    |
| :--------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------- |
| `"Logging"`      | See [Logging in .NET Core and ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/logging/?view=aspnetcore-5.0).                                                                                                                       |                            |
| `"AllowedHosts"` | See [HostingFiltereingOptions.AllowedHosts Property](https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.hostfiltering.hostfilteringoptions.allowedhosts?view=aspnetcore-5.0).                                                                |                            |
| `"Urls"`         | Use this to set up the port on your local or remote server that your dotnet app listens on. Note: It should still use "localhost" on the remote server. We'll [configure the server](./httpd.md) to use a reverse proxy to forward external request to it. | `"http://localhost:5000/"` |
| `"WS"`           | The WS Modules' custom settings are organised into sub-sections. See the list below.                                                                                                                                                                       |                            |

- [Core](./appsettings-core.md)
- [Identity](./appsettings-identity.md)
- [Search](./appsettings-search.md)
- [Content](./appsettings-content.md)
- [Messaging](./appsettings-messaging.md)
- [Gallery](./appsettings-gallery.md)
- [Forum](./appsettings-forum.md)
