/ [Home](../../README.md) / [Config files](./README.md) / [appsettings.json](./appsettings.md) / Gallery

# Gallery

| Key                    | Description                                                                                                   | Example value            |
| :--------------------- | :------------------------------------------------------------------------------------------------------------ | :----------------------- |
| AllowedImageExtensions | Extensions of image file types the site allows uploads of.                                                    | `".gif,.jpg,.jpeg,.png"` |
| GalleryFolderName      | The sub-folder name under the storage folder or bucket where gallery images are stored.                       |                          |
| UploadAlbumName        | The name of the user gallery folder to create and upload images to when uploaded from the markdown editor.    |
| AvatarAlbumName        | The name of the user gallery folder to create and upload avatars to when uploaded from the user details page. |                          |
