/ [Home](../../README.md) / [Config files](./README.md) / [appsettings.json](./appsettings.md) / Search

# Search

| Key               | Description                                     | Example value                                                                                                        |
| :---------------- | :---------------------------------------------- | :------------------------------------------------------------------------------------------------------------------- |
| SearchIndexesPath | Defines the location of the search index files. | `"D:\\wyatt-software\\example\\server\\temp\\search-indexes"` or `/home/ec2-user/example/server/temp/search-indexes` |
