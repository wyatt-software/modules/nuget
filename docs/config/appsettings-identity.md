/ [Home](../../README.md) / [Config files](./README.md) / [appsettings.json](./appsettings.md) / Identity

# Identity

| Key           | Description                                                                                      | Example value              |
| :------------ | :----------------------------------------------------------------------------------------------- | :------------------------- |
| `"Authority"` | The web app uses IdentityServer4 on the same server. This should match the `Urls` setting above. | `"https://api.example.com` |

## EmailVerificationCode

When a new user signs up, an email is sent with a verification code that they need to use to verify that they have access to the email address used.

| Key             | Description                                               | Example value   |
| :-------------- | :-------------------------------------------------------- | :-------------- |
| Chars           | A string containing the valid chars to use in the code.   | `"0123456789"`. |
| Length          | The number of characters to use when generating a code.   | 6               |
| ExpirationHours | The number of hours until the code can no longer be used. | 1               |

## Password

| Key                      | Description                                                  | Example value  |
| :----------------------- | :----------------------------------------------------------- | :------------- |
| AllowedFailedAttempts    | The number of tries a user has before the account is locked. | 3              |
| MinLength                | The minimum length allowed for passwords.                    | 8              |
| ResetCodeChars           | A string containing the valid chars to use in the code.      | `"0123456789"` |
| ResetCodeLength          | The number of characters to use when generating a code.      | 6              |
| ResetCodeExpirationHours | The number of hours until the code can no longer be used.    | 1              |
