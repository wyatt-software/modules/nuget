/ [Home](../../README.md) / [Config files](./README.md) / [appsettings.json](./appsettings.md) / Core

# Core

## Database

| Key          | Description                                                         | Example value                                               |
| :----------- | :------------------------------------------------------------------ | :---------------------------------------------------------- |
| `"Server"`   | The domain name of the server to connect to (without the protocol). | `"localhost"`, `"db.example.com"` or `"db-dev.example.com"` |
| `"User"`     | The database user to connect with.                                  | `"db-user"`                                                 |
| `"Password"` | The above user's password.                                          |                                                             |
| `"Database"` | The name of the database to use.                                    | `"ws"`, `"example.com"` or `"dev.example.com"`, etc         |

## Clients

This is an array. We can configure multiple clients. i.e.: Web, iOS, and Android apps. For now, we only have the web app.

Each object in the array has the following settings:

| Key                             | Description                                                                                                                                                                 | Example value                                                                     |
| :------------------------------ | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :-------------------------------------------------------------------------------- |
| `"ClientId"`                    | Codename given to the client app. It needs to also be configured on the client, and sent as the `client_id` when authenticating.                                            |                                                                                   |
| `"ClientSecret"`                | This is the public key assigned to the client app. It needs to also be configured on the client, and sent as the `client_secret` when authenticating.                       |                                                                                   |
| `"AccessTokenLifetime"`         | Number of seconds until the access token expires.                                                                                                                           | `3600` (1 hour)                                                                   |
| `"SlidingRefreshTokenLifetime"` | Number of seconds until the refresh token expires.                                                                                                                          | `1296000` (15 days)                                                               |
| `"CorsAllowedOrigins"`          | This is an array. We can configure multiple allowed origins per client. Each entry should be the full protocol, domain, and port of the client, _without_ a trailing slash. | `"http://localhost:3000"`, `"https://example.com"` or `"https://dev.example.com"` |

## Encryption

| Key                | Description                                                                                         | Example value |
| :----------------- | :-------------------------------------------------------------------------------------------------- | :------------ |
| `"HashStringSalt"` | A secret string used when creating passwords hashes etc. It must be kept in sync with the database. |               |

## Email

This is the section for configuring transactional emails sent from the site.

| Key            | Description                                                                                                                                                                                                                                                                                                                                                                                                                              |
| :------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `"From"`       | The email address the site sends emails from. You must verify with ZeptoMail that you own this email address.                                                                                                                                                                                                                                                                                                                            |
| `"RedirectTo"` | The email address to redirect all outbound emails to when running on anything other than production environment (as determined by the [HostingEnvironmentExtensions.IsProduction(IHostingEnvironment) Method](https://docs.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting.hostingenvironmentextensions.isproduction?view=dotnet-plat-ext-6.0)). This prevents the site from sending real emails to real users when testing. |

### ZeptoMail

Currently the only implementation uses Zoho ZeptoMail's API to send transactional email.

| Key                     | Description                                                                             |
| :---------------------- | :-------------------------------------------------------------------------------------- |
| `"ApiUrl"`              | This must be `"https://api.zeptomail.com/v1.1/email"`.                                  |
| `"AuthorizationScheme"` | This must be `"Zoho-enczapikey"`.                                                       |
| `"ApiKey"`              | This is your ZeptoMail API key, _without_ the `"Zoho-enczapikey"` authorization scheme. |
| `"BounceAddress"`       | This must match the bounce address that's been set up in ZeptoMail.                     |

## Macros

These are global macros available in the site content pages and email templates.

Eg:

```
"Macros": {
    "DomainName": "http://localhost:5000/",
    "SiteName": "WS Modules",
    "SiteYearEstablished": 2022
},
```

## Storage

| Key                | Description                                    | Example value                          |
| :----------------- | :--------------------------------------------- | :------------------------------------- |
| `"Implementation"` | Specify the implementation to use for storage. | Valid options are `"Local"` or `"S3"`. |

See the relevant section under Implementations below for related settings.

### Implementations

#### Local

| Key                 | Description                                                                                                                                                                                                                                           | Example value                                      |
| :------------------ | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------- |
| `"AbsolutePath"`    | The full absolute path on the local filesystem of the storage folder.Don't use trailing slashes.Note the use of backslashes (escaped as double-backslashes, since this is JSON) for Windows systems. Forward-slashes should be used on Linux systems. | `"D:\\websites\\example\\example-server\\storage"` |
| `"FolderSeparator"` | For Windows systems, use `"\\"` (this is a single back-slash, but is escaped with a backslash prefix since this us JSON).For Linux systems, uses `"/"`.                                                                                               |                                                    |
| `"RequestPath"`     | The relative URL request path that should be mapped to the local filesystem.                                                                                                                                                                          | `"/storage"`                                       |

#### S3

| Key                 | Description                                                                | Example value                      |
| :------------------ | :------------------------------------------------------------------------- | :--------------------------------- |
| `"Region"`          | The Amazon AWS region to use for your connection to the API.               | `"ap-southeast-2"`                 |
| `"Bucket"`          | The name of the S3 bucket used for image and file storage.                 | `"storage-dev.wyatt-software.com"` |
| `"AccessKeyId"`     | An Access key ID for an IAM user with the `AmazonS3FullAccess` permission. |                                    |
| `"SecretAccessKey"` | The secret access key for the above Access key ID.                         |                                    |

## Vendor

### ReCaptcha

| Key            | Description                                                           | Example value |
| :------------- | :-------------------------------------------------------------------- | :------------ |
| `"Enabled"`    | Allows you to disable recaptchas for testing locally via Postman etc. |               |
| `"PrivateKey"` | The private key from your ReCaptcha account.                          |               |
