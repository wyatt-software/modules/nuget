/ [Home](../../README.md) / [Config files](./README.md) / launchsetting.json

# launchsetting.json

See [https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-5.0#lsj](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/environments?view=aspnetcore-5.0#lsj)

Eg:

```
{
	"iisSettings": {
		"windowsAuthentication": false,
		"anonymousAuthentication": true,
		"iisExpress": {
			"applicationUrl": "http://localhost:5000/",
			"sslPort": 5001
		}
	},
	"profiles": {
		"YourSite": {
			"commandName": "Project",
			"launchBrowser": false,
			"environmentVariables": {
				"ASPNETCORE_ENVIRONMENT": "Development"
			},
			"applicationUrl": "https://localhost:5001;http://localhost:5000"
		}
	}
}
```
