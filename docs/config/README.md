/ [Home](../../README.md) / Config files

# Config files

- [appsettings.json](./appsettings.md)
- [bash terminal helpers](./bash.md) (`.bashrc`)
- [httpd.conf](./httpd.md)
- [launchsettings.json](./launchsettings.md)
