/ [Home](../../../README.md) / [Remote server](../README.md) / Configure domains and sub-domains

# Configure domains and sub-domains

Previous: [Deploy to the remote server](../deploy/README.md)

---

Domiains are configured via _Route 53_.

To serve the web app over HTTPS, you need to set up the following DNS records:

| Component       | Record name (prod)                    | Record name (dev)         | Type    | Configuration                                    |
| :-------------- | :------------------------------------ | :------------------------ | :------ | :----------------------------------------------- |
| **Vue.js App:** | `example.com` and `www.example.com`\* | `dev.example.com`         | `A`     | Alias to _CloudFront_ distribution               |
| **API Server:** | `api.example.com`                     | `api-dev.example.com`     | `A`     | Alias to _Application and Classic Load Balancer_ |
| **Database:**   | `db.example.com`                      | `db-dev.example.com`      | `CNAME` | _RDS_ instance endpoint                          |
| **Storage:**    | `storage.example.com`                 | `storage-dev.example.com` | `A`     | Alias to _CloudFront_ distribution               |

\*_In production, you need to configure `A` records for each the root domain and `www.` sub-domain - both as an alias to the same CloudFront distribution._

---

Next: [Configure HTTPS](../https/README.md)
