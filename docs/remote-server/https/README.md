/ [Home](../../../README.md) / [Remote server](../README.md) / Configure HTTPS

# Configure HTTPS

Previous: [Configure domains and sub-domains](../domains/README.md)

---

HTTPS is achieved on the various AWS services in different ways. This guide does not provide step-by-step instructions for configuring each. It simply outlines the methods used.

## SSL/TLS certificates

You'll need to request two certificates (issued for free by the _AWS Certificate Manager_):

- One in the "US East (N. Virginia)" `us-east-1` region
  - for your _S3_ buckets
  - configured with both the root domain and a wildcard. Eg: `example.com` and `*.example.com`
  - this is so we can configure the `www.` sub-domain to redirect to the root domain
- One in your local region
  - for your API Server (_EC2_) and database (_RDS_)
  - only needs the wildcare name. Eg: `*.example.com`

## API Server (EC2)

An "Application Load Balancer" should be configured in the same region as your _EC2_ instance.

The Target group only needs to be configured for your _EC2_ instance over HTTP on port 80.

The health check also only needs to be over HTTP, and should be to a path known to return a status of 200. eg: `/v1/content/routes`.

A listener is only needed for HTTPS on port 443. It should forward to the target group with Group-level stickiness enabled. Don't forget to choose the SSL certificate from your local region.

## Storage (S3)

Image files etc. are served over HTTPS by _Cloudfront_ using the certificate issued in the `us-east-1` region. It should be configured to redirect HTTP to HTTPS.

## Vue.js static website (S3)

This also uses _Cloudfront_ and certificate from the `us-east-1` region.

You should configure the default root object to be "index.html".

In production, you'll want to create two distributions. One for the root domain, and one for the `www.` sub-domain:

- The distribution for the root domain should redirect HTTP to HTTPS.
- The distribution for the `www.` sub-domain should allow HTTP and HTTPS.

Although already covered in the [ws-client-web](https://gitlab.com/retrojdm/ws-client-web) repo's docs, remember to configure the _S3_ buckets as follows:

- The main bucket (eg: `example.com` in prod, or `dev.example.com` in dev)
  - **Static website hosting:** Enable
  - **Hosting type:** Host a static website
  - **Index document:** index.html
  - **Error document:** index.html
- The `www.` sub-domain bucket (eg: `www.example.com`) only needed in prod
  - **Static website hosting:** Enable
  - **Hosting type:** Redirect requests for an object
  - **Host name:** The root domain (eg: `example.com`)
  - **Protocol:** https

## Database (RDS)

The database connection is not over HTTPS, since it's only accessed internally by AWS services on the same _VPC_ and by trusted IP addresses.

Security Group inbound rules should be set up to limit access on port 3306 to only trusted IP addresses (including the EC2 instance).
