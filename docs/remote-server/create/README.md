/ [Home](../../../README.md) / [Remote server](../README.md) / Create a remote server

# Create a remote server

## Step 1

Navigate to the EC2 service, and ensure you're in the desired region.

![EC2 instances](./images/ec2-instances.png)

## Step 2

Choose a linux-compatible machine image. We don't need a GUI. A command-line only image is fine.

Eg: Amazon Linux 2 AMI (HVM), SSD Volume Type.

![Choose an Amazon Machine Image (AMI)](./images/choose-an-amazon-machine-image.png)

## Step 3

Choose an instance type. Eg: The free tier eligible "t2.micro".

The default options should be fine. Click on the "Review and Launch" button.

![Choose an instance type](./images/choose-an-instance-type.png)

## Step 4

Review the instance configuration, and click on the "Edit security groups" link.

![Review instance launch](./images/review-instance-launch.png)

## Step 5

Give the group a meaningful name and description, and restrict SSH on port 22 to your own IP address.

Create inbound rules for HTTP and HTTPS open to anyone.

![Configure\security group](./images/configure-security-group.png)

## Step 6

Review the instance configuration again, and click the "Launch" button.

![Review instance launch (again)](./images/review-instance-launch-2.png)

## Step 7

In the popup, create a new key pair and download the .pem file (you'll need this later for connecting to the remote server via SSH).

Finally, click the "Launch instances" button.

![Create new key pair](./images/create-new-key-pair.png)

## Done!

You should have been redirected to the launch status page.

![Launch status](./images/launch-status.png)

---

Next: [Connect to the remote server](../connect/README.md)
