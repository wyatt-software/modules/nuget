/ [Home](../../../README.md) / [Remote server](../README.md) / Deploy to the remote server

# Deploy to the remote server

Previous: [Set up the remote server](../set-up/README.md)

---

## Step 1. Stop the site if already running

You can skip this step if the site wasn't already running.

1. Stop the server with `systemctl stop httpd`
2. Find the process IDs of any processes containing "example" (or whatever your site's startup project is) in their name with `ps -ef | grep "example"`
   - Note: the command you _just_ ran will also have "WS" in the name. We want the one _without_ `grep` in the name
3. stop the "WS" process with `kill 12345`, substituting the actual process id you just found

## Step 2. Get the latest source files

1. If you already have a copy of your repo, get the latest changes with `git pull`
2. Otherwise clone your site's repo
3. [Ensure the appsettings.json file is configured](../config/appsettings.md)

## Step 3. Test run

1. Navigate to the `src` folder
2. Restore any dependencies with `dotnet restore`
3. Test run the dotnet app with `dotnet run --project ./example/example.csproj`
4. Make sure there are no error messages
   - if there are, try to troubleshoot from the error message
     - The problem is probably caused by an `appsettings.json` setting, or a missing folder
     - The first error message probably points to the `.cs` source file and line number
     - Check the source code to see what's failing
   - keep trying until the app runs without errors
   - See the section on checking the logs below
5. Shut the app down with `CTRL+C`

## Step 4. Build and publish the site

1. Ensure the output folder exists. Eg: `dist`
2. Ensure the output folder is empty with `rm dist/* -rf`
3. Navigate to the source folder with `cd src`
4. Run `dotnet publish -r linux-x64 -c Release -o ../dist/`
5. Wait for the build/publish to finish

## Step 5. Run the dotnet app in the background

1. Navigate to the output folder with `cd ../dist`
2. Run the web app in the background with `nohup ./example &`
   - The `nohup` "no hang up" command ensures the process isn't killed when you log out of your SSH session
   - The `&` at the end runs the command in the background, so that you can keep entering commands
3. Double-check that the process is actually running with `ps -ef | grep "example"`

## Step 6. Start the server

1. Ensure the [server is configured](../../config/httpd.md)
2. Start the server with `systemctl start httpd`
3. Ensure there's no issues with `systemctl status httpd`

## Step 7. Test

TODO

- Make sure the EC2 instance allows inbound access on port 80
- Make requests via Google Postman
- View site from a browser (if the web client has been deployed)

## Step 8. Check the logs

The server should be configured to keep logs under `/etc/httpd/logs/`.

You can check these logs for more info when troubleshooting.

- `access_log` contains logs of HTTP requests and their response codes
- `error_log` contains any warnings or errors returned by the web server (httpd).

You can search the logs by piping a `cat` command to a `grep` command. Eg: `cat error_log | grep "your search term"`

---

Next: [Configuring domains and sub-domains](../domains/README.md)
