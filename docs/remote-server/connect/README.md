/ [Home](../../../README.md) / [Remote server](../README.md) / Connect to the remote server

# Connect to the remote server

Previous: [Create a remote server](../create/README.md)

---

## SSH via bash terminal

### Step 1.

When creating the EC2 instance, you should have created a key value pair and downloaded a .pem file.

Navigate to your user folder (i.e. `"C:\Users\{your-username}\"`) and create a new folder named `".ssh"` if it doesn't already exist.

Move your .pem file here.

### Step 2.

Ensure that your EC2 instance has inbound rules for port 22 from your current IP address (via the "Security groups" section of the EC2 dashboard).

![Inbound rules](./images/inbound-rules.png)

### Step 3.

In the Instance summary, make a note of your instance's public IP4 address.

![Instance summary](./images/instance-summary.png)

### Step 4.

Open a bash terminal and connect to the remote serve via SSH with the following command (substituting your own public IP4 address and .pem file location):

```
ssh ec2-user@12.34.56.78 -i ~/.ssh/ws-dev.pem
```

You'll probably get a warning that the "The authenticity of host '12.34.56.78 (12.34.56.78)' can't be established".

Type "yes" to continue.

You should then see a message like:

```
Warning: Permanently added '12.34.56.78' (ED25519) to the list of known hosts.
Connection closed by 12.34.56.78 port 22
```

If the connection was closed, you'll simply have to run the ssh command again.

When the connection is successful, you should see a message like:

```
Last login: Sun Oct  3 14:46:08 2021 from 012.345.678.910

       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-2/
11 package(s) needed for security, out of 35 available
Run "sudo yum update" to apply all updates.
[ec2-user@ip-12-34-56-78 ~]$
```

You can exit the SSH session by entering: `exit` at the command prompt.

---

Next: [Set up the remote server](../set-up/README.md)
