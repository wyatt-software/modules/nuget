/ [Home](../../../README.md) / [Remote server](../README.md) / Set up the remote server

# Set up the remote server

Previous: [Connect to the remote server](../connect/README.md)

---

## Step 1. Update

When you first connect to the remote server via SSH, you should see a message like:

```
Last login: Sun Oct  3 14:46:08 2021 from 012.345.678.910

       __|  __|_  )
       _|  (     /   Amazon Linux 2 AMI
      ___|\___|___|

https://aws.amazon.com/amazon-linux-2/
11 package(s) needed for security, out of 35 available
Run "sudo yum update" to apply all updates.
[ec2-user@ip-12-34-56-78 ~]$
```

Go ahead and run the suggested update command:

```
sudo yum update
```

Wait for it to complete.

## Step 2. Configure bash for the default user (ec2-user)

The default command prompt is fairly plain.

Configure the [Bash terminal helpers](../../config/bash.md) to get a snazzy new colourful prompt that shows your git branch.

Note: your `.bashrc` file is located in your home folder (`~` or `/home/ec2-user` for the default user).

You can edit it with:
`vim .bashrc`

See [Basic Vim commands - For getting started](https://coderwall.com/p/adv71w/basic-vim-commands-for-getting-started).

Reload the bash settings by entering the following command (or exiting then re-connecting to your SSH session):

```
source ~/.bashrc
```

## Step 3. Configure bash for the super user

Elevate your privileges to the super user by entering:

```
sudo su
```

Note: If you see an error message like "bash: git_branch: command not found", don't worry. We'll fix that in a second.

You'll need to configure the [Bash terminal helpers](../../config/bash.md) for the super user aswell.

Edit their `.bashrc` file with:

```
vim ~/.bashrc
```

And again, reload the bash settings by entering the following command (or exiting then re-connecting to your SSH session):

```
source ~/.bashrc
```

The rest of this guide assumes you're logged in as the super user, so you won't have to use `sudo` infront of the commands.

## Step 3. Install git

Use yum, CentOS's native package manager, to search for and install the latest git package available in CentOS’s repositories:

```
yum install git
```

When complete, you can check the got version with:

```
git --version
```

## Step 4. Install git credential manager

You can optionally store your git credentials in cache (specifying a timeout in seconds) with:

```
git config --global credential.helper 'cache --timeout=3600'
```

...or you can store them permanently with:

```
git config --global credential.helper 'store'
```

...but be aware that this stores your git credentials on disk in plain-text, without any encryption whatsoever in `~/.git-credentials`.

A third (and more secure) alternative is to use an SSH key, which gitlabs supports. See their docs here: [GitLab and SSH keys](https://docs.gitlab.com/ee/ssh/)

## Step 5. Install dotnet

The following instructions were taken from Microsoft's docs: [Install the .NET SDK on CentOS 7](https://docs.microsoft.com/en-us/dotnet/core/install/linux-centos#centos-7-).

Before you install .NET, run the following commands to add the Microsoft package signing key to your list of trusted keys and add the Microsoft package repository. Open a terminal and run the following commands:

```
rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm
```

The .NET SDK allows you to develop apps with .NET. If you install the .NET SDK, you don't need to install the corresponding runtime. To install the .NET SDK, run the following commands:

```
yum install dotnet-sdk-5.0
```

## Step 6. Install apache

Update the local Apache httpd package index to reflect the latest upstream changes:

```
yum update httpd
```

Once the packages are updated, install the Apache package:

```
yum install httpd
```

Apache does not automatically start on CentOS once the installation completes. You will need to start the Apache process manually:

```
systemctl start httpd
```

Verify that the service is running with the following command:

```
systemctl status httpd
```

---

Next: [Deploy to the remote server](../deploy/README.md)
