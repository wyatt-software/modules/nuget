/ [Home](../../README.md) / Visual Studio solution structure

# Visual Studio solution structure

The solution is split into modules, with each module implemented as a class library project (assembly). It's divided into modules (columns) instead of a traditional 3-tier application so that modules can be easily added or removed as needed.

However, the modules are each organised internally as separate mini 3-tier applications; i.e.: each module is responsible for it's own Data Access, Business Logic, and API layers.

Care has been taken to ensure that each module internally adheres to a strict separation of layers. The API layer depends only on the Business Logic layer, and the BL layer depends only on the Data Access layer.

Note that modules can access the classes etc within the other modules that they depend on, but the same separation between layers exists.

![Module Internal Layers](./images/module-internal-layers.png)

## Internal structure

A typical module is structured with the following namespace/folder heirarchy:

- `WyattSoftware.Modules.{module}`
  - `API`
    - `Controllers`
    - `DTOs`
  - `BLL`
    - `Enums`
    - `Extensions`
    - `Implementation`
    - `Interface`
    - `Models`
    - `Singletons`
    - `Static`
  - `DAL`
    - `Entities`
    - `Enums`
    - `Implementation`
    - `Interface`
    - `QueryResults`
  - `Infrastructure` - Dependency injection, app builder, and add-ons.
  - `Options` - Strongly typed classes bound to [appsettings.json](../config/appsettings.md) data

For more information about what goes into each namespace/folder, see the following docs:

- [API](./API.md)
- [BLL](./BLL.md)
- [DAL](./DAL.md)
