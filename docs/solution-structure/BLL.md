/ [Home](../../README.md) / [Visual Studio solution structure](./README.md) / BLL

# BLL

The Business Logic Layer (BLL) is where logic is handled. We use "thin" services and "fat" models. i.e.: Models are responsible for as much of their own logic as possible, with the services only responsible for fetching or reading those models.

## Enums

Enums used in business logic can be defined here.

## Implementation

This is where implementations for each of the modules interfaces go.

Since we're already splitting modules into namespaces, it shouldn't be neccessary to have your namespaces go any deeper than this.

A typical module will implement their own Service and Factory classes.

The Service class should inherit from `BaseDomainService`, and usually only provides Fetch and Read actions. The Create, Update, and Delete actions are the responsibility of our "fat" models.

The purpose of the factory classes is to replace the domain model constructors, injecting any dependencies the "fat" domain model might need.

## Interface

A typical module would define an interface for an `IDomainService<TDomainModel>` and `IDomainFactory<TDomainModel, TEntity>`.

## Models

As mentioned previously, we use "fat" models. This means that any action performed on a specific resource should be implemented as a method on the model. The exception is the Read action, which is part of the domain service since we don't have the model yet.

Models inherit their properties from the DAL Entities, and add methods for Creating, Updating, Deleting, and anything else the model may need to do (eg: Upload image, or Send password reset code).
