/ [Home](../../README.md) / [Visual Studio solution structure](./README.md) / API

# API

## Controllers

The controller base route should be in the form `"{module}/{resource}"` (eg: `"gallery/images"`), with a resource typically representing a table in a databese.

There should typically be a Fetch action and one for each of the C.R.U.D actions, with the Read, Update, and Delete actions using a route like `"{module}/{resourse}/{id}"` (eg: `"gallery/images/123"`).

The `{id}` doesn't have to be an integer. A resource might have some unique code or alias that can be used to identify it.

Note: routes don't need to be versioned. This entire app represents a single version. If you have multiple versions of your API, you should set up separate subdomains to point to separate apps.

Any actions other than Fetch or C.R.U.D should be routed like `"{module}/{resource}/{id}/actions/{action}"` (eg: `"gallery/images/123/actions/set-as-avatar"`).

If you need to process multiple resources, the route should be like `"{module}/batch/{action}"` path (eg `"gallery/batch/move-images"` or `"gallery/batch/delete-images"`).

Usually Fetch and Read actions don't need Authorization, but Create, Update, and Delete do. You can specify roles with an `[Authorize(Roles = "...")]` attribute. see the `Role` enum.

In some cases, you want to authorize only a certain user to perform an action. In that case, use the `AssertResourceOwnerOrRole()` helper method within the API action method. Eg:

```
this.currentSession.AssertResourceOwnerOrRole(image.UserId, "Moderator");
```

## DTOs

Request and Response DTOs should have properties named exactly the same as their BLL model equivalents, for auto-mapping.

You can include only the properties you want to expose on the API.
