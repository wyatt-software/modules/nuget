/ [Home](../../README.md) / [Visual Studio solution structure](./README.md) / DAL

# DAL

The Data Access Layer (DAL) is where persistence is handled. Eg: reading/writing from a database.

## Entities

Entities represent a single row in a database table.

## Enums

Enums required for use as database field values can be defined.

## Implementation

Typically a module would implement repositories for each of its database tables.

Repositories usually inherit from `BaseRepository`, which contains implementations for basic Fetch and C.R.U.D actions.

## Interface

A typical module would define an interface for an `IRepository<TEntity>` which itself inherits from `IReadOnlyRepository<TEntity>`.

## QueryResults

Results of queries that don't represent a row in a database can be defined here.
