/ [Home](../../README.md) / Example solution

# Example solution

## Create a new ASP.Net Core Empty solution

### Step 1

Open Visual Studio (you'll need at least 2022 for .NET 6.0 support).

![Visiual Studio](./images/01-visual-studio.png)

### Step 2

Choose the "ASP.Net Core Empty" template.

![ASP.Net Core Empty](./images/02-aspnetcore-empty.png)

### Step 3

Give your new project a name etc.

![Configure your new project](./images/03-configure-your-new-project.png)

### Step 4

We won't need HTTPS, even on the remote sites since we'll be using Apache's Proxy Pass to redirect the domain to a localhost port. See [Configuring the httpd.conf file](../config/httpd.md) for more details.

![Additional information](./images/04-additional-information.png)

### Step 5

Add this repo as a git sub-module:

```bash
cd <your-solution-folder>
git submodule add https://gitlab.com/wyatt-software/modules/server-modules
git config submodule.recursive true
```

TODO

## Configure your Program.cs file.

Eg:

```
using WyattSoftware.Modules.Content.Infrastructure;
using WyattSoftware.Modules.Core.Infrastructure;
using WyattSoftware.Modules.Forum.Infrastructure;
using WyattSoftware.Modules.Gallery.Infrastructure;
using WyattSoftware.Modules.Identity.Infrastructure;
using WyattSoftware.Modules.Messaging.Infrastructure;
using WyattSoftware.Modules.Search.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

var env = builder.Environment;

var configuration = builder.Configuration;

builder.Services
    .AddCoreModule(configuration)
    .AddIdentityModule(configuration)
    .AddSearchModule(configuration)
    .AddContentModule()
    .AddGalleryModule(configuration)
    .AddMessagingModule()
    .AddForumModule(configuration);

var app = builder.Build();

app
    .UseCoreModule(env, configuration)
    .UseIdentityModule()
    .UseCoreModuleEndpoints() // <-- This must go after .UseIdentityModule()
    .UseSearchModule()
    .UseContentModule()
    .UseGalleryModule()
    .UseMessagingModule()
    .UseForumModule();

app.Run();
```

## See also

- [aspsettings.json](../config/appsettings.md)
- [launchSettings.json](../config/launchsettings.md)
- [Database](../database/README.md)
