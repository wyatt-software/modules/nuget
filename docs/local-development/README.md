/ [Home](../../README.md) / Set up your local dev environment

# Set up your local dev environment

This guide is for Windows 10 or newer. IF you're developing .NET on Linux, it's assumed you already know this stuff.

## Install

### git

Download and install [git for windows](https://gitforwindows.org/).

### Database tools

Follow the guide to [install MySQL and MySQL Workbench](../database/dev-tools/README.md)

### Create blank local database

1. [Create a local database](../database/create-local-database/README.md)
2. [Set up a db-user](../database/users-and-privileges/README.md) with limited privileges.
3. Follow [this guide](../database/restore-database/README.md) to restore the blank `create_database.sql` database from the [database/](../../database/) folder of this repo.

### Visual Studio

Download and install [Visual Studio Community](https://visualstudio.microsoft.com/downloads/).

Note: You'll need 2022 or newer to use .NET 8.0.

### Visual Studio Code

Download and install [Visual Studio Code](https://code.visualstudio.com/download).

### .NET 8.0

Download and install [.NET 8.0 SDK](https://dotnet.microsoft.com/download/dotnet/8.0).

## Configure

### Configure .bashrc

Follow the guide to set up some [Bash terminal helpers](../config/bash.md)

## Google Postman

Download and install [the Postman app](https://www.postman.com/downloads/).

Import the [WS API Endpoints](./ws.postman_collection.json)
