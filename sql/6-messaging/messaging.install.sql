-- Messaging module - Install
--
-- You must have already installed all modules dependencies first.
-- Eg:
--  1. Core
--  2. Identity
--  3. Gallery

CREATE TABLE `messaging_message` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `from_user_id` int NOT NULL COMMENT 'Foreign key of the community_member the message was created by.',
  `to_user_id` int NOT NULL COMMENT 'Foreign key of the community_member the message was sent to.',
  `body` mediumtext NOT NULL,
  `read` bit(1) NOT NULL DEFAULT b'0' COMMENT 'Has the message been seen by the recipient.',
  `deleted_from_inbox` bit(1) NOT NULL DEFAULT b'0',
  `deleted_from_outbox` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `body_FULLTEXT` (`body`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `messaging_message`
ADD KEY `fk_from_user_id_idx` (`from_user_id`),
ADD KEY `fk_to_user_id_idx` (`to_user_id`),
ADD CONSTRAINT `fk_messaging_message_from_user_id` FOREIGN KEY (`from_user_id`) REFERENCES `identity_user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
ADD CONSTRAINT `fk_messaging_message_to_user_id` FOREIGN KEY (`to_user_id`) REFERENCES `identity_user` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE;