-- Messaging module - Uninstall
--
-- You must have already uninstalled all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum

UPDATE `identity_user` SET `preferences` = JSON_REMOVE(`preferences`, "$.emailNewMessages");

ALTER TABLE `messaging_message`
DROP CONSTRAINT `fk_messaging_message_from_user_id`,
DROP CONSTRAINT `fk_messaging_message_to_user_id`;

DROP TABLE `messaging_message`;