-- Messaging module - Reset
--
-- You must have already reset all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum

DELETE FROM `messaging_message`;
ALTER TABLE `messaging_message` AUTO_INCREMENT = 1;
