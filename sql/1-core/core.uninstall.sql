-- Core module - Uninstall
--
-- You must have already uninstalled all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging
--  3. Gallery
--  4. Content
--  5. Identity

DROP TABLE `core_email_template`;

DROP FUNCTION `func_content_page_get_unique_alias`;
DROP FUNCTION `func_forum_topic_get_unique_alias`;
DROP FUNCTION `func_gallery_album_get_unique_alias`;
DROP FUNCTION `func_get_code_name`;
