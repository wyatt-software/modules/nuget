-- Core module - Install
--
-- All other modules depend on the core module. This must be installed first.

CREATE TABLE `core_email_template`(
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `code_name` varchar(200) NOT NULL DEFAULT '',
  `display_name` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `subject` varchar(200) NOT NULL DEFAULT '',
  `plain_text` text,
  `html` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_name_UNIQUE` (`code_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DELIMITER ;;
CREATE FUNCTION `func_content_page_get_unique_alias` (`parentId` INT, `name` VARCHAR(200)) RETURNS varchar(255) CHARSET utf8mb4
    READS SQL DATA
BEGIN
	DECLARE `original_alias` VARCHAR(200);
	DECLARE `suffix` INT;
	DECLARE `alias` VARCHAR(200);
    
	SET
		`original_alias` = `func_get_code_name` (`name`),
		`suffix` = 0,
		`alias` = `original_alias`;
        
	WHILE EXISTS (SELECT CP.`id` FROM `content_page` CP WHERE CP.`parent_id` <=> `parentId` AND CP.`alias` = `alias`) DO
		SET `suffix` = `suffix` + 1;
		SET `alias` = CONCAT(`original_alias`, '-', CAST(`suffix` AS CHAR(10)));
	END WHILE;

	RETURN `alias`;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE FUNCTION `func_forum_topic_get_unique_alias` (`title` VARCHAR(200)) RETURNS varchar(255) CHARSET utf8mb4
    READS SQL DATA
BEGIN
	DECLARE `original_alias` VARCHAR(200);
	DECLARE `suffix` INT;
	DECLARE `alias` VARCHAR(200);
    
	SET
		`original_alias` = `func_get_code_name` (title),
		`suffix` = 0,
		`alias` = `original_alias`;
        
	WHILE EXISTS (SELECT FT.`id` FROM `forum_topic` FT WHERE FT.`alias` = `alias`) DO
		SET `suffix` = `suffix` + 1;
		SET `alias` = CONCAT(`original_alias`, '-', CAST(`suffix` AS CHAR(10)));
	END WHILE;

	RETURN `alias`;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE FUNCTION `func_gallery_album_get_unique_alias` (`identity_user_id` INT, `display_name` VARCHAR(200)) RETURNS varchar(200) CHARSET utf8mb4
    READS SQL DATA
BEGIN
	DECLARE `original_alias` VARCHAR(200);
	DECLARE `suffix` INT;
	DECLARE `alias` VARCHAR(200);

	SET
		`original_alias` = `func_get_code_name` (`display_name`),
		`suffix` = 0,
		`alias` = `original_alias`;
        
	WHILE EXISTS (SELECT GA.`id` FROM `gallery_album` GA WHERE GA.`identity_user_id` = `identity_user_id` AND GA.`alias` = `alias`) DO
		SET `suffix` = `suffix` + 1;
		SET `alias` = CONCAT(`original_alias`, '-', CAST(`suffix` AS CHAR(10)));
	END WHILE;

	RETURN `alias`;
END ;;
DELIMITER ;

DELIMITER ;;
CREATE FUNCTION `func_get_code_name` (`input` VARCHAR(1000)) RETURNS varchar(1000) CHARSET utf8mb4
    DETERMINISTIC
BEGIN
	DECLARE `replace` VARCHAR(20);
	DECLARE `with` CHAR;
	DECLARE `keep` VARCHAR(20);
	DECLARE `output` VARCHAR(1000);
	DECLARE `i` INT;
	DECLARE `char_in` VARCHAR(1);
	DECLARE `char_out` VARCHAR(1);
	DECLARE `last_out` VARCHAR(1);
        
	SET
		`replace` = '^[ ]$',
		`with` = '-',
		`keep` = '^[0-9a-z-]$',
		`output` = '',
		`i` = 1, 
		`char_in` = '',
		`char_out` = '',
		`last_out` = '';

	WHILE `i` <= LENGTH(`input`) DO
		SET `char_in` = SUBSTRING(`input`, `i`, 1);
		SET `char_out` = LOWER(`char_in`);
	
		IF `char_out` REGEXP `replace` THEN
			SET `char_out` = `with`;
		END IF;
			
		IF (`char_out` REGEXP `keep`) AND NOT ((`char_out` = `with`) AND (`last_out` = `with`)) THEN
			SET `output` = CONCAT(`output`, `char_out`);
			SET `last_out` = `char_out`;
		END IF;
        	
		SET `i` = `i` + 1;
	END WHILE;

    RETURN TRIM(TRAILING `with` FROM `output`);
END ;;
DELIMITER ;
