-- Core module - Reset
--
-- You must have already reset all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging
--  3. Gallery
--  4. Content
--  5. Identity

DELETE FROM `core_email_template`;
ALTER TABLE `core_email_template` AUTO_INCREMENT = 1;
