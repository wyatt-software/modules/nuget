-- Gallery module - Reset
--
-- You must have already reset all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging

UPDATE `identity_user` SET `properties` = JSON_SET(`properties`, "$.imageId", null);
UPDATE `gallery_album` SET `gallery_image_id` = null;
DELETE FROM `allery_image`;
ALTER TABLE `gallery_image` AUTO_INCREMENT = 1;

DELETE FROM `gallery_album`;
ALTER TABLE `gallery_album` AUTO_INCREMENT = 1;
