-- Gallery module - Uninstall
--
-- You must have already uninstalled all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging

UPDATE `identity_user` SET `properties` = JSON_REMOVE(`properties`, "$.imageId");

ALTER TABLE `gallery_image`
DROP CONSTRAINT `fk_gallery_image_gallery_album_id`,
DROP CONSTRAINT `fk_gallery_image_identity_user_id`;

ALTER TABLE `gallery_album`
DROP CONSTRAINT `fk_gallery_album_gallery_image_id`,
DROP CONSTRAINT `fk_gallery_album_identity_user_id`;

DROP TABLE `gallery_image`;
DROP TABLE `gallery_album`;
