-- Gallery module - Install
--
-- You must have already installed all modules dependencies first.
-- Eg:
--  1. Core
--  2. Identity

CREATE TABLE `gallery_album` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL,
  `gallery_image_id` int DEFAULT NULL,
  `alias` varchar(200) NOT NULL DEFAULT '',
  `display_name` varchar(200) NOT NULL DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `alias_INDEX` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `gallery_image` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL,
  `gallery_album_id` int NOT NULL,
  `uuid` varchar(36) NOT NULL DEFAULT 'UUID()',
  `filename` varchar(200) NOT NULL DEFAULT '' COMMENT 'Includes the extension.',
  `extension` varchar(50) NOT NULL DEFAULT '' COMMENT 'The extension part of the filename, including the dot.',
  `caption` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid_UNIQUE` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `gallery_album`
ADD KEY `fk_gallery_album_gallery_image_id_idx` (`gallery_image_id`),
ADD KEY `fk_gallery_album_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_gallery_album_gallery_image_id` FOREIGN KEY (`gallery_image_id`) REFERENCES `gallery_image` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `fk_gallery_album_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `gallery_image`
ADD KEY `fk_gallery_image_gallery_album_id_idx` (`gallery_album_id`),
ADD KEY `fk_gallery_image_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_gallery_image_gallery_album_id` FOREIGN KEY (`gallery_album_id`) REFERENCES `gallery_album` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_gallery_image_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON UPDATE CASCADE;