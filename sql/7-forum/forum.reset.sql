-- Forum module - Reset

DELETE FROM `forum_post`;
ALTER TABLE `forum_post` AUTO_INCREMENT = 1;
DELETE FROM `forum_subscription`;
ALTER TABLE `forum_subscription` AUTO_INCREMENT = 1;
DELETE FROM `forum_topic`;
ALTER TABLE `forum_topic` AUTO_INCREMENT = 1;
DELETE FROM `forum_section`;
ALTER TABLE `forum_section` AUTO_INCREMENT = 1;
DELETE FROM `forum_category`;
ALTER TABLE `forum_category` AUTO_INCREMENT = 1;
