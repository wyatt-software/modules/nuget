-- Forum module - Install
--
-- You must have already installed all modules dependencies first.
-- Eg:
--  1. Core
--  2. Identity
--  3. Gallery
--  4. Messaging

CREATE TABLE `forum_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `display_name` varchar(200) NOT NULL DEFAULT '',
  `sort_order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `forum_section` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `forum_category_id` int NOT NULL,
  `code_name` varchar(50) NOT NULL DEFAULT '',
  `display_name` varchar(200) NOT NULL DEFAULT '',
  `description` varchar(200) NOT NULL DEFAULT '',
  `classifieds` bit(1) NOT NULL DEFAULT b'0',
  `sort_order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_name_UNIQUE` (`code_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `forum_topic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL,
  `forum_section_id` int NOT NULL,
  `gallery_image_id` int DEFAULT NULL,
  `alias` varchar(200) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `sub_heading` varchar(200) NOT NULL DEFAULT '',
  `stickied` bit(1) NOT NULL DEFAULT b'0',
  `locked` bit(1) NOT NULL DEFAULT b'0',
  `image_auto` bit(1) NOT NULL DEFAULT b'1',
  `view_counter` int NOT NULL DEFAULT '0',
  `status` int NOT NULL DEFAULT '0',
  `price` decimal(10,0) NOT NULL DEFAULT '0',
  `last_post_created` datetime DEFAULT NULL COMMENT 'redundant, but included as an optimisation for the ORDER BY clause in queries.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias_UNIQUE` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `forum_subscription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL COMMENT 'Foreign key to the community_member subscribed.',
  `forum_topic_id` int NOT NULL COMMENT 'Foreign key to the forum_topic subscribed to.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `forum_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL,
  `forum_topic_id` int NOT NULL COMMENT 'Foreign key to the forum_topic this post belongs to.',
  `edited_by_user_id` int DEFAULT NULL COMMENT 'Foreign key to the community_member that last edited this post.',
  `edited_date` datetime DEFAULT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `content_FULLTEXT` (`content`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `forum_section`
ADD KEY `fk_forum_section_forum_category_id_idx` (`forum_category_id`),
ADD CONSTRAINT `fk_forum_section_forum_category_id` FOREIGN KEY (`forum_category_id`) REFERENCES `forum_category` (`id`) ON UPDATE CASCADE;

ALTER TABLE `forum_topic`
ADD KEY `fk_forum_topic_forum_section_id_idx` (`forum_section_id`),
ADD KEY `fk_forum_topic_gallery_image_id_idx` (`gallery_image_id`),
ADD KEY `fk_forum_topic_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_forum_topic_forum_section_id` FOREIGN KEY (`forum_section_id`) REFERENCES `forum_section` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_forum_topic_gallery_image_id` FOREIGN KEY (`gallery_image_id`) REFERENCES `gallery_image` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_forum_topic_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON UPDATE CASCADE;

ALTER TABLE `forum_subscription`
ADD KEY `fk_forum_subscription_forum_topic_id_idx` (`forum_topic_id`),
ADD KEY `fk_forum_subscription_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_forum_subscription_forum_topic_id` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_forum_subscription_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `forum_post`
ADD KEY `fk_forum_post_forum_topic_id_idx` (`forum_topic_id`),
ADD KEY `fk_forum_post_edited_by_user_id_idx` (`edited_by_user_id`),
ADD KEY `fk_forum_post_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_forum_post_edited_by_user_id` FOREIGN KEY (`edited_by_user_id`) REFERENCES `identity_user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
ADD CONSTRAINT `fk_forum_post_forum_topic_id` FOREIGN KEY (`forum_topic_id`) REFERENCES `forum_topic` (`id`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_forum_post_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON UPDATE CASCADE;
