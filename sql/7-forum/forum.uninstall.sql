-- Forum module - Uninstall

UPDATE `identity_user` SET `preferences` = JSON_REMOVE(`preferences`, "$.emailNewPosts");
UPDATE `identity_user` SET `properties` = JSON_REMOVE(`properties`, "$.location");
UPDATE `identity_user` SET `properties` = JSON_REMOVE(`properties`, "$.signature");

ALTER TABLE `forum_post`
DROP CONSTRAINT `fk_forum_post_edited_by_user_id`,
DROP CONSTRAINT `fk_forum_post_forum_topic_id`,
DROP CONSTRAINT `fk_forum_post_identity_user_id`;

ALTER TABLE `forum_subscription`
DROP CONSTRAINT `fk_forum_subscription_forum_topic_id`,
DROP CONSTRAINT `fk_forum_subscription_identity_user_id`;

ALTER TABLE `forum_topic`
DROP CONSTRAINT `fk_forum_topic_forum_section_id`,
DROP CONSTRAINT `fk_forum_topic_gallery_image_id`,
DROP CONSTRAINT `fk_forum_topic_identity_user_id`;

ALTER TABLE `forum_section`
DROP CONSTRAINT `fk_forum_section_forum_category_id`;

DROP TABLE `forum_post`;
DROP TABLE `forum_subscription`;
DROP TABLE `forum_topic`;
DROP TABLE `forum_section`;
DROP TABLE `forum_category`;
