-- Identity module - Reset
--
-- You must have already reset all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging
--  3. Gallery
--  4. Content

DELETE FROM `identity_user_role`;
ALTER TABLE `identity_user_role` AUTO_INCREMENT = 1;
DELETE FROM `identity_user`;
ALTER TABLE `identity_user` AUTO_INCREMENT = 1;

-- Re-add default 'admin' user.
INSERT INTO `identity_user` (
  `username`,
  `password_hash`,
  `password_format`,
  `email`,
  `email_verified`
)
VALUES (
  'admin',
  'password',
  'PlainText',
  'admin@ws.local',
  1
);

-- Re-add roles.
INSERT INTO `identity_user_role` (`identity_user_id`, `role`)
VALUES
  (1, 1), -- Member
  (1, 2), -- Moderator
  (1, 3); -- Administrator

-- Reset email templates.
UPDATE `core_email_template`
SET
	`created` = UTC_TIMESTAMP,
	`modified` = UTC_TIMESTAMP,
	`display_name` = 'Send password reset link',
	`description` = '',
	`subject` = 'Request to reset password',
	`plain_text` = 'Hi {{username}},\n\nA request has been made to send you a password reset link.\n\nPlease follow the link below to change your password: {{link}}\n\nIf your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.\n\nPassword reset code: {{passwordResetCode}}\n\nNote: This code and link will only be valid for 24 hours.\n\nThankyou  The team at {{siteDomainName}}',
	`html` = '<p>Hi {{username}},</p>\n\n<p>A request has been made to send you a password reset link.</p>\n\n<p>Please follow the link below to change your password:<br />\n<a href=\"{{link}}\">{{link}}</a></p>\n\n<p>If your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.</p>\n\n<p>Password reset code: <pre>{{passwordResetCode}}</pre></p>\n\n<p>Note: This code and link will only be valid for 24 hours.</p>\n\n<p>Thankyou  The team at<br />\n{{siteDomainName}}</p>'
WHERE
  `code_name` = 'IdentityUserSendPasswordResetLink';

UPDATE `core_email_template`
SET
	`created` = UTC_TIMESTAMP,
	`modified` = UTC_TIMESTAMP,
	`display_name` = 'Send email verification',
	`description` = '',
	`subject` = 'Welcome to {{siteDomainName}}',
	`plain_text` = 'Hi {{username}},\n\nBefore you can log in, we require that your verify you email address.\n\nPlease follow the link below:\n{{link}}\n\nIf your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.\n\nEmail verification code: {{emailVerificationCode}}\n\nNote: This code and link will only be valid for 24 hours.\n\nThankyou,\nThe team at {{siteDomainName}}',
	`html` = '<p>Hi {{username}},</p>\n\n<p>Before you can log in, we require that your verify you email address.</p>\n\n<p>Please follow the link below:<br />\n<a href=\"{{link}}\">{{link}}</a></p>\n\n<p>If your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.</p>\n\n<p>Email verification code: <pre>{{emailVerificationCode}}</pre></p>\n\n<p>Note: This code and link will only be valid for 24 hours.</p>\n\n<p>Thankyou,<br />\nThe team at {{siteDomainName}}</p>'
WHERE
  `code_name` = 'IdentityUserSendEmailVerification';