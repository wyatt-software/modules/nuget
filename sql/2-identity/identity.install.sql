-- Identity module - Install
--
-- You must have already installed all modules dependencies first.
-- Eg:
--  1. Core

CREATE TABLE `identity_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `last_logged_on` datetime DEFAULT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `password_hash` varchar(255) NOT NULL DEFAULT '' COMMENT 'Hash of the member''s password, using the method specified by password_format.',
  `password_format` varchar(12) NOT NULL DEFAULT 'SHA256' COMMENT 'SHA256: All new passwords use this.\nMD5: Used for passwords migrated from the old site.\nPlainText: Only supported in the dev environment.',
  `password_reset_code` varchar(255) NOT NULL DEFAULT '',
  `password_reset_code_created` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `email_verified` bit(1) NOT NULL DEFAULT b'0',
  `email_verification_code` varchar(255) NOT NULL DEFAULT '',
  `email_verification_code_created` datetime DEFAULT NULL,
  `properties` json,
  `preferences` json,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `identity_user_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `identity_user_id` int NOT NULL,
  `role` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `identity_user_role`
ADD KEY `fk_identity_user_id_idx` (`identity_user_id`),
ADD CONSTRAINT `fk_identity_user_role_identity_user_id` FOREIGN KEY (`identity_user_id`) REFERENCES `identity_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- Add default 'admin' user.
INSERT INTO `identity_user` (
  `username`,
  `password_hash`,
  `password_format`,
  `email`,
  `email_verified`
)
VALUES (
  'admin',
  'password',
  'PlainText',
  'admin@ws.local',
  1
);

-- Adds roles.
INSERT INTO `identity_user_role` (`identity_user_id`, `role`)
VALUES
  (1, 1), -- Member
  (1, 2), -- Moderator
  (1, 3); -- Administrator

-- Add email templates.
INSERT INTO `core_email_template`
VALUES (
	1,
	(UTC_TIMESTAMP),
	(UTC_TIMESTAMP),
	'IdentityUserSendPasswordResetLink',
	'Send password reset link',
	'',
	'Request to reset password',
	'Hi {{username}},\n\nA request has been made to send you a password reset link.\n\nPlease follow the link below to change your password: {{link}}\n\nIf your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.\n\nPassword reset code: {{passwordResetCode}}\n\nNote: This code and link will only be valid for 24 hours.\n\nThankyou  The team at {{siteDomainName}}',
	'<p>Hi {{username}},</p>\n\n<p>A request has been made to send you a password reset link.</p>\n\n<p>Please follow the link below to change your password:<br />\n<a href=\"{{link}}\">{{link}}</a></p>\n\n<p>If your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.</p>\n\n<p>Password reset code: <pre>{{passwordResetCode}}</pre></p>\n\n<p>Note: This code and link will only be valid for 24 hours.</p>\n\n<p>Thankyou  The team at<br />\n{{siteDomainName}}</p>'
),
(
	2,
	(UTC_TIMESTAMP),
	(UTC_TIMESTAMP),
	'IdentityUserSendEmailVerification',
	'Send email verification',
	'',
	'Welcome to {{siteDomainName}}',
	'Hi {{username}},\n\nBefore you can log in, we require that your verify you email address.\n\nPlease follow the link below:\n{{link}}\n\nIf your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.\n\nEmail verification code: {{emailVerificationCode}}\n\nNote: This code and link will only be valid for 24 hours.\n\nThankyou,\nThe team at {{siteDomainName}}',
	'<p>Hi {{username}},</p>\n\n<p>Before you can log in, we require that your verify you email address.</p>\n\n<p>Please follow the link below:<br />\n<a href=\"{{link}}\">{{link}}</a></p>\n\n<p>If your email program doesn''t let you click on links, you can copy/paste the above URL into your browser.</p>\n\n<p>Email verification code: <pre>{{emailVerificationCode}}</pre></p>\n\n<p>Note: This code and link will only be valid for 24 hours.</p>\n\n<p>Thankyou,<br />\nThe team at {{siteDomainName}}</p>'
);