-- Identity module - Uninstall
--
-- You must have already uninstalled all dependent modules in reverse order of dependency.
-- Eg:
--  1. Forum
--  2. Messaging
--  3. Gallery
--  4. Content

ALTER TABLE `identity_user_role`
DROP CONSTRAINT `fk_identity_user_role_identity_user_id`;

DELETE FROM `identity_user_role`;
DROP TABLE `identity_user_role`;
DELETE FROM `identity_user`;
DROP TABLE `identity_user`;
