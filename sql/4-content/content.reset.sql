-- Content module - Reset

DELETE FROM `content_page_role`;
ALTER TABLE `content_page_role` AUTO_INCREMENT = 1;
UPDATE `content_page` SET `parent_id` = NULL;
DELETE FROM `content_page`;
ALTER TABLE `content_page` AUTO_INCREMENT = 1;
