-- Content module - Uninstall

ALTER TABLE `content_page_role`
DROP CONSTRAINT `fk_content_page_role_content_page_id`;

DROP VIEW `view_content_route`;
DROP TABLE `content_page_role`;
DROP TABLE `content_page`;
