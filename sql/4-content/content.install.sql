-- Content module - Install
--
-- You must have already installed all modules dependencies first.
-- Eg:
--  1. Core
--  2. Identity

CREATE TABLE `content_page` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `parent_id` int DEFAULT NULL COMMENT 'Foreign key to another page in this table, to create a page tree hierarchy.',
  `alias` varchar(200) NOT NULL DEFAULT '' COMMENT 'alias for this section of the URL.\nShould be kebab-case (only contain alpha-numeric characters, and the ''-'' character).',
  `title` varchar(200) NOT NULL DEFAULT '',
  `icon_class` varchar(50) NOT NULL DEFAULT '' COMMENT 'CSS classname to use for the icon for this page.',
  `component` varchar(50) NOT NULL DEFAULT '' COMMENT 'Vue.js component name to use for this page.',
  `redirect` varchar(200) NOT NULL DEFAULT '' COMMENT 'the URL of another page to redirect this page to.',
  `menu_item` bit(1) NOT NULL DEFAULT b'0',
  `content` text NOT NULL COMMENT 'The page content if using the ContentPage Vue.js component.',
  `requires_authentication` tinyint NOT NULL DEFAULT '0' COMMENT '0 = public\n1 = must be logged in to see this page.',
  `sort_order` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `alias_INDEX` (`alias`),
  FULLTEXT KEY `content_FULLTEXT` (`content`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `content_page_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `modified` datetime NOT NULL DEFAULT (UTC_TIMESTAMP),
  `content_page_id` int NOT NULL,
  `role` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

ALTER TABLE `content_page`
ADD KEY `fk_content_page_parent_id_idx` (`parent_id`),
ADD CONSTRAINT `fk_content_page_parent_id_content_page_id` FOREIGN KEY (`parent_id`) REFERENCES `content_page` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `content_page_role`
ADD KEY `fk_content_page_role_content_page_id_idx` (`content_page_id`),
ADD CONSTRAINT `fk_content_page_role_content_page_id` FOREIGN KEY (`content_page_id`) REFERENCES `content_page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
CREATE VIEW `view_content_route` AS
WITH RECURSIVE `tree` (`id`, `alias`, `name`, `parent`, `path`, `sort_path`) AS (
    SELECT
        `content_page`.`id` AS `id`,
        `content_page`.`alias`,
        `content_page`.`alias` AS `name`,
        CAST(NULL AS CHAR(255) CHARSET utf8mb4) AS `parent`,
        CAST(CONCAT('/', `content_page`.`alias`) AS CHAR(255) CHARSET utf8mb4) AS `path`,
        CAST('/' AS CHAR(255) CHARSET utf8mb4) AS `sort_path`
    FROM
        `content_page`
    WHERE
        `content_page`.`parent_id` IS NULL
        
    UNION ALL
    
    SELECT
        CP.`id` AS `id`,
        CP.`alias` AS `alias`,
        CONCAT(T.`name`, '-', CONVERT(CP.`alias` USING utf8mb4)) AS `name`,
        T.`name` AS `parent`,
        CONCAT(T.`path`, '/', CONVERT(CP.`alias` USING utf8mb4)) AS `path`,
        CONCAT(T.`sort_path`, '/', CONVERT(LPAD(CP.`sort_order`, 5, 0) USING utf8mb4)) AS `sort_path`
    FROM
        `tree` T
        JOIN `content_page` CP ON T.`id` = CP.`parent_id`
)
SELECT
    CP.`id`,
    CP.`sort_order`,
	T.`name`,
    T.`parent`,
    T.`path`,
    CP.`title`,
    CP.`menu_item`,
    CP.`icon_class`,
    CP.`component`,
    CP.`redirect`,
    CP.`requires_authentication`,
    (
        SELECT
            IFNULL(GROUP_CONCAT(CPR.`role` SEPARATOR ','), '')
        FROM
            `content_page_role` CPR
        WHERE
            CPR.`content_page_id` = CP.`id`
    ) AS `required_roles`
FROM 
    `content_page` CP
    JOIN `tree` T ON T.`id` = CP.`id`
ORDER BY
    T.`sort_path`;